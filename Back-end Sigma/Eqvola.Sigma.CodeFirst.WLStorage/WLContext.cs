﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.CodeFirst.WLStorage
{
    public class WLContext : DbContext
    {
        static WLContext()
        {
            Database.SetInitializer<WLContext>(null);
        }

        public WLContext()
            : base("Name=WLSigmaDbContext")
        {
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public WLContext(string connectionString)
            : base(connectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
          .Where(type => !string.IsNullOrEmpty(type.Namespace))
          .Where(type => type.BaseType != null && type.BaseType.IsGenericType
               && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }

            modelBuilder.Ignore<Department>();
            modelBuilder.Entity<Manager>().Ignore(i => i.Tags);
            modelBuilder.Entity<Manager>().Ignore(i => i.Rank);
            modelBuilder.Entity<Manager>().Ignore(i => i.Department);

            modelBuilder.Entity<TwoFactor>().ToTable("TwoFactors");
            modelBuilder.Entity<UserTwoFactor>().ToTable("UserTwoFactors");
            modelBuilder.Entity<TwoFactorAction>().ToTable("TwoFactorActions");

            base.OnModelCreating(modelBuilder);
        }
    }
}

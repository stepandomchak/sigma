namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniquePhone : DbMigration
    {
        public override void Up()
        {
            Sql("delete im FROM ImContacts im " +
                  "inner join Managers m on m.Id = im.manager_Id " +
                  "inner join Users u on u.id = m.user_id " +
                  "WHERE u.Email in (select u.Email from users u1, users u " +
                  "where u1.Phone_code + u1.Phone_number like u.Phone_code + u.Phone_number " +
                  "AND u1.Phone_code is not null and u1.Phone_number is not null AND u.Phone_code is not null and u.Phone_number is not null " +
                  "AND(u1.id <= u.id) GROUP BY u.Email HAVING count(*) > 1)");
            Sql("delete u from users u1, users u " +
                "where u.Email in (select u.Email from users u1, users u " +
                "where u1.Phone_code + u1.Phone_number like u.Phone_code + u.Phone_number " +
                "AND u1.Phone_code is not null and u1.Phone_number is not null AND u.Phone_code is not null and u.Phone_number is not null " +
                "AND(u1.id <= u.id) GROUP BY u.Email HAVING count(*) > 1 )");
            Sql("CREATE UNIQUE NONCLUSTERED INDEX IX_PhoneNumber ON " +
                   "Users(Phone_code, Phone_number) WHERE Phone_code IS NOT NULL and Phone_number is not null;");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", "IX_PhoneNumber");
        }
    }
}

namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using Eqvola.Sigma.Service.Storage.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Eqvola.Sigma.CodeFirst.WLStorage.WLContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Eqvola.Sigma.CodeFirst.WLStorage.WLContext context)
        {
            context.Set<TwoFactor>().AddOrUpdate(new TwoFactor() { Id = 1, Type = "Google2FA", Status = 1 } );
            context.Set<TwoFactor>().AddOrUpdate(new TwoFactor() { Id = 2, Type = "Sms", Status = 1 } );
            context.Set<TwoFactor>().AddOrUpdate(new TwoFactor() { Id = 3, Type = "Call", Status = 1 } );

            var withdrawalAction = new TwoFactorAction() { Id = 2, Name = "Withdrawal", Status = 1 };
            var tradeLimitAction = new TwoFactorAction() { Id = 4, Name = "TradeLimit", Status = 1 };
            context.Set<TwoFactorAction>().AddOrUpdate(new TwoFactorAction() { Id = 1, Name = "Auth", Status = 1 } );
            context.Set<TwoFactorAction>().AddOrUpdate(withdrawalAction);
            context.Set<TwoFactorAction>().AddOrUpdate(new TwoFactorAction() { Id = 3, Name = "Api", Status = 1 } );
            context.Set<TwoFactorAction>().AddOrUpdate(tradeLimitAction);
            
            context.Set<TwoFactorActionOption>().AddOrUpdate(new TwoFactorActionOption() { Key = "from", Action = withdrawalAction});
            context.Set<TwoFactorActionOption>().AddOrUpdate(new TwoFactorActionOption() { Key = "from", Action = tradeLimitAction });

            Permission managerRead = new Permission() { Name = "ManagerRead" };
            Permission managerWrite = new Permission() { Name = "ManagerWrite" };
            Permission managerCreate = new Permission() { Name = "ManagerCreate" };
            Permission documentRead = new Permission() { Name = "DocumentRead" };
            Permission documentWrite = new Permission() { Name = "DocumentWrite" };
            Permission userRead = new Permission() { Name = "UserRead" };
            Permission userWrite = new Permission() { Name = "UserWrite" };
            Permission supportRead = new Permission() { Name = "SupportRead" };
            Permission supportWrite = new Permission() { Name = "SupportWrite" };

            context.Set<Permission>().AddOrUpdate(managerRead, managerWrite, managerCreate, documentRead, documentWrite, userRead, userWrite, supportRead, supportWrite);

            Settings takerCommission = new Settings() { Key = "TakerCommission", Value = 0.1 };
            Settings makerCommission = new Settings() { Key = "MakerCommission", Value = 0.2 };

            context.SaveChanges();

        }
    }
}

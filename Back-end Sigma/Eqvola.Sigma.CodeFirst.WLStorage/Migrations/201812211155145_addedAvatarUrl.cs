namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAvatarUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "AvatarUrl", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "AvatarUrl");
        }
    }
}

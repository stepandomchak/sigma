namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveTwoFactor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserTwoFactors",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        Secret = c.String(),
                        TwoFactor_Id = c.Long(),
                        TwoFactorAction_Id = c.Long(),
                        User_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TwoFactors", t => t.TwoFactor_Id)
                .ForeignKey("dbo.TwoFactorActions", t => t.TwoFactorAction_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.TwoFactor_Id)
                .Index(t => t.TwoFactorAction_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.TwoFactors",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 128),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Type, unique: true);
            
            CreateTable(
                "dbo.TwoFactorActions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.TwoFactorActionOptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 128),
                        Action_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TwoFactorActions", t => t.Action_Id, cascadeDelete: true)
                .Index(t => t.Action_Id);
            
            CreateTable(
                "dbo.UserTwoFactorOptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(),
                        Value = c.Double(nullable: false),
                        UserTwoFactor_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserTwoFactors", t => t.UserTwoFactor_Id)
                .Index(t => t.UserTwoFactor_Id);

            Sql("CREATE UNIQUE NONCLUSTERED INDEX IX_UserTwoFactor ON " +
                "UserTwoFactors(User_Id, TwoFactorAction_Id) WHERE User_Id IS NOT NULL and TwoFactorAction_Id is not null;");

            Sql("CREATE UNIQUE NONCLUSTERED INDEX IX_TwoFactorSettings ON " +
                "TwoFactorActionOptions([Key], Action_id) WHERE [Key] IS NOT NULL and Action_Id is not null;");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTwoFactorOptions", "UserTwoFactor_Id", "dbo.UserTwoFactors");
            DropForeignKey("dbo.UserTwoFactors", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserTwoFactors", "TwoFactorAction_Id", "dbo.TwoFactorActions");
            DropForeignKey("dbo.TwoFactorActionOptions", "Action_Id", "dbo.TwoFactorActions");
            DropForeignKey("dbo.UserTwoFactors", "TwoFactor_Id", "dbo.TwoFactors");
            DropIndex("dbo.UserTwoFactorOptions", new[] { "UserTwoFactor_Id" });
            DropIndex("dbo.TwoFactorActionOptions", new[] { "Action_Id" });
            DropIndex("dbo.TwoFactorActions", new[] { "Name" });
            DropIndex("dbo.TwoFactors", new[] { "Type" });
            DropIndex("dbo.UserTwoFactors", new[] { "User_Id" });
            DropIndex("dbo.UserTwoFactors", new[] { "TwoFactorAction_Id" });
            DropIndex("dbo.UserTwoFactors", new[] { "TwoFactor_Id" });
            DropTable("dbo.UserTwoFactorOptions");
            DropTable("dbo.TwoFactorActionOptions");
            DropTable("dbo.TwoFactorActions");
            DropTable("dbo.TwoFactors");
            DropTable("dbo.UserTwoFactors");
        }
    }
}

namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWlAdminManagers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Managers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        FiringReason = c.String(),
                        User_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PermissionManagers",
                c => new
                    {
                        Permission_Id = c.Long(nullable: false),
                        Manager_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Permission_Id, t.Manager_Id })
                .ForeignKey("dbo.Permissions", t => t.Permission_Id, cascadeDelete: true)
                .ForeignKey("dbo.Managers", t => t.Manager_Id, cascadeDelete: true)
                .Index(t => t.Permission_Id)
                .Index(t => t.Manager_Id);
            
            AddColumn("dbo.Users", "RegistrationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ImContacts", "manager_Id", c => c.Long());
            CreateIndex("dbo.ImContacts", "manager_Id");
            AddForeignKey("dbo.ImContacts", "manager_Id", "dbo.Managers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Managers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.PermissionManagers", "Manager_Id", "dbo.Managers");
            DropForeignKey("dbo.PermissionManagers", "Permission_Id", "dbo.Permissions");
            DropForeignKey("dbo.ImContacts", "manager_Id", "dbo.Managers");
            DropIndex("dbo.PermissionManagers", new[] { "Manager_Id" });
            DropIndex("dbo.PermissionManagers", new[] { "Permission_Id" });
            DropIndex("dbo.Managers", new[] { "User_Id" });
            DropIndex("dbo.ImContacts", new[] { "manager_Id" });
            DropColumn("dbo.ImContacts", "manager_Id");
            DropColumn("dbo.Users", "RegistrationDate");
            DropTable("dbo.PermissionManagers");
            DropTable("dbo.Permissions");
            DropTable("dbo.Managers");
        }
    }
}

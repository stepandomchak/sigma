namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDocuments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserDocuments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Url = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        UploadDate = c.DateTime(nullable: false),
                        Guid = c.String(nullable: false, maxLength: 128),
                        Comment = c.String(maxLength: 128),
                        Status = c.Int(nullable: false),
                        Type_Id = c.Long(nullable: false),
                        User_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentTypes", t => t.Type_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Guid, unique: true)
                .Index(t => t.Type_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Status = c.Int(nullable: false),
                        Description = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Users", "IsVerified", c => c.Boolean(nullable: false));

            Sql("CREATE UNIQUE NONCLUSTERED INDEX IX_UserDocument ON " +
                "UserDocuments(Type_Id, User_Id) WHERE Type_Id IS NOT NULL and User_Id is not null;");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDocuments", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserDocuments", "Type_Id", "dbo.DocumentTypes");
            DropIndex("dbo.UserDocuments", new[] { "User_Id" });
            DropIndex("dbo.UserDocuments", new[] { "Type_Id" });
            DropIndex("dbo.UserDocuments", new[] { "Guid" });
            DropColumn("dbo.Users", "IsVerified");
            DropTable("dbo.DocumentTypes");
            DropTable("dbo.UserDocuments");
        }
    }
}

namespace Eqvola.Sigma.CodeFirst.WLStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Registrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        phoneNumber = c.String(nullable: false, maxLength: 32),
                        Code = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Attempts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.phoneNumber, unique: true);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 128),
                        Password = c.String(nullable: false),
                        FirstName = c.String(maxLength: 64),
                        LastName = c.String(maxLength: 64),
                        Phone_code = c.String(maxLength: 16),
                        Phone_number = c.String(maxLength: 32),
                        Country = c.String(maxLength: 128),
                        City = c.String(maxLength: 128),
                        Address = c.String(maxLength: 256),
                        TimeZone = c.String(maxLength: 64),
                        IsTwoFactorEnabled = c.Boolean(nullable: false),
                        TwoFactorSecretKey = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true);
            
            CreateTable(
                "dbo.ImContacts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        messanger = c.String(),
                        contact = c.String(),
                        user_Id = c.Long()
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.user_Id)
                .Index(t => t.user_Id);
            
            CreateTable(
                "dbo.ResetPasswords",
                c => new
                    {
                        UserId = c.Long(nullable: false),
                        Code = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Attempts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResetPasswords", "UserId", "dbo.Users");
            DropForeignKey("dbo.ImContacts", "user_Id", "dbo.Users");
            DropIndex("dbo.ResetPasswords", new[] { "UserId" });
            DropIndex("dbo.ImContacts", new[] { "user_Id" });
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.Registrations", new[] { "phoneNumber" });
            DropTable("dbo.ResetPasswords");
            DropTable("dbo.ImContacts");
            DropTable("dbo.Users");
            DropTable("dbo.Registrations");
        }
    }
}

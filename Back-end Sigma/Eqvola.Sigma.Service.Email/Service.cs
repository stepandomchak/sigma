﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using System.IO;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Diagnostics;

namespace Eqvola.Sigma.Service.Email
{
    public class Service
    {
        private string from = CloudConfigurationManager.GetSetting("SMTP.Email");
        private string apiKey = CloudConfigurationManager.GetSetting("SENDGRID.APIKEY");

        private string storageConnectionString = CloudConfigurationManager.GetSetting("Microsoft.Storage.ConnectionString");
        private string containerName = "etemplates";

        public async Task SendEmailAsync(SendEmailRequest<EmailValues> request) 
        {
            try
            {
                var message = GetBody(request.TemplateName, request.Parametrs);

                if (message == null)
                {
                    return;
                }
                var client = new SendGridClient(apiKey);
                var fromEmail = new EmailAddress(from, "Sigma team");
                var to = new EmailAddress(request.SendTo);
                var plainTextContent = message;
                var msg = MailHelper.CreateSingleEmail(fromEmail, to, request.Subject, plainTextContent, message);
                var response = await client.SendEmailAsync(msg);
            }
            catch (SmtpException ex)
            {
                throw ex;
            }
        }


        private string GetBody(string tName, IDictionary<string, string> Values)
        {
            var template = GetTemplate(tName);
            if (template != null)
            {
                foreach (var value in Values)
                {
                    template = template.Replace("{" + value.Key + "}", value.Value);
                }

                return template;
            }

            return null;
        }

        private string GetTemplate(string templateName)
        {
            if (templateName != null)
            {
                string filename = Templates[templateName];
                var template = ReadCloudBlob(filename);
                if (template != null)
                {
                    return template;
                }
            }

            return null;
        }

        private IDictionary<string, string> Templates
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { "invite-manager", "invitemanagertemplate.txt" }
                };
            }
        }

        private string ReadCloudBlob(string filename)
        {
            string blobText = string.Empty;
            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            var container = storageAccount.CreateCloudBlobClient().GetContainerReference(containerName);
            var blob = container.GetBlobReference(filename);
            
            using (Stream stream = new MemoryStream())
            {
                blob.DownloadToStream(stream);
                stream.Position = 0;

                using (StreamReader reader = new StreamReader(stream))
                {
                    blobText = reader.ReadToEnd();
                }
            }

            return blobText;
        }
    }
}

﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.CodeFirst.AdminStorage
{
    public class AdminContext : DbContext
    {
        static AdminContext()
        {
            Database.SetInitializer<AdminContext>(null);
        }

        public AdminContext()
            : base("Name=SigmaDbContext")
        {
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public AdminContext(string connectionString)
            : base(connectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
          .Where(type => !string.IsNullOrEmpty(type.Namespace))
          .Where(type => type.BaseType != null && type.BaseType.IsGenericType
               && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            //modelBuilder.Entity<User>().Ignore(i => i.Address);
            modelBuilder.Entity<User>().Ignore(i => i.ImContacts);
            modelBuilder.Entity<User>().Ignore(i => i.RegistrationDate);
            modelBuilder.Entity<WLUser>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            //modelBuilder.Entity<ImContact>().Ignore(e => e.user);
            modelBuilder.Entity<TwoFactor>().ToTable("TwoFactors");
            modelBuilder.Entity<TwoFactorAction>().ToTable("TwoFactorActions");
            modelBuilder.Entity<UserTwoFactor>().ToTable("UserTwoFactors");

            modelBuilder.Entity<User>().Ignore(i => i.UserDocuments);
            modelBuilder.Entity<User>().Ignore(i => i.IsVerified);

            base.OnModelCreating(modelBuilder);
        }
    }
}

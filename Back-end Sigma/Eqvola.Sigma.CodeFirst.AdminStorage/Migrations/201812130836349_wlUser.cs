namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class wlUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WhiteLabels",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true);
            
            CreateTable(
                "dbo.WLUsers",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        Wl_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WhiteLabels", t => t.Wl_Id)
                .Index(t => t.Wl_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WLUsers", "Wl_Id", "dbo.WhiteLabels");
            DropIndex("dbo.WLUsers", new[] { "Wl_Id" });
            DropIndex("dbo.WhiteLabels", new[] { "name" });
            DropTable("dbo.WLUsers");
            DropTable("dbo.WhiteLabels");
        }
    }
}

namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManagerPassword : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "FirstName", c => c.String(maxLength: 64));
            AlterColumn("dbo.Users", "LastName", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "LastName", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Users", "FirstName", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 64));
        }
    }
}

namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "City", c => c.String(maxLength: 128));
            DropColumn("dbo.Users", "Address_StreetAddress");
            DropColumn("dbo.Users", "Address_City");
            DropColumn("dbo.Users", "Address_ZipCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Address_ZipCode", c => c.String(maxLength: 32));
            AddColumn("dbo.Users", "Address_City", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "Address_StreetAddress", c => c.String(maxLength: 128));
            DropColumn("dbo.Users", "City");
        }
    }
}

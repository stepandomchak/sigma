namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moveToAdminStorage : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Registrations", new[] { "phoneNumber" });
            AddColumn("dbo.ImContacts", "user_Id", c => c.Long());
            AddColumn("dbo.Users", "Address", c => c.String(maxLength: 256));
            CreateIndex("dbo.ImContacts", "user_Id");
            DropForeignKey("dbo.ImContacts", "manager_Id", "dbo.Managers");
            DropTable("dbo.Registrations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Registrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        phoneNumber = c.String(nullable: false, maxLength: 32),
                        Code = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Attempts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            AddForeignKey("dbo.ImContacts", "manager_Id", "dbo.Managers", "Id");
            DropIndex("dbo.ImContacts", new[] { "user_Id" });
            DropColumn("dbo.Users", "Address");
            DropColumn("dbo.ImContacts", "user_Id");
            CreateIndex("dbo.Registrations", "phoneNumber", unique: true);
        }
    }
}

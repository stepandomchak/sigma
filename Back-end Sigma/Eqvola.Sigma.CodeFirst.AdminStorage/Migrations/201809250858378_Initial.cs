namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Managers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Rank = c.Int(nullable: false),
                        Department_Id = c.Long(nullable: false),
                        User_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Department_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DepartmentTags",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManagerTags",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 128),
                        Password = c.String(nullable: false, maxLength: 64),
                        FirstName = c.String(nullable: false, maxLength: 64),
                        LastName = c.String(nullable: false, maxLength: 64),
                        Phone_code = c.String(maxLength: 16),
                        Phone_number = c.String(maxLength: 32),
                        Country = c.String(maxLength: 128),
                        Address_StreetAddress = c.String(maxLength: 128),
                        Address_City = c.String(maxLength: 128),
                        Address_ZipCode = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PermissionDepartments",
                c => new
                    {
                        Permission_Id = c.Long(nullable: false),
                        Department_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Permission_Id, t.Department_Id })
                .ForeignKey("dbo.Permissions", t => t.Permission_Id, cascadeDelete: true)
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .Index(t => t.Permission_Id)
                .Index(t => t.Department_Id);
            
            CreateTable(
                "dbo.PermissionManagers",
                c => new
                    {
                        Permission_Id = c.Long(nullable: false),
                        Manager_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Permission_Id, t.Manager_Id })
                .ForeignKey("dbo.Permissions", t => t.Permission_Id, cascadeDelete: true)
                .ForeignKey("dbo.Managers", t => t.Manager_Id, cascadeDelete: true)
                .Index(t => t.Permission_Id)
                .Index(t => t.Manager_Id);
            
            CreateTable(
                "dbo.DepartmentTagDepartments",
                c => new
                    {
                        DepartmentTag_Id = c.Long(nullable: false),
                        Department_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentTag_Id, t.Department_Id })
                .ForeignKey("dbo.DepartmentTags", t => t.DepartmentTag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .Index(t => t.DepartmentTag_Id)
                .Index(t => t.Department_Id);
            
            CreateTable(
                "dbo.ManagerTagManagers",
                c => new
                    {
                        ManagerTag_Id = c.Long(nullable: false),
                        Manager_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ManagerTag_Id, t.Manager_Id })
                .ForeignKey("dbo.ManagerTags", t => t.ManagerTag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Managers", t => t.Manager_Id, cascadeDelete: true)
                .Index(t => t.ManagerTag_Id)
                .Index(t => t.Manager_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Managers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.ManagerTagManagers", "Manager_Id", "dbo.Managers");
            DropForeignKey("dbo.ManagerTagManagers", "ManagerTag_Id", "dbo.ManagerTags");
            DropForeignKey("dbo.Managers", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.DepartmentTagDepartments", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.DepartmentTagDepartments", "DepartmentTag_Id", "dbo.DepartmentTags");
            DropForeignKey("dbo.PermissionManagers", "Manager_Id", "dbo.Managers");
            DropForeignKey("dbo.PermissionManagers", "Permission_Id", "dbo.Permissions");
            DropForeignKey("dbo.PermissionDepartments", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.PermissionDepartments", "Permission_Id", "dbo.Permissions");
            DropIndex("dbo.ManagerTagManagers", new[] { "Manager_Id" });
            DropIndex("dbo.ManagerTagManagers", new[] { "ManagerTag_Id" });
            DropIndex("dbo.DepartmentTagDepartments", new[] { "Department_Id" });
            DropIndex("dbo.DepartmentTagDepartments", new[] { "DepartmentTag_Id" });
            DropIndex("dbo.PermissionManagers", new[] { "Manager_Id" });
            DropIndex("dbo.PermissionManagers", new[] { "Permission_Id" });
            DropIndex("dbo.PermissionDepartments", new[] { "Department_Id" });
            DropIndex("dbo.PermissionDepartments", new[] { "Permission_Id" });
            DropIndex("dbo.Departments", new[] { "Name" });
            DropIndex("dbo.Managers", new[] { "User_Id" });
            DropIndex("dbo.Managers", new[] { "Department_Id" });
            DropTable("dbo.ManagerTagManagers");
            DropTable("dbo.DepartmentTagDepartments");
            DropTable("dbo.PermissionManagers");
            DropTable("dbo.PermissionDepartments");
            DropTable("dbo.Users");
            DropTable("dbo.ManagerTags");
            DropTable("dbo.DepartmentTags");
            DropTable("dbo.Permissions");
            DropTable("dbo.Departments");
            DropTable("dbo.Managers");
        }
    }
}

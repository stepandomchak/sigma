namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManagerIms : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImContacts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        messanger = c.String(),
                        contact = c.String(),
                        manager_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Managers", t => t.manager_Id)
                .Index(t => t.manager_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ImContacts", "manager_Id", "dbo.Managers");
            DropIndex("dbo.ImContacts", new[] { "manager_Id" });
            DropTable("dbo.ImContacts");
        }
    }
}

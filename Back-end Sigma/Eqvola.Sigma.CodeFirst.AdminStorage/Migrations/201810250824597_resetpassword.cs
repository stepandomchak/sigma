namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resetpassword : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ResetPasswords",
                c => new
                    {
                        UserId = c.Long(nullable: false),
                        Code = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Attempts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResetPasswords", "UserId", "dbo.Users");
            DropIndex("dbo.ResetPasswords", new[] { "UserId" });
            DropTable("dbo.ResetPasswords");
        }
    }
}

namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DepartmentManagerStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Managers", "Status", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.Managers", "FiringReason", c => c.String());
            AddColumn("dbo.Departments", "Status", c => c.Int(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "Status");
            DropColumn("dbo.Managers", "FiringReason");
            DropColumn("dbo.Managers", "Status");
        }
    }
}

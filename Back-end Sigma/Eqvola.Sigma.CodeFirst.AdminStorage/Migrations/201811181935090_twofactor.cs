namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class twofactor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsTwoFactorEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "TwoFactorSecretKey", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "TwoFactorSecretKey");
            DropColumn("dbo.Users", "IsTwoFactorEnabled");
        }
    }
}

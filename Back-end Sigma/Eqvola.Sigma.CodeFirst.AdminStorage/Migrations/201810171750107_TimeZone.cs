namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TimeZone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "TimeZone", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "TimeZone");
        }
    }
}

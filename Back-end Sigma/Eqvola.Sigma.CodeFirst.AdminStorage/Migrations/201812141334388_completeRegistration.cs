namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class completeRegistration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Registrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        phoneNumber = c.String(nullable: false, maxLength: 32),
                        Code = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Attempts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.phoneNumber, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Registrations", new[] { "phoneNumber" });
            DropTable("dbo.Registrations");
        }
    }
}

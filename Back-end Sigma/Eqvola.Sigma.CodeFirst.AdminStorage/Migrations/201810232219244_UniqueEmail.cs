namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqueEmail : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Users", "Email", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "Email" });
        }
    }
}

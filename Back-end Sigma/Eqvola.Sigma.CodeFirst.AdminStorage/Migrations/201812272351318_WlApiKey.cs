namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WlApiKey : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WlApiKeys",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 128),
                        ValidFrom = c.DateTime(),
                        ValidTo = c.DateTime(),
                        Wl_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WhiteLabels", t => t.Wl_Id)
                .Index(t => t.Wl_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WlApiKeys", "Wl_Id", "dbo.WhiteLabels");
            DropIndex("dbo.WlApiKeys", new[] { "Wl_Id" });
            DropTable("dbo.WlApiKeys");
        }
    }
}

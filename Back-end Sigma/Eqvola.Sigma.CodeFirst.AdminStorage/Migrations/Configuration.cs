namespace Eqvola.Sigma.CodeFirst.AdminStorage.Migrations
{
    using Eqvola.Sigma.Service.Storage.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Eqvola.Sigma.CodeFirst.AdminStorage.AdminContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Eqvola.Sigma.CodeFirst.AdminStorage.AdminContext context)
        {
            User user = new User()
            {
                Id = 1,
                Email = "admin@admin.com",
                Password = "Main4453",
                FirstName = "Vasya",
                LastName = "Blablabla",
                Country = "Country",
                City = "City"
            };
            context.Set<User>().AddOrUpdate(user);

            Permission departmentPermission = new Permission() { Id = 1, Name = "Department" };
            Permission managerPermission = new Permission() { Id = 2, Name = "Manager" };
            Permission dashboardPermission = new Permission() { Id = 3, Name = "Dashboard" };
            Permission wlSettingsPermission = new Permission() { Id = 4, Name = "WLSettings" };
            context.Set<Permission>().AddOrUpdate(departmentPermission, managerPermission, dashboardPermission, wlSettingsPermission);

            Department mainDepartment = new Department() { Id = 1, Name = "Main Department" };
            mainDepartment.Permissions = new List<Permission>();
            mainDepartment.Permissions.Add(departmentPermission);
            mainDepartment.Permissions.Add(managerPermission);
            mainDepartment.Permissions.Add(dashboardPermission);
            mainDepartment.Permissions.Add(wlSettingsPermission);
            context.Set<Department>().AddOrUpdate(mainDepartment);

            Manager mainManager = new Manager() { Department = mainDepartment, User = user, Rank = 1 };
            mainManager.Permissions = new List<Permission>();
            mainManager.Permissions.Add(departmentPermission);
            mainManager.Permissions.Add(managerPermission);
            mainManager.Permissions.Add(dashboardPermission);
            mainManager.Permissions.Add(wlSettingsPermission);
            context.Set<Manager>().AddOrUpdate(mainManager);

            WhiteLabel wl = new WhiteLabel() { name = "EqvolaTest" };
            wl.Users = new List<WLUser>();
            context.Set<WhiteLabel>().AddOrUpdate(wl);

            context.Set<TwoFactor>().AddOrUpdate(new TwoFactor() { Id = 1, Type = "Google", Status = 1 });
            context.Set<TwoFactor>().AddOrUpdate(new TwoFactor() { Id = 2, Type = "Sms", Status = 1 });
            context.Set<TwoFactor>().AddOrUpdate(new TwoFactor() { Id = 3, Type = "Call", Status = 1 });

            context.Set<TwoFactorAction>().AddOrUpdate(new TwoFactorAction() { Id = 1, Name = "Auth", Status = 1 });
            context.Set<TwoFactorAction>().AddOrUpdate(new TwoFactorAction() { Id = 2, Name = "Api", Status = 1 });


            context.SaveChanges();
        }
    }
}

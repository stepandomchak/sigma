using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Twilio;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;

namespace Eqvola.Sigma.TwilioService
{
    public class WorkerRole : ServiceBusRole<Service.Twilio.Service>
    {
        protected override IEnumerable<string> queues => new string[] { "twilio" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "twilio":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        {
                            "send-message",
                            SendMessage
                        }
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }

        private async Task<Response> SendMessage(BrokeredMessage message)
        {
            SendMessageRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<SendMessageRequest>(json);
            }

            var status = await service.SendMessage(request.PhoneNumber, request.Message);

            return new SendMessageResponse() { status = status };
        }
    }
}

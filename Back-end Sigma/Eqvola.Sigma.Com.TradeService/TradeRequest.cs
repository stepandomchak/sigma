﻿namespace Eqvola.Sigma.Com.TradeService
{
    public class TradeRequest : Request
    {
        public override string QueueName => "trade-service";
        public override string RequestName => "trade";

        public long OrderId { get; set; }
    }
}

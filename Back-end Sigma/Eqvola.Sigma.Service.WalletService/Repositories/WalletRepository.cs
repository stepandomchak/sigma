﻿using Eqvola.Sigma.Service.WalletService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.WalletService.Repositories
{
    public class WalletRepository : Repository<Wallet, long>
    {
        public WalletRepository(WalletContext context) : base(context) { }
    }
}

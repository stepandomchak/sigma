﻿using Eqvola.Sigma.Service.WalletService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.WalletService.Repositories
{
    public class DepositRepository : Repository<Deposit, long>
    {
        public DepositRepository(WalletContext context) : base(context) { }

        public async Task<List<Deposit>> GetDeposits(long userId, int status, string currencyCode, int count, int margin, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null, string orderBy = null, bool isAscending = true)
        {
            return await Task.Run(() =>
            {
                var request = BuildRequest(userId, status, currencyCode, dateFrom, dateTo, search);
                if (margin < 0)
                    margin = 0;
                if (count <= 0)
                    count = 20;

                if (!string.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.ToLower())
                    {
                        case "amount":
                            {
                                request = isAscending ? request.OrderBy(o => o.Amount) : request.OrderByDescending(o => o.Amount);
                                break;
                            }
                        case "currency":
                            {
                                request = isAscending ? request.OrderBy(o => o.Wallet.CurrencyCode) : request.OrderByDescending(o => o.Wallet.CurrencyCode);
                                break;
                            }
                        case "status":
                            {
                                request = isAscending ? request.OrderBy(o => o.Status) : request.OrderByDescending(o => o.Status);
                                break;
                            }
                        case "date":
                            {
                                request = isAscending ? request.OrderBy(o => o.CreationTime) : request.OrderByDescending(o => o.CreationTime);
                                break;
                            }
                        case "wallet":
                            {
                                request = isAscending ? request.OrderBy(o => o.Wallet.Address) : request.OrderByDescending(o => o.Wallet.Address);
                                break;
                            }
                        default:
                            {
                                request = isAscending ? request.OrderBy(o => o.Id) : request.OrderByDescending(o => o.Id);
                                break;
                            }

                    }
                }
                else
                {
                    request = isAscending ? request.OrderBy(o => o.Id) : request.OrderByDescending(o => o.Id);
                }
                request = request.Skip(margin).Take(count);
                return request.ToList();
            });
        }

        private IQueryable<Deposit> BuildRequest(long userId, int status, string currencyCode, DateTime? dateFrom, DateTime? dateTo, string search)
        {
            IQueryable<Deposit> request = Entities.AsQueryable();
            if (userId > 0)
            {
                request = Entities.Where(o => o.Wallet.UserId == userId);
            }
            if (status > 0)
            {
                request = request.Where(o => (o.Status & status) > 0);
            }
            if (dateFrom != null)
            {
                request = request.Where(o => (o.CreationTime > dateFrom));
            }
            if (dateTo != null)
            {
                request = request.Where(o => (o.CreationTime < dateTo));
            }
            if (!string.IsNullOrEmpty(search))
            {
                request = request.Where(o => (o.TxId.Contains(search) || o.Wallet.CurrencyName.Contains(search) || 
                    o.Wallet.CurrencyCode.Contains(search) || o.Wallet.Address.Contains(search)));
            }
            if (!string.IsNullOrEmpty(currencyCode))
            {
                request = request.Where(o => (o.Wallet.CurrencyCode == currencyCode));
            }

            return request;
        }

        public async Task<int> GetDepositsCount(long userId, int status, DateTime? dateFrom = null, DateTime? dateTo = null, string currencyCode = null, string search = null)
        {
            return await Task.Run(() =>
            {
                return BuildRequest(userId, status, currencyCode, dateFrom, dateTo, search).Count();
            });
        }
    }
}

namespace Eqvola.Sigma.Service.WalletService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Withdrawals", "TransactionFee", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Withdrawals", "TransactionFee");
        }
    }
}

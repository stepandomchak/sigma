namespace Eqvola.Sigma.Service.WalletService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqueWallet : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Wallets", new[] { "UserId", "CurrencyCode" }, unique: true, name: "IX_UniqueWallet");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Wallets", "IX_UniqueWallet");
        }
    }
}

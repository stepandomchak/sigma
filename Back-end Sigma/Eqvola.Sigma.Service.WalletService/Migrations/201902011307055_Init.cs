namespace Eqvola.Sigma.Service.WalletService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlockedFunds",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OrderId = c.Long(nullable: false),
                        Amount = c.Double(nullable: false),
                        Wallet_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Wallets", t => t.Wallet_Id, cascadeDelete: true)
                .Index(t => t.Wallet_Id);
            
            CreateTable(
                "dbo.Wallets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        CurrencyName = c.String(nullable: false, maxLength: 128),
                        CurrencyCode = c.String(nullable: false, maxLength: 128),
                        Balance = c.Double(nullable: false),
                        Address = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Deposits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        Status = c.Int(nullable: false),
                        CreationTime = c.DateTime(),
                        ConfirmationTime = c.DateTime(),
                        TxId = c.String(maxLength: 512),
                        From = c.String(maxLength: 512),
                        Wallet_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Wallets", t => t.Wallet_Id, cascadeDelete: true)
                .Index(t => t.Wallet_Id);
            
            CreateTable(
                "dbo.Withdrawals",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        Status = c.Int(nullable: false),
                        CreationTime = c.DateTime(),
                        ConfirmationTime = c.DateTime(),
                        TxId = c.String(maxLength: 512),
                        Comment = c.String(),
                        To = c.String(maxLength: 512),
                        Wallet_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Wallets", t => t.Wallet_Id, cascadeDelete: true)
                .Index(t => t.Wallet_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlockedFunds", "Wallet_Id", "dbo.Wallets");
            DropForeignKey("dbo.Withdrawals", "Wallet_Id", "dbo.Wallets");
            DropForeignKey("dbo.Deposits", "Wallet_Id", "dbo.Wallets");
            DropIndex("dbo.Withdrawals", new[] { "Wallet_Id" });
            DropIndex("dbo.Deposits", new[] { "Wallet_Id" });
            DropIndex("dbo.BlockedFunds", new[] { "Wallet_Id" });
            DropTable("dbo.Withdrawals");
            DropTable("dbo.Deposits");
            DropTable("dbo.Wallets");
            DropTable("dbo.BlockedFunds");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.WalletService.Models
{
    public class Wallet : BaseEntity<long>
    {

        [Required]
        [Index("IX_UniqueWallet", 1, IsUnique = true)]
        public long UserId { get; set; }

        [Required]
        [MaxLength(128)]
        public string CurrencyName { get; set; }

        [Required]
        [MaxLength(128)]
        [Index("IX_UniqueWallet", 2, IsUnique = true)]
        public string CurrencyCode { get; set; }

        [Required]
        public double Balance { get; set; }

        [Required]
        public string Address { get; set; }
        
        public virtual ICollection<Deposit> Deposits { get; set; }
        public virtual ICollection<Withdrawal> Withdrawal { get; set; }
        public virtual ICollection<BlockedFunds> BlockedFunds { get; set; }

        public Wallet()
        {
            BlockedFunds = new List<BlockedFunds>();
            Deposits = new List<Deposit>();
            Withdrawal = new List<Withdrawal>();
    }

    }
}

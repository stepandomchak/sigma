﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.WalletService.Models
{
    public class BlockedFunds : BaseEntity<long>
    {
        [Required]
        public virtual Wallet Wallet { get; set; }

        [Required]
        public long OrderId { get; set; }

        [Required]
        public double Amount { get; set; }

    }
}

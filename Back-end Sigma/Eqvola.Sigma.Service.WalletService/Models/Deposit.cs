﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.WalletService.Models
{
    public class Deposit : BaseEntity<long>
    {
        [Required]
        public double Amount { get; set; }

        [Required]
        public virtual Wallet Wallet { get; set; }

        public int Status { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime? CreationTime { get; set; }
                
        [Column(TypeName = "DateTime")]
        public DateTime? ConfirmationTime { get; set; }
        
        [MaxLength(512)]
        public string TxId { get; set; }
        
        [MaxLength(512)]
        public string From { get; set; }
    }
}

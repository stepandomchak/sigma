﻿using Eqvola.Sigma.Service.WalletService.Models;
using Eqvola.Sigma.Service.WalletService.Repositories;
using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.WalletService
{
    public class Service : IDisposable
    {
        private WalletContext db;

        public Service()
        {
            db = new WalletContext(CloudConfigurationManager.GetSetting("WalletDbContext"));
        }
        public void Dispose()
        {
            db.Dispose();
        }

        #region Wallet
        public async Task<Wallet> GetWalletByCurrency(long userId, string currencyCode)
        {
            return (await new WalletRepository(db).Where(m => m.UserId == userId && m.CurrencyCode == currencyCode)).FirstOrDefault();
        }
        public async Task<Wallet> GetWalletById(long userId)
        {
            return (await new WalletRepository(db).Where(m => m.Id == userId)).FirstOrDefault();
        }
        public async Task<IEnumerable<Wallet>> GetUserWallets(long userId)
        {
            return (await new WalletRepository(db).Where(m => m.UserId == userId)).ToList();
        }
        public async Task<RepositoryResponse<Wallet>> CreateWallet(Wallet wallet)
        {
            return (await new WalletRepository(db).Add(wallet));
        }
        public async Task<RepositoryResponse<Wallet>> UpdateWallet(Wallet wallet)
        {
            return await new WalletRepository(db).Update(wallet);
        }
        public async Task<IEnumerable<Wallet>> GetAllWallets()
        {
            return await new WalletRepository(db).All();
        }
        #endregion

        #region BlockFund
        public async Task<BlockedFunds> GetFundsByOrder(long orderId)
        {
            return (await new BlockFundsRepository(db).Where(m => m.OrderId == orderId)).FirstOrDefault();
        }
        public async Task<IEnumerable<BlockedFunds>> GetFundsByWallet(long walletId)
        {
            return (await new BlockFundsRepository(db).Where(m => m.Wallet.Id == walletId)).ToList();
        }
        public async Task<RepositoryResponse<BlockedFunds>> SetBlockFunds(BlockedFunds funds)
        {
            return await new BlockFundsRepository(db).Add(funds);
        }

        public async Task<RepositoryResponse<BlockedFunds>> UpdateFunds(BlockedFunds funds)
        {
            return await new BlockFundsRepository(db).Update(funds);
        }

        public async Task<BlockedFunds> DeleteFunds(BlockedFunds funds)
        {
            return await new BlockFundsRepository(db).Delete(funds);
        }
        #endregion

        #region Deposit
        public async Task<RepositoryResponse<Deposit>> CreateDeposit(Deposit deposit)
        {
            return (await new DepositRepository(db).Add(deposit));
        }
        public async Task<Deposit> GetDeposit(string txId)
        {
            return (await new DepositRepository(db).Where(m => m.TxId == txId)).FirstOrDefault();
        }

        public async Task<IEnumerable<Deposit>> GetDeposits(int status, long userId, string currencyCode = null, int count = 20, int margin = 0, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null, string orderBy = null, bool isAscending = true)
        {
            var repository = new DepositRepository(db);
            var deposits = await repository.GetDeposits(userId: userId, status: status, currencyCode: currencyCode, margin: margin, count: count, dateFrom: dateFrom, dateTo: dateTo, search: search, orderBy: orderBy, isAscending: isAscending);
            return deposits;
        }

        public async Task<int> GetDepositsCount(int status, long userId, string currencyCode = null, int count = 20, int margin = 0, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            var repository = new DepositRepository(db);
            return await repository.GetDepositsCount(userId: userId, status: status, currencyCode: currencyCode, dateFrom: dateFrom, dateTo: dateTo, search: search);
        }

        public async Task<IEnumerable<Deposit>> GetUserDeposits(long userId)
        {
            return (await new DepositRepository(db).Where(m => m.Wallet.UserId == userId)).ToList();
        }
        public async Task<IEnumerable<Deposit>> GetDepositsByCurrency(long userId, string currencyCode)
        {
            return (await new DepositRepository(db).Where(m => m.Wallet.UserId == userId && m.Wallet.CurrencyCode == currencyCode)).ToList();
        }
        public async Task<RepositoryResponse<Deposit>> UpdateDeposit(Deposit deposit)
        {
            return (await new DepositRepository(db).Update(deposit));
        }
        public async Task<IEnumerable<Deposit>> GetAllDeposits()
        {
            return (await new DepositRepository(db).All());
        }
        #endregion

        #region Withdrawal
        public async Task<RepositoryResponse<Withdrawal>> CreateWithdrawal(Withdrawal withdrawal)
        {
            return (await new WithdrawalRepository(db).Add(withdrawal));
        }
        public async Task<Withdrawal> GetWithdrawal(string txId)
        {
            return (await new WithdrawalRepository(db).Where(m => m.TxId == txId)).FirstOrDefault();
        }

        public async Task<IEnumerable<Withdrawal>> GetWithdrawals(int status, long userId, string currencyCode = null, int count = 20, int margin = 0, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null, string orderBy = null, bool isAscending = true)
        {
            var repository = new WithdrawalRepository(db);
            var withdrawals = await repository.GetWithdrawals(userId: userId, status: status, currencyCode: currencyCode, margin: margin, count: count, dateFrom: dateFrom, dateTo: dateTo, search: search, orderBy: orderBy, isAscending: isAscending);
            return withdrawals;
        }

        public async Task<int> GetWithdrawalsCount(int status, long userId, string currencyCode = null, int count = 20, int margin = 0, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            var repository = new WithdrawalRepository(db);
            return await repository.GetWithdrawalsCount(userId: userId, status: status, currencyCode: currencyCode, dateFrom: dateFrom, dateTo: dateTo, search: search);
        }

        public async Task<IEnumerable<Withdrawal>> GetUserWithdrawals(long userId)
        {
            return (await new WithdrawalRepository(db).Where(m => m.Wallet.UserId == userId)).ToList();
        }
        public async Task<IEnumerable<Withdrawal>> GetWithdrawalsByCurrency(long userId, string currencyCode)
        {
            return (await new WithdrawalRepository(db).Where(m => m.Wallet.UserId == userId && m.Wallet.CurrencyCode == currencyCode)).ToList();
        }
        public async Task<RepositoryResponse<Withdrawal>> UpdateWithdrawal(Withdrawal withdrawal)
        {
            return (await new WithdrawalRepository(db).Update(withdrawal));
        }

        public async Task<IEnumerable<Withdrawal>> GetAllWithdrawals()
        {
            return await new WithdrawalRepository(db).All();
        }

        #endregion
    }
}

﻿using System.Web.Http;

namespace Eqvola.Sigma.WLAdminAPIGateway
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}


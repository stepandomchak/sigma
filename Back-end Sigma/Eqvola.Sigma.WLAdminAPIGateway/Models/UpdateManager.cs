﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Data to update manager
    /// </summary>
    public class UpdateManager
    {
        /// <summary>
        /// New firstname of manager
        /// </summary>
        public string firstname { get; set; }

        /// <summary>
        /// New lastname of manager
        /// </summary>
        public string lastname { get; set; }
        
        /// <summary>
        /// New phone code
        /// </summary>
        public string phone_code { get; set; }

        /// <summary>
        /// New phone number
        /// </summary>
        public string phone_number { get; set; }

        /// <summary>
        /// New country of manager
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// New city of manager
        /// </summary>
        public string address_city { get; set; }

        /// <summary>
        /// New time zone of manager
        /// </summary>
        public string time_zone { get; set; }

        public DateTime registrationDate { get; set; }

        /// <summary>
        /// New permissions 
        /// </summary>
        public IEnumerable<string> permissions { get; set; }

        /// <summary>
        /// New messangers
        /// </summary>
        public IEnumerable<MessangerEntry> messangers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class Login : Password
    {
        /// <summary>
        /// User email address
        /// </summary>
        [Required]
        [EmailAddress]
        public string email { get; set; }

        /// <summary>
        /// Code from two step authentication
        /// </summary>
        public string code { get; set; }
    }
}
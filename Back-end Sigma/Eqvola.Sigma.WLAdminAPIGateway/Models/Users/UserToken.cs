﻿using Eqvola.Sigma.Com.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Users
{
    public class UserToken
    {

        public string email { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string phoneCode { get; set; }

        public string phoneNumber { get; set; }

        public string country { get; set; }

        public string city { get; set; }

        public string address { get; set; }

        public string avatarUrl { get; set; }

        public bool isVerified { get; set; }

        public ICollection<UserTwoFactor> twoFactorSettings { get; set; }

        public IDictionary<string, string> messangers { get; set; }

        public UserToken()
        {
            messangers = new Dictionary<string, string>();
            twoFactorSettings = new List<UserTwoFactor>();
        }
                
    }

    public class UserTwoFactor
    {
        public long settingId { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public int state { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Users
{
    public class UserUpdate
    {
        /// <summary>
        /// Email address
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Firstname
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// Lastname
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// Phone code
        /// </summary>
        public string phoneCode { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        public string phoneNumber { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// Registration Date
        /// </summary>
        public DateTime? registrationDate{ get; set; }

        /// <summary>
        /// City 
        /// </summary>
        public string city { get; set; }

        public string address { get; set; }


    }
}
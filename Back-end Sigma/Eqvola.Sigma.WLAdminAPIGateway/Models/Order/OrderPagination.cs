﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Order
{
    public class OrderPagination : Pagination
    {
        public string email { get; set; }
        public int status { get; set; }
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public string orderBy { get; set; }
        public bool isAscending { get; set; }
        public string search { get; set; }
    }
}
﻿namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class SupportPagination : Pagination
    {
        public int status { get; set; }

        public string categoryName { get; set; }

        public string email { get; set; }

        public string search { get; set; }
        public string orderBy { get; set; }
        public bool isAscending { get; set; }

    }
}
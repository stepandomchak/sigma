﻿using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class Problem
    {
        [Required]
        public string question { get; set; }
        [Required]
        public string answer { get; set; }        
        [Required]
        public long topicId { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class Category
    {
        [Required]
        public string name { get; set; }
        
        public bool isEnable { get; set; }

        public Category()
        {
            isEnable = true;
        }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class Ticket
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string title { get; set; }
        
        [Required]
        public string message { get; set; }       

        public List<string> attachments { get; set; }


        [Required]
        public long categoryId { get; set; }

       
        public Ticket()
        {
            attachments = new List<string>();
        }
    }
}
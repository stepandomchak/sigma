﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class Message
    {

        [Required]
        public string text { get; set; }


        [Required]
        public long ticketId { get; set; }

        public List<string> attachments { get; set; }
    }
}
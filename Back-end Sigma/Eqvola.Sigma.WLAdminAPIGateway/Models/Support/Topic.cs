﻿using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class TopicCreate
    {
        [Required]
        public string name { get; set; }
        [Required]
        public string text { get; set; }
        [Required]
        public long categoryId { get; set; }        
    }

    public class TopicUpdate : TopicCreate
    {
        [Required]
        public int topicStatus { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class Pagination
    {
        public int count { get; set; }
        public int margin { get; set; }

        public Pagination()
        {
            count = 20;
            margin = 0;
        }
    }
}
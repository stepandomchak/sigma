﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class DocumentPagination : Pagination
    {
        public int status { get; set; }

        public DocumentPagination()
        {
            status = 7;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class UpdateCommission
    {
        [Required]
        public double TakerCommission { get; set; }
        [Required]
        public double MakerCommission { get; set; }
    }
}
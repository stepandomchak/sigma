﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Document
{
    public class UpdateUserDoc
    {
        public string guid { get; set; }

        public int status { get; set; }

        public string comment { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Document
{
    public class DocType
    {
        public string name { get; set; }
        public string description { get; set; }
        
    }
}
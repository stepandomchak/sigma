﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Document
{
    public class UploadUserDoc
    {
        public string type { get; set; }

        public string email { get; set; }
               
        public string name { get; set; }

        public string comment { get; set; }

        public byte[] data { get; set; }

    }
}
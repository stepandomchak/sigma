﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class PhoneNumber
    {
        [Required]
        [MaxLength(64)]
        public string phoneCode { get; set; }

        [Required]
        [MaxLength(64)]
        public string phoneNumber { get; set; }
    }
}
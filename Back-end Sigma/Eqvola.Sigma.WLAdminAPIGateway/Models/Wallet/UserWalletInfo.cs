﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models.Wallet
{
    public class UserWalletInfo
    {
        public double totalSum { get; set; }
        public double freeSum { get; set; }
        public double tradedVolume { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Models
{
    public class CheckAvailable
    {
        public bool isExist { get; set; }
        public string message { get; set; }
    }
}
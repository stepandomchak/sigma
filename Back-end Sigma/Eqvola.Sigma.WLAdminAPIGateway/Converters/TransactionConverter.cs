﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Converters
{
    public static class TransactionConverter
    {
        public static Com.OrderStorage.Models.Transaction ToComObject(this Service.OrderStorage.Models.Transaction src)
        {
            return new Com.OrderStorage.Models.Transaction()
            {
                Id = src.Id,
                SellOrder = src.SellOrder.ToComObject(),
                BuyOrder = src.BuyOrder.ToComObject(),
                Date_Time = src.Date_Time,
                Price = src.Price,
                Ammount = src.Amount,
                TakerId = src.Taker
            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Converters
{
    public static class UserConverter
    {
        public static Models.User ToComObject(this Service.Storage.Models.User src)
        {
            return new Models.User()
            {
                Email = src.Email,
                FirstName = src.FirstName,
                LastName = src.LastName,
                PhoneCode = src.Phone_code,
                PhoneNumber = src.Phone_number,
                Country = src.Country,
                Address = src.Address,
                City = src.City,
                AvatarUrl = src.AvatarUrl,
                RegistrationDate = src.RegistrationDate,
                IsVerified = src.IsVerified,
                TimeZone = src.TimeZone,
                Messangers = src.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            };
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Converters
{
    public static class ManagerConverter
    {
        public static Models.Manager ToComObject(this Service.Storage.Models.Manager src)
        {
            return new Models.Manager()
            {
                User = src.User.ToComObject(),
                Status = src.Status,
                Rank = src.Rank,
                FiringReason = src.FiringReason,
                Permissions = src.Permissions.Select(p => p.Name).ToList()          
            };
        }
    }
}
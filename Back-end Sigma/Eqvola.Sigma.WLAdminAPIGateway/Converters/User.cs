﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAdminAPIGateway.Converters
{
    public static class UserConverter
    {
        public static Models.User ToComObject(this Service.Storage.Models.User src)
        {
            return new Models.User()
            {
                email = src.Email,
                firstName = src.FirstName,
                lastName = src.LastName,
                phoneCode = src.Phone_code,
                phoneNumber = src.Phone_number,
                country = src.Country,
                address = src.Address,
                city = src.City,
                avatarUrl = src.AvatarUrl,
                messangers = src.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            };
        }
    }
}
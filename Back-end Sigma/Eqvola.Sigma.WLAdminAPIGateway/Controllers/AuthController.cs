﻿using Eqvola.Sigma.Com.Auth;
using Eqvola.Sigma.Service.Storage;
using Eqvola.Sigma.WLAdminAPIGateway.Converters;
using Eqvola.Sigma.WLAdminAPIGateway.Models;
using Eqvola.Sigma.WLAdminAPIGateway.Providers;
using Google.Authenticator;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class AuthController : ApiController
    {
        /// <summary>
        /// Method for authentication.
        /// </summary>
        /// <param name="model">User data</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("api/auth/Login")]
        [SwaggerOperation("Login")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(LoginResponse), Description = "token - Token for access to other methods\n\r" +
            "Status values:\n\r\t" +
            "0 - wrong data\n\t" +
            "1 - success\n\t" +
            "2 - manager fired\n\t" +
            "3 - department deleted\n\t" +
            "4 - 2FA code not found\n\t" +
            "5 - Wrong 2FAcode\n\t" +
            "6 - manager invited\n\t" +
            "7 - status undefind\n\r" +
            "message - Description of failed attempt")]
        public async Task<IHttpActionResult> Login(Login model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var service = new Service.Storage.Service();

            var manager = await service.GetManagerByUidAndPassword(model.email, model.password);

            if (manager == null)
            {
                return Ok(new LoginResponse() { token = "", message = "No Such Manager", status = 0 });
            }

            if (manager.User.IsTwoFactorEnabled)
            {
                if (string.IsNullOrEmpty(model.code))
                {
                    return Ok(new LoginResponse() { token = "", message = "Сode not found", status = 4 });
                }
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                if (!tfa.ValidateTwoFactorPIN(manager.User.TwoFactorSecretKey, model.code))
                {
                    return Ok(new LoginResponse() { token = "", message = "Wrong code", status = 5 });
                }
            }
            var token = await CreateToken(TokenValidator.JWTExtraHeaders, ManagerConverter.ToComObject(manager), TokenValidator.JwtSecretKey);

            return Ok(new LoginResponse() { token = token, message = "", status = 1 });
        }

        private async Task<string> CreateToken(string headers, object payload, string key)
        {
            return await Task.Run(() =>
            {
                return TokenValidator.Encode<object>(headers, payload, Encoding.UTF8.GetBytes(key));
            });
        }

        /// <summary>
        /// Check user phone number
        /// </summary>
        /// <param name="model">Phone number</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/CheckPhone")]
        [SwaggerOperation("Check phone number")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success", Type = typeof(CheckAvailable))]
        public async Task<IHttpActionResult> CheckPhone(PhoneNumber model)
        {
            var service = new Service.Storage.Service();
            var isEmpty = await service.CheckNumber(model.phoneCode, model.phoneNumber);
            if (isEmpty)
            {
                return Ok(new CheckAvailable()
                {
                    isExist = false,
                    message = "Phone not found"
                });
            }
            return Ok(new CheckAvailable()
            {
                isExist = true,
                message = "Phone already used"
            });
        }

        /// <summary>
        /// Check the availability of email
        /// </summary>
        /// <param name="model">Email address</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/CheckEmail/{email}")]
        [SwaggerOperation("Check email address")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success", Type = typeof(CheckAvailable))]
        public async Task<IHttpActionResult> CheckEmail(string email)
        {
            var service = new Service.Storage.Service();
            var user = await service.GetUserByEmail(email);
            if (user == null)
            {
                return Ok(new CheckAvailable()
                {
                    isExist = false,
                    message = "Email not found"
                });
            }
            return Ok(new CheckAvailable()
            {
                isExist = true,
                message = "Email already used"
            });
        }

    }
}

﻿using Eqvola.Sigma.Com.Support.Attachment;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.Com.Support.Message;
using Eqvola.Sigma.Com.Support.Ticket;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.WLAdminAPIGateway.Models;
using Eqvola.Sigma.WLAdminAPIGateway.Models.Users;
using Eqvola.Sigma.WLAdminAPIGateway.Providers;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Result = Eqvola.Sigma.Com.Support.Result;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class SupportController : ApiController
    {
        #region Category
        [HttpPost]
        [Route("api/support/createCategory/")]
        [SwaggerOperation("CreateCategory")]
        public async Task<IHttpActionResult> CreateCategory([FromBody] Category model)
        {
            var request = new CreateCategoryRequest()
            {
                name = model.name,
                IsEnable = model.isEnable
            };
            var response = await request.GetResponse<CategoryResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    id = response.entity.Id,
                    name = response.entity.Name,
                    IsEnable = response.entity.IsEnable,
                });

            return BadRequest(response.result.ToString());
        }

        [HttpPut]
        [Route("api/support/updateCategory/{id}")]
        [SwaggerOperation("UpdateCategory")]
        public async Task<IHttpActionResult> UpdateCategory(long id, [FromBody] Category model)
        {
            var request = new UpdateCategoryRequest()
            {
                id = id,
                newName = model.name,
                IsEnable = model.isEnable
            };
            var response = await request.GetResponse<CategoryResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    id = response.entity.Id,
                    name = response.entity.Name,
                    isEnable = response.entity.IsEnable,
                });

            return BadRequest(response.result.ToString());
        }

        [HttpGet]
        [Route("api/support/getCategories")]
        [SwaggerOperation("GetCategories")]
        public async Task<IHttpActionResult> GetCategories()
        {
            var request = new GetCategoriesRequest();

            var response = await request.GetResponse<CategoriesResponse>();

            return Ok(response.categories.Select(c => new
            {
                id = c.Id,
                name = c.Name,
                isEnable = c.IsEnable,
                topics = c.Topics.Select(t => new
                {
                    id = t.Id,
                    name = t.Name,
                    categoryId = t.Category?.Id,
                    topicStatus = t.TopicStatus,
                    text = t.Text
                }).ToList()
            }));
        }
        #endregion

        #region Ticket
        [HttpPost]
        [Route("api/support/getTickets/")]
        [SwaggerOperation("GetTickets")]
        public async Task<IHttpActionResult> GetTickets([FromBody]SupportPagination model)
        {
            var request = new GetTicketRequest()
            {
                count = model.count,
                margin = model.margin,
                status = model.status,
                categoryName =  model.categoryName,
                email = model.email,
                search = model.search,
                orderBy = model.orderBy,
                isAscending = model.isAscending
            };

            var response = await request.GetResponse<TicketsResponse>();

            if (response.result == Result.Ok)
            {
                return Ok(new
                {
                    tickets = response.tickets.Select(m => new
                    {
                        id = m.Id,
                        title = m.Title,
                        ticketStatus = m.TicketStatus,
                        created = m.Created,
                        categoryName = m.Category.Name,
                        unreadMessages = m.Messages.Where(ms => ms.Status == 0).Count(),
                        lastMessage = m.Messages.Select(s => new
                        {
                            email = s.Email,
                            text = s.Text,
                            date = s.Created,
                            fullName = "",
                            attachments = s.Attachments.Select(a =>
                            new
                            {
                                id = a.Id,
                                url = a.Url,
                                fileName = a.FileName
                            })
                        }).LastOrDefault()
                    }),
                    totalCount = response.totalCount
                });
            }
            return BadRequest(response.result.ToString());
        }

        [HttpGet]
        [Route("api/support/getTicketById/{id}/")]
        [SwaggerOperation("GetTicketById")]
        public async Task<IHttpActionResult> GetTicketById(long id)
        {
            UserToken tokenUser = null;
            if (Request?.Headers?.Authorization != null)
            {
                string token = Request.Headers.Authorization.ToString();

                token = token.StartsWith("Bearer ") ? token.Substring(7) : token;
                
                try
                {
                    tokenUser = TokenValidator.Decode<UserToken>(token);
                }
                catch
                {
                    return BadRequest("Wrong token");
                }
            }

            var request = new GetTicketRequest()
            {
                id = id
            };

            if(tokenUser != null)
            {
                request.takerEmail = tokenUser.email;
            }
            var response = await request.GetResponse<TicketResponse>();

            if (response.result == Result.Ok)
            {
                return Ok(new
                {
                    ticket = (response.entity == null) ? 
                        null :
                        new
                        {
                            id = response.entity.Id,
                            title = response.entity.Title,
                            email = response.entity.Email,
                            created = response.entity.Created,
                            catetoryName = response.entity.Category.Name,
                            ticketStatus = response.entity.TicketStatus,
                            unreadMessages = response.entity.Messages.Where(ms => ms.Status == 0).Count(),
                            messages = response.entity.Messages.Select(m => new
                            {
                                email = response.entity.Email,
                                text = m.Text,
                                date = m.Created,
                                attachments = m.Attachments.Select(a =>
                                new
                                {
                                    id = a.Id,
                                    url = a.Url,
                                    fileName = a.FileName
                                })
                            })
                        },
                });
            }
            return BadRequest(response.result.ToString());
        }

        #endregion

        #region Topic

        [HttpPost]
        [Route("api/support/createTopic/")]
        [SwaggerOperation("CreateTopic")]
        public async Task<IHttpActionResult> CreateTopic([FromBody] TopicCreate model)
        {
            var request = new CreateTopicRequest()
            {
                Name = model.name,
                Text = model.text,
                CategoryId = model.categoryId,
                
            };
            var response = await request.GetResponse<TopicResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    id = response.entity.Id,
                    name = response.entity.Name,
                    categoryId = response.entity.Category?.Id,
                    topicStatus = response.entity.TopicStatus
                });

            return BadRequest(response.result.ToString());
        }

        [HttpPut]
        [Route("api/support/updateTopic/{id}")]
        [SwaggerOperation("UpdateTopic")]
        public async Task<IHttpActionResult> UpdateTopic(long id, [FromBody]TopicUpdate model)
        {
            var request = new UpdateTopicRequest()
            {
                Id = id,
                NewName = model.name,
                Text = model.text,
                NewCategoryId = model.categoryId,
                TopicStatus = model.topicStatus
            };
            var response = await request.GetResponse<TopicResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    id = response.entity.Id,
                    name = response.entity.Name,
                    categoryId = response.entity.Category?.Id,
                    topicStatus = response.entity.TopicStatus,
                    text = response.entity.Text
                });

            return BadRequest(response.result.ToString());
        }

        [HttpDelete]
        [Route("api/support/deleteTopic/{id}")]
        [SwaggerOperation("DeleteTopic")]
        public async Task<IHttpActionResult> DeleteTopic(long id)
        {
            var request = new DeleteTopicRequest()
            {
                id = id
            };
            var response = await request.GetResponse<TopicResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    id = response.entity.Id,
                    name = response.entity.Name,
                    categoryId = response.entity.Category?.Id,
                    topicStatus = response.entity.TopicStatus,
                    text = response.entity.Text
                });

            return BadRequest(response.result.ToString());
        }

        [HttpGet]
        [Route("api/support/getTopicsByCategory/{categoryId}/")]
        [SwaggerOperation("GetTopicsByCategory")]
        public async Task<IHttpActionResult> GetTopicsByCategory(long categoryId)
        {
            var request = new GetTopicRequest
            {
                categoryId = categoryId
            };
            var response = await request.GetResponse<TopicsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.topics.Select(item => new
                {
                    id = item.Id,
                    name = item.Name,
                    topicStatus = item.TopicStatus,
                    categoryId = item.Category?.Id,
                    text = item.Text
                }));

            return BadRequest(response.result.ToString());
        }

        [HttpGet]
        [Route("api/support/getAllTopics/")]
        [SwaggerOperation("GetAllTopics")]
        public async Task<IHttpActionResult> GetAllTopics()
        {
            var request = new GetTopicRequest();
            var response = await request.GetResponse<TopicsResponse>();
            if (response.result == Result.Ok)
                return Ok(response.topics.Select(item => new
                {
                    id = item.Id,
                    name = item.Name,
                    text = item.Text,
                    topicStatus = item.TopicStatus,
                    categoryId = item.Category?.Id
                }));
            return BadRequest(response.result.ToString());
        }

        #endregion

        #region Message

        [HttpPost]
        [Route("api/support/sendMessage/")]
        [SwaggerOperation("SendMessage")]
        public async Task<IHttpActionResult> SendMessage([FromBody]Message message)
        {
            if (Request?.Headers?.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();

            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;
            UserToken tokenUser;

            try
            {
                tokenUser = TokenValidator.Decode<UserToken>(token);
            }
            catch
            {
                return BadRequest("Wrong token");
            }

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(tokenUser.email);

            if (user == null)
                return BadRequest("Wrong authorization");

            if (string.IsNullOrEmpty(message.text) && (message.attachments == null || message.attachments.Count == 0))
                return BadRequest("Message is empty");

            var request = new SendMessageRequest()
            {
                email = user.Email,
                text = message.text ?? string.Empty,
                ticketId = message.ticketId,
                listAttachments = message.attachments,
                Created = DateTime.UtcNow
            };
            var response = await request.GetResponse<MessageResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    email = response.entity.Email,
                    text = response.entity.Text,
                    date = response.entity.Created,
                    attachments = response.entity.Attachments.Select(a =>
                    new
                    {
                        id = a.Id,
                        url = a.Url,
                        fileName = a.FileName
                    })
                });

            return BadRequest(response.result.ToString());
        }
        #endregion

        #region Attachment
        [HttpPut]
        [Route("api/support/uploadAttachment/{id}/")]
        [SwaggerOperation("UploadAttachmentById")]
        public async Task<IHttpActionResult> UploadAttachment(long id, [FromBody] Attachment model)
        {
            var request = new UploadAttachmentRequest()
            {
                id = id,
            };

            var response = await request.GetResponse<AttachmentResponse>();

            if (response.result != Result.Ok)
                return BadRequest(response.result.ToString());

            var mimeType = MimeMapping.GetMimeMapping(response.entity.FileName);
            var url = await new Service.Blob.Service().UploadFile(response.entity.Id.ToString(), mimeType,
                model.base64, "attachments");
            if (string.IsNullOrEmpty(url))
                return BadRequest("File is not upload");

            request.url = url;
            response = await request.GetResponse<AttachmentResponse>();

            if (response.result == Result.Ok)

                return Ok(
                    new
                    {
                        id = response.entity.Id,
                        url = response.entity.Url,
                        fileName = response.entity.FileName
                    });

            return BadRequest(response.result.ToString());
        }
        #endregion
    }
}
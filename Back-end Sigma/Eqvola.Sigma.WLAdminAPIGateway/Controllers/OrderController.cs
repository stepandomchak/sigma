﻿using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Service.Storage.Models;
using Eqvola.Sigma.WLAdminAPIGateway.Converters;
using Eqvola.Sigma.WLAdminAPIGateway.Models;
using Eqvola.Sigma.WLAdminAPIGateway.Models.Order;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class OrderController : ApiController
    {
        [HttpPost]
        [SwaggerOperation("GetOrders")]
        [Route("api/order/getOrders/")]
        public async Task<IHttpActionResult> GetOrders([FromBody]OrderPagination model)
        {
            var service = new Service.Storage.Service();
            var orderService = new Service.OrderStorage.Service();

            Service.Storage.Models.User user = null;
            if (!string.IsNullOrEmpty(model.email))
            {
                user = await service.GetUserByEmail(model.email);

                if (user == null)
                    return NotFound();
            }

            var orders = await orderService.GetUserOrders(userId: user.Id, status: model.status, count: model.count, margin: model.margin, dateFrom: model.dateFrom, dateTo: model.dateTo, search: model.search);
            var ordersCount = await orderService.GetUserOrdersCount(userId: user.Id, status: model.status, dateFrom: model.dateFrom, dateTo: model.dateTo, search: model.search);

            var pairs = await Service.MarketService.Service.getPairs();
            if (orders != null)
                return Ok(new
                {
                    orders = orders.Select(o => new
                    {
                        orderId = o.Id,
                        currency = o.Currency,
                        targetCurrency = o.TargetCurrency,
                        expiresTime = o.ExpiresTime,
                        creationTime = o.CreationTime,
                        price = o.Price,
                        amount = o.Amount,
                        takeProfit = o.TakeProfit,
                        stopLoss = o.StopLoss,
                        stop = o.Stop,
                        limit = o.Limit,
                        orderType = o.Type,
                        marketCurrency = pairs?.Where(p => p.CurrencyPair == o.Currency + "_" + o.TargetCurrency).FirstOrDefault()?.MarketCurrency,
                        sum = Math.Round(o.Price * o.Amount, 15),
                        commission = Math.Round((o.Price * o.Amount / 100) * o.Commission, 15),
                        clearSum = Math.Round((o.Price * o.Amount) - ((o.Price * o.Amount / 100)) * o.Commission, 15)
                    }).ToList(),
                    totalCount = ordersCount
                });
            
            return NotFound();
        }

        [HttpPost]
        [SwaggerOperation("GetTransactions")]
        [Route("api/order/getTransactions/")]
        public async Task<IHttpActionResult> GetTransactions([FromBody]TransactionPagination model)
        {
            var service = new Service.Storage.Service();
            var orderService = new Service.OrderStorage.Service();

            Service.Storage.Models.User user = null;
            if (!string.IsNullOrEmpty(model.email))
            {
                user = await service.GetUserByEmail(model.email);

                if (user == null)
                    return NotFound();
            }

            var transactions = await orderService.GetTransactions(userId: user.Id, count: model.count, margin: model.margin, dateFrom: model.dateFrom, dateTo: model.dateTo, search: model.search);
            var transactionsCount = await orderService.GetTransactionsCount(userId: user.Id, dateFrom: model.dateFrom, dateTo: model.dateTo, search: model.search);

            var pairs = await Service.MarketService.Service.getPairs();

            if (transactions != null)
            {
                var resp = new
                {
                    transactions = transactions.Select(o => new
                    {
                        id = o.Id,
                        order = o.GetUserOrder(user.Id),
                        amount = o.Amount,
                        price = o.Price,
                        dateTime = o.Date_Time,
                        takerId = o.Taker,
                        marketCurrency = o.BuyOrder.UserId == user.Id ? 
                        pairs?.Where(p => p.CurrencyPair == o.BuyOrder.Currency + "_" + o.BuyOrder.TargetCurrency).FirstOrDefault()?.MarketCurrency
                        : pairs.Where(p => p.CurrencyPair == o.SellOrder.Currency + "_" + o.SellOrder.TargetCurrency).FirstOrDefault()?.MarketCurrency

                    }).ToList(),
                    totalCount = transactionsCount
                };
                return Ok(resp);
            }
            
            return NotFound();

        }

        [HttpGet]
        [SwaggerOperation("GetCommissions")]
        [Route("api/order/getCommissions/")]
        public async Task<IHttpActionResult> GetCommissions()
        {
            var service = new Service.Storage.Service();
            var response = await service.GetSettings(new List<string>() { "MakerCommission", "TakerCommission" });
            return Ok(new
            {
                sommissions = response.Select(s => new { key = s.Key, value = s.Value }).ToList()
            });
        }

        [HttpPut]
        [SwaggerOperation("UpdateCommissions")]
        [Route("api/order/updateCommissions/")]
        public async Task<IHttpActionResult> UpdateCommissions([FromBody]UpdateCommission model)
        {
            var settings = new Dictionary<string, double>();
            settings.Add("TakerCommission", model.TakerCommission);
            settings.Add("MakerCommission", model.MakerCommission);

            var service = new Service.Storage.Service();
            var response = await service.UpdateSettings(settings);
            
            return Ok(new
            {
                sommissions = response.Select(s => new { key = s.Key, value = s.Value }).ToList()
            });
        }

    }
}

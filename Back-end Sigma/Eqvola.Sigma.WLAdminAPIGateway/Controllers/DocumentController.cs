﻿using Eqvola.Sigma.WLAdminAPIGateway.Models;
using Eqvola.Sigma.WLAdminAPIGateway.Models.Document;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class DocumentController : ApiController
    {
        /// <summary>
        /// Get document types 
        /// </summary>
        /// <returns> List of document types </returns>
        [HttpGet]
        [Route("api/document/get/")]
        [SwaggerOperation("GetDocTypes")]
        public async Task<IHttpActionResult> GetDocTypes()
        {
            var service = new Service.Storage.Service();

            var response = await service.GetDocumentTypes();

            if (response != null)
                return Ok(response.Select(item => new
                {
                    name = item.Name,
                    description = item.Description,
                    status = item.Status
                }).ToList());

            return BadRequest();
        }

        [HttpPost]
        [SwaggerOperation("CreateDocType")]
        [Route("api/document/createDocType/")]
        public async Task<IHttpActionResult> CreateDocType([FromBody]DocType model)
        {
            var service = new Service.Storage.Service();

            Service.Storage.Models.DocumentType type = new Service.Storage.Models.DocumentType()
            {
                Name = model.name,
                Description = model.description,
                Status = (int)Service.Storage.Models.DocTypeStatus.Active
            };

            var response = await service.CreateDocumentType(type);

            if (response != null)
                return Ok(new
                {
                    name = response.entity.Name,
                    description = response.entity.Description,
                    status = response.entity.Status
                });

            return BadRequest();

        }

        [HttpPut]
        [SwaggerOperation("UpdateDocType")]
        [Route("api/document/updateDocType/")]
        public async Task<IHttpActionResult> UpdateDocType([FromBody]UpdateDocType model)
        {
            var service = new Service.Storage.Service();

            Service.Storage.Models.DocumentType type = await service.GetDocumentType(model.name);

            if (!Enum.IsDefined(typeof(Service.Storage.Models.DocTypeStatus), model.status))
                return BadRequest("No Such Status");

            type.Status = model.status;

            type.Description = model.description;

            var response = await service.UpdateDocumentType(type);

            if (response != null)
                return Ok(new
                {
                    name = response.entity.Name,
                    description = response.entity.Description,
                    status = response.entity.Status
                });

            return BadRequest();
        }



        [HttpGet]
        [Route("api/document/get/{email}")]
        [SwaggerOperation("GetUserDocs")]
        public async Task<IHttpActionResult> GetUserDocs(string email)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
                return BadRequest("No Such User!");

            var response = await service.GetUserDocuments(user.Id);

            if (response != null)
                return Ok(response.Select(item => new
                {
                    name = item.Name,
                    type = item.Type.Name,
                    guid = item.Guid,
                    url = item.Url,
                    status = item.Status,
                    comment = item.Comment,
                    uploadDate = item.UploadDate
                }).ToList());

            return BadRequest();
        }

        [HttpPost]
        [Route("api/document/getAllDocs/")]
        [SwaggerOperation("GetAllDocs")]
        public async Task<IHttpActionResult> GetAllDocs(DocumentPagination model)
        {
            var service = new Service.Storage.Service();

            var count = (await service.GetUserDocsCount());
            var response = await service.GetAllDocumentsByStatus(model.status, model.count, model.margin);

            if (response != null)
                return Ok(new
                {
                    docs = response.Select(item => new
                    {
                        user = new
                        {
                            email = item.User.Email,
                            firstName = item.User.FirstName,
                            lastName = item.User.LastName,
                            phoneCode = item.User.Phone_code,
                            phoneNumber = item.User.Phone_number,
                            country = item.User.Country,
                            address = item.User.Address,
                            city = item.User.City,
                            avatarUrl = item.User.AvatarUrl,
                            registrationDate = item.User.RegistrationDate,
                            isVerified = item.User.IsVerified,
                            timeZone = item.User.TimeZone,
                            messangers = item.User.ImContacts.ToDictionary(s => s.messanger, c => c.contact)
                        },
                        fileName = item.Name,
                        type = item.Type.Name,
                        guid = item.Guid,
                        url = item.Url,
                        status = item.Status,
                        comment = item.Comment,
                        uploadDate = item.UploadDate
                    }),
                    totalCount = count
                });

            return BadRequest();
        }

        [HttpPut]
        [SwaggerOperation("ChangeDocStatus")]
        [Route("api/document/changeDocStatus/")]
        public async Task<IHttpActionResult> ChangeDocStatus([FromBody]UpdateUserDoc model)
        {
            var service = new Service.Storage.Service();

            Service.Storage.Models.UserDocument doc = await service.GetUserDocument(model.guid);

            if (!Enum.IsDefined(typeof(Service.Storage.Models.DocumentStatus), model.status))
                return BadRequest("No Such Status");

            doc.Status = model.status;
            doc.Comment = model.comment;

            var response = await service.UpdateUserDocument(doc);

            if (response != null)
            {
                // Changing user verification state by uploaded documents
                /*var docs = (await service.GetUserDocumentsByStatus(doc.User.Id, Service.Storage.Models.DocumentStatus.Accepted)).ToList();
                var types = ((await service.GetDocumentTypes()).Where(t => t.Status == (int)Service.Storage.Models.DocTypeStatus.Active)).ToList();

                if (docs.Count >= types.Count)
                {
                    doc.User.IsVerified = true;
                    await service.UpdateUser(doc.User);
                }
                */
                return Ok(new
                {
                    name = response.Name,
                    status = response.Status,
                    email = response.User.Email,
                    type = response.Type.Name,
                    guid = response.Guid,
                    url = response.Url,
                    comment = response.Comment,
                    uploadDate = response.UploadDate
                });
            }
            return BadRequest();
        }

        [HttpDelete]
        [SwaggerOperation("DeleteDoc")]
        [Route("api/document/deleteDoc/")]
        public async Task<IHttpActionResult> DeleteDoc(string guid)
        {
            var service = new Service.Storage.Service();

            Service.Storage.Models.UserDocument doc = await service.GetUserDocument(guid);
            if (doc == null)
                return NotFound();

            var blob = new Service.Blob.Service();
            var mimeType = MimeMapping.GetMimeMapping(doc.Name);

            var delBlob = await blob.DeleteFile(doc.Guid, "documents");

            if (!string.IsNullOrEmpty(delBlob))
            {
                var res = await service.DeleteUserDoc(doc);
                if(res != null)
                    return Ok();
            }

            return BadRequest();
        }

    }
}

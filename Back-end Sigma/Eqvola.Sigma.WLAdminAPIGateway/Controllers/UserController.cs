﻿using Eqvola.Sigma.WLAdminAPIGateway.Converters;
using Eqvola.Sigma.WLAdminAPIGateway.Models;
using Eqvola.Sigma.WLAdminAPIGateway.Models.Users;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        [SwaggerOperation("Get")]
        [Route("api/user/{email}/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Models.User>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> GetUserByEmail(string email)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(new {
                email = user.Email,
                firstName = user.FirstName,
                lastName = user.LastName,
                phoneCode = user.Phone_code,
                phoneNumber = user.Phone_number,
                country = user.Country,
                address = user.Address,
                city = user.City,
                avatarUrl = user.AvatarUrl,
                registrationDate = user.RegistrationDate,
                isVerified = user.IsVerified,
                timeZone = user.TimeZone,
                messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            });
        }

        [HttpPost]
        [SwaggerOperation("GetAll")]
        [Route("api/user/getAll/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Models.User>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> GetAllUsers([FromBody]UserPagination pagination)
        {
            var service = new Service.Storage.Service();

            var count = (await service.GetUsersCount());
            var users = await service.GetAllUsers(pagination.count, pagination.margin);
            
            if (users == null)
            {
                return NotFound();
            }

            return Ok(new
            {
                count = count,
                users = users.Select(m => new
                {
                    email = m.Email,
                    firstName = m.FirstName,
                    lastName = m.LastName,
                    phoneCode = m.Phone_code,
                    phoneNumber = m.Phone_number,
                    country = m.Country,
                    address = m.Address,
                    city = m.City,
                    avatarUrl = m.AvatarUrl,
                    registrationDate = m.RegistrationDate,
                    isVerified = m.IsVerified,
                    timeZone = m.TimeZone,
                    messangers = m.ImContacts.ToDictionary(s => s.messanger, c => c.contact)
                })
            });
        }

        /// <summary>
        /// Update user information
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="model">User data for updating</param>
        /// <returns> Updated user </returns>
        [HttpPut]
        [SwaggerOperation("Update")]
        [Route("api/user/{email}/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<UserUpdate>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> UpdateUser(string email, [FromBody]UserUpdate model)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }
            else
            {
                if (!user.Email.Equals(model.email))
                {
                    var checkEmail = await service.GetUserByEmail(model.email);
                    if (checkEmail != null)
                        return BadRequest("Existen Email!");
                }

                string number = user.Phone_code + user.Phone_number;
                if (!number.Equals(model.phoneCode + model.phoneNumber))
                {
                    var checkNumber = await service.CheckNumber(model.phoneCode, model.phoneNumber);
                    if (checkNumber == false)
                        return BadRequest("Existen Phone!");
                }

                user.FirstName = model.firstName;
                user.LastName = model.lastName;
                user.Email = model.email;
                user.Address = model.address;
                user.City = model.city;
                user.Country = model.country;

                if(model.registrationDate != null)
                    user.RegistrationDate = model.registrationDate.Value;

                var response = await service.UpdateUser(user);
                if (response.entity == null)
                    return BadRequest(response.message);

            }
            return Ok(user.ToComObject());
        }

        [HttpPut]
        [SwaggerOperation("VerifyUser")]
        [Route("api/user/verifyUser/{email}/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Models.User>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> VerifyUser(string email)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }

            user.IsVerified = true;

            var res = await service.UpdateUser(user);

            if (res.entity != null)
                return Ok();

            return BadRequest();
        }

        [HttpPut]
        [SwaggerOperation("CancelVerifyUser")]
        [Route("api/user/cancelVerify/{email}/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Models.User>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> CancelVerifyUser(string email)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }

            user.IsVerified = false;

            var res = await service.UpdateUser(user);

            if (res.entity != null)
                return Ok();

            return BadRequest();
        }

    }
}

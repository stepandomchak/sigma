﻿using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.WalletService.Handlers;
using Eqvola.Sigma.WLAdminAPIGateway.Models.Wallet;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Result = Eqvola.Sigma.Com.Wallet.Result;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class WalletController : ApiController
    {
        #region Wallets
        [HttpGet]
        [Route("api/wallet/get/{email}/")]
        [SwaggerOperation("GetUserWallets")]
        public async Task<IHttpActionResult> GetWallets(string email)
        {
            if (string.IsNullOrEmpty(email))
                return BadRequest("Email is empty");

            var request = new GetUserWalletRequest()
            {
                email = email
            };
            var response = await request.GetResponse<GetWalletsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.wallets.Select(m => m.GetFullWallet()));

            return BadRequest();
        }

        [HttpGet]
        [Route("api/wallet/getAllWallets")]
        [SwaggerOperation("GetAllWallets")]
        public async Task<IHttpActionResult> GetAllWallets()
        {
            var request = new GetUserWalletRequest();            
                          
            var response = await request.GetResponse<GetWalletsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.wallets.Select(m => m.GetFullWallet()));

            return BadRequest();
        }

        [HttpGet]
        [Route("api/wallet/getUserInfo/{email}")]
        [SwaggerOperation("GetUserInfo")]
        public async Task<IHttpActionResult> GetUserInfo(string email)
        {
            if(string.IsNullOrEmpty(email))
            {
                return BadRequest("Wrong email");
            }

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }

            UserWalletInfo userInfo = new UserWalletInfo();

            var walletsRequest = new GetUserWalletRequest() { userId = user.Id};
            var walletsResponse = await walletsRequest.GetResponse<GetWalletsResponse>();

            var quotationsRequest = new GetQuotationsRequest();
            var quotationsResponse = await quotationsRequest.GetResponse<GetQuotationsResponse>();

            var ordersRequest = new GetFilteredOrdersRequest() { userId = user.Id};
            var ordersResponse = await ordersRequest.GetResponse<OrdersResponse>();

            foreach (var wallet in walletsResponse.wallets)
            {
                if(wallet.CurrencyCode == "BTC")
                {
                    userInfo.totalSum += wallet.Balance + wallet.GetBlockedFundsSum();
                    userInfo.freeSum += wallet.Balance;
                }
                else
                {
                    foreach(var item in quotationsResponse.PairPrices)
                    {
                        //todo
                        var pairs = item.Key.Split(new char[] { '_' });
                        if (item.Value == 0 || pairs == null || pairs.Count() < 2) continue;
                        if(pairs[0] == "BTC" && pairs[1] == wallet.CurrencyCode)
                        {
                            userInfo.totalSum += (wallet.Balance + wallet.GetBlockedFundsSum()) * item.Value;
                            userInfo.freeSum += (wallet.Balance) * item.Value;
                        }
                    }
                }
            }

            foreach(var order in ordersResponse.orders)
            {
                if (order.Status != 2 && order.Status != 4)
                    continue;
                if (order.Currency == "BTC")
                {
                    userInfo.tradedVolume += order.Amount;
                }
                else
                {
                    foreach (var item in quotationsResponse.PairPrices)
                    {
                        //todo
                        var pairs = item.Key.Split(new char[] { '_' });
                        if (item.Value == 0 || pairs == null || pairs.Count() < 2) continue;
                        if (pairs[0] == "BTC" && pairs[1] == order.Currency)
                        {
                            userInfo.tradedVolume += order.Amount * item.Value;
                        }
                    }
                }
            }
            
            return Ok(userInfo);
        }
        

        #endregion

        #region Withdrawal
        [HttpGet]
        [Route("api/wallet/getWithdrawal/{txId}")]
        [SwaggerOperation("GetWithdrawal")]
        public async Task<IHttpActionResult> GetWithdrawal(string txId)
        {
            var request = new GetWithdrawalRequest()
            {
                txId = txId
            };
            var response = await request.GetResponse<WithdrawalResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.GetWithdrawal());
                    
            return BadRequest();
        }

        [HttpPost]
        [Route("api/wallet/getAllWithdrawals")]
        [SwaggerOperation("GetAllWithdrawals")]
        public async Task<IHttpActionResult> GetWithdrawals([FromBody]WalletHistory model)
        {
            var request = new GetWithdrawalRequest() {
                email = model.email,
                status = model.status,
                currencyCode = model.currencyCode,
                count = model.count,
                margin = model.margin,
                dateFrom = model.dateFrom,
                dateTo = model.dateTo,
                search = model.search,
                orderBy = model.orderBy,
                isAscending = model.isAscending
            };
            
            var response = await request.GetResponse<WithdrawalsResponse>();


            var service = new Service.Storage.Service();
            List<long> users = response.withdrawals.GroupBy(t => t.Wallet.UserId).Select(s => s.Key).ToList();
            var getUsers = await service.GetRangeUsersByIds(users);

            if (response.result == Result.Ok)
                return Ok(new
                {
                    withdrawals = response.withdrawals.Select(m => m.GetWithdrawal(getUsers.Where(u => u.Id == m.Wallet.UserId).FirstOrDefault())),
                    totalCount = response.totalCount
                });

            return BadRequest();
        }

        #endregion

        #region Deposit

        [HttpGet]
        [Route("api/wallet/getDeposit/{txId}")]
        [SwaggerOperation("GetWithdrawal")]
        public async Task<IHttpActionResult> GetDeposit(string txId)
        {
            var request = new GetDepositRequest()
            {
                txId = txId
            };
            var response = await request.GetResponse<DepositResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.GetDeposit());

            return BadRequest();
        }

        [HttpPost]
        [Route("api/wallet/getAllDeposits")]
        [SwaggerOperation("GetAllWithdrawals")]
        public async Task<IHttpActionResult> GetDeposits([FromBody]WalletHistory model)
        {
            var request = new GetDepositRequest()
            {
                email = model.email,
                status = model.status,
                currencyCode = model.currencyCode,
                count = model.count,
                margin = model.margin,
                dateFrom = model.dateFrom,
                dateTo = model.dateTo,
                search = model.search,
                orderBy = model.orderBy,
                isAscending = model.isAscending
            };

            var response = await request.GetResponse<DepositsResponse>();


            var service = new Service.Storage.Service();
            List<long> users = response.deposits.GroupBy(t => t.Wallet.UserId).Select(s => s.Key).ToList();
            var getUsers = await service.GetRangeUsersByIds(users);

            if (response.result == Result.Ok)
                return Ok(new
                {
                    deposits = response.deposits.Select(m => m.GetDeposit(getUsers.Where(u => u.Id == m.Wallet.UserId).FirstOrDefault())),
                    totalCount = response.totalCount
                });

            return BadRequest();
        }

        #endregion
    }
}
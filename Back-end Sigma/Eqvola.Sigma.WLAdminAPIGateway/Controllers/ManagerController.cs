﻿using Eqvola.Sigma.APIGateway.Models;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Eqvola.Sigma.WLAdminAPIGateway.Controllers
{
    public class ManagerController : ApiController
    {
        /// <summary>
        /// Get list of managers 
        /// </summary>
        /// <param name="status"> (Not Required) Status of managers which need to get </param>
        /// <returns> List of managers </returns>
        [HttpGet]
        [SwaggerOperation("GetAll")]
        [Route("api/manager/{status:int?}")]
        public async Task<IHttpActionResult> Get(int status = 7)
        {
            var service = new Service.Storage.Service();
            var response = await service.GetAllManagers(status);

            if (response != null)
                return Ok(response.Select(m => new
                {
                    User = new
                    {
                        email = m.User.Email,
                        firstName = m.User.FirstName,
                        lastName = m.User.LastName,
                        phoneCode = m.User.Phone_code,
                        phoneNumber = m.User.Phone_number,
                        country = m.User.Country,
                        city = m.User.City,
                        timeZone = m.User.TimeZone,
                        avatar = m.User.AvatarUrl,
                        registrationDate = m.User.RegistrationDate
                    },
                    permissions = m.Permissions.Select(p => p.Name),
                    status = m.Status,
                    firingReason = m.FiringReason
                }));

            return NotFound();
        }

        /// <summary>
        /// Get manager details
        /// </summary>
        /// <param name="email"> Manager email (url encoded) </param>
        /// <returns> Manager data or not found </returns>
        [HttpGet]
        [Route("api/manager/{email}")]
        [SwaggerOperation("Details")]
        public async Task<IHttpActionResult> Get(string email)
        {
            var service = new Service.Storage.Service();
            var response = await service.GetManagerByUId(email);

            if (response != null)
            {
                var messangers = new Dictionary<string, string>();
                foreach (var entry in response.ImContacts)
                    messangers.Add(entry.messanger, entry.contact);
                return Ok(new
                {
                    User = new
                    {
                        email = response.User.Email,
                        firstName = response.User.FirstName,
                        lastName = response.User.LastName,
                        phoneCode = response.User.Phone_code,
                        phoneNumber = response.User.Phone_number,
                        country = response.User.Country,
                        city = response.User.City,
                        timeZone = response.User.TimeZone,
                        avatar = response.User.AvatarUrl,
                        registrationDate = response.User.RegistrationDate
                    },
                    permissions = response.Permissions.Select(p => p.Name),
                    status = response.Status,
                    firingReason = response.FiringReason,
                    imContacts = messangers
                });
            }
            return NotFound();
        }

        /// <summary>
        /// Create new manager 
        /// </summary>
        /// <param name="model"> Model of new manager </param>
        /// <returns> Object of created manager or reason why it's not created </returns>
        [HttpPost]
        [Route("api/manager")]
        [SwaggerOperation("Create")]
        public async Task<IHttpActionResult> Post([FromBody] CreateManager model)
        {
            var service = new Service.Storage.Service();

            Service.Storage.Models.User user = null;


            user = await service.GetUserByEmail(model.email);
            if (user == null)
            {
                user = new Service.Storage.Models.User()
                {
                    Email = model.email,
                    City = model.address_city,
                    Country = model.country,
                    FirstName = model.firstname,
                    LastName = model.lastname,
                    Phone_code = model.phone_code,
                    Phone_number = model.phone_number,
                    Password = model.password,
                    TimeZone = model.time_zone,
                    RegistrationDate = DateTime.UtcNow
                };
            }
            else
            {
                Service.Storage.Models.Manager managerCheck = await service.GetManagerByUId(model.email);
                if(managerCheck != null)
                {
                    return BadRequest("Manager is already exist");
                }
            }

            Service.Storage.Models.Manager manager = new Service.Storage.Models.Manager()
            {
                Permissions = await service.MapPermissions(model.permissions),
                User = user,
                ImContacts = model.messangers?.Select(m => new Service.Storage.Models.ImContact()
                {
                    messanger = m.name,
                    contact = m.value
                }).ToList(),
                Status = (int)Com.Storage.Models.ManagerStatus.Active
            };

            var response = await service.CreateManager(manager);
            if (string.IsNullOrEmpty(response.message))
            {
                return Ok(new
                {
                    email = response.entity.User.Email,
                    city = response.entity.User.City,
                    country = response.entity.User.Country,
                    firstName = response.entity.User.FirstName,
                    lastName = response.entity.User.LastName,
                    phone_code = response.entity.User.Phone_code,
                    phone_number = response.entity.User.Phone_number,
                    timeZone = response.entity.User.TimeZone,
                    registrationDate = response.entity.User.RegistrationDate
                });
            }
            return BadRequest(response.message);
        }


        /// <summary>
        /// Update manager
        /// </summary>
        /// <param name="email"> Email of updatable manager </param>
        /// <param name="model"> New data of manager</param>
        /// <returns> Data about updatable manager or reason why it's not updated </returns>
        [HttpPut]
        [Route("api/manager/{email}")]
        [SwaggerOperation("Update")]
        public async Task<IHttpActionResult> Put(string email, [FromBody] UpdateManager model)
        {
            var service = new Service.Storage.Service();

            Service.Storage.Models.Manager manager = await service.GetManagerByUId(email);

            if (manager == null)
                return NotFound();

            manager.User.City = model.address_city;
            manager.User.Country = model.country;
            manager.User.FirstName = model.firstname;
            manager.User.LastName = model.lastname;
            manager.User.Phone_code = model.phone_code;
            manager.User.Phone_number = model.phone_number;
            manager.User.TimeZone = model.time_zone;
            manager.User.RegistrationDate = model.registrationDate;
           
            {
                var messangers = new Dictionary<string, string>();
                foreach (var entry in model.messangers)
                    messangers.Add(entry.name, entry.value);

                var rem = manager.ImContacts.Where(p => !messangers.Keys.Contains(p.messanger)).ToList();
                foreach (var p in rem)
                    manager.ImContacts.Remove(p);
                foreach (var c in manager.ImContacts)
                    if (messangers.ContainsKey(c.messanger))
                        c.contact = messangers[c.messanger];
                var exists = manager.ImContacts.Select(p => p.messanger);
                var add = messangers.Where(e => !exists.Contains(e.Key));
                foreach (var p in add)
                    manager.ImContacts.Add(new Service.Storage.Models.ImContact()
                    {
                        messanger = p.Key,
                        contact = p.Value
                    });
            }
            
            {
                var rem = manager.Permissions.Where(p => !model.permissions.Contains(p.Name)).ToList();
                foreach (var p in rem)
                    manager.Permissions.Remove(p);
                var exists = manager.Permissions.Select(p => p.Name);
                var add = await service.MapPermissions(model.permissions.Where(p => !exists.Contains(p)));
                foreach (var p in add)
                    manager.Permissions.Add(p);
            }
                        
            var response = await service.UpdateManager(manager);

            if (string.IsNullOrEmpty(response.message))
            {
                return Ok(new
                {
                    email = response.entity.User.Email,
                    city = model.address_city,
                    country = model.country,
                    firstName = model.firstname,
                    lastName = model.lastname,
                    phone_code = model.phone_code,
                    phone_number = model.phone_number,
                    timeZone = model.time_zone,
                    registrationDate = model.registrationDate                    
                });
            }
            return BadRequest(response.message);
        }

    }
}

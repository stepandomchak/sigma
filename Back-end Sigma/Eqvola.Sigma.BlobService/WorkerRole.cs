using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Service;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.ServiceBus.Messaging;
using Eqvola.Sigma.Com.Blob;
using Eqvola.Sigma.BlobService.Handlers;

namespace Eqvola.Sigma.BlobService
{
        public partial class WorkerRole : ServiceBusRole
        {
            protected override IEnumerable<string> queues => new string[] { "blob" };

            protected override IDictionary<string, Func<BrokeredMessage, Task<Com.Response>>> CreateHandlers(string queueName)
            {
                switch (queueName)
                {
                    case "blob":
                        return new Dictionary<string, Func<BrokeredMessage, Task<Com.Response>>>
                            {
                                { "upload-file", (message) => { return HandlerFactory<UploadFileRequest, UploadFile>.ProccessMessage(message); } }
                            };
                            default:
                        throw new IndexOutOfRangeException();
                }
            }

        }
    }
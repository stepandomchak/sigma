﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.BlobService.Handlers
{
    public abstract class BlobHandler<TRequest> : BaseHandler<TRequest, Service.Blob.Service> where TRequest : Request
    {
        public override void Dispose()
        {
            //service.Dispose();
        }

        public override async Task<Response> ProccessMessage(BrokeredMessage message)
        {
            request = message.GetBody<TRequest>();
            try
            {
                return await ProccessAsync();
            }
            catch (DataException ex)
            {
                return CreateFailResponse();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected abstract Response CreateFailResponse();
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.BlobService.Handlers
{
    public class UploadFile : BlobHandler<UploadFileRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new UrlResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (string.IsNullOrEmpty(request.BlobReference))
                return new UrlResponse()
                {
                    result = Result.BlobReferenceIsEmpty
                };

            if (string.IsNullOrEmpty(request.BlobContainer))
                return new UrlResponse()
                {
                    result = Result.BlobContainerIsEmpty
                };

            if (request.File == null || request.File.Count() == 0)
                return new UrlResponse()
                {
                    result = Result.FileIsEmpty
                };

            string url = await service.UploadFile(request.BlobReference, request.MimeType, request.File, request.BlobContainer);
            if (url == null)
                return new UrlResponse()
                {
                    result = Result.Exception
                };
            else
                return new UrlResponse()
                {
                    entity = url,
                    result = Result.Ok
                };
        }
    }
}

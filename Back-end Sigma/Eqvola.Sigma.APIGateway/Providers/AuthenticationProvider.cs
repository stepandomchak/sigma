﻿using System.Threading.Tasks;
using Eqvola.Sigma.Com.Auth;
using Eqvola.Sigma.APIGateway.Models.Auth;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System;
using Eqvola.Sigma.Com.Auth.TwoFactor;

namespace Eqvola.Sigma.APIGateway.Providers
{
    public class AuthenticationProvider
    {
        /// <summary>
        /// Creates new authentication
        /// </summary>
        /// <param name="model">Model with user creeds</param>
        /// <returns>LoginResponse that represents JWT token</returns>
        public static async Task<LoginResponse> Login(Login model)
        {
            var request = new LoginRequest()
            {
                uid = model.email,
                password = model.password,
                code = model.code,
                headers = JWTExtraHeaders,
                key = JwtSecretKey
            };

            return await request.GetResponse<LoginResponse>();
        }

        public static string JwtSecretKey { get; } = ConfigurationManager.AppSettings["JWT.Secret"];

        public static string JWTExtraHeaders
        {
            get
            {
                IDictionary<string, string> headers = new Dictionary<string, string>()
                {
                    { "expiresOn", DateTime.UtcNow.AddMinutes(10).Ticks.ToString() }
                };

                return Newtonsoft.Json.JsonConvert.SerializeObject(headers);
            }
        }

        private static byte[] JWTSecretKeyBytes
        {
            get
            {
                return Encoding.ASCII.GetBytes(JwtSecretKey);
            }
        }
    }
}
﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;

namespace Eqvola.Sigma.APIGateway.Providers
{
    public static class TokenValidator
    {
        #region Private Fields
        private static byte[] _secretKey;
        private static IJwtDecoder _decoder;
        #endregion

        #region Private Properties

        private static IJwtDecoder Decoder
        {
            get
            {
                if (_decoder == null)
                    _decoder = CreateDecoder();
                return _decoder;
            }
        }

        private static byte[] SecretKey
        {
            get
            {
                if (_secretKey == null)
                    _secretKey = Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["JWT.Secret"]);
                return _secretKey;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Decode JWT string to Model
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="token">JWT string</param>
        /// <returns>Object with Type<typeparamref name="T"/></returns>
        public static T Decode<T>(string token) where T : class
        {
            try
            {
                var payload = Decoder.Decode(token, SecretKey, true);
                return JsonConvert.DeserializeObject<T>(payload);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Validating JWT token using JWT headers and client Request headers
        /// </summary>
        /// <param name="tokenHeaders">Extra headers from JWT</param>
        /// <param name="clientHeaders">Client request headers</param>
        /// <returns>Returns True if token is valid</returns>
        public static bool IsTokenValid(IDictionary<string, object> tokenHeaders, IDictionary<string, string> clientHeaders)
        {
            try
            {
                long expiresOnTicks = Convert.ToInt64(tokenHeaders.Where(q => q.Key == "expiresOn").FirstOrDefault().Value);

                if (expiresOnTicks < DateTime.UtcNow.Ticks)
                {
                    throw new UnauthorizedAccessException(string.Format("Token was expired in {0}", Convert.ToDateTime(expiresOnTicks).ToShortDateString()));
                }

                return true;
            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex;
            }
        }

        #region Helper JWT methods
        /// <summary>
        /// Take and decode JWT token header
        /// </summary>
        /// <param name="token">JWT token</param>
        /// <returns>string that represents JWT header</returns>
        public static string GetTokenHeaders(string token)
        {
            var tokenHeader = token.Split('.')[0];

            return Encoding.UTF8.GetString(TokenValidator.Base64UrlDecode(tokenHeader));
        }

        /// <summary>
        /// Adds headers to Dictionary
        /// </summary>
        /// <param name="headers">headers that will added to JWT header</param>
        /// <returns>Dictionary with extra headers</returns>
        private static IDictionary<string, object> KeepExtraHeaders(object headers)
        {
            IDictionary<string, object> extraHeaders = new Dictionary<string, object>();
            if (headers != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(headers))
                {
                    object value = descriptor.GetValue(headers);
                    extraHeaders.Add(descriptor.Name, value);
                }
            }

            return extraHeaders;
        }

        // from JWT spec
        private static string Base64UrlEncode(byte[] input)
        {
            var output = Convert.ToBase64String(input);
            output = output.Split('=')[0]; // Remove any trailing '='s
            output = output.Replace('+', '-'); // 62nd char of encoding
            output = output.Replace('/', '_'); // 63rd char of encoding
            return output;
        }

        // from JWT spec
        private static byte[] Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+'); // 62nd char of encoding
            output = output.Replace('_', '/'); // 63rd char of encoding
            switch (output.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: output += "=="; break; // Two pad chars
                case 3: output += "="; break; // One pad char
                default: throw new System.Exception("Illegal base64url string!");
            }
            var converted = Convert.FromBase64String(output); // Standard base64 decoder
            return converted;
        }
        #endregion

        #region Init Methods
        private static IJwtDecoder CreateDecoder()
        {
            var serializer = new JsonNetSerializer();
            var provider = new UtcDateTimeProvider();
            var validator = new JwtValidator(serializer, provider);
            var urlEncoder = new JwtBase64UrlEncoder();

            return new JwtDecoder(serializer, validator, urlEncoder);
        }
        #endregion


        #endregion
    }
}

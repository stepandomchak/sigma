﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Threading;
using System.Security.Principal;

namespace Eqvola.Sigma.APIGateway.Filters
{
    public class PermissionAttribute : AuthorizeAttribute
    {
        private GenericPrincipal _principal;

        private string _permissions;
        private string _ranks;

        private string[] _permissionsSplit = new string[0];
        private string[] _ranksSplit = new string[0];

        public string Permissions
        {
            get { return _permissions ?? String.Empty; }
            set
            {
                _permissions = value;
                _permissionsSplit = SplitString(value);
            }
        }

        public string Ranks
        {
            get
            {
                return _ranks ?? String.Empty;
            }
            set
            {
                _ranks = value;
                _ranksSplit = SplitString(value);
            }
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            _principal = Thread.CurrentPrincipal as GenericPrincipal;
            actionContext.RequestContext.Principal = _principal;

            base.OnAuthorization(actionContext);
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var permissions = _principal.Claims.Where(q => q.Type == "Permissions").FirstOrDefault().Value.Split(',');

            if (_permissionsSplit.Length > 0 && !_permissionsSplit.Any(q => IsInPermission(permissions, q)))
            {
                return false;
            }
            
            var ranks = _principal.Claims.Where(q => q.Type == "Roles").FirstOrDefault().Value.Split(',');

            if (_ranksSplit.Length > 0 && !_ranksSplit.Any(q => IsInRole(ranks, q)))
            {
                return false;
            }

            return base.IsAuthorized(actionContext);
        }

        internal bool IsInRole(string[] roles, string role)
        {
            if (roles.Contains(role))
                return true;
            return false;
        }

        internal bool IsInPermission(string[] permissions, string permission)
        {
            if (permissions.Contains(permission))
                return true;
            return false;
        }

        internal static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }
    }
}
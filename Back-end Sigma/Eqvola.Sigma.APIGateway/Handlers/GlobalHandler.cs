﻿using Eqvola.Sigma.APIGateway.Providers;
using Eqvola.Sigma.Com.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Handlers
{
    public class GlobalHandler : DelegatingHandler
    {
        protected async override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            System.Threading.CancellationToken cancellationToken)
        {
            Thread.CurrentPrincipal = TryGetAuthentication(request);
            request.GetRequestContext().Principal = Thread.CurrentPrincipal;

            return await base.SendAsync(request, cancellationToken);
        }

        private GenericPrincipal TryGetAuthentication(HttpRequestMessage request)
        {
            string token;

            if (request.Headers.Authorization != null)
            {
                token = request.Headers.Authorization.Parameter;
            }
            else
            {
                token = GetTokenFromQueryString(request.RequestUri.Query);
            }

            if (token != null)
            {
                var identity = GetContext(token);
                var roles = identity.Claims.Where(q => q.Type == "Role");
                if (roles.Count() == 0)
                    return null;
                var principal = new GenericPrincipal(identity, identity.Claims.Where(q => q.Type == "Role").FirstOrDefault().Value.Split(','));

                return principal;
            }

            return null;
        }

        private GenericIdentity GetContext(string token)
        {
            var user = TokenValidator.Decode<Manager>(token);
            if (user == null) return new GenericIdentity(string.Empty);

            var roleClaim = new Claim("Role", user.Rank.ToString());
            string permissions = string.Empty;
            foreach (var per in user.Permissions)
            {
                permissions = permissions + String.Concat(",", per);
            }
            permissions = permissions.Remove(0,1);

            var permissionsClaim = new Claim("Permissions", permissions);

            var claims = new List<Claim>()
            {
                roleClaim,
                permissionsClaim
            };

            GenericIdentity identity = new GenericIdentity(user.User.Email);

            identity.AddClaims(claims);

            return identity;
        }

        private string GetTokenFromQueryString(string queryStr)
        {
            var queryParams = HttpUtility.ParseQueryString(queryStr);
            if (queryParams.HasKeys())
            {
                string api_key = queryParams.Get("api_key");
                if (api_key != null)
                {
                    return api_key;
                }
                else
                {
                    string token = queryParams.Get("token");
                    if (token != null)
                    {
                        return token;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }
}
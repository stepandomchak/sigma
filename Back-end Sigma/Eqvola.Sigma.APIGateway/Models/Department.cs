﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Department model
    /// </summary>
    public class Department: DepartmentShort
    {
        /// <summary>
        /// List of department tags
        /// </summary>
        public IEnumerable<string> Tags { get; set; }
    }
}

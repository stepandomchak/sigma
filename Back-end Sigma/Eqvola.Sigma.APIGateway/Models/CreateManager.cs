﻿using Eqvola.Sigma.APIGateway.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Data for adding messangers
    /// </summary>
    public class MessangerEntry
    {
        /// <summary>
        /// Name of manager's messanger
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Contact of manager's messanger
        /// </summary>
        public string value { get; set; }
    }

    /// <summary>
    /// Data for creating manager
    /// </summary>
    public class CreateManager : Password
    {
        /// <summary>
        /// User email address
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Name of department in which the manager will work
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// Rank which are issued to the manager (Admin/Manager/Supervisor)
        /// </summary>
        public int rank { get; set; }

        /// <summary>
        /// Firstname of manager
        /// </summary>
        public string firstname { get; set; }

        /// <summary>
        /// Lastname of manager
        /// </summary>
        public string lastname { get; set; }

        /// <summary>
        /// Phone code of manager
        /// </summary>
        public string phone_code { get; set; }

        /// <summary>
        /// Phone number of manager
        /// </summary>
        public string phone_number { get; set; }

        /// <summary>
        /// Country in which manager live
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// City in which manager live
        /// </summary>
        public string address_city { get; set; }

        /// <summary>
        /// Time zone in which manager live        
        /// </summary>
        public string time_zone { get; set; }

        /// <summary>
        /// Permissions which are issued to the manager
        /// </summary>
        public IEnumerable<string> permissions { get; set; }

        /// <summary>
        /// Messangers of manager
        /// </summary>
        public IEnumerable<MessangerEntry> messangers {get;set;}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    public class CreateTransaction
    {
        public long sellOrderId { get; set; }
        public long buyOrderId { get; set; }
        public int taker { get; set; }
        public double amount { get; set; }
    }
}
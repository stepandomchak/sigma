﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Department short model
    /// </summary>
    public class DepartmentShort
    {
        /// <summary>
        /// Department name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// List of department permissions
        /// </summary>
        public IEnumerable<string> Permissions { get; set; }
    }
}

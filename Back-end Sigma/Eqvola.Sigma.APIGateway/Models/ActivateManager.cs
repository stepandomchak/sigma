﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Data for activating manager
    /// </summary>
    public class ActivateManager
    {
        /// <summary>
        /// Manager email address
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Name of department in which the manager will work
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// Permissions which are issued to the manager
        /// </summary>
        public IEnumerable<string> permissions { get; set; }
    }
}
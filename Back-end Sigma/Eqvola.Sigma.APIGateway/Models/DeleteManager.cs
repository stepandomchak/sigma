﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Data for set manager status as fired
    /// </summary>
    public class DeleteManager
    {
        /// <summary>
        /// Manager email address
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Reason why the manager was fired
        /// </summary>
        public string reason { get; set; }
    }
}
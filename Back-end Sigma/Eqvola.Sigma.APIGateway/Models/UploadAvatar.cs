﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    public class UploadAvatar
    {
        /// <summary>
        /// Data about picture
        /// </summary>
        [Required]
        public string avatarBase64 { get; set; }

        /// <summary>
        /// Type of picture
        /// </summary>
        [Required]
        public string mimeType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private Image image;

        private enum ImageTypes
        {
            JPG,
            PNG
        }

        /// <summary>
        /// Check image size and type
        /// </summary>
        /// <returns></returns>
        public ValidateStatus ValidateAvatar()
        {
            ValidateStatus status = new ValidateStatus() { status = true };
            status.bytes = Convert.FromBase64String(avatarBase64);

            var data = avatarBase64.Substring(0, 5);

            switch (data.ToUpper())
            {
                case "IVBOR":
                    if("image/png" != mimeType.ToLower())
                    {
                        status.status = false;
                        status.description = "Wrong image type";
                        return status;
                    }
                    break;
                case "/9J/4":
                    if ("image/jpeg" != mimeType.ToLower())
                    {
                        status.status = false;
                        status.description = "Wrong image type";
                        return status;
                    }
                    break;
                default:
                    status.status = false;
                    status.description = "Wrong image type";
                    return status;
            }

            if (status.bytes.Length > 1048576)
            {
                status.status = false;
                status.description = "image size limit exceeded";
                return status;
            }
            return status;
        }
        
    }

    /// <summary>
    /// Data to validate image
    /// </summary>
    public class ValidateStatus
    {
        /// <summary>
        /// Status is the image valid
        /// </summary>
        public bool status { get; set; }

        /// <summary>
        /// Image in bytes
        /// </summary>
        public byte[] bytes { get; set; }

        /// <summary>
        /// Error description if validation failed
        /// </summary>
        public string description { get; set; }
    }


}
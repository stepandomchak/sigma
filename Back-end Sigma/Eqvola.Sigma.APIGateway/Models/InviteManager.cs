﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Data for inviting manager
    /// </summary>
    public class InviteManager
    {
        /// <summary>
        /// Manager email address
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Department name in which manager is invited
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// Rank which offered to manager
        /// </summary>
        public int rank { get; set; }

        /// <summary>
        /// Permissions which are issued to the manager
        /// </summary>
        public IEnumerable<string> permissions { get; set; }
    }
}
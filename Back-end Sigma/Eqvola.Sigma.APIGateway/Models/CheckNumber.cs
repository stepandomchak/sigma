﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    /// <summary>
    /// Data to check the phone number on uniqueness
    /// </summary>
    public class CheckNumber
    {
        /// <summary>
        /// Phone code of the phone to be checked
        /// </summary>
        public string phone_code { get; set; }

        /// <summary>
        /// Number of the phone to be checked
        /// </summary>
        public string phone_number { get; set; }
    }
}
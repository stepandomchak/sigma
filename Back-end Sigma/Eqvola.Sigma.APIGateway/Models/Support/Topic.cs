﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models.Support
{
    public class Topic
    {
        /// <summary>
        /// Total information about manager
        /// </summary>
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> Queslion { get; set; }

     
    }
}
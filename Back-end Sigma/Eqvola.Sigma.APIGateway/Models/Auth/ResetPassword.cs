﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models.Auth
{
    /// <summary>
    /// User data for reset password
    /// </summary>
    public class ResetPassword
    {
        /// <summary>
        /// User email address
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        [Required]
        public string phone_number { get; set; }
    }
}

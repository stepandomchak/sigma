﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models.Auth
{
    public class AcceptChangePassword : Password
    {
        /// <summary>
        /// Code from SMS
        /// </summary>
        [Required]
        public string code { get; set; }
    }
}

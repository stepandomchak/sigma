﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models.Auth
{
    /// <summary>
    /// Manager model
    /// </summary>
    public class Manager
    {
        /// <summary>
        /// Total information about manager
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Status of manager
        /// </summary>
        public int Status { get; set; }
        public string FiringReason { get; set; }

        /// <summary>
        /// Rank of manager
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Permissions of manager
        /// </summary>
        public IEnumerable<string> Permissions { get; set; }

        /// <summary>
        /// Tags of manager
        /// </summary>
        public IEnumerable<string> Tags { get; set; }


        public DepartmentModel Department { get; set; }

        /// <summary>
        /// Manager's messangers
        /// </summary>
        public IDictionary<string, string> ImContacts { get; set; }
    }
    /// <summary>
    /// User model
    /// </summary>
    public class User
    {
        /// <summary>
        /// Email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Firstname
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Lastname
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Phone code
        /// </summary>
        public string PhoneCode { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// City 
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Time zone 
        /// </summary>
        public string TimeZone { get; set; }
        public bool IsTwoFactorEnabled { get; set; }
        public string AvatarUrl { get; set; }
    }
    /// <summary>
    /// User model
    /// </summary>
    public class DepartmentModel
    {
        /// <summary>
        /// Name of department
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Permissions of department
        /// </summary>
        public IEnumerable<string> Permissions { get; set; }

        /// <summary>
        /// Tags of department
        /// </summary>
        public IEnumerable<string> Tags { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.APIGateway.Models
{
    public class TwoFactor
    {
        /// <summary>
        /// Code from two-factor application
        /// </summary>
        public string code { get; set; }
    }
}
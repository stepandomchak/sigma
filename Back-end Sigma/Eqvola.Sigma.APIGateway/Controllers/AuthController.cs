﻿using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using Eqvola.Sigma.APIGateway.Providers;
using Eqvola.Sigma.APIGateway.Models.Auth;
using Eqvola.Sigma.Com.Auth;
using Eqvola.Sigma.Com.Storage;
using System.ComponentModel.DataAnnotations;
using Eqvola.Sigma.Service.Auth;
using System;
using System.Diagnostics;
using Eqvola.Sigma.APIGateway.Models;
using Eqvola.Sigma.Com.Auth.TwoFactor;

namespace Eqvola.Sigma.APIGateway.Controllers
{
    [RoutePrefix("api/Auth")]
    public class AuthController : ApiController
    {
        /// <summary>
        /// Method for authentication.
        /// </summary>
        /// <param name="model">User data</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [SwaggerOperation("Login")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(LoginResponse), Description = "token - Token for access to other methods\n\r" +
            "Status values:\n\r\t0 - wrong data\n\t1 - success\n\t2 - manager fired\n\t3 - department deleted\n\t4 - code not found\n\t5 - wrong code\n\r" +
            "message - Description of failed attempt")]
        public async Task<IHttpActionResult> Login(Login model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = await AuthenticationProvider.Login(model);
            
            return Ok(response);
        }


        /// <summary>
        /// Reset user password
        /// </summary>
        /// <param name="model">User data for reset password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("ResetPassword")]
        [Route("ResetPassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> ResetPassword(ResetPassword model)
        {
            if(string.IsNullOrEmpty(model.email) || string.IsNullOrEmpty(model.phone_number))
            {
                return BadRequest("Parametrs is not valid ");
            }

            var request = new ResetRequest()
            {
                phone_number = model.phone_number,
                email = model.email
            };

            var response = await request.GetResponse<ResetResponse>();

            if(response != null)
            {
                Trace.WriteLine("ResetPassword" + response.result.ToString() + " " + response.message);
                if (response.result == Result.Ok)
                    return Ok();
                else

                    return BadRequest(response.message);
            }
            return BadRequest();
        }

        /// <summary>
        /// Confirm resetting user password
        /// </summary>
        /// <param name="model">User data to confirm resetting user password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("AcceptResetPassword")]
        [Route("AcceptResetPassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptResetPassword(AcceptResetPassword model)
        {
            if (string.IsNullOrEmpty(model.password) || string.IsNullOrEmpty(model.code))
            {
                return BadRequest("Parametrs is not valid");
            }

            Trace.WriteLine($"AcceptResetPassword request received");
            AcceptResetRequest request = new AcceptResetRequest()
            {
                email = model.email,
                phone_number = model.phone_number,
                code = model.code,
                newPassword = model.password
            };

            var response = await request.GetResponse<AcceptResetResponse>();

            if (response != null)
            {
                if (response.result == Result.Ok)
                    return Ok();
                else
                    return BadRequest(response.message);
            }
            return BadRequest();
        }


        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="model">User data to change user password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("ChangePassword")]
        [Route("ChangePassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> ChangePassword(Password model)
        {
            
            if (string.IsNullOrEmpty(model.password))
            {
                return BadRequest("Parametrs is not valid ");
            }

            if (Request == null || Request.Headers == null || Request.Headers.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();

            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;

            Manager manager = null;

            try
            {
                manager = TokenValidator.Decode<Manager>(token);
                if(manager == null) return BadRequest("Wrong token");
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            var request = new ChangePasswordRequest()
            {
                email = manager.User.Email,
                password = model.password
            };

            var response = await request.GetResponse<ChangePasswordResponse>();

            if (response != null)
            {
                if (response.result == Result.Ok)
                    return Ok();
                else
                    return BadRequest(response.message);
            }
            return BadRequest();
        }

        /// <summary>
        /// Confirm changing user password
        /// </summary>
        /// <param name="model">User data to confirm changing user password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("AcceptChangePassword")]
        [Route("AcceptChangePassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptChangePassword(AcceptChangePassword model)
        {
            
            if (string.IsNullOrEmpty(model.password))
            {
                return BadRequest("Parametrs is not valid ");
            }

            if (Request == null || Request.Headers == null || Request.Headers.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();
            
            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;

            Com.Storage.Models.Manager manager = null;

            try
            {
                manager = TokenValidator.Decode<Com.Storage.Models.Manager>(token);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            
            bool isHash = model.code.Length == 32 ? true : false;

            var request = new AcceptChangePasswordRequest()
            {
                email = manager.User.Email,
                code = model.code,
                newPassword = model.password,
                isHash = isHash
            };

            var response = await request.GetResponse<AcceptChangePasswordResponse>();

            if (response != null)
            {
                if (response.result == Result.Ok)
                    return Ok();
                else
                    return BadRequest(response.message);
            }
            return BadRequest();
        }

        /// <summary>
        /// Generate two-factor secret
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        [HttpPost]
        [Route("EnableTwoFactor/{email}")]
        [SwaggerOperation("EnableTwoFactor")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(TwoFactorEnableResponse), Description = "Status values:\n\r\t0 - wrong data\n\t1 - success\n\r" +
            "message - Description of failed attempt\n\r" +
            "url - link to QR code\n\r" +
            "manualCode - code for manually adding two-factor")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> EnableTwoFactor(string email)
        {
            var request = new TwoFactorEnableRequest()
            {
                uid = email
            };
            
            var response = await request.GetResponse<TwoFactorEnableResponse>();
            if (response.status)
                return Ok(new { response.url, response.manualCode});
            else
            {
                return BadRequest(response.message);
            }
        }

        /// <summary>
        /// Enabling two-factor
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="model">Data to confirm setting two-factor</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AcceptEnableTwoFactor/{email}")]
        [SwaggerOperation("AcceptEnableTwoFactor")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptEnableTwoFactor(string email, TwoFactor model)
        {
            var request = new TwoFactorAcceptEnableRequest()
            {
                uid = email,
                code = model.code
            };

            var response = await request.GetResponse<TwoFactorEnableResponse>();
            if (response.status)
                return Ok();
            else
            {
                return BadRequest(response.message);
            }
        }

        /// <summary>
        /// Disabling two-factor
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="model">Data to confirm setting two-factor</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DisableTwoFactor/{email}")]
        [SwaggerOperation("DisableTwoFactor")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> DisableTwoFactor(string email, TwoFactor model)
        {
            var request = new TwoFactorDisableRequest()
            {
                uid = email,
                code = model.code
            };
            
            var response = await request.GetResponse<TwoFactorDisableResponse>();
            if (response.status)
                return Ok();
            else
            {
                return BadRequest(response.message);
            }
        }

    }
}

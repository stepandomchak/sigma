﻿using Eqvola.Sigma.APIGateway.Models;
using Eqvola.Sigma.APIGateway.Models.Auth;
using Eqvola.Sigma.Com.Email;
using Eqvola.Sigma.Com.Storage;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Eqvola.Sigma.APIGateway.Controllers
{
    public class ManagerController : ApiController
    {
        /// <summary>
        /// Get list of managers
        /// </summary>
        /// <param name="status"> (Not Required) Status of managers which need to get </param>
        /// <returns> List of managers </returns>
        [HttpGet]
        [SwaggerOperation("GetAll")]
        [Route("api/manager/{status:int?}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Manager>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Get(int status = 7)
        {
            var request = new GetManagerRequest()
            { status = status };
            var response = await request.GetResponse<ManagersResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.Select(m => new
                {
                    User = new
                    {
                        m.User.Email,
                        m.User.FirstName,
                        m.User.LastName,
                        m.User.PhoneCode,
                        m.User.PhoneNumber,
                        m.User.Country,
                        m.User.City,
                        m.User.TimeZone,
                        m.User.AvatarUrl
                    },
                    m.Permissions,
                    m.Rank,
                    m.Tags,
                    m.Status,
                    m.FiringReason,
                    Department = new
                    {
                        m.Department.Name,
                        m.Department.Permissions,
                        m.Department.Tags
                    }
                }));

            return NotFound();
        }

        /// <summary>
        /// Get manager details
        /// </summary>
        /// <param name="email"> Manager email (url encoded) </param>
        /// <returns> Manager data or not found </returns>
        [HttpGet]
        [Route("api/manager/{email}")]
        [SwaggerOperation("Details")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(Manager), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Get(string email)
        {
            var request = new GetManagerRequest()
            {
                uid = email
            };
            var response = await request.GetResponse<ManagerResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    User = new
                    {
                        response.entity.User.Email,
                        response.entity.User.FirstName,
                        response.entity.User.LastName,
                        response.entity.User.PhoneCode,
                        response.entity.User.PhoneNumber,
                        response.entity.User.Country,
                        response.entity.User.City,
                        response.entity.User.TimeZone,
                        response.entity.User.IsTwoFactorEnabled,
                        response.entity.User.AvatarUrl
                    },
                    response.entity.Status,
                    response.entity.FiringReason,
                    response.entity.Permissions,
                    response.entity.Rank,
                    response.entity.Tags,
                    Department = new
                    {
                        response.entity.Department.Name,
                        response.entity.Department.Permissions,
                        response.entity.Department.Tags
                    },
                    response.entity.ImContacts
                });

            return NotFound();
        }

        /// <summary>
        /// Create new manager
        /// </summary>
        /// <param name="model"> Model of new manager </param>
        /// <returns> Object of created manager or reason why it's not created </returns>
        [HttpPost]
        [Route("api/manager")]
        [SwaggerOperation("Create")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(Manager))]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Post([FromBody] CreateManager model)
        {
            var request = new CreateManagerRequest()
            {
                address_city = model.address_city,
                country = model.country,
                department = model.department,
                email = model.email,
                firstname = model.firstname,
                lastname = model.lastname,
                password = model.password,
                phone_code = model.phone_code,
                phone_number = model.phone_number,
                rank = model.rank,
                permissions = model.permissions,
                time_zone = model.time_zone
            };
            request.messangers = new Dictionary<string, string>();
            foreach (var entry in model.messangers)
                request.messangers.Add(entry.name, entry.value);

            var response = await request.GetResponse<ManagerResponse>();

            if (response.result == Result.Ok)
            {
                return Ok(new
                {
                    response.entity.User.Email,
                    response.entity.Permissions,
                    response.entity.Rank,
                    Department = new
                    {
                        response.entity.Department.Name,
                        response.entity.Department.Permissions
                    }
                });
            }
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Update manager
        /// </summary>
        /// <param name="email"> Email of updatable manager </param>
        /// <param name="model"> New data of manager</param>
        /// <returns> Data about updatable manager or reason why it's not updated </returns>
        [HttpPut]
        [Route("api/manager/{email}")]
        [SwaggerOperation("Update")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(Manager), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Put(string email, [FromBody] UpdateManager model)
        {
            var request = new UpdateManagerRequest()
            {
                email = email,
                address_city = model.address_city,
                country = model.country,
                department = model.department,
                firstname = model.firstname,
                lastname = model.lastname,
                phone_code = model.phone_code,
                phone_number = model.phone_number,
                rank = model.rank,
                permissions = model.permissions,
                time_zone = model.time_zone
            };
            request.messangers = new Dictionary<string, string>();
            foreach (var entry in model.messangers)
                request.messangers.Add(entry.name, entry.value);

            var response = await request.GetResponse<ManagerResponse>();

            if (response.result == Result.Ok)
            {
                return Ok(new
                {
                    response.entity.User.Email,
                    response.entity.Permissions,
                    response.entity.Rank,
                    Department = new
                    {
                        response.entity.Department.Name,
                        response.entity.Department.Permissions
                    }
                });
            }
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Sending invite message to new manager
        /// </summary>
        /// <param name="model"> Data for inviting manager </param>
        /// <returns> Response code 200 or error code </returns>
        [HttpPost]
        [Route("api/manager/invite")]
        [SwaggerOperation("Invite")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Invite([FromBody] InviteManager model)
        {
            var request = new InviteManagerRequest()
            {
                department = model.department,
                email = model.email,
                rank = model.rank,
                permissions = model.permissions
            };
            var response = await request.GetResponse<StorageResponse<string>>();

            if (response.result == Result.Ok)
            {
                return Ok();
            }
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Uploading avatar for manager
        /// </summary>
        /// <param name="email"> Managers email </param>
        /// <param name="model"> Data about avatar</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/manager/avatar/{email}")]
        [SwaggerOperation("UploadAvatar")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> UploadAvatar(string email, [FromBody]UploadAvatar model)
        {
            if (model == null)
                return BadRequest();

            ValidateStatus validateResponse = model.ValidateAvatar();

            if (!validateResponse.status)
            {
                return BadRequest(validateResponse.description);
            }

            //Search manager by mail
            var requestFindManager = new GetManagerRequest()
            {
                uid = email
            };
            var responseFindManager = await requestFindManager.GetResponse<ManagerResponse>();
            if (responseFindManager.entity == null)
                return BadRequest(Result.NotFound.ToString());

            var request = new UploadAvatarRequest { email = email };

            var response = await request.GetResponse<StorageResponse<string>>();
            if (response.result != Result.Ok)
                return BadRequest(response.result.ToString());

            var url = await new Service.Blob.Service().UploadFile(email, model.mimeType, validateResponse.bytes, "avatars");
            if (string.IsNullOrEmpty(url))
                return BadRequest("link of avatar is empty"); 

            request.url = url;
            response = await request.GetResponse<StorageResponse<string>>();
            if (response.result == Result.Ok)
                return Ok(response.entity);
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Set manager status as fired
        /// </summary>
        /// <param name="model"> Data for manager firing</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/manager/delete")]
        [SwaggerOperation("Delete Manager")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Description = "Error description")]
        public async Task<IHttpActionResult> DeleteManager([FromBody] DeleteManager model)
        {
            var request = new DeleteManagerRequest()
            {
                email = model.email,
                reason = model.reason
            };
            var response = await request.GetResponse<ManagerResponse>();

            if (response.result == Result.Ok)
                return Ok();
            else if (response.result == Result.NotFound)
                return StatusCode(System.Net.HttpStatusCode.NotFound);
            else
                return StatusCode(System.Net.HttpStatusCode.BadRequest);

        }

        /// <summary>
        /// Set manager status as active
        /// </summary>
        /// <param name="model"> Data for manager activating</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/manager/activate")]
        [SwaggerOperation("Activate Manager")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Description = "Error description")]
        public async Task<IHttpActionResult> ActivateManager([FromBody] ActivateManager model)
        {
            var request = new ActivateManagerRequest()
            {
                email = model.email,
                department = model.department,
                permissions = model.permissions
            };
            var response = await request.GetResponse<ManagerResponse>();

            if (response.result == Result.Ok)
                return Ok();
            else if (response.result == Result.NotFound)
                return StatusCode(System.Net.HttpStatusCode.NotFound);
            else
                return StatusCode(System.Net.HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Check phone number for uniqueness
        /// </summary>
        /// <param name="model">Data about phone number</param>
        /// <returns> Not found if number is unique or Ok </returns>
        [HttpPost]
        [Route("api/manager/check")]
        [SwaggerOperation("Check Number")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> CheckNumber([FromBody] CheckNumber model)
        {
            var request = new CheckNumberRequest()
            {
                phone_code = model.phone_code,
                phone_number = model.phone_number
            };
            var response = await request.GetResponse<CheckNumberResponse>();

            return Ok(response.result.ToString());
        }
    }
}

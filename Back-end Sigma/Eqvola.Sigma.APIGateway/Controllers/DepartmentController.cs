﻿using Eqvola.Sigma.APIGateway.Models;
using Eqvola.Sigma.Com.Storage;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Eqvola.Sigma.APIGateway.Controllers
{
    /// <summary>
    /// Methods related to Department management.
    /// </summary>
    public class DepartmentController : ApiController
    {
        /// <summary>
        /// Get list of departments
        /// </summary>
        /// <param name="status"> (Not Required) Status of department which need to get </param>
        /// <returns> List of departments </returns>
        [HttpGet]
        [SwaggerOperation("GetAll")]
        [Route("api/department/{status:int?}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Department>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Get(int status = 3)
        {
            var request = new GetDepartmentRequest()
            {
                status = status
            };
            var response = await request.GetResponse<DepartmentsResponse>();
            if (response.result == Result.Ok)
                return Ok(response.entity.Select(d => new
                {
                    d.Name,
                    d.Permissions,
                    d.Tags,
                    d.Status,
                    Admins = d.Managers?
                    .Where(m => (m.Rank & (int)Com.Storage.Models.ManagerRank.Admin) > 0)
                    .Select(a => new {
                        FirstName = a.User.FirstName,
                        LastName = a.User.LastName,
                        Rank = a.Rank, 
                        Email = a.User.Email,
                        Status = a.Status,
                        Tags = a.Tags, 
                        AvatarUrl = a.User.AvatarUrl
                    })
                }));
            return NotFound();
        }

        /// <summary>
        /// Get department details
        /// </summary>
        /// <param name="name"> Department name (url encoded) </param>
        /// <returns> Department data or not found </returns>
        [HttpGet]
        [Route("api/department/{name}")]
        [SwaggerOperation("Details")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(Department), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> Get(string name)
        {
            var request = new GetDepartmentRequest()
            {
                name = name
            };
            var response = await request.GetResponse<DepartmentResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    response.entity.Name,
                    response.entity.Status,
                    response.entity.Permissions,
                    response.entity.Tags,
                    Managers = response.entity.Managers?
                    .Select(a => new {
                        Email = a.User.Email,
                        FirstName = a.User.FirstName,
                        LastName = a.User.LastName,
                        Rank = a.Rank,
                        Status = a.Status,
                        Tags = a.Tags,
                        AvatarUrl = a.User.AvatarUrl
                    })
                });
            return NotFound();
        }

        /// <summary>
        /// Create new department
        /// </summary>
        /// <param name="model"> Model of new department </param>
        /// <returns> Object of created department or reason why it's not created </returns>
        [HttpPost]
        [Route("api/department")]
        [SwaggerOperation("Create")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(DepartmentShort), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Description = "Error description")]
        public async Task<IHttpActionResult> Post([FromBody] DepartmentShort model)
        {
            var request = new SetDepartmentRequest()
            {
                name = model.Name,
                permissions = model.Permissions
            };
            var response = await request.GetResponse<DepartmentResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity);
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Update department
        /// </summary>
        /// <param name="name"> Name of updatable department </param>
        /// <param name="model"> New data for department </param>
        /// <returns> Object of updated department or reason why it's not updated </returns>
        [HttpPut]
        [Route("api/department/{name}")]
        [SwaggerOperation("Update")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(DepartmentShort), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Description = "Error description")]
        public async Task<IHttpActionResult> Put(string name, [FromBody] DepartmentShort model)
        {
            var request = new SetDepartmentRequest()
            {
                oldname = name,
                name = model.Name,
                permissions = model.Permissions
            };
            var response = await request.GetResponse<DepartmentResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity);
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Set department status as deleted
        /// </summary>
        /// <param name="name"> Name of deletable department </param>
        /// <returns> Result code 200 or Error code </returns>
        [HttpDelete]
        [SwaggerOperation("Delete")]
        [Route("api/department/delete/{name}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Description = "Error description")]
        public async Task<IHttpActionResult> DeleteDepartment(string name)
        {
            var request = new DeleteDepartmentRequest()
            {
                name = name
            };
            var response = await request.GetResponse<DepartmentResponse>();

            if (response.result == Result.Ok)
                return Ok();
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Set department status as active
        /// </summary>
        /// <param name="name"> Name of activatable department </param>
        /// <returns> Result code 200 or Error code</returns>
        [HttpPost]
        [Route("api/department/activate/{name}")]
        [SwaggerOperation("Activate")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Description = "Error description")]
        public async Task<IHttpActionResult> ActivateDepartment(string name)
        {
            var request = new ActivateDepartmentRequest()
            {
                name = name
            };
            var response = await request.GetResponse<DepartmentResponse>();

            if (response.result == Result.Ok)        
                return Ok();
            return BadRequest(response.result.ToString());
        }

    }
}
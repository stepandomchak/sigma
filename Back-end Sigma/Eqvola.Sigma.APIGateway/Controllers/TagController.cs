﻿using Eqvola.Sigma.Com.Storage;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Eqvola.Sigma.APIGateway.Controllers
{
    public class TagController : ApiController
    {
        /// <summary>
        /// Tag department
        /// </summary>
        /// <param name="name">Department to tag</param>
        /// <param name="tag">Tag name</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("tagDepartment")]
        [Route("api/tag/department/{name}/{tag}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        public async Task<IHttpActionResult> TagDepartment(string name, string tag)
        {
            var response = await (new TagRequest
            {
                action = TagAction.Tag,
                target = TagTarget.Department,
                tid = name,
                tag = tag
            }).GetResponse<StorageResponse<string>>();
            
            if (response.result == Result.Ok)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Untag department
        /// </summary>
        /// <param name="name">Department to untag</param>
        /// <param name="tag">Tag name</param>
        /// <returns></returns>
        [HttpDelete]
        [SwaggerOperation("untagDepartment")]
        [Route("api/tag/department/{name}/{tag}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Department not found")]
        public async Task<IHttpActionResult> UntagDepartment(string name, string tag)
        {
            var response = await (new TagRequest
            {
                action = TagAction.Untag,
                target = TagTarget.Department,
                tid = name,
                tag = tag
            }).GetResponse<StorageResponse<string>>();

            if (response.result == Result.Ok)
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// Delete department tag
        /// </summary>
        /// <param name="tag">Tag name</param>
        /// <returns></returns>
        [HttpDelete]
        [SwaggerOperation("DeleteDepartmentTag")]
        [Route("api/tag/department/{tag}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Tag not found")]
        public async Task<IHttpActionResult> DeleteDepartmentTag(string tag)
        {
            var response = await (new TagRequest
            {
                action = TagAction.Delete,
                target = TagTarget.Department,
                tag = tag
            }).GetResponse<StorageResponse<string>>();

            if (response.result == Result.Ok)
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// List of departments tags
        /// </summary>
        /// <returns>List of departments tags</returns>
        [HttpGet]
        [SwaggerOperation("DepartmentsTags")]
        [Route("api/tag/department")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        public async Task<IHttpActionResult> DepartmentsTags()
        {
            var response = await (new TagRequest
            {
                action = TagAction.GetAll,
                target = TagTarget.Department
            }).GetResponse<TagsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity);
            return NotFound();
        }

        /// <summary>
        /// Tag manager
        /// </summary>
        /// <param name="name">Manager to tag</param>
        /// <param name="tag">Tag name</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("tagManager")]
        [Route("api/tag/manager/{mid}/{tag}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Manager not found")]
        public async Task<IHttpActionResult> TagManager(string mid, string tag)
        {
            var response = await (new TagRequest
            {
                action = TagAction.Tag,
                target = TagTarget.Manager,
                tid = mid,
                tag = tag
            }).GetResponse<StorageResponse<string>>();

            if (response.result == Result.Ok)
                return Ok();
            return BadRequest();
        }

        /// <summary>
        /// Untag manager
        /// </summary>
        /// <param name="name">Manager to untag</param>
        /// <param name="tag">Tag name</param>
        /// <returns></returns>
        [HttpDelete]
        [SwaggerOperation("untagManager")]
        [Route("api/tag/manager/{mid}/{tag}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Tag or manager not found")]
        public async Task<IHttpActionResult> UntagManager(string mid, string tag)
        {
            var response = await (new TagRequest
            {
                action = TagAction.Untag,
                target = TagTarget.Manager,
                tid = mid,
                tag = tag
            }).GetResponse<StorageResponse<string>>();

            if (response.result == Result.Ok)
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// Delete manager tag
        /// </summary>
        /// <param name="tag">Tag name</param>
        /// <returns></returns>
        [HttpDelete]
        [SwaggerOperation("DeleteManagerTag")]
        [Route("api/tag/manager/{tag}")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Tag not found")]
        public async Task<IHttpActionResult> DeleteManagerTag(string tag)
        {
            var response = await (new TagRequest
            {
                action = TagAction.Delete,
                target = TagTarget.Manager,
                tag = tag
            }).GetResponse<StorageResponse<string>>();

            if (response.result == Result.Ok)
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// List of managers tags
        /// </summary>
        /// <returns>List of departments tags</returns>
        [HttpGet]
        [SwaggerOperation("ManagersTags")]
        [Route("api/tag/manager")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        public async Task<IHttpActionResult> ManagersTags()
        {
            var response = await (new TagRequest
            {
                action = TagAction.GetAll,
                target = TagTarget.Manager
            }).GetResponse<TagsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity);
            return NotFound();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class GetWalletsResponse : WalletResponse
    {
        public List<Models.Wallet> wallets{ get; set; }

        public GetWalletsResponse()
        {
            wallets = new List<Models.Wallet>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class GetUserWalletRequest : Request
    {
        public override string QueueName => "wallet";
        public override string RequestName => "get-wallet";

        public string email { get; set; }
        public long userId { get; set; }
        public string currency { get; set; }

    }
}

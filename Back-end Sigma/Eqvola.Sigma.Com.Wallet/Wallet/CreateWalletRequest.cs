﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class CreateWalletRequest : Request
    {
        public override string QueueName => "wallet";

        public override string RequestName => "create-wallet";

        public string email { get; set; }

        public string currencyCode { get; set; }

    }
}

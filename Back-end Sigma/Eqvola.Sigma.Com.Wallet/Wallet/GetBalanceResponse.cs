﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class GetBalanceResponse : WalletResponse
    {
        public double balance { get; set; }

    }
}

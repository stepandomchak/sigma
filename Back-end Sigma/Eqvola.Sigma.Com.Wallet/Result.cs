﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public enum Result
    {
        Ok,
        NotFound,
        InvalidRequest,
        UnexpectedAmount,
        NoSuchWallet,
        InsufficientFunds,
        UnexpectedCurrency,
        WalletAlreadyExists,
        UnexpectedFee
    }
}

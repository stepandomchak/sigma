﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet.Models
{
    public class Withdrawal
    {
        public long Id { get; set; }
        public double Amount { get; set; }
             
        public Wallet Wallet { get; set; }

        public int Status { get; set; }
        
        public DateTime? CreationTime { get; set; }
        
        public DateTime? ConfirmationTime { get; set; }
        
        public string TxId { get; set; }

        public string Comment { get; set; }
        
        public string To { get; set; }

        public double TransactionFee { get; set; }

        public object GetWithdrawal(User user = null)
        {
            if (user == null)
                return new
                {
                    amount = this.Amount,
                    to = this.To,
                    comment = this.Comment,
                    status = this.Status,
                    txId = this.TxId,
                    creationTime = this.CreationTime,
                    confirmationTime = this.ConfirmationTime,
                    transactionFee = this.TransactionFee,
                    wallet = this.Wallet.GetShotWallet()
                };
            return new
            {
                amount = this.Amount,
                to = this.To,
                comment = this.Comment,
                status = this.Status,
                txId = this.TxId,
                creationTime = this.CreationTime,
                confirmationTime = this.ConfirmationTime,              
                transactionFee = this.TransactionFee,
                email = user.Email,
                fullName = user.FirstName + " " + user.LastName,
                wallet = this.Wallet.GetShotWallet()
            };
        }
    }
}

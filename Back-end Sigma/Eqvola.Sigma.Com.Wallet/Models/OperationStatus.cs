﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet.Models
{
    public enum OperationStatus
    {
        InProcessing = 1,
        Successful = 2,
        Rejected = 4,
        Canceled = 8,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet.Models
{
    public class BlockedFunds
    {
        public long Id { get; set; }
        public Wallet Wallet { get; set; }
        public long OrderId { get; set; }
        public double Amount { get; set; }

        public object GetShotBlockedFunds()
        {
            return new
            {
                orderId = this.OrderId,
                amount = this.Amount
            };
        }
    }
}

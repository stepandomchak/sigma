﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet.Models
{
    public class Deposit
    {        
        public long Id { get; set; }

        public double Amount { get; set; }
        
        public Wallet Wallet { get; set; }

        public int Status { get; set; }
                
        public DateTime? CreationTime { get; set; }
                
        public DateTime? ConfirmationTime { get; set; }
        
        public string TxId { get; set; }
        
        public string From { get; set; }

        public object GetDeposit(User user = null)
        {
            if(user == null)
                return new
                {
                    amount = this.Amount,
                    from = this.From,
                    status = this.Status,
                    txId = this.TxId,
                    creationTime = this.CreationTime,
                    confirmationTime = this.ConfirmationTime,
                    wallet = this.Wallet.GetShotWallet()
                };
            return new
            {
                amount = this.Amount,
                from = this.From,
                status = this.Status,
                txId = this.TxId,                
                creationTime = this.CreationTime,
                confirmationTime = this.ConfirmationTime,
                email = user.Email,
                fullName = user.FirstName + " " + user.LastName,
                wallet = this.Wallet.GetShotWallet()
            };
        }

    }
}

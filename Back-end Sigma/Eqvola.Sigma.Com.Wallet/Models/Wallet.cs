﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet.Models
{
    public class Wallet
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public double Balance { get; set; }
        public string Address { get; set; }

        public IEnumerable<BlockedFunds> BlockedFunds { get; set; }

        public double GetBlockedFundsSum()
        {
            if (BlockedFunds == null)
                return 0;
            return BlockedFunds.Sum(s => s.Amount);
        }

        public object GetShotWallet()
        {
            return new
            {
                currencyCode = this.CurrencyCode,
                carrencyName = this.CurrencyName,
                balance = this.Balance,
                address = this.Address,                
            };            
        }

        public object GetFullWallet()
        {
            return new
            {
                currencyCode = this.CurrencyCode,
                currencyName = this.CurrencyName,
                balance = this.Balance,
                address = this.Address,
                blockedFunds = this.BlockedFunds.Sum(b => b.Amount),
                btcValue = this.Balance / 10
            };
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class DepositsResponse : WalletResponse
    {
        public List<Models.Deposit> deposits { get; set; }
        public int totalCount { get; set; }

        public DepositsResponse()
        {
            deposits = new List<Models.Deposit>();
        }
    }
}

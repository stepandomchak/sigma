﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class CreateDepositRequest : Request
    {
        public override string QueueName => "wallet";
        public override string RequestName => "create-deposit";
                
        public double amount { get; set; }

        public int status { get; set; }       

        public DateTime? confirmationTime { get; set; }

        public string txId { get; set; }

        public string from { get; set; }

        public long walletId { get; set; }
        
    }
}

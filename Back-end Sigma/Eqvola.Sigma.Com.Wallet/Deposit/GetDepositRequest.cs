﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class GetDepositRequest : Request
    {
        public override string QueueName => "wallet";

        public override string RequestName => "get-deposit";

        public string txId { get; set; }

        public string email { get; set; }

        public string currencyCode { get; set; }

        public int status { get; set; }

        public int margin { get; set; }

        public int count { get; set; }
        
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }

        public string search { get; set; }
        public string orderBy { get; set; }
        public bool isAscending { get; set; }

    }
}

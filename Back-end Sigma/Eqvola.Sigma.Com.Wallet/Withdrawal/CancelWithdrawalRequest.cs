﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class CancelWithdrawalRequest : Request
    {
        public override string QueueName => "wallet";
        public override string RequestName => "withdrawal-cancel";

        public string txId { get; set; }
    }
}

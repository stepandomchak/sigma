﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class CreateWithdrawalRequest : Request
    {
        public override string QueueName => "wallet";
        public override string RequestName => "create-withdrawal";

        public double amount { get; set; }

        public string email { get; set; }

        public string currencyCode { get; set; }       

        public DateTime? confirmationTime { get; set; }

        public string txId { get; set; }

        public string comment { get; set; }

        public string to { get; set; }

        public double fee { get; set; }
    }
}

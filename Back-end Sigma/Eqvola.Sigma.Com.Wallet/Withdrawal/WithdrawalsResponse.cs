﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class WithdrawalsResponse : WalletResponse
    {
        public List<Models.Withdrawal> withdrawals { get; set; }
        public int totalCount { get; set; }

        public WithdrawalsResponse()
        {
            withdrawals = new List<Models.Withdrawal>();
        }
    }
}


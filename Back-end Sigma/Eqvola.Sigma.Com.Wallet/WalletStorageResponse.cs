﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class WalletStorageResponse : Response
    {         
        public Result result { get; set; }
    }

    public class WalletStorageResponse<T> : WalletStorageResponse
    {
        public T entity { get; set; }
        public string message { get; set; } = "";
    }
}

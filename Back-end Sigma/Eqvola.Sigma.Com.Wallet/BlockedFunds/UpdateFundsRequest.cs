﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class UpdateFundsRequest : Request
    {
        public override string QueueName => "wallet";
        public override string RequestName => "update-funds";

        public long orderId { get; set; }
        public double amount { get; set; }
    }
}

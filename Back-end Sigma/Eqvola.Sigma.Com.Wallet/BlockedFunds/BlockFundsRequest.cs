﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class BlockFundsRequest : Request
    {
        public override string QueueName => "wallet";

        public override string RequestName => "block-funds";
        
        public long userId { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public long orderId { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class BlockedFundsResponse : WalletResponse
    {
        public List<Models.BlockedFunds> funds { get; set; }

        public BlockedFundsResponse()
        {
            funds = new List<Models.BlockedFunds>();
        }
    }
}

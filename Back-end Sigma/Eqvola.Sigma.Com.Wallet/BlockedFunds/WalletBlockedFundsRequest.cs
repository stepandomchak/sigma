﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class WalletBlockedFundsRequest : Request
    {
        public override string QueueName => "wallet";

        public override string RequestName => "get-blocked";

        public long userId { get; set; }
        public string currencyCode { get; set; }

    }
}

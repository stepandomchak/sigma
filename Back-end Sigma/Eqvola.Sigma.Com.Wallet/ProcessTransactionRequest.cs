﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class ProcessTransactionRequest : Request
    {
        public override string QueueName => "wallet";
        public override string RequestName => "process-transaction";

        public Transaction transaction { get; set; }
    }
}

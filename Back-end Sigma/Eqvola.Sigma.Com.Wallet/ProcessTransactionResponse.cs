﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Wallet
{
    public class ProcessTransactionResponse : Response
    {         
        public Result result { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Eqvola.Sigma.Com.Email
{
    public class SendEmailRequest<T> : Request
    {
        public override string QueueName => "email";
        public override string RequestName => "send";

        public string TemplateName { get; set; }
        public string SendTo { get; set; }
        public string Subject { get; set; }
        public Dictionary<string, string> Parametrs { get; set; }
    }

    public class EmailValues
    {
        public string QueryParameters { get; set; }

        public Dictionary<string, string> Parametr = new Dictionary<string, string>();
    }

}

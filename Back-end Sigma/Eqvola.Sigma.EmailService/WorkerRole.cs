using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Email;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using System.IO;
using Newtonsoft.Json;

namespace Eqvola.Sigma.EmailService
{
    public class WorkerRole : ServiceBusRole<Service.Email.Service>
    {
        protected override IEnumerable<string> queues => new string[] { "email" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "email":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        {
                            "send",
                            SendEmail
                        }
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }

        private async Task<Response> SendEmail(BrokeredMessage message)
        {
            Trace.WriteLine($"Send email request received (SID: {message.SessionId})");
            SendEmailRequest<EmailValues> request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<SendEmailRequest<EmailValues>>(json);
            }

            await service.SendEmailAsync(request);

            return null;
        }
    }
}

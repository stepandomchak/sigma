﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Repositories
{
    class SellOrderRepository : Repository<SellOrder, long>
    {
        public SellOrderRepository(Context context) : base(context) { }

    }
}

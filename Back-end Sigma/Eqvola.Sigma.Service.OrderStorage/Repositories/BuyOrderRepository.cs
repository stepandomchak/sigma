﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Repositories
{
    public class BuyOrderRepository : Repository<BuyOrder, long>
    {
        public BuyOrderRepository(Context context) : base(context) { }
        
    }
}

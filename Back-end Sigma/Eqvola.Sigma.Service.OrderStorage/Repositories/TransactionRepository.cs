﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Repositories
{
    public class TransactionRepository : Repository<Transaction, long>
    {
        public TransactionRepository(Context context) : base(context) { }


        public async Task<List<Transaction>> GetByPaginationWithFilters(long userId = 0, int count = 20, int margin = 0, string market = null)
        {
            return await Task.Run(() =>
            {
                var request = BuildRequest(userId, market);

                if (margin < 0)
                    margin = 0;

                if (count <= 0)
                    count = 20;
                
                request = request.OrderBy(o => o.Id).Skip(margin).Take(count);

                return request.ToList();
            });
        }

        public IQueryable<Transaction> BuildRequest(long userId, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            var request = Entities.AsQueryable();
            
            if (userId > 0)
                request = request.Where(o => (o.BuyOrder.UserId == userId || o.SellOrder.UserId == userId));

            if (!string.IsNullOrEmpty(market))
                request = request.Where(o => (o.BuyOrder.Currency == market || o.SellOrder.Currency == market));

            if (dateFrom != null)
            {
                request = request.Where(o => (o.Date_Time > dateFrom));
            }
            if (dateTo != null)
            {
                request = request.Where(o => (o.Date_Time < dateTo));
            }

            if (!string.IsNullOrEmpty(search))
            {
                request = request.Where(o => (o.Price.ToString().Contains(search) || o.Amount.ToString().Contains(search) ||
                    o.BuyOrder.Currency.Contains(search) || o.BuyOrder.TargetCurrency.Contains(search)));
            }
            return request;
        }

        public async Task<int> GetByPaginationWithFiltersCount(long userId, string market = null)
        {
            return await Task.Run(() =>
            {
                return BuildRequest(userId, market).Count();
            });
        }
    }
}

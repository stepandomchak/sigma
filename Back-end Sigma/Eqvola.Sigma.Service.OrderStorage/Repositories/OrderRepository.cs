﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Repositories
{
    public class OrderRepository : Repository<Order, long>
    {
        public OrderRepository(Context context) : base(context) { }


        public async Task<List<Order>> GetOrdersByPagination(long userId, int status, int count, int margin, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            return await Task.Run(() =>
            {
                var request = BuildRequest(userId, status, market, dateFrom, dateTo, search);

                if (margin < 0)
                    margin = 0;

                if (count <= 0)
                    count = 20;

                request = request.OrderByDescending(o => o.Id).Skip(margin).Take(count);

                return request.ToList();
            });
        }

        public IQueryable<Order> BuildRequest(long userId, int status, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            var request = Entities.AsQueryable();
            if(userId > 0)
                request = request.Where(o => o.UserId == userId);

            if (status > 0)
                request = request.Where(o => (o.Status & status) > 0);

            if (!string.IsNullOrEmpty(market))
                request = request.Where(o => (o.Currency == market || o.TargetCurrency == market));

            if (dateFrom != null)
            {
                request = request.Where(o => (o.CreationTime > dateFrom));
            }
            if (dateTo != null)
            {
                request = request.Where(o => (o.CreationTime < dateTo));
            }

            if (!string.IsNullOrEmpty(search))
            {
                request = request.Where(o => (o.Comment.Contains(search) || o.Currency.Contains(search) ||
                    o.TargetCurrency.Contains(search)));
            }
            return request;
        }

        public async Task<int> GetUserOrdersCount(long userId, int status, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            return await Task.Run(() =>
            {
                return BuildRequest(userId, status, market, dateFrom, dateTo, search).Count();
            });
        }

    }
}

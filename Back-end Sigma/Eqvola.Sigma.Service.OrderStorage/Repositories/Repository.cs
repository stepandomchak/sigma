﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Repositories
{
    public class Repository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        private readonly Context context;
        private IDbSet<TEntity> entities;

        protected IDbSet<TEntity> Entities
        {
            get
            {
                if (entities == null)
                    entities = context.Set<TEntity>();
                return entities;
            }
        }

        public Repository(Context context)
        {
            this.context = context;
        }

        public async Task<ICollection<TEntity>> All()
        {
            return await this.Entities.ToListAsync();
        }

        public async Task<TEntity> GetById(TKey id)
        {
            return await Task.Run(() => {
                return this.Entities.Find(id);
            });
        }

        public virtual async Task<RepositoryResponse<TEntity>> Add(TEntity entity)
        {
            this.Entities.Add(entity);
            try
            {
                await context.SaveChangesAsync();

                return new RepositoryResponse<TEntity>()
                {
                    entity = entity,
                    message = ""
                }; ;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                RepositoryResponse<TEntity> resp = new RepositoryResponse<TEntity>()
                {
                    entity = entity,
                    message = ex.InnerException.InnerException.Message
                };
                return resp;
            }
        }

        public async Task<RepositoryResponse<TEntity>> Update(TEntity entity)
        {
            try
            {
                if (!this.Entities.Local.Any(e => e == entity))
                    this.Entities.Attach(entity);

                await context.SaveChangesAsync();

                return new RepositoryResponse<TEntity>()
                {
                    entity = entity,
                    message = ""
                };
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                RepositoryResponse<TEntity> resp = new RepositoryResponse<TEntity>()
                {
                    entity = entity,
                    message = ex.InnerException.InnerException.Message
                };
                return resp;
            }
        }

        public async Task<IQueryable<TEntity>> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var res = this.Entities.Where(predicate);
                    return res;
                }
                catch (Exception ex) { return null; }
            });
        }

        public async Task<TEntity> Delete(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Deleted;
            await context.SaveChangesAsync();
            return entity;
        }
    }
    public class RepositoryResponse<TEntity> where TEntity : BaseEntity
    {
        public TEntity entity;
        public string message;
    }
    public class Repository<TEntity> where TEntity : BaseEntity
    {
        private readonly Context context;
        private IDbSet<TEntity> entities;

        protected IDbSet<TEntity> Entities
        {
            get
            {
                if (entities == null)
                    entities = context.Set<TEntity>();
                return entities;
            }
        }

        public Repository(Context context)
        {
            this.context = context;
        }

        public async Task<ICollection<TEntity>> All()
        {
            return await this.Entities.ToListAsync();
        }

        public async Task<TEntity> Add(TEntity entity)
        {
            this.Entities.Add(entity);
            await context.SaveChangesAsync();

            return entity;
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            if (!this.Entities.Local.Any(e => e == entity))
                this.Entities.Attach(entity);

            await context.SaveChangesAsync();

            return entity;
        }

        public async Task<IQueryable<TEntity>> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return await Task.Run(() =>
            {
                return this.Entities.Where(predicate);
            });
        }

        public async Task<TEntity> Delete(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Deleted;
            await context.SaveChangesAsync();
            return entity;
        }
    }
}


﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using Eqvola.Sigma.Service.OrderStorage.Repositories;
using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage
{
    public class Service : IDisposable
    {
        private Context db;

        public Service()
        {
            db = new Context(CloudConfigurationManager.GetSetting("OrderDbContext"));
        }

        public void Dispose()
        {
            db.Dispose();
        }

        #region Order
        public async Task<Order> GetOrderById(long id)
        {
            return (await new OrderRepository(db).Where(m => m.Id == id)).FirstOrDefault();
        }

        public async Task<IEnumerable<Order>> GetOrders()
        {
            return await new OrderRepository(db).All();
        }

        public async Task<IEnumerable<Order>> GetOrdersByUserStatusAndMarket(long userId, int status, string market)
        {
            return (await new OrderRepository(db).Where(m => (m.Currency == market || m.TargetCurrency == market) && m.UserId == userId && (m.Status & status) > 0)).ToList();
        }
        
        public async Task<IEnumerable<Order>> GetOrdersByUser(long userId, int count = 20, int margin = 0)
        {
            if (margin > 0)
                return (await new OrderRepository(db).Where(m => m.UserId == userId && m.Id >= margin)).Take(count);
            else
                return (await new OrderRepository(db).Where(m => m.UserId == userId));
        }

        public async Task<IEnumerable<Order>> GetOrdersByFilters(int status, int count, int margin)
        {
            if (status > 0)
                return (await new OrderRepository(db).Where(m => (m.Status & status) > 0 && m.Id >= margin)).Take(count);
            else
                return (await new OrderRepository(db).Where(m => m.Id >= margin)).Take(count);
        }

        public async Task<IEnumerable<Order>> GetOrdersByWl(long wlId)
        {
            return (await new OrderRepository(db).Where(m => m.WLId == wlId));
        }

        public async Task<IEnumerable<Order>> GetOrdersByStatus(int status)
        {
            return (await new OrderRepository(db).Where(m => (m.Status & status) > 0));
        }

        public async Task<List<Order>> GetUserOrders(long userId = 0, int count = 20, int margin = 0, int status = 0, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            return (await new OrderRepository(db).GetOrdersByPagination(userId, status, count, margin, market, dateFrom, dateTo, search)).ToList();
        }

        public async Task<int> GetUserOrdersCount(long userId = 0, int status = 0, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            return (await (new OrderRepository(db)).GetUserOrdersCount(userId, status, market, dateFrom, dateTo, search));
        }

        public async Task<IEnumerable<Order>> GetOrdersByCurrencyPair(int status, string currency, string targetCurrency, int ordersCount)
        {
            IEnumerable<Order> orders = null;
            if (status != 0)
            {
                if (ordersCount != 0)
                    orders = ((await new OrderRepository(db).Where(m => m.Currency == currency && m.TargetCurrency == targetCurrency
                                                                    && (m.Status & status) > 0)).OrderBy(m => m.Price)).Take(ordersCount);
                else
                    orders = (await new OrderRepository(db).Where(m => m.Currency == currency && m.TargetCurrency == targetCurrency
                                                                    && (m.Status & status) > 0));
            }
            else
            {
                orders = (await new OrderRepository(db).Where(m => m.Currency == currency && m.TargetCurrency == targetCurrency));
            }
            return orders;
        }

        public async Task<IEnumerable<Order>> GetOrdersByIds(IEnumerable<long> ids)
        {
            return (await (new OrderRepository(db)).Where(m => ids.Contains(m.Id))).ToList();
        }

        public async Task<Order> GetFirstOrder(string currency, string targetCurrency)
        {
            return (await (new OrderRepository(db)).Where(m => m.Currency == currency && m.TargetCurrency == targetCurrency)).OrderBy(o => o.CreationTime).FirstOrDefault();
        }

        public async Task<IQueryable<Order>> GetSuitOrders(Order order)
        {
            return await new OrderRepository(db).Where(m => m.Currency == order.TargetCurrency && m.TargetCurrency == order.Currency &&
                                                               m.WLId == order.WLId && (m.Status & 3) > 0 &&
                                                               m.Price <= (1 / order.Price));
        }

        public async Task<RepositoryResponse<Order>> CreateOrder(Order order)
        {
            return await new OrderRepository(db).Add(order);
        }      

        public async Task<RepositoryResponse<Order>> UpdateOrder(Order order)
        {
            return await new OrderRepository(db).Update(order);
        }

        public async Task<Order> DeleteOrder(Order order)
        {
            return await new OrderRepository(db).Delete(order);
        }

        #endregion

        #region Transaction
        public async Task<RepositoryResponse<Transaction>> CreateTransaction(Transaction transaction)
        {
            return await new TransactionRepository(db).Add(transaction);
        }

        public async Task<IEnumerable<Transaction>> GetTransactions(long userId = 0, int count = 20, int margin = 0, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            return await new TransactionRepository(db).GetByPaginationWithFilters(userId: userId, count: count, margin: margin, market: market);
        }
        public async Task<int> GetTransactionsCount(long userId = 0, int count = 20, int margin = 0, string market = null, DateTime? dateFrom = null, DateTime? dateTo = null, string search = null)
        {
            return await new TransactionRepository(db).GetByPaginationWithFiltersCount(userId: userId, market: market);
        }

        public async Task<Transaction> GetTransactionById(long id)
        {
            return (await new TransactionRepository(db).Where(t => t.Id == id)).FirstOrDefault();
        }

        public async Task<IEnumerable<Transaction>> GetTransactionByOrderId(long orderId)
        {
            return (await new TransactionRepository(db).Where(t => t.BuyOrder.Id == orderId || t.SellOrder.Id == orderId)).ToList();
        }
        #endregion

    }
}

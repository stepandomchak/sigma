﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Models
{    
    public class Order : BaseEntity<long>
    {     
        [Required]
        [MaxLength(128)]        
        public string Currency{ get; set; }

        [Required]
        [MaxLength(128)]
        public string TargetCurrency { get; set; }

        [Required]
        [Column(TypeName = "DateTime")]
        public DateTime? CreationTime { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime? ExpiresTime { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime? CloseTime { get; set; }

        [Required]        
        public double Price { get; set; }

        [Required]
        public double Amount { get; set; }

        [Required]
        public long UserId { get; set; }

        [Required]
        public double Rest { get; set; }
        
        [Required]
        public double AverageCost { get; set; }
        
        [Required]
        public int Status { get; set; }

        [Required]
        public string Type { get; set; }

        public double StopLoss { get; set; }

        public double TakeProfit { get; set; }

        public double Stop { get; set; }
        
        public double Limit { get; set; }
        
        [Required]
        public long WLId { get; set; }

        [MaxLength(128)]
        public string Comment { get; set; }
        
        public double Commission { get; set; }
    }
}

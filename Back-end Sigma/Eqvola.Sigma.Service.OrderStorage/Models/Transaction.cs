﻿using Eqvola.Sigma.Service.OrderStorage.Repositories;
using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Models
{
    public class Transaction : BaseEntity<long>
    {        
        [Required]
        public virtual Order SellOrder { get; set; }
       
        [Required]
        public virtual Order BuyOrder { get; set; }

        [Required]
        public long Taker { get; set; }

        [Required]
        [Column(TypeName = "DateTime")]
        public DateTime Date_Time { get; set; }
        
        [Required]
        public double Price { get; set; }

        [Required]
        public double Amount { get; set; }

        public Order getTaker()
        {
            if(Taker == SellOrder.Id)
            {
                return SellOrder;
            }
            else
            {
                return BuyOrder;
            }
        }

        public Order getMaker()
        {
            if (Taker == SellOrder.Id)
            {
                return BuyOrder;
            }
            else
            {
                return SellOrder;
            }
        }

        public object GetUserOrder(long userId)
        {
            Order order = null;
            if (this.BuyOrder.UserId == userId)
            {
                order = BuyOrder;
            }
            else
            {
                order = SellOrder;
            }
            return new
            {
                orderId = order.Id,
                currency = order.Currency,
                targetCurrency = order.TargetCurrency,
                expiresTime = order.ExpiresTime,
                creationTime = order.CreationTime,
                price = order.Price,
                amount = order.Amount,
                takeProfit = order.TakeProfit,
                stopLoss = order.StopLoss,
                stop = order.Stop,
                limit = order.Limit,
                orderType = order.Type,
                sum = Math.Round(order.Price * order.Amount, 15),
                commission = Math.Round((order.Price * order.Amount / 100) * order.Commission, 15),
                clearSum = Math.Round((order.Price * order.Amount) - ((order.Price * order.Amount / 100)) * order.Commission, 15)
            };
        }
    }
}

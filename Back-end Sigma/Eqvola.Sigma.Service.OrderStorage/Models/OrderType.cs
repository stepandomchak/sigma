﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Models
{
    public enum OrderType
    {
        Buy = 1,
        Sell = 2
    }
}

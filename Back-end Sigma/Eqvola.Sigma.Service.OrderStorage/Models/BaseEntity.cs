﻿using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.Service.OrderStorage.Models
{
    public abstract class BaseEntity
    {
    }

    public abstract class BaseEntity<TKey> : BaseEntity
    {
        [Key]
        public TKey Id { get; set; }
    }
}

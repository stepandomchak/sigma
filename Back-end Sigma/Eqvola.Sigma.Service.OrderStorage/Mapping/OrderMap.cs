﻿using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.OrderStorage.Mapping
{
    public class OrderMap : EntityTypeConfiguration<Order>
    {
    }
    public class BuyOrderMap : EntityTypeConfiguration<BuyOrder>
    {
    }
    public class SellOrderMap : EntityTypeConfiguration<SellOrder>
    {
    }
}

namespace Eqvola.Sigma.Service.OrderStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Currency = c.String(nullable: false, maxLength: 128),
                        TargetCurrency = c.String(nullable: false, maxLength: 128),
                        CreationTime = c.DateTime(nullable: false),
                        ExpiresTime = c.DateTime(),
                        Price = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        UserId = c.Long(nullable: false),
                        Rest = c.Double(nullable: false),
                        AverageCost = c.Double(nullable: false),
                        Status = c.Int(nullable: false),
                        Type = c.String(nullable: false),
                        StopLoss = c.Double(nullable: false),
                        TakeProfit = c.Double(nullable: false),
                        WLId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Taker = c.Int(nullable: false),
                        Date_Time = c.DateTime(nullable: false),
                        Price = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        BuyOrder_Id = c.Long(nullable: false),
                        SellOrder_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.BuyOrder_Id)
                .Index(t => t.SellOrder_Id);
            
            CreateTable(
                "dbo.BuyOrder",
                c => new
                    {
                        Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.SellOrder",
                c => new
                    {
                        Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SellOrder", "Id", "dbo.Orders");
            DropForeignKey("dbo.BuyOrder", "Id", "dbo.Orders");
            DropIndex("dbo.SellOrder", new[] { "Id" });
            DropIndex("dbo.BuyOrder", new[] { "Id" });
            DropIndex("dbo.Transactions", new[] { "SellOrder_Id" });
            DropIndex("dbo.Transactions", new[] { "BuyOrder_Id" });
            DropTable("dbo.SellOrder");
            DropTable("dbo.BuyOrder");
            DropTable("dbo.Transactions");
            DropTable("dbo.Orders");
        }
    }
}

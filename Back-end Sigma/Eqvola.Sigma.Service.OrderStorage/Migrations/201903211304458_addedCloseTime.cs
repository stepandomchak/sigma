namespace Eqvola.Sigma.Service.OrderStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCloseTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CloseTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "CloseTime");
        }
    }
}

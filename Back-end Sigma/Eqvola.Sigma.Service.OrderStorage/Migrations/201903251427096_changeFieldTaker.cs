namespace Eqvola.Sigma.Service.OrderStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeFieldTaker : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "Taker", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "Taker", c => c.Int(nullable: false));
        }
    }
}

namespace Eqvola.Sigma.Service.OrderStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Comment", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Comment");
        }
    }
}

namespace Eqvola.Sigma.Service.OrderStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StopLimit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Stop", c => c.Double(nullable: false));
            AddColumn("dbo.Orders", "Limit", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Limit");
            DropColumn("dbo.Orders", "Stop");
        }
    }
}

namespace Eqvola.Sigma.Service.OrderStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCommission : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Commission", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Commission");
        }
    }
}

﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Blob
{
    public class Service
    {
        /// <summary>
        /// container = "avatars", "attachments", "documents"
        /// </summary>      
        /// <returns></returns>
        public async Task<string> UploadFile(string blobReference, string mimeType, byte[] file, string container)
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;
            string blobConnectionString = CloudConfigurationManager.GetSetting("AzureBlobConnectionString");

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(blobConnectionString, out storageAccount))
            {
                // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
               
                // Create a container.
                #region Create container
                //var blobContainer = cloudBlobClient.GetContainerReference("attachments");

                //// Create the container and set the permission  
                //if (blobContainer.CreateIfNotExists())
                //{
                //    blobContainer.SetPermissions(
                //        new BlobContainerPermissions
                //        {
                //            PublicAccess = BlobContainerPublicAccessType.Blob
                //        }
                //    );
                //}
                #endregion

                cloudBlobContainer = cloudBlobClient.GetContainerReference(container);
                // Get the reference to the block blob from the container
                CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(blobReference);
                blockBlob.Properties.ContentType = mimeType;

                var blobUrl = blockBlob.Uri.AbsoluteUri;
                try
                {
                    await blockBlob.UploadFromByteArrayAsync(file, 0, file.Length);
                }
                catch (Exception ex)
                {
                    string str = "";
                }

                return blobUrl;
            }

            return "";
        }

        public async Task<string> DeleteFile(string blobReference, string container)
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;
            string blobConnectionString = CloudConfigurationManager.GetSetting("AzureBlobConnectionString");

            if (CloudStorageAccount.TryParse(blobConnectionString, out storageAccount))
            {
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

                CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(blobReference);

                var blobUrl = blockBlob.Uri.AbsoluteUri;
                try
                {
                    await blockBlob.DeleteIfExistsAsync();
                }
                catch (Exception ex)
                {
                    string str = "";
                }

                return blobUrl;
            }

            return "";
        }

    }
}

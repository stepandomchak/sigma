﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.User
{
    public class SetPassword : StorageHandler<SetPasswordRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new SetPasswordResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if ((!string.IsNullOrEmpty(request.email)) && !string.IsNullOrEmpty(request.newPassword))
            {
                var response = await service.UpdateUserPassword(request.email, request.phone_number, request.code, request.newPassword, request.isHash);

                if (response != null)
                {
                    return new SetPasswordResponse()
                    {
                        result = Result.Ok
                    };
                }
                return new SetPasswordResponse()
                {
                    result = Result.InvalidRequest,
                    message = "Failed to change password"
                };
            }

            return new SetPasswordResponse()
            {
                result = Result.InvalidRequest,
                message = "Invalid params"
            };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class UpdateTwoFactorState : StorageHandler<UpdateTwoFactorStateRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new UpdateTwoFactorStateResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            
            if (string.IsNullOrEmpty(request.email))
            {
                return new UpdateTwoFactorStateResponse()
                {
                    result = Result.InvalidRequest
                };
            }
            Trace.WriteLine("2FA update storage");
            var user = (await service.GetUserByEmail(request.email));
            if(user == null)
            {
                return new UpdateTwoFactorStateResponse()
                {
                    result = Result.NotFound
                };
            }
            switch (request.isEnabled)
            {
                /*case Com.Storage.TwoFactorAction.Enable:
                    user.IsTwoFactorEnabled = false;
                    user.TwoFactorSecretKey = request.secretkey;
                    break;
                case Com.Storage.TwoFactorAction.Accept:
                    user.IsTwoFactorEnabled = true;
                    break;
                case Com.Storage.TwoFactorAction.Disable:
                    user.IsTwoFactorEnabled = false;
                    user.TwoFactorSecretKey = "";
                    break;*/
            }

            await service.UpdateUser(user);

            Trace.WriteLine("2FA update storage response");
            return new UpdateTwoFactorStateResponse()
            {
                result = Result.Ok
            };
        }

    }
}

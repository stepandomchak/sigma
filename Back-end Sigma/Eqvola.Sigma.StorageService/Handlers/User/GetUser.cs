﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class GetUser : StorageHandler<GetUserRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new UserResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            Com.Storage.Models.User user = null;

            if (!string.IsNullOrEmpty(request.uid))
            {
                user = (await service.GetUserByEmail(request.uid))?.ToComObject();
            }

            return new UserResponse()
            {
                result = user == null ? Result.NotFound : Result.Ok,
                entity = user
            };
        }

    }
}

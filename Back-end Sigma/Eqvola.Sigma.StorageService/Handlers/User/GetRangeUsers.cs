﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class GetRangeUsers : StorageHandler<GetRangeUsersRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new UsersResponse()
            {
                users = new List<Eqvola.Sigma.Com.Storage.Models.User>()
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            var users = (await service.GetRangeUsersByEmails(request.emailUsers)).Select(u => u.ToComObject()).ToList();
            

            return new UsersResponse()
            {
                users = users
            };
        }

    }
}

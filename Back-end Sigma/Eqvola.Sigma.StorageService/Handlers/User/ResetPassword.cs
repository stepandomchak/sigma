﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.User
{
    public class ResetPassword : StorageHandler<ResetPasswordRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new ResetPasswordResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var result = new ResetPasswordResponse()
            {
                result = Result.InvalidRequest
            };

            if ((!string.IsNullOrEmpty(request.code)) && (!string.IsNullOrEmpty(request.email)) && (!string.IsNullOrEmpty(request.phone_number)))
            {
                var user = await service.GetUserByEmail(request.email);

                if (user != null)
                {

                    string phone = user.Phone_code + user.Phone_number;

                    if (!phone.Equals(request.phone_number))
                    {
                        result.message = "Wrong email or phone number";
                        return result;
                    }

                    var saveCodeResponse = await service.UpdateUserResetCode(request.code, user);

                    if (saveCodeResponse == null)
                    {
                        return new ResetPasswordResponse()
                        {
                            result = Result.Ok
                        };
                    }
                    else
                    {
                        result.message = saveCodeResponse;
                        return result;
                    }
                }
                result.message = "Wrong email or phone number";
                return result;
            }
            result.message = "Wrong params";
            return result;
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.User
{
    public class CheckNumber : StorageHandler<CheckNumberRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CheckNumberResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if ((string.IsNullOrEmpty(request.phone_code)) || (string.IsNullOrEmpty(request.phone_number)))
            {
                return new CheckNumberResponse()
                {
                    result = Result.Exception,
                    entity = null
                };
            }

            var phone = (await service.CheckNumber(request.phone_code, request.phone_number));

            return new CheckNumberResponse()
            {
                result = phone == false ? Result.Ok : Result.NotFound,
                entity = null
            };

        }

    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Settings
{
    public class UpdateSettings : StorageHandler<UpdateSettingsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new SettingsResponse();
        }

        protected override async Task<Response> ProccessAsync()
        {
            var settings = await service.UpdateSettings(request.settings);
            return new SettingsResponse()
            {
                settings = settings.Select(s => s.ToComObject()).ToList()
            };
        }
        

    }
}

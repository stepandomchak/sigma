﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers
{
    public abstract class StorageHandler<TRequest> : BaseHandler<TRequest, Service.Storage.Service> where TRequest : Request
    {
        public override void Dispose()
        {
            //service.Dispose();
        }

        public override void InitService()
        {
            service = new Service.Storage.Service(true);
        }

        public override async Task<Response> ProccessMessage(BrokeredMessage message)
        {
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TRequest>(json);
            }
            try
            {
                return await ProccessAsync();
            }
            catch(DataException ex)
            {
                return CreateFailResponse();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        protected abstract Response CreateFailResponse();
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Department
{
    class ActivateDepartment : StorageHandler<ActivateDepartmentRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            Eqvola.Sigma.Service.Storage.Models.Department dep = await service.GetDepartmentByName(request.name);
            if (dep == null)
                return new DepartmentResponse()
                {
                    result = Result.NonexistentDepartment,
                    entity = null
                };

            if (dep.Status == (int)DepartmentStatus.Active)
                return new DepartmentResponse()
                {
                    result = Result.InvalidRequest,
                    entity = null
                };
          
            dep.Status = (int)DepartmentStatus.Active;

            var department = (await service.UpdateDepartment(dep));
            return new DepartmentResponse()
            {
                result = department != null ? Result.Ok : Result.NotFound,
                entity = department.entity.ToComObject()
            };
        }

        protected override Response CreateFailResponse()
        {
            return new DepartmentResponse()
            {
                result = Result.InvalidRequest
            };
        }

    }
}

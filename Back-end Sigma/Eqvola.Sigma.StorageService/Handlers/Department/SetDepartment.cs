﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Service.Storage.Models;
using Eqvola.Sigma.Service;
using Eqvola.Sigma.StorageService.Converters;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Department
{
    public class SetDepartment : StorageHandler<SetDepartmentRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            if (string.IsNullOrEmpty(request.name) || char.IsDigit(request.name[0]) == true)
            {
                return new DepartmentResponse()
                {
                    result = Result.InvalidRequest,
                    entity = null
                };
            }
            Response resp = null;
            if (!string.IsNullOrEmpty(request.oldname))
                resp = await UpdateDepartment();
            else
                resp = await CreateDepartment();

            return resp;
        }

        protected async Task<DepartmentResponse> CreateDepartment()
        {
            Eqvola.Sigma.Service.Storage.Models.Department dep = new Service.Storage.Models.Department()
            {
                Name = request.name,
                Permissions = await service.MapPermissions(request.permissions)
            };
            dep.Status = 1;
            var result = await service.CreateDepartment(dep);
            if (result != null)
            {
                if (result.message.Contains("unique"))
                    return new DepartmentResponse()
                    {
                        result = Result.ExistenDepartment,
                        entity = null
                    };
                else
                    return new DepartmentResponse()
                    {
                        result = Result.Ok,
                        entity = result.entity.ToComObject()
                    };
            }
            else
                return new DepartmentResponse()
                {
                    result = Result.NotFound,
                    entity = null
                };
        }

        protected async Task<DepartmentResponse> UpdateDepartment()
        {
            Eqvola.Sigma.Service.Storage.Models.Department dep = await service.GetDepartmentByName(request.oldname);

            if(dep == null)
                return new DepartmentResponse()
                {
                    result = Result.NotFound,
                    entity = null
                };

            dep.Name = request.name;

            var rem = dep.Permissions.Where(p => !request.permissions.Contains(p.Name)).ToList();
            foreach (var p in rem)
                dep.Permissions.Remove(p);
            var exists = dep.Permissions.Select(p => p.Name);
            var add = await service.MapPermissions(request.permissions.Where(p => !exists.Contains(p)));
            foreach (var p in add)
                dep.Permissions.Add(p);

            var result = (await service.UpdateDepartment(dep));
            if (result != null)
            {
                if (result.message.Contains("unique"))
                    return new DepartmentResponse()
                    {
                        result = Result.ExistenDepartment,
                        entity = null
                    };
                else
                    return new DepartmentResponse()
                    {
                        result = Result.Ok,
                        entity = result.entity.ToComObject()
                    };
            }
            else
                return new DepartmentResponse()
                {
                    result = Result.Exception,
                    entity = null
                };
        }

        protected override Response CreateFailResponse()
        {
            return new DepartmentResponse()
            {
                result = Result.InvalidRequest
            };
        }

    }
}

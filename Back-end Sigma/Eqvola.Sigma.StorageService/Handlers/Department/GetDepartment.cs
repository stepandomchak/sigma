﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Department
{
    public class GetDepartment : StorageHandler<GetDepartmentRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            Response resp = null;
            if (!string.IsNullOrEmpty(request.name))
                resp = await GetDepartmentByName(request.name);
            else
            {
                resp = await GetAllDepartments(request.status);
            }
            return resp;
        }

        protected async Task<DepartmentResponse> GetDepartmentByName(string name)
        {
            var department = (await service.GetDepartmentByName(request.name))?.ToComObject();
            return new DepartmentResponse()
            {
                result = department != null ? Result.Ok : Result.NotFound,
                entity = department
            };
        }

        protected async Task<DepartmentsResponse> GetAllDepartments(int status)
        {
            return new DepartmentsResponse()
            {
                result = Result.Ok,
                entity = (await service.GetAllDepartments(status)).Select(m => m.ToComObject()).ToList()
            };
        }

        protected override Response CreateFailResponse()
        {
            return new DepartmentResponse()
            {
                result = Result.InvalidRequest
            };
        }
    }
}

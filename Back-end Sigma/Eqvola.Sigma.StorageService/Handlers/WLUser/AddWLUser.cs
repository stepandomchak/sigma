﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.User
{
    public class AddWLUser : StorageHandler<AddWLUserRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new StorageResponse<WLUser>()
            {
                result = Result.InvalidRequest
            };
        }


        private long getLongFromBitArray(BitArray bitArray)
        {
            if (bitArray.Length > 64)
                throw new ArgumentException("Argument length shall be at most 32 bits.");

            Byte[] array = new Byte[8];
            bitArray.CopyTo(array, 0);

            return BitConverter.ToInt64(array, 0);
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (string.IsNullOrEmpty(request.wlName))
            {
                return new CheckNumberResponse()
                {
                    result = Result.Exception,
                    entity = null
                };
            }
            var WL = await service.GetWhiteLabelByName(request.wlName);

            WLUser wlUser = new WLUser() { UserId = request.userId, Wl = WL };

            BitArray userIdBitArray = new BitArray(BitConverter.GetBytes(request.userId));
            BitArray wlIdBitArray = new BitArray(BitConverter.GetBytes(WL.Id));
            BitArray result = new BitArray(64);

            for (int i = 0, wl = 0, uid = 0; i < 64; i++)
            {
                if (i >= 22 && i < 42)
                {
                    result[i] = wlIdBitArray[wl];
                    wl++;
                }
                else
                {
                    result[i] = userIdBitArray[wl];
                    uid++;
                }
            }

            wlUser.Id = getLongFromBitArray(result);

            var wlUserResponse = await service.AddWlUser(wlUser);

            return new StorageResponse<WLUser>()
            {
                result = wlUserResponse == null? Result.Ok : Result.InvalidRequest
            };

        }

    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers
{
    public class GetWLUser : StorageHandler<GetWLUserRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new StorageResponse<Com.Storage.Models.WLUser>()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.userId == 0)
            {
                return new CheckNumberResponse()
                {
                    result = Result.Exception,
                    entity = null
                };
            }

            Com.Storage.Models.WLUser wlUserResponse = (await service.GetWlUserByUser(request.userId))?.ToComObject();
             
            return new StorageResponse<Com.Storage.Models.WLUser>()
            {
                entity = wlUserResponse,
                result = wlUserResponse == null ? Result.Ok : Result.InvalidRequest
            };
        }

    }
}


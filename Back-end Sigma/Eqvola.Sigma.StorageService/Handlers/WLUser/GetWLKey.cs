﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers
{
    public class GetWLKey : StorageHandler<GetWLApiKeyRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new StorageResponse<Com.Storage.Models.WLApiKey>()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            Com.Storage.Models.WLApiKey wlApiKey = null;

            if (request.userId != 0)
            {
                var wl = (await service.GetWhiteLabelByUser(request.userId));
                wlApiKey = (await service.GetWlApiKey(wl.Id))?.ToComObject();

                if(wlApiKey != null)
                    return new StorageResponse<Com.Storage.Models.WLApiKey>()
                    {
                        entity = wlApiKey,
                        result = Result.Ok
                    };
            }
                        
            return new StorageResponse<Com.Storage.Models.WLApiKey>()
            {                
                result = Result.NotFound                
            };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Avatar
{
    
    public class UploadAvatar : StorageHandler<UploadAvatarRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new StorageResponse<string>()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var manager = await service.GetManagerByUId(request.email);
            if (manager == null)
                return new StorageResponse<string>()
                {
                    result = Result.NotFound
                };

            if (!string.IsNullOrEmpty(request.url))
            {
                manager.User.AvatarUrl = request.url;
                await service.UpdateUser(manager.User);
            }            
            return new StorageResponse<string>()
            {
                result = Result.Ok,
                entity = request.url
            };
        }
    }
    
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class GetManager : StorageHandler<GetManagerRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new ManagerResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            Com.Storage.Models.Manager manager = null;

            if (!string.IsNullOrEmpty(request.uid) && !string.IsNullOrEmpty(request.password))
            {
                manager = (await service.GetManagerByUidAndPassword(request.uid, request.password))?.ToComObject();
            }
            else if (!string.IsNullOrEmpty(request.uid))
            {
                manager = (await service.GetManagerByUId(request.uid))?.ToComObject();
            }
            else
            {
                return await GetAllManagers(request.status);                                
            }

            return new ManagerResponse()
            {
                result = manager == null ? Result.NotFound : Result.Ok,
                entity = manager
            };
        }

        protected async Task<ManagersResponse> GetAllManagers(int status)
        {
            return new ManagersResponse()
            {
                result = Result.Ok,
                entity = (await service.GetAllManagers(status)).Select(m => m.ToComObject()).ToList()
            };
        }

    }
}

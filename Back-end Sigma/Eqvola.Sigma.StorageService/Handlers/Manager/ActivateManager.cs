﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class ActivateManager : StorageHandler<ActivateManagerRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            Eqvola.Sigma.Service.Storage.Models.Manager manager = await service.GetManagerByUId(request.email);
            if (manager == null)
            {
                return new ManagerResponse()
                {
                    result = Result.NotFound,
                    entity = null
                };
            }

            if (manager.Status == (int)ManagerStatus.Active)
                return new ManagerResponse()
                {
                    result = Result.InvalidRequest,
                    entity = null
                };

            Eqvola.Sigma.Service.Storage.Models.Department dep = await service.GetDepartmentByName(request.department);
            if (dep.Status == (int)Com.Storage.Models.DepartmentStatus.Deleted)
            {
                return new ManagerResponse()
                {
                    result = Result.Exception,
                    entity = null
                };
            }
            manager.Department = dep;
            var rem = manager.Permissions.Where(p => !request.permissions.Contains(p.Name)).ToList();
            foreach (var p in rem)
                manager.Permissions.Remove(p);
            var exists = manager.Permissions.Select(p => p.Name);
            var add = await service.MapPermissions(request.permissions.Where(p => !exists.Contains(p)));
            foreach (var p in add)
                manager.Permissions.Add(p);

            manager.Status = (int)ManagerStatus.Active;
            manager.FiringReason = "";

            var resultManager = (await service.UpdateManager(manager));
            return new ManagerResponse()
            {
                result = resultManager != null ? Result.Ok : Result.NotFound,
                entity = resultManager.entity.ToComObject()
            };
        }

        protected override Response CreateFailResponse()
        {
            return new ManagerResponse()
            {
                result = Result.InvalidRequest
            };
        }
    }
}
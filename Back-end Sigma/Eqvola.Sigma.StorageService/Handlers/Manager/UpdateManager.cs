﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class UpdateManager : StorageHandler<UpdateManagerRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new ManagerResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            var manager = await service.GetManagerByUId(request.email);
            if (manager != null)
            {
                if (manager.Department.Name != request.department)
                {
                    var dep = await service.GetDepartmentByName(request.department);

                    if (dep == null)
                        return new ManagerResponse()
                        {
                            result = Result.NonexistentDepartment,
                            entity = manager.ToComObject()
                        };
                    if (dep.Status == (int)DepartmentStatus.Deleted)
                    {
                        manager.Status = (int)ManagerStatus.Active;
                        manager.FiringReason = "Department Was Deleted";
                    }
                    else
                    {
                        manager.Status = (int)ManagerStatus.Active;
                        manager.FiringReason = "";
                    }

                    manager.Department = dep;
                }
                manager.Rank = request.rank;
                manager.User.City = request.address_city;
                manager.User.Country = request.country;
                manager.User.FirstName = request.firstname;
                manager.User.LastName = request.lastname;
                manager.User.Phone_code = request.phone_code;
                manager.User.Phone_number = request.phone_number;
                manager.User.TimeZone = request.time_zone;

                {
                    var rem = manager.Permissions.Where(p => !request.permissions.Contains(p.Name)).ToList();
                    foreach (var p in rem)
                        manager.Permissions.Remove(p);
                    var exists = manager.Permissions.Select(p => p.Name);
                    var add = await service.MapPermissions(request.permissions.Where(p => !exists.Contains(p)));
                    foreach (var p in add)
                        manager.Permissions.Add(p);
                }
                {
                    var rem = manager.ImContacts.Where(p => !request.messangers.Keys.Contains(p.messanger)).ToList();
                    foreach (var p in rem)
                        manager.ImContacts.Remove(p);
                    foreach (var c in manager.ImContacts)
                        if (request.messangers.ContainsKey(c.messanger))
                            c.contact = request.messangers[c.messanger];
                    var exists = manager.ImContacts.Select(p => p.messanger);
                    var add = request.messangers.Where(e => !exists.Contains(e.Key));
                    foreach (var p in add)
                        manager.ImContacts.Add(new Service.Storage.Models.ImContact()
                        {
                            messanger = p.Key,
                            contact = p.Value
                        });
                }
                var result = await service.UpdateManager(manager);
                if (result != null)
                {
                    if (result.message.Contains("unique"))
                    {
                        if (result.message.Contains("Email"))
                            return new ManagerResponse()
                            {
                                result = Result.ExistenEmail,
                                entity = null
                            };
                        else if (result.message.Contains("Phone"))
                            return new ManagerResponse()
                            {
                                result = Result.ExistenPhone,
                                entity = null
                            };
                    }
                    else
                        return new ManagerResponse()
                        {
                            result = Result.Ok,
                            entity = manager.ToComObject()
                        };
                }

            }
            return new ManagerResponse()
            {
                result = Result.NotFound,
                entity = null
            };
        }
    }
}

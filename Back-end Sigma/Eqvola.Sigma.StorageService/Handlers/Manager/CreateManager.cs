﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers
{
    public class CreateManager : StorageHandler<CreateManagerRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new ManagerResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            var dep = await service.GetDepartmentByName(request.department);
            if (dep == null)
            {
                return new ManagerResponse()
                {
                    result = Result.NonexistentDepartment,
                    entity = null
                };
            }
            if (dep.Status == (int)DepartmentStatus.Deleted)
                return new ManagerResponse()
                {
                    result = Result.DeletedDepartment,
                    entity = null
                };
            var manager = new Service.Storage.Models.Manager()
            {
                Department = dep,
                Rank = request.rank,
                Permissions = await service.MapPermissions(request.permissions),
                User = new Service.Storage.Models.User()
                {
                    Email = request.email,
                    City = request.address_city,
                    Country = request.country,
                    FirstName = request.firstname,
                    LastName = request.lastname,
                    Phone_code = request.phone_code,
                    Phone_number = request.phone_number,
                    Password = request.password,
                    TimeZone = request.time_zone
                },
                ImContacts = request.messangers.Select(m => new Service.Storage.Models.ImContact()
                {
                    messanger = m.Key,
                    contact = m.Value
                }).ToList(),

            };
            manager.Status = (int)ManagerStatus.Active;
            var result = await service.CreateManager(manager);
            if (result != null)
            {
                if (result.message.Contains("unique"))
                {
                    if (result.message.Contains("Email"))
                        return new ManagerResponse()
                        {
                            result = Result.ExistenEmail,
                            entity = null
                        };
                    else if (result.message.Contains("Phone"))
                        return new ManagerResponse()
                        {
                            result = Result.ExistenPhone,
                            entity = null
                        };
                }
                else
                    return new ManagerResponse()
                    {
                        result = Result.Ok,
                        entity = manager.ToComObject()
                    };
            }


            return new ManagerResponse()
            {
                result = Result.Exception,
                entity = null
            };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Email;
using Eqvola.Sigma.Com.Storage;
using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class InviteManager : StorageHandler<InviteManagerRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new StorageResponse<string>()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {
            string newPassword = GeneratePassword();

            var dep = await service.GetDepartmentByName(request.department);
            if (dep == null)
            {
                return new StorageResponse<string>()
                {
                    result = Result.NonexistentDepartment,
                    entity = null
                };
            }
                      
            var manager = new Service.Storage.Models.Manager()
            {
                Department = dep,
                Rank = request.rank,
                Permissions = await service.MapPermissions(request.permissions),
                User = new Service.Storage.Models.User()
                {
                    Email = request.email,
                    Password = EncodingPassword(newPassword)
                },
                ImContacts =  new List<Service.Storage.Models.ImContact>(),
                Tags = new List<Service.Storage.Models.ManagerTag>()
            };

            var result = await service.CreateManager(manager);
            if (result != null)
            {
                if (result.message.Contains("unique"))
                {
                    if (result.message.Contains("Email"))
                        return new StorageResponse<string>()
                        {
                            result = Result.ExistenEmail,
                            entity = null
                        };
                    else if (result.message.Contains("Phone"))
                        return new StorageResponse<string>()
                        {
                            result = Result.ExistenPhone,
                            entity = null
                        };
                }
                else
                {
                    var emailRequest = new SendEmailRequest<EmailValues>()
                    {
                        TemplateName = "invite-manager",
                        Subject = "Eqvola manager invitation",
                        SendTo = request.email,
                        Parametrs = new Dictionary<string, string>() { { "password", newPassword }, { "email", manager.User.Email } }
                    };
                    await emailRequest.Send();
                    
                    return new StorageResponse<string>()
                    {
                        result = Result.Ok
                    };
                }
            }
            return new StorageResponse<string>()
            {
                result = Result.Exception,
                entity = null
            };
        }

        private string GeneratePassword()
        {
            const string chars = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm";
            const int minlen = 8;
            const int maxlen = 16;

            var rand = new Random((int)DateTime.UtcNow.Ticks);

            int len = minlen + rand.Next(maxlen - minlen);
            string pwd = "";
            while (len-- > 0)
                pwd += chars[rand.Next(chars.Length - 1)];

            return EncodingPassword(pwd);
        }

        private string EncodingPassword(string password)
        {
            string salt = CloudConfigurationManager.GetSetting("MD5.Salt");
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] bytes = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(salt + password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }
    }
}

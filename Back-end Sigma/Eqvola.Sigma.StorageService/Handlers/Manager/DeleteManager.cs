﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Manager
{
    public class DeleteManager : StorageHandler<DeleteManagerRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            Eqvola.Sigma.Service.Storage.Models.Manager manager = await service.GetManagerByUId(request.email);
            if (manager == null)
            {
                return new ManagerResponse()
                {
                    result = Result.NotFound,
                    entity = null
                };
            }

            if (manager.Status == (int)ManagerStatus.Deleted)
                return new ManagerResponse()
                {
                    result = Result.InvalidRequest,
                    entity = null
                };

            manager.Status = (int)ManagerStatus.Deleted;
            manager.FiringReason = request.reason;

            var resultManager = (await service.UpdateManager(manager));
            return new ManagerResponse()
            {
                result = resultManager != null ? Result.Ok : Result.NotFound,
                entity = resultManager.entity.ToComObject()
            };

        }
        protected override Response CreateFailResponse()
        {
            return new ManagerResponse()
            {
                result = Result.InvalidRequest
            };
        }
    }
}

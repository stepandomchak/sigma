﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage.TwoFactor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.TwoFactor
{
    public class GetTwoFactorAction : StorageHandler<GetTwoFactorActionRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TwoFactorActionResponse()
            {
                result = Com.Storage.Result.Exception
            };
        }

        protected override Task<Response> ProccessAsync()
        {
            throw new NotImplementedException();
        }
    }
}

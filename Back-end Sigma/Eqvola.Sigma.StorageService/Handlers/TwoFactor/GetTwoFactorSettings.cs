﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage.TwoFactor;
using Eqvola.Sigma.StorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.TwoFactor
{
    public class GetTwoFactorSettings : StorageHandler<GetTwoFactorSettingsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new SettingsResponse()
            {
                result = Com.Storage.Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.actionId > 0)
            {
                var setting = (await service.GetSettingsByUserAction(request.userId, request.actionId))?.ToComObject();
                if(setting == null)
                    return new SettingsResponse()
                    {
                        result = Com.Storage.Result.Exception
                    };

                return new SettingsResponse()
                {
                    entity = setting,
                    result = Com.Storage.Result.Ok
                };

            }
            else
            {
                var settings = (await service.GetSettingsByUser(request.userId)).Select(s=>s.ToComObject());
                if (settings == null)
                    return new SettingsResponse()
                    {
                        result = Com.Storage.Result.Exception
                    };
                return new SettingsResponse()
                {
                    settings = settings.ToList(),
                    result = Com.Storage.Result.Ok
                };
            }

        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Handlers.Tag
{
    public class TagHandler : StorageHandler<TagRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new StorageResponse<string>()
            {
                result = Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {

            
            switch (request.action)
            {
                case TagAction.Tag:
                    return await Tag(request);
                case TagAction.Untag:
                    return await UnTag(request);
                case TagAction.Delete:
                    return await DeleteTag(request);
                case TagAction.GetAll:
                    return await GetAll(request);
            }

            return new StorageResponse<string>
            {
                result = Result.InvalidRequest
            };
        }

        private async Task<Response> Tag(TagRequest request)
        {
            bool success = false;
            switch (request.target)
            {
                case TagTarget.Department:
                    success = await service.TagDepartment(request.tid, request.tag);
                    break;
                case TagTarget.Manager:
                    success = await service.TagManager(request.tid, request.tag);
                    break;
            }

            return new StorageResponse<string>
            {
                result = success ? Result.Ok : Result.InvalidRequest,
                entity = request.tag
            };
        }

        private async Task<Response> UnTag(TagRequest request)
        {
            bool success = false;
            switch (request.target)
            {
                case TagTarget.Department:
                    success = await service.UntagDepartment(request.tid, request.tag);
                    break;
                case TagTarget.Manager:
                    success = await service.UntagManager(request.tid, request.tag);
                    break;
            }

            return new StorageResponse<string>
            {
                result = success ? Result.Ok : Result.InvalidRequest,
                entity = request.tag
            };
        }

        private async Task<Response> DeleteTag(TagRequest request)
        {
            Trace.WriteLine("Delete tag request received");
            bool success = false;
            switch (request.target)
            {
                case TagTarget.Department:
                    success = await service.DeleteDepartmentTag(request.tag);
                    break;
                case TagTarget.Manager:
                    success = await service.DeleteManagerTag(request.tag);
                    break;
            }

            return new StorageResponse<string>
            {
                result = success ? Result.Ok : Result.InvalidRequest,
                entity = request.tag
            };
        }

        private async Task<TagsResponse> GetAll(TagRequest request)
        {
            Trace.WriteLine("Tags getAll");
            List<String> tagsList = new List<string>();
            switch (request.target)
            {
                case TagTarget.Department:
                    var departmentTags = await service.GetAllDepartmentTags();
                    if (departmentTags == null)
                        break;
                    foreach (var tag in departmentTags)
                    {
                        tagsList.Add(tag.Name);
                    }
                    return new TagsResponse()
                    {
                        result = Result.Ok,
                        entity = tagsList
                    };
                case TagTarget.Manager:
                    var managerTags = await service.GetAllManagerTags();
                    if (managerTags == null)
                        break;
                    foreach (var tag in managerTags)
                    {
                        tagsList.Add(tag.Name);
                    }
                    return new TagsResponse()
                    {
                        result = Result.Ok,
                        entity = tagsList
                    };
            }

            return new TagsResponse
            {
                result = Result.InvalidRequest
            };
        }
    }
}

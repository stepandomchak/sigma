﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.TwoFactor;
using Eqvola.Sigma.Service;
using Eqvola.Sigma.StorageService.Handlers;
using Eqvola.Sigma.StorageService.Handlers.Avatar;
using Eqvola.Sigma.StorageService.Handlers.Department;
using Eqvola.Sigma.StorageService.Handlers.Manager;
using Eqvola.Sigma.StorageService.Handlers.Settings;
using Eqvola.Sigma.StorageService.Handlers.Tag;
using Eqvola.Sigma.StorageService.Handlers.TwoFactor;
using Eqvola.Sigma.StorageService.Handlers.User;
using Microsoft.ServiceBus.Messaging;

namespace Eqvola.Sigma.StorageService
{
    public partial class WorkerRole : ServiceBusRole
    {
        protected override IEnumerable<string> queues => new string[] { "storage" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch(queueName)
            {
                case "storage":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        { "manager-get", (message) => { return HandlerFactory<GetManagerRequest, GetManager>.ProccessMessage(message); } },
                        { "manager-create", (message) => { return HandlerFactory<CreateManagerRequest, CreateManager>.ProccessMessage(message); } },
                        { "manager-update", (message) => { return HandlerFactory<UpdateManagerRequest, UpdateManager>.ProccessMessage(message); } },
                        { "manager-delete", (message) => { return HandlerFactory<DeleteManagerRequest, DeleteManager>.ProccessMessage(message); } },
                        { "manager-activate", (message) => { return HandlerFactory<ActivateManagerRequest, ActivateManager>.ProccessMessage(message); } },
                        { "manager-invite", (message) => { return HandlerFactory<InviteManagerRequest, InviteManager>.ProccessMessage(message); } },
                        { "manager-checkNumber", (message) => { return HandlerFactory<CheckNumberRequest, CheckNumber>.ProccessMessage(message); } },
                        
                        { "department-get", (message) => { return HandlerFactory<GetDepartmentRequest, GetDepartment>.ProccessMessage(message); } },
                        { "department-set", (message) => { return HandlerFactory<SetDepartmentRequest, SetDepartment>.ProccessMessage(message); } },
                        { "department-delete", (message) => { return HandlerFactory<DeleteDepartmentRequest, DeleteDepartment>.ProccessMessage(message); } },
                        { "department-activate", (message) => { return HandlerFactory<ActivateDepartmentRequest, ActivateDepartment>.ProccessMessage(message); } },

                        { "tag", (message) => { return HandlerFactory<TagRequest, TagHandler>.ProccessMessage(message); } },
                        
                        { "password-set", (message) => { return HandlerFactory<SetPasswordRequest, SetPassword>.ProccessMessage(message); } },
                        { "password-reset-code", (message) => { return HandlerFactory<ResetPasswordRequest, ResetPassword>.ProccessMessage(message); } },
                        
                        { "upload-avatar", (message) => { return HandlerFactory<UploadAvatarRequest, UploadAvatar>.ProccessMessage(message); } },
                        
                        { "user-get", (message) => { return HandlerFactory<GetUserRequest, GetUser>.ProccessMessage(message); } },
                        { "user-get-range", (message) => { return HandlerFactory<GetRangeUsersRequest, GetRangeUsers>.ProccessMessage(message); } },

                        //{ "update-two-factor-state", (message) => { return HandlerFactory<UpdateTwoFactorStateRequest, UpdateTwoFactorState>.ProccessMessage(message); } },

                        { "wluser-add", (message) => { return HandlerFactory<AddWLUserRequest, AddWLUser>.ProccessMessage(message); } },
                        { "wluser-get", (message) => { return HandlerFactory<GetWLUserRequest, GetWLUser>.ProccessMessage(message); } },
                        { "wlkey-get", (message) => { return HandlerFactory<GetWLApiKeyRequest, GetWLKey>.ProccessMessage(message); } },

                        { "twofactor-get", (message) => { return HandlerFactory<GetTwoFactorRequest, GetTwoFactor>.ProccessMessage(message); } },
                        { "twofactoraction-get", (message) => { return HandlerFactory<GetTwoFactorActionRequest, GetTwoFactorAction>.ProccessMessage(message); } },
                        { "twofactorsettings-get", (message) => { return HandlerFactory<GetTwoFactorSettingsRequest, GetTwoFactorSettings>.ProccessMessage(message); } },

                        { "settings-get", (message) => { return HandlerFactory<GetSettingsRequest, GetSettings>.ProccessMessage(message); } },
                        { "settings-update", (message) => { return HandlerFactory<UpdateSettingsRequest, UpdateSettings>.ProccessMessage(message); } }

            };
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class WLUser
    {
        public static Com.Storage.Models.WLUser ToComObject(this Service.Storage.Models.WLUser src)
        {
            return new Com.Storage.Models.WLUser()
            {
                Id = src.Id,
                UserId = src.UserId,
                Wl = src.Wl.ToComObject()
            };
        }
    }
}

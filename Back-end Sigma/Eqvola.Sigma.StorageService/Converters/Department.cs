﻿using Eqvola.Sigma.Com.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class Department
    {
        public static Com.Storage.Models.Department ToComObject(this Service.Storage.Models.Department src)
        {
            return new Com.Storage.Models.Department()
            {
                Id = src.Id,
                Name = src.Name,
                Managers = src.Managers?.Select(m => m.ToShortComObject()).ToList(),
                Permissions = src.Permissions?.Select(p => p.Name).ToList(),
                Tags = src.Tags?.Select(t => t.Name).ToList(),
                Status = src.Status
            };
        }

        public static Com.Storage.Models.Department ToShortComObject(this Service.Storage.Models.Department src)
        {
            return new Com.Storage.Models.Department()
            {
                Id = src.Id,
                Name = src.Name,
                Permissions = src.Permissions?.Select(p => p.Name).ToList(),
                Tags = src.Tags?.Select(t => t.Name).ToList(),
                Status = src.Status
            };
        }
    }
}

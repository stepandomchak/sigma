﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class TwoFactorSettings
    {
        public static Com.Storage.Models.UserTwoFactor ToComObject(this Service.Storage.Models.UserTwoFactor src)
        {
            return new Com.Storage.Models.UserTwoFactor()
            {
                User = src.User.ToComObject(),
                TwoFactor = src.TwoFactor.ToComObject(),
                TwoFactorAction = src.TwoFactorAction.ToComObject(),
                Secret = src.Secret,
                State = src.State
            };
        }
    }
}

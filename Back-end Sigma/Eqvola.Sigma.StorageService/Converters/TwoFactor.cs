﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class TwoFactor
    {
        public static Com.Storage.Models.TwoFactor ToComObject(this Service.Storage.Models.TwoFactor src)
        {
            return new Com.Storage.Models.TwoFactor()
            {
                Type = src.Type,
                Status = src.Status
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class Settings
    {
        public static Com.Storage.Models.Settings ToComObject(this Service.Storage.Models.Settings src)
        {
            return new Com.Storage.Models.Settings()
            {
                Key = src.Key,
                Value = src.Value
            };
        }
    }
}

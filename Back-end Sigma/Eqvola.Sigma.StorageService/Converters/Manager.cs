﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class Manager
    {
        public static Com.Storage.Models.Manager ToComObject(this Service.Storage.Models.Manager src)
        {
            return new Com.Storage.Models.Manager()
            {
                Department = src.Department.ToShortComObject(),
                Rank = src.Rank,
                User = src.User.ToComObject(),
                Permissions = src.Permissions.Select(p => p.Name).ToList(),
                ImContacts = src.ImContacts.ToDictionary(m => m.messanger, c => c.contact),
                Status = src.Status,
                FiringReason = src.FiringReason,
                Tags = src.Tags?.Select(t => t.Name).ToList()
            };
        }
        
        public static Com.Storage.Models.Manager ToShortComObject(this Service.Storage.Models.Manager src)
        {
            return new Com.Storage.Models.Manager()
            {
                Rank = src.Rank,
                User = src.User.ToComObject(),
                Permissions = src.Permissions.Select(p => p.Name).ToList(),
                Tags = src.Tags?.Select(t => t.Name).ToList(),
                Status = src.Status
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class TwoFactorAction
    {
        public static Com.Storage.Models.TwoFactorAction ToComObject(this Service.Storage.Models.TwoFactorAction src)
        {
            return new Com.Storage.Models.TwoFactorAction()
            {
                Name = src.Name,
                Status = src.Status
            };
        }
    }
}

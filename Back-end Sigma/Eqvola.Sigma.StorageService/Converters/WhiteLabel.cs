﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class WhiteLabel
    {
        public static Com.Storage.Models.WhiteLabel ToComObject(this Service.Storage.Models.WhiteLabel src)
        {
            return new Com.Storage.Models.WhiteLabel()
            {
                Id = src.Id,
                name = src.name
            };
        }
    }
}

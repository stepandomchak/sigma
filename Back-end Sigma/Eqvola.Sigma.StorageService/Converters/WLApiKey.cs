﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StorageService.Converters
{
    public static class WLApiKey
    {
        public static Com.Storage.Models.WLApiKey ToComObject(this Service.Storage.Models.WlApiKey src)
        {
            return new Com.Storage.Models.WLApiKey()
            {
                Id = src.Id,
                Key = src.Key,
                ValidFrom = src.ValidFrom,
                ValidTo = src.ValidTo,
                Wl = src.Wl.ToComObject()
            };
        }

    }
}

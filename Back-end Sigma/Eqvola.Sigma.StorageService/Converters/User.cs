﻿namespace Eqvola.Sigma.StorageService.Converters
{
    public static class User
    {
        public static Com.Storage.Models.User ToComObject(this Service.Storage.Models.User src)
        {
            return new Com.Storage.Models.User()
            {
                Id = src.Id,
                Email = src.Email,
                FirstName = src.FirstName,
                LastName = src.LastName,
                PhoneCode = src.Phone_code,
                PhoneNumber = src.Phone_number,
                Country = src.Country,
                City = src.City,
                TimeZone = src.TimeZone,
                IsTwoFactorEnabled = src.IsTwoFactorEnabled,
                SecretKey = src.TwoFactorSecretKey,
                AvatarUrl = src.AvatarUrl,
                IsVerified = src.IsVerified
            };
        }
    }
}

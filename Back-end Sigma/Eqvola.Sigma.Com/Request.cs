﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace Eqvola.Sigma.Com
{
    public abstract class Request
    {
        [JsonIgnore]
        public abstract string QueueName { get; }
        [JsonIgnore]
        public abstract string RequestName { get; }
        [JsonIgnore]
        public virtual string SessionId { get; private set; }

        public DateTime Created { get; set; }
        public string Source { get; set; }

        public async virtual Task Send()
        {
            SessionId = Guid.NewGuid().ToString();
            Created = DateTime.UtcNow;

            var sender = ServiceBusFactory.GetQueueClient(QueueName);
            var message = BuildMessage();
            await sender.SendAsync(message);
        }

        protected virtual BrokeredMessage BuildMessage()
        {
            string json = JsonConvert.SerializeObject(this);
            MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json));
            return new BrokeredMessage(ms, true)
            {
                ContentType = "application/json",
                Label = RequestName,
                SessionId = this.SessionId
            };
        }

        public async virtual Task<T> GetResponse<T>(bool send = true) where T : Response
        {
            if (send) await Send();

            var session = await ServiceBusFactory.GetSession(this.SessionId);
            var message = await session.ReceiveAsync();
            T ret = default(T);
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                ret = JsonConvert.DeserializeObject<T>(json);
            }
            await session.CompleteAsync(message.LockToken);
            await session.CloseAsync();

            return ret;
        }
    }
}

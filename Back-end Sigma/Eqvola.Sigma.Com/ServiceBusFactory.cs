﻿using Microsoft.Azure;
using Microsoft.ServiceBus.Messaging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public class ServiceBusFactory
    {
        private static Dictionary<string, QueueClient> clients;
        private static QueueClient responsClient;

        static ServiceBusFactory()
        {
            clients = new Dictionary<string, QueueClient>();
            responsClient = QueueClient.CreateFromConnectionString(CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.ConnectionString"), "responses");
        }

        public static QueueClient GetQueueClient(string queueName)
        {
            if (!clients.ContainsKey(queueName))
            {
                var sender = QueueClient.CreateFromConnectionString(CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.ConnectionString"), queueName);
                clients[queueName] = sender;
            }

            return clients[queueName];
        }

        public static QueueClient GetResponseClient()
        {
            return responsClient;
        }

        public async static Task<MessageSession> GetSession(string sessionId)
        {
            return await responsClient.AcceptMessageSessionAsync(sessionId);
        }
    }
}

﻿using Eqvola.Sigma.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public class Client : WebSocket
    {
        public delegate void PhaseChanged(Client client, WebSocket.ProtocolPhase oldPhase, WebSocket.ProtocolPhase newPhase);
        public event PhaseChanged OnPhaseChanged;

        public Client(Socket socket) : base(socket) { }

        protected override void SetPhase(ProtocolPhase phase)
        {
            OnPhaseChanged?.Invoke(this, this.phase, phase);
            base.SetPhase(phase);
        }
    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public static class WaitHandleEx
    {
        public static Task ToTask(this WaitHandle waitHandle)
        {
            var tcs = new TaskCompletionSource<object>();

            ThreadPool.RegisterWaitForSingleObject(
                waitHandle,
                (o, timeout) => { tcs.SetResult(null); },
                null,
                -1,
                true);

            return tcs.Task;
        }
    }
}
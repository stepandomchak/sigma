﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public class MultySocketHandler
    {
        private Task handleTask = null;

        protected virtual int Timeout { get { return 0; } }

        protected Dictionary<Socket, SocketWrapper> clients = new Dictionary<Socket, SocketWrapper>();

        public virtual void AddClient(SocketWrapper client)
        {
            clients.Add(client.socket, client);
        }

        public virtual void RemoveClient(SocketWrapper client)
        {
            if (clients.ContainsKey(client.socket))
                clients.Remove(client.socket);
        }

        public virtual void Send(byte[] data)
        {
            foreach (var client in clients.Values)
                client.Send(data);
        }

        protected virtual List<Socket> BuildReadList()
        {
            return clients.Keys.ToList();
        }

        protected virtual List<Socket> BuildWriteList()
        {
            return clients.Values.Where(v => v.sendRequired).Select(v => v.socket).ToList();
        }

        protected virtual List<Socket> BuildErrList()
        {
            return clients.Keys.ToList();
        }

        public virtual void Start()
        {
            if (handleTask == null)
                handleTask = Task.Run(() => { StartHandle(); });
        }

        protected Task StartHandle()
        {     
            while (true)
            {
                try
                {
                    IList read = BuildReadList();
                    IList write = BuildWriteList();
                    IList err = BuildErrList();
                    HashSet<Socket> toRemove = new HashSet<Socket>();

                    Socket.Select(read, write, err, Timeout);
                    foreach (Socket socket in read)
                    {
                        if (!clients[socket].HandleRead())
                        {
                            clients[socket].Close();
                            clients.Remove(socket);
                        }
                    }
                    foreach (Socket socket in write)
                    {
                        if (!clients[socket].HandleWrite())
                        {
                            clients[socket].Close();
                            clients.Remove(socket);
                        }
                    }
                    foreach (Socket socket in err)
                    {
                        if (!clients[socket].HandleError())
                        {
                            Trace.TraceWarning($"{socket.RemoteEndPoint} failed");
                            clients[socket].Close();
                            clients.Remove(socket);
                        }
                    }

                    //foreach (var pair in clients)
                    //{
                    //    if (!CheckState(pair.Value))
                    //    {
                    //        toRemove.Add(pair.Key);
                    //    }
                    //}

                    foreach (var k in toRemove)
                        clients.Remove(k);
                }
                catch (Exception e)
                {
                    Trace.TraceError(e.ToString());
                    //throw e;
                }
            }
        }
    }
}

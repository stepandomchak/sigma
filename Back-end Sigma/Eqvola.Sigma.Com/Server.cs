﻿using Eqvola.Sigma.Com;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public class Server : MultySocketHandler
    {
        protected class ServerSocket : SocketWrapper
        {
            private MultySocketHandler handler;

            public ServerSocket(Socket socket, MultySocketHandler handler) : base(socket)
            {
                this.handler = handler;
            }

            public override bool HandleRead()
            {
                Socket client = this.socket.Accept();
                handler.AddClient(new Client(client));
                return true;
            }

            public override bool HandleError()
            {
                return true;
            }
        }

        private int listeningPort;
        private Task listenProc;
        private ServerSocket socketServer;

        public Server(int port)
        {
            listeningPort = port;
        }

        public override void Start()
        {
            Trace.TraceInformation($"Starting listening on [{System.Net.IPAddress.Any}:{listeningPort}] ...");

            try
            {
                Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.Bind(new System.Net.IPEndPoint(System.Net.IPAddress.Any, listeningPort));
                server.Listen(10);
                socketServer = new ServerSocket(server, this);
                clients.Add(server, socketServer);
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                throw e;
            }

            listenProc = Task.Run(() =>
            {
                StartHandle();
            });
        }
    }
}

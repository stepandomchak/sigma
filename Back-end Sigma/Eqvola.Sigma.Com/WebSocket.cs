﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public class WebSocket : SocketWrapper
    {
        public enum ProtocolPhase
        {
            Handshake,
            DataTransffer,
            Disconnecting
        }

        public string Path { get; private set; }

        protected ProtocolPhase phase;
        private StringBuilder handshakeRequest;

        private readonly Regex rgGet = new Regex("^GET ([^\\s]*) HTTP", RegexOptions.Compiled | RegexOptions.Multiline);
        private readonly Regex rgSecurityKey = new Regex("Sec-WebSocket-Key: (.*)", RegexOptions.Compiled | RegexOptions.Multiline);

        public WebSocket(Socket socket) : base(socket)
        {
            phase = ProtocolPhase.Handshake;
            handshakeRequest = new StringBuilder();
        }

        public override void Send(byte[] data)
        {
            int mlen = data.Length;
            int addLen = mlen > 125 ? 4 : 2;
            byte[] newData = new byte[data.Length + addLen];
            newData[0] = 1;
            newData[1] = mlen > 125 ? (byte)126 : (byte)mlen;
            if (mlen > 125)
                Array.Copy(BitConverter.GetBytes((ushort)data.Length).Reverse().ToArray(), 0, newData, 2, 2);
            Array.Copy(data, 0, newData, addLen, data.Length);
            base.Send(newData);
        }

        public override bool HandleRead()
        {
            base.HandleRead();

            if (phase == ProtocolPhase.Handshake)
                return HandleHandshake();
            else if (phase == ProtocolPhase.DataTransffer)
                return HandleData();
            else if (phase == ProtocolPhase.Disconnecting)
                return HandleDisconnect();

            return false;
        }

        public override bool HandleWrite()
        {
            base.HandleWrite();

            if (phase == ProtocolPhase.Handshake)
            {
                if (!sendRequired)
                {
                    Trace.TraceInformation($"[{socket.RemoteEndPoint}] connected to '{Path}'");
                    SetPhase(ProtocolPhase.DataTransffer);
                }
            }
            return true;
        }

        protected virtual void SetPhase(ProtocolPhase phase)
        {
            this.phase = phase;
        }

        private bool HandleHandshake()
        {
            const string eol = "\r\n";

            handshakeRequest.Append(Encoding.UTF8.GetChars(receiveBuffer, 0, dataInBuffer));

            if (handshakeRequest.ContainFullHttpMessage())
            {
                string req = handshakeRequest.ToString();
                var matchGet = rgGet.Match(req);
                if (matchGet.Success && !string.IsNullOrWhiteSpace(matchGet.Groups[1].Value.Trim()))
                {
                    var matchKey = rgSecurityKey.Match(req);
                    if (matchKey.Success && !string.IsNullOrWhiteSpace(matchKey.Groups[1].Value.Trim()))
                    {
                        Path = matchGet.Groups[1].Value.Trim();

                        string resp =
                            "HTTP/1.1 101 Switching Protocols" + eol
                            + "Connection: Upgrade" + eol
                            + "Upgrade: websocket" + eol
                            + "Sec-WebSocket-Accept: " + Convert.ToBase64String(
                                System.Security.Cryptography.SHA1.Create().ComputeHash(
                                    Encoding.UTF8.GetBytes(matchKey.Groups[1].Value.Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
                                    )
                                )
                            ) + eol
                            + eol;
                        Send(Encoding.UTF8.GetBytes(resp));
                        return true;
                    }
                    else
                        Trace.TraceWarning($"[{socket.RemoteEndPoint}] not websocket request => '{req}'");
                }
                else
                    Trace.TraceWarning($"[{socket.RemoteEndPoint}] invalid HTTP request => '{req}'");

                return false;
            }

            return true;
        }

        private bool HandleData()
        {
            return false;
        }

        private bool HandleDisconnect()
        {
            return false;
        }
    }
}

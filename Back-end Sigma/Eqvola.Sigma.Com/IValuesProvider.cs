﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public interface IValuesProvider
    {
        IDictionary<string, object> GetValues();
    }
}

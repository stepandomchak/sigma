﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public abstract class SocketWrapper
    {
        private struct QueueEntry
        {
            public byte[] data;
            public int sended;

            public QueueEntry(byte[] data)
            {
                this.data = data;
                sended = 0;
            }
        }
        private Queue<QueueEntry> sendQueue = new Queue<QueueEntry>();

        private const int DEFAULT_RECEIVE_BUFFER_SIZE = 1024;

        protected internal Socket socket { get; private set; }
        internal bool sendRequired { get { return sendQueue.Count > 0; } }

        protected long lastReceive { get; private set; }
        protected byte[] receiveBuffer;
        protected int dataInBuffer;

        public SocketWrapper(Socket socket) : this(socket, DEFAULT_RECEIVE_BUFFER_SIZE) { }

        public SocketWrapper(Socket socket, int bufferSize)
        {
            this.socket = socket;
            dataInBuffer = 0;
            lastReceive = DateTime.UtcNow.Ticks;
            receiveBuffer = new byte[bufferSize];
        }

        public virtual void Send(byte[] data)
        {
            sendQueue.Enqueue(new QueueEntry(data));
        }

        public virtual void Close()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

        public virtual bool HandleRead()
        {
            if (socket.Available > 0)
            {
                lastReceive = DateTime.UtcNow.Ticks;
                dataInBuffer = socket.Receive(receiveBuffer, 0, Math.Min(receiveBuffer.Length, socket.Available), SocketFlags.None);
            }
            return true;
        }

        public virtual bool HandleWrite()
        {
            if (sendQueue.Count > 0)
            {
                QueueEntry entry = sendQueue.Peek();
                int left = entry.data.Length - entry.sended;
                entry.sended += socket.Send(entry.data, entry.sended, left, SocketFlags.None);
                if (entry.sended >= entry.data.Length)
                    sendQueue.Dequeue();
            }
            return true;
        }

        public virtual bool HandleError() { return false; }
    }
}

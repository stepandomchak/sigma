﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public abstract class Response
    {
        public async virtual Task Send(string sessionId)
        {
            try
            {
                var sender = ServiceBusFactory.GetQueueClient("responses");

                string json = JsonConvert.SerializeObject(this);
                using (MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)))
                {
                    var message = new BrokeredMessage(ms, true)
                    {
                        ContentType = "application/json",
                        SessionId = sessionId
                    };
                    await sender.SendAsync(message);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

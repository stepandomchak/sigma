﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public static class HttpHelper
    {
        public static bool ContainFullHttpMessage(this StringBuilder builder)
        {
            return builder.Length > 4
                && builder[builder.Length - 1] == '\n'
                && builder[builder.Length - 2] == '\r'
                && builder[builder.Length - 3] == '\n'
                && builder[builder.Length - 4] == '\r';
        }
    }
}

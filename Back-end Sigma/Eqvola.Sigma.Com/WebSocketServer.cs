﻿using Eqvola.Sigma.Com;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com
{
    public class WebSocketServer : Server
    {
        private Client.PhaseChanged phaseChanghedDelegate;
        private Dictionary<string, MultySocketHandler> handlers = new Dictionary<string, MultySocketHandler>();

        public WebSocketServer(int port) : base(port)
        {
            phaseChanghedDelegate = OnClientPhaseChanged;
        }

        public override void AddClient(SocketWrapper client)
        {
            if (client is Client)
                (client as Client).OnPhaseChanged += phaseChanghedDelegate;
            base.AddClient(client);
        }

        private void OnClientPhaseChanged(Client client, WebSocket.ProtocolPhase oldPhase, WebSocket.ProtocolPhase newPhase)
        {
            if (newPhase == WebSocket.ProtocolPhase.DataTransffer)
            {
                if (handlers.ContainsKey(client.Path))
                {
                    handlers[client.Path].AddClient(client);
                }
                else
                {
                    client.Close();
                }
                RemoveClient(client);
            }
        }

        public virtual void RegisterEndpoint<T>(string path, T handler) where T : MultySocketHandler
        {
            handlers[path] = handler;
        }
    }
}

﻿using System;

namespace Eqvola.Sigma.Com
{
    public static class DoubleExtensions
    {
        private const double _inaccuracy = 0.00000000000000000001;

        public static bool EqualsWithInaccuracy(this double value1, double value2) => CompareToWithInaccuracy(value1, value2) == 0;

        public static int CompareToWithInaccuracy(this double value1, double value2)
        {
            if (value1 < value2 - _inaccuracy)
            {
                return -1;
            }
            if (value1 > value2 + _inaccuracy)
            {
                return 1;
            }
            if (Math.Abs(value1 - value2) <= _inaccuracy)
            {
                return 0;
            }
            if (double.IsNaN(value1))
            {
                if (!double.IsNaN(value2))
                {
                    return -1;
                }
                return 0;
            }
            return 1;
        }
    }
}

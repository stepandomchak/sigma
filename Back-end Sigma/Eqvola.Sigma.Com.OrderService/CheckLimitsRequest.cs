﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderService
{
    public class CheckLimitsRequest : Request
    {
        public override string QueueName => "order-service";

        public override string RequestName => "check-limits";

        public string currency { get; set; }
        public string targetCurrency { get; set; }
        public double price { get; set; }

    }
}

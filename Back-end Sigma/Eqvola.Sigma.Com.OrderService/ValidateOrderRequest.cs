﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderService
{
    public class ValidateOrderRequest : Request
    {
        public override string QueueName => "order-service";
        public override string RequestName => "check-order";
            
        public Order order { get; set; }
        public long userId { get; set; }
    }
}

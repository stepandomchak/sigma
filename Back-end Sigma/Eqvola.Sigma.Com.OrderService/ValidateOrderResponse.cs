﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderService
{
    public class ValidateOrderResponse<T> : Response
    {
        public T entity { get; set; }
        public Result result { get; set; }
        public string message { get; set; }
    }
}

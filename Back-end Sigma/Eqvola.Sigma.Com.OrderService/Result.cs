﻿namespace Eqvola.Sigma.Com.OrderService
{
    public enum Result
    {
        Ok,
        NotFound,
        InvalidRequest,
        Exception,
        BadRequest,
        UnexpectedAmount,
        UnexpectedCurrency,
        UnexpectedPrice,
        NonExistenOrder,
        ExpiredOrder,
        UnexpectedOrderType,
        NonExistenPair,
        NotEnoughtBalance
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Converters
{
    public static class Message
    {
        public static Com.Support.Models.Message ToComObject(this Service.SupportService.Models.Message src)
        {
            return new Com.Support.Models.Message()
            {
                Id = src.Id,
                Email = src.Email,
                Text = src.Text,
                Created = src.Created,
                Status = src.Status,
                Ticket = src.Ticket.ToComObject(),
                Attachments = src?.Attachments?.Select(x => x?.ToComObject())
            };
        }
        public static Com.Support.Models.Message ToShortComObject(this Service.SupportService.Models.Message src)
        {
            return new Com.Support.Models.Message()
            {
                Id = src.Id,
                Email = src.Email,
                Text = src.Text,
                Status = src.Status,
                Created = src.Created,
                Attachments = src?.Attachments?.Select(x=>x?.ToComObject()),
            };
        }
    }
}

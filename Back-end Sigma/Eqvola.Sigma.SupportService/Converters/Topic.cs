﻿using Eqvola.Sigma.Com.Support.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Converters
{
    public static class Topic
    {
        public static Com.Support.Models.Topic ToComObject(this Service.SupportService.Models.Topic src)
        {
            return new Com.Support.Models.Topic()
            {
                Id = src.Id,
                Category = src.Category.ToShortComObject(),
                Name = src.Title,
                TopicStatus = (TopicStatus)src.TopicStatus,
                Text = src.Text
            };
        }
        public static Com.Support.Models.Topic ToShortComObject(this Service.SupportService.Models.Topic src)
        {
            return new Com.Support.Models.Topic()
            {
                Id = src.Id,
                Name = src.Title,
                TopicStatus = (TopicStatus)src.TopicStatus,
                Text = src.Text
            };
        }
    }
}

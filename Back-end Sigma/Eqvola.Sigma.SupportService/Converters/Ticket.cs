﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Converters
{
    public static class Ticket
    {
        public static Com.Support.Models.Ticket ToComObject(this Service.SupportService.Models.Ticket src)
        {
            return new Com.Support.Models.Ticket()
            {
                Id = src.Id,
                Email = src.Email,
                Title = src.Title,
                Category = src.Category.ToComObject(),
                TicketStatus = src.TicketStatus,
                Created = src.Created,
                Messages = src.Messages?.Select(m => m.ToShortComObject()).ToList()
            };
        }
    }
}

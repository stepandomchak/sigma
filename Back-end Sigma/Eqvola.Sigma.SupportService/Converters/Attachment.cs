﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Converters
{
    public static class Attachment
    {
        public static Com.Support.Models.Attachment ToComObject(this Service.SupportService.Models.Attachment src)
        {
            return new Com.Support.Models.Attachment()
            {
                Id = src.Id,
                FileName = src.FileName,
                Url = src.Url
            };
        }

    }
}

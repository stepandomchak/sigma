﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Converters
{
    public static class Category
    {
        public static Com.Support.Models.Category ToComObject(this Service.SupportService.Models.Category src)
        {
            return new Com.Support.Models.Category()
            {
                Id = src.Id,
                IsEnable = src.IsEnable,
                Name = src.Name,
                Topics = src.Topics.Select(t => t.ToShortComObject()).ToList()
            };
        }

        public static Com.Support.Models.Category ToShortComObject(this Service.SupportService.Models.Category src)
        {
            return new Com.Support.Models.Category()
            {
                Id = src.Id,
                IsEnable = src.IsEnable,
                Name = src.Name
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService
{
    public class MimeTypes
    {
        public List<string> AllowerMymeTypes { get; private set; }
                    
        public MimeTypes()
        {
            AllowerMymeTypes = new List<string>()
                {
                    "image/gif",
                    "image/jpeg",
                    "image/pjpeg",
                    "image/png",

                    "application/pdf"
                };
        }
    }
}

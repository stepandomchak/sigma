﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Attachment;
using Eqvola.Sigma.SupportService.Converters;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Attachment
{
    public class UploadAttachment : SupportHandler<UploadAttachmentRequest>
    {
        private readonly string blobContainer = "attachments"; 

        protected override Response CreateFailResponse()
        {
            return new AttachmentResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var attachment = await service.GetAttachmenttById(request.id);
            if (attachment == null)
                return new AttachmentResponse()
                {
                    result = Result.AttachmentNotFound
                };

            if (!string.IsNullOrEmpty(request.url))
                attachment.Url = request.url;

            var result = await service.UploadAttachment(attachment);
            return new AttachmentResponse
            {
                entity = result.entity.ToComObject(),
                result = Result.Ok
            };         
        }

    }
}

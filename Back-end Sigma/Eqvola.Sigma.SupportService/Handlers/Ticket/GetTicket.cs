﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Ticket;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Ticket
{
    public class GetTicket : SupportHandler<GetTicketRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TicketResponse()
            {
                result = Com.Support.Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.id != 0)
                return await GetTicketById();
            else
                return await GetTickets();

        }

        private async Task<Response> GetTicketById()
        {
            var ticket = await service.GetTicketById(request.id, request.takerEmail);

            return new TicketResponse()
            {
                entity = ticket?.ToComObject(),
                result = Com.Support.Result.Ok
            };
        }
        private async Task<Response> GetTickets()
        {
            var tickets = await service.GetTickets(request.email, request.categoryName, request.status, request.search, request.count, request.margin, request.orderBy, request.isAscending);
            var ticketsCount = await service.GetTicketsCount(request.email, request.categoryName, request.status, request.search);

            if(tickets == null)
                return new TicketsResponse()
                {
                    result = Com.Support.Result.Exception
                };
            return new TicketsResponse()
            {
                tickets = tickets.Select(s => s.ToComObject()).ToList(),
                totalCount = ticketsCount,
                result = Com.Support.Result.Ok
            };
        }
    }
}

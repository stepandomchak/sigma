﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Ticket;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Ticket
{
    public class UpdateTicket : SupportHandler<UpdateTicketRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TicketResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if(request.id != 0)
            {
                var ticket = await service.GetTicketById(request.id);

                if(ticket == null)
                    return new TicketResponse()
                    {
                        result = Result.NotFound
                    };
                                
                ticket.TicketStatus = request.status;

                var res = await service.UpdateTicket(ticket);
                if (res.entity != null)
                    return new TicketResponse()
                    {
                        entity = res.entity.ToComObject(),
                        result = Result.Ok
                    };

                return new TicketResponse()
                {
                    result = Result.Exception
                };
            }

            return new TicketResponse()
            {
                result = Result.Exception
            };

        }
    }
}

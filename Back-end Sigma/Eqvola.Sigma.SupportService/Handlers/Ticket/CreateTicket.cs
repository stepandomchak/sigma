﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Models;
using Eqvola.Sigma.Com.Support.Ticket;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Eqvola.Sigma.SupportService.Handlers.Ticket
{
    public class CreateTicket : SupportHandler<CreateTicketRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TicketResponse()
            {
                result = Com.Support.Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (!string.IsNullOrEmpty(request.email))
            {
                var storageService = new Service.Storage.Service();
                var user = await storageService.GetUserByEmail(request.email);

                if (user == null)
                    return new TicketResponse()
                    {
                        result = Com.Support.Result.Exception
                    };

                var category = await service.GetCategoryById(request.categoryId);

                if (category == null)
                    return new TicketResponse()
                    {
                        result = Com.Support.Result.Exception
                    };

                var ticket = new Service.SupportService.Models.Ticket()
                {
                    Title = request.title,
                    Category = category,
                    Email = user.Email,
                    Created = DateTime.UtcNow,
                    TicketStatus = (int)TicketStatus.Open
                };

                var resultTicket = await service.CreateTicket(ticket);

                var message = new Service.SupportService.Models.Message()
                {
                    Text = request.message,
                    Email = request.email,
                    Ticket = resultTicket.entity,
                    Created = DateTime.UtcNow
                };

                if (request.attachments != null && request.attachments.Count > 0)
                {
                    var listAttachment = new List<Service.SupportService.Models.Attachment>();
                    foreach (var item in request.attachments)
                    {
                        var mimeType = MimeMapping.GetMimeMapping(item);
                        if (new MimeTypes().AllowerMymeTypes.Contains(mimeType))
                        {
                            listAttachment.Add(new Service.SupportService.Models.Attachment()
                            {
                                FileName = item
                            });
                        }
                    }
                    message.Attachments = listAttachment;
                }
                
                var resultMessage = await service.CreateMessage(message);

                resultTicket.entity.Messages = new List<Service.SupportService.Models.Message>();
                resultTicket.entity.Messages.Add(resultMessage.entity);


                if (resultTicket.entity != null)
                    return new TicketResponse()
                    {
                        entity = resultTicket.entity.ToComObject(),
                        result = Com.Support.Result.Ok
                    };

                return new TicketResponse()
                {
                    result = Com.Support.Result.Exception
                };
            }

            return new TicketResponse()
            {
                result = Com.Support.Result.Exception
            };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Message;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Message
{
    public class GetMessage : SupportHandler<GetMessageRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new MessagesResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if(request.ticketId != 0)
            {
                var messages = (await service.GetMessagesByTicket(request.ticketId, request.count, request.margin));

                if(messages != null)
                {
                    return new MessagesResponse()
                    {
                        messages = messages.Select(s => s.ToComObject()).ToList(), 
                        result = Result.Ok
                    };
                }
            }

            return new MessagesResponse()
            {
                result = Result.Exception
            };
        }
    }
}

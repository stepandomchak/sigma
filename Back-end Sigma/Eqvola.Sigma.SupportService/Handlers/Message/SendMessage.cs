﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Message;
using Eqvola.Sigma.Com.Support.Models;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Eqvola.Sigma.SupportService.Handlers.Message
{
    public class SendMessage : SupportHandler<SendMessageRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new MessageResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            
            var ticket = (await service.GetTicketById(request.ticketId));
            if(ticket == null)
                return new MessageResponse()
                {
                    result = Result.Exception
                };

            var message = new Service.SupportService.Models.Message()
            {
                Text = request.text,
                Email = request.email,
                Ticket = ticket,
                Created = DateTime.UtcNow
            };

            if (request.listAttachments != null && request.listAttachments.Count() > 0)
            {
                var listAttachment = new List<Service.SupportService.Models.Attachment>();
                foreach (var item in request.listAttachments)
                {
                    var mimeType = MimeMapping.GetMimeMapping(item);
                    if (new MimeTypes().AllowerMymeTypes.Contains(mimeType))
                    {
                        listAttachment.Add(new Service.SupportService.Models.Attachment()
                        {
                            FileName = item
                        });
                    }
                }
                message.Attachments = listAttachment;
            }

            var result = await service.CreateMessage(message);

            if(result.entity != null)
                return new MessageResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Result.Ok
                };
            return new MessageResponse()
            {
                message = result.message,
                result = Result.Exception
            };
            
        }

    }
}

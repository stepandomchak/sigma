﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Models;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Topic
{
    public class CreateTopic : SupportHandler<CreateTopicRequest> 
    {
        protected override Response CreateFailResponse()
        {
            return new TopicResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {

            if (string.IsNullOrEmpty(request.Name))
                return new TopicResponse()
                {
                    entity = null,
                    result = Result.TopicIsEmpty
                };

            var category = await service.GetCategoryById(request.CategoryId);
            if (category == null)
                return new TopicResponse()
                {
                    entity = null,
                    result = Result.CategoryNotFound
                };

            if (!category.IsEnable)
                return new TopicResponse()
                {
                    entity = null,
                    result = Result.CategoryIsNotTopic
                };

            var topic = new Service.SupportService.Models.Topic()
            {
                Title = request.Name,
                Category = category,
                Text = request.Text,
                TopicStatus = (int)TopicStatus.Active
            };

            var result = await service.CreateTopic(topic);
            if (result.entity != null)
                return new TopicResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Result.Ok
                };
            else
                return new TopicResponse()
                {
                    entity = null,
                    result = Result.InvalidRequest
                };
        }

        protected async Task<Response> SetTopic()
        {
            var topic = new Service.SupportService.Models.Topic()
            {                
                Title = request.Name
            };

            var result = await service.CreateTopic(topic);

            if (result.entity != null)
                return new TopicResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Result.Ok
                };

            return new TopicResponse()
            {
                entity = null,
                result = Result.InvalidRequest
            };
        }

    }
}

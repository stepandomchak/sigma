﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Topic
{
    public class DeleteTopic : SupportHandler<DeleteTopicRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TopicResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var topic = await service.GetTopicById(request.id);

            if (topic != null)
            {
                var result = await service.DeleteTopic(topic);
                if (result != null)
                    return new TopicResponse()
                    {
                        entity = result.ToComObject(),
                        result = Result.Ok
                    };
            }
            return new TopicResponse()
            {
                entity = null,
                result = Result.Exception
            };
        }
    }
}

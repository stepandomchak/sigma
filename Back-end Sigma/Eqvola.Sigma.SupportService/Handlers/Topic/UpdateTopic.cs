﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Topic
{
    public class UpdateTopic : SupportHandler<UpdateTopicRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TopicResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var topic = service.GetAllTopics().Result.Where(x => x.Id == request.Id).FirstOrDefault();

            if (topic == null)
                return new TopicResponse()
                {
                    result = Result.TopicNotFound
                };

            if (string.IsNullOrEmpty(request.NewName))
                return new TopicResponse()
                {
                    result = Result.TopicIsEmpty
                };

            var category = await service.GetCategoryById(request.NewCategoryId);
            if (category == null)
                return new TopicResponse()
                {
                    result = Result.CategoryNotFound
                };

            if (!category.IsEnable)
                return new TopicResponse()
                {
                    entity = null,
                    result = Result.CategoryIsNotTopic
                };

            topic.Title = request.NewName;
            topic.Category = category;
            topic.TopicStatus = request.TopicStatus;
            topic.Text = request.Text;

            var result = await service.UpdateTopic(topic);
            if (result.entity != null)
                return new TopicResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Result.Ok
                };
            else
                return new TopicResponse()
                {
                    result = Result.InvalidRequest
                };
        }
    }
}

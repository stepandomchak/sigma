﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Topic
{
    public class GetTopic : SupportHandler<GetTopicRequest>
    {

        protected override Response CreateFailResponse()
        {
            return new TopicResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if(request.categoryId > 0)
                return await GetTopicByCategory();
            else
                return await GetAllTopic();
        }

        private async Task<Response> GetTopicByCategory()
        {
            var category = (await service.GetCategoryById(request.categoryId));
            if (category == null)
            {
                return new TopicsResponse()
                {
                    topics = null,
                    result = Result.CategoryNotFound
                };
            }

            var res = category.Topics.Select(c => c.ToComObject()).ToList();
            
            return new TopicsResponse()
            {
                topics = res,
                result = Result.Ok
            };
           
        }

        private async Task<Response> GetAllTopic()
        {
            var res = (await service.GetAllTopics());
            var list = new List<Com.Support.Models.Topic>();
            foreach (var item in res) list.Add(item.ToComObject());

            if (res != null)
                return new TopicsResponse()
                {
                    topics = list,
                    result = Result.Ok
                };
            return new TopicsResponse()
            {
                topics = null,
                result = Result.Exception
            };
        }
        
    }
}


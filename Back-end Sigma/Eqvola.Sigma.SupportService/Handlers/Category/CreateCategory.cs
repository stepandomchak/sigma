﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Category
{
    public class CreateCategory : SupportHandler<CreateCategoryRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (!string.IsNullOrEmpty(request.name))
            {
                var category = new Service.SupportService.Models.Category()
                {
                    Name = request.name,
                    IsEnable = request.IsEnable
                };
                var result = await service.CreateCategory(category);

                if (result.entity != null)
                {
                    if (result.message.Contains("unique"))
                    {
                        if (result.message.Contains("Name"))
                            return new CategoryResponse()
                            {
                                result = Result.ExistenCategory,
                            };
                    }
                    return new CategoryResponse()
                    {
                        entity = result.entity.ToComObject(),
                        result = Result.Ok
                    };
                }
                

            }

            return new CategoryResponse()
            {
                entity = null,
                result = Result.Exception
            };
        }
    }
}

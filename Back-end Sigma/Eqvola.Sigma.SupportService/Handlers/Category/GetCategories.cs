﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Category
{
    public class GetCategory : SupportHandler<GetCategoryRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (!string.IsNullOrEmpty(request.name))
                return await GetCategoryByName();
            else if(string.IsNullOrEmpty(request.name))
                return await GetCategoryByStatus();

            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

        private async Task<Response> GetCategoryByName()
        {
            var res = (await service.GetCategoryByName(request.name))?.ToComObject();

            if (res != null)
                return new CategoryResponse()
                {
                    entity = res,
                    result = Result.Ok
                };
            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

        private async Task<Response> GetCategoryByStatus()
        {
            var res = (await service.GetCategoryByStatus(request.IsEnable))?.Select(s => s.ToComObject()).ToList();

            if (res != null)
                return new CategoriesResponse()
                {
                    categories = res,
                    result = Result.Ok
                };
            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

    }
}

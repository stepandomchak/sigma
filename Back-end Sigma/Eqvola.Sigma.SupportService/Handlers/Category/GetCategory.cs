﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Category
{
    public class GetCategories : SupportHandler<GetCategoriesRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var res = (await service.GetCategories()).Select(c => c.ToComObject()).ToList();

            if (res != null)
                return new CategoriesResponse()
                {
                    categories = res
                };
            return new CategoriesResponse();
        }


    }
}

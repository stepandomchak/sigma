﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.SupportService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.SupportService.Handlers.Category
{
    public class UpdateCategory : SupportHandler<UpdateCategoryRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CategoryResponse()
            {
                result = Result.Exception
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.id > 0)
            {
                var category = (await service.GetCategoryById(request.id));

                if (category == null)
                    return new CategoryResponse()
                    {
                        result = Result.Exception
                    };

                if(!string.IsNullOrEmpty(request.newName))
                    category.Name = request.newName;

                category.IsEnable = request.IsEnable;

                var result = await service.UpdateCategory(category);

                if (result.entity != null)
                {
                    return new CategoryResponse()
                    {
                        entity = result.entity.ToComObject(),
                        result = Result.Ok
                    };
                }
            }

            return new CategoryResponse()
            {
                entity = null,
                result = Result.Exception
            };

        }
    }
}

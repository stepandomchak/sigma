using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.Support;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.Com.Support.Message;
using Eqvola.Sigma.Com.Support.Ticket;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.Com.Support.Attachment;
using Eqvola.Sigma.Service;
using Eqvola.Sigma.SupportService.Handlers;
using Eqvola.Sigma.SupportService.Handlers.Category;
using Eqvola.Sigma.SupportService.Handlers.Message;
using Eqvola.Sigma.SupportService.Handlers.Attachment;
using Eqvola.Sigma.SupportService.Handlers.Ticket;
using Eqvola.Sigma.SupportService.Handlers.Topic;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;

namespace Eqvola.Sigma.SupportService
{
    public partial class WorkerRole : ServiceBusRole
    {
        protected override IEnumerable<string> queues => new string[] { "support" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Sigma.Com.Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "support":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Sigma.Com.Response>>>
                    {
                        { "topic-get", (message) => { return HandlerFactory<GetTopicRequest, GetTopic>.ProccessMessage(message); } },
                        { "topic-create", (message) => {return HandlerFactory<CreateTopicRequest, CreateTopic>.ProccessMessage(message); } },
                        { "topic-update", (message) => {return HandlerFactory<UpdateTopicRequest, UpdateTopic>.ProccessMessage(message); } },
                        { "topic-delete", (message) => {return HandlerFactory<DeleteTopicRequest, DeleteTopic>.ProccessMessage(message); } },

                        { "category-get", (message) => { return HandlerFactory<GetCategoryRequest, GetCategory>.ProccessMessage(message); } },
                        { "category-get-all", (message) => { return HandlerFactory<GetCategoriesRequest, GetCategories>.ProccessMessage(message); } },
                        { "category-update", (message) => { return HandlerFactory<UpdateCategoryRequest, UpdateCategory>.ProccessMessage(message); } },
                        { "category-create", (message) => { return HandlerFactory<CreateCategoryRequest, CreateCategory>.ProccessMessage(message); } },

                        { "ticket-get", (message) => { return HandlerFactory<GetTicketRequest, GetTicket>.ProccessMessage(message); } },
                        { "ticket-create", (message) => { return HandlerFactory<CreateTicketRequest, CreateTicket>.ProccessMessage(message); } },
                        { "ticket-close", (message) => { return HandlerFactory<UpdateTicketRequest, UpdateTicket>.ProccessMessage(message); } },

                        { "message-get", (message) => {return HandlerFactory<GetMessageRequest, GetMessage>.ProccessMessage(message); } },
                        { "message-send", (message) => {return HandlerFactory<SendMessageRequest, SendMessage>.ProccessMessage(message); } },

                        { "attachment-upload", (message)=> { return HandlerFactory<UploadAttachmentRequest, UploadAttachment>.ProccessMessage(message); } },                       
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }

    }
}

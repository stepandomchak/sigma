﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetDepartmentRequest : Request
    {
        public override string QueueName => "storage";

        public override string RequestName => "department-get";

        public string name { get; set; }

        public int status { get; set; }
    }
}

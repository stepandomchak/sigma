﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class DeleteDepartmentRequest : Request
    {
        public override string QueueName => "storage";

        public override string RequestName => "department-delete";

        public string name { get; set; }
    }
}

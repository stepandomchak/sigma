﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class SetDepartmentRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "department-set";

        public string oldname { get; set; }

        public string name { get; set; }

        public IEnumerable<string> permissions { get; set; }
    }
}

﻿using Eqvola.Sigma.Com.Storage.Models;
using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Storage
{
    public class DepartmentsResponse : StorageResponse<IEnumerable<Department>>
    {
    }
}

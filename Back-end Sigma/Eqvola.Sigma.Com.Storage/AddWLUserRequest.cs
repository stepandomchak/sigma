﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class AddWLUserRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "wluser-add";

        public long userId { get; set; }
        public string wlName { get; set; }
    }
}

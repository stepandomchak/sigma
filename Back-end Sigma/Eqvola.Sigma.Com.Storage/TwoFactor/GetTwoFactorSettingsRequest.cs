﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.TwoFactor
{
    public class GetTwoFactorSettingsRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "twofactorsettings-get";

        public long userId { get; set; }
        public int actionId { get; set; }
    }
}

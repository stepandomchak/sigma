﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.TwoFactor
{
    public class GetTwoFactorActionRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "twofactoractions-get";

    }
}

﻿using Eqvola.Sigma.Com.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.TwoFactor
{
    public class SettingsResponse : StorageResponse<UserTwoFactor>
    {
        public List<UserTwoFactor> settings { get; set; }

        public SettingsResponse()
        {
            settings = new List<UserTwoFactor>();
        }
    }
}

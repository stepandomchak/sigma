﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class SetPasswordRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "password-set";

        public string email { get; set; }
        public string phone_number { get; set; }
        public string code { get; set; }
        public string newPassword { get; set; }
        public bool isHash { get; set; }
    }
}

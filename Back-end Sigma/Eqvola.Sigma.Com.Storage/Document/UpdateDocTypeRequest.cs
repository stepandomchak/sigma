﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Document
{
    public class UpdateDocTypeRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "doctype-update";

        public string name { get; set; }
        public int status { get; set; }

    }
}

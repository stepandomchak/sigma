﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Document
{
    public class CreateDocTypeRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "doctype-create";

        public string name { get; set; }

    }
}

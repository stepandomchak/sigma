﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Document
{
    public class CreateUserDocRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "userdoc-create";

        public string email { get; set; }
        public string docType { get; set; }
        public string name { get; set; }
        public string comment { get; set; }
        public byte[] data { get; set; }

    }
}

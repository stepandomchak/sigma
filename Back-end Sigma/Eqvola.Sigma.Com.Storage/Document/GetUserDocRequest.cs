﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Document
{
    public class GetUserDocRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "userdoc-get";

        public string email { get; set; }

    }
}

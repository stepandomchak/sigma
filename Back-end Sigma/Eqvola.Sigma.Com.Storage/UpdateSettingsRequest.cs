﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class UpdateSettingsRequest : Request
    {

        public override string QueueName => "storage";
        public override string RequestName => "settings-update";

        public Dictionary<string, double> settings { get; set; }
    }
}

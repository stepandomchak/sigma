﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class StorageResponse : Response
    {
        public Result result { get; set; }
    }

    public class StorageResponse<T> : StorageResponse
    {
        public T entity { get; set; }
        public string message { get; set; } = "";
    }
}

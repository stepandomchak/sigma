﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetUserRequest : Request
    {

        public override string QueueName => "storage";
        public override string RequestName => "user-get";

        public string uid { get; set; }
    }
}

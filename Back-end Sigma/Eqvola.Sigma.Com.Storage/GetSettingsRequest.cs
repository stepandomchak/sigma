﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetSettingsRequest : Request
    {

        public override string QueueName => "storage";
        public override string RequestName => "settings-get";

        public List<string> keys { get; set; }
    }
}

﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class ResetPasswordRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "password-reset-code";

        public string email { get; set; }
        public string phone_number { get; set; }
        public string code { get; set; }
    }
}

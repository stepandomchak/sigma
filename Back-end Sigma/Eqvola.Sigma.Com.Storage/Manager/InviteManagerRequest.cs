﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class InviteManagerRequest : StorageRequest
    {
        public override string RequestName => "manager-invite";

        public string email { get; set; }
        public string department { get; set; }
        public int rank { get; set; }
        public IEnumerable<string> permissions { get; set; }

    }
}

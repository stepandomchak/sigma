﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class UploadAvatarRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "upload-avatar";
        
        public string email { get; set; }
        public string url { get; set; }
    }
}

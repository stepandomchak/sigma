﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class CreateManagerRequest : Request
    {
        public override string QueueName => "storage";

        public override string RequestName => "manager-create";

        public string email { get; set; }
        public string password { get; set; }
        public string department { get; set; }
        public int rank { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string phone_code { get; set; }
        public string phone_number { get; set; }
        public string country { get; set; }
        public string address_street { get; set; }
        public string address_city { get; set; }
        public string address_zip { get; set; }
        public string time_zone { get; set; }

        public IEnumerable<string> permissions { get; set; }
        public IDictionary<string, string> messangers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetManagerRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "manager-get";

        public int? id { get; set; }
        public string uid { get; set; }
        public string password { get; set; }

        public int status { get; set; }

    }
}

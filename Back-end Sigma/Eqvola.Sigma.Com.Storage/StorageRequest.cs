﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public abstract class StorageRequest : Request
    {
        public override string QueueName => "storage";
    }
}

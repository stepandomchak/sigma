﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetWLUserRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "wluser-get";

        public long userId { get; set; }
        public string wlName { get; set; }
    }
}

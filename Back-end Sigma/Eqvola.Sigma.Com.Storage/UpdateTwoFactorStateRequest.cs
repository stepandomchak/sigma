﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public enum TwoFactorActions
    {
        Enable,
        Accept,
        Disable
    }

    public class UpdateTwoFactorStateRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "update-two-factor-state";


        public string email { get; set; }
        public TwoFactorAction isEnabled { get; set; }
        public string secretkey { get; set; }
    }
}

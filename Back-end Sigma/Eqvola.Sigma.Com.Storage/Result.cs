﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public enum Result
    {
        Ok,
        NotFound,
        InvalidRequest,
        Exception,
        NonexistentDepartment,        
        ExistenDepartment,
        ExistenEmail,
        ExistenPhone,
        DeletedDepartment,
        UserWithThisEmailNotFound
    }
}

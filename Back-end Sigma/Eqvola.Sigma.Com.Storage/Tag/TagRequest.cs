﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public enum TagTarget
    {
        Department,
        Manager
    }

    public enum TagAction
    {
        Tag,
        Untag,
        Delete,
        GetAll
    }

    public class TagRequest : Request
    {
        public override string QueueName => "storage";

        public override string RequestName => "tag";

        public TagAction action { get; set; }
        public TagTarget target { get; set; }
        public string tid { get; set; }
        public string tag { get; set; }
    }
}

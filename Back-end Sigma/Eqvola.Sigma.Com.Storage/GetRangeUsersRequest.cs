﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetRangeUsersRequest : Request
    {

        public override string QueueName => "storage";
        public override string RequestName => "user-get-range";

        public List<string> emailUsers { get; set; }
    }
}

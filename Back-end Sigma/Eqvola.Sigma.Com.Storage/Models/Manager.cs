﻿using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public enum ManagerRank : int
    {
        SystemAdmin = 1,
        Admin = 2,
        Supervisor = 4,
        Manager = 8
    }


    public enum ManagerStatus : int
    {
        Invited = 0,
        Active = 1,
        Deleted = 2
    }


    public class Manager
    {
        public virtual Department Department { get; set; }
        public User User { get; set; }
        public int Rank { get; set; }
        
        public int Status { get; set; }
    
        public string FiringReason { get; set; }
        public virtual ICollection<string> Permissions { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public virtual IDictionary<string, string> ImContacts { get; set; }
    }
}

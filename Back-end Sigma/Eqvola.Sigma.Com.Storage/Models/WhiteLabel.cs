﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public class WhiteLabel
    {
        public long Id { get; set; }
        public string name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public enum UserTwoFactorState
    {
        Confirmation = 0,
        Enabled = 1,
        Disabled = 2,
    }

    public class UserTwoFactor
    {
        public virtual User User { get; set; }

        public virtual TwoFactor TwoFactor { get; set; }

        public virtual TwoFactorAction TwoFactorAction { get; set; }

        public int State { get; set; }

        public string Secret { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public class Permission
    {
        public long Id { get; set; }
        public string PermissionName { get; set; }

        public virtual ICollection<Manager> Managers { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
    }
}

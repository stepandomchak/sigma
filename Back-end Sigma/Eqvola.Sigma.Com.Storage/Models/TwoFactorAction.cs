﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public class TwoFactorAction
    {
        public string Name { get; set; }

        public int Status { get; set; }
    }
}

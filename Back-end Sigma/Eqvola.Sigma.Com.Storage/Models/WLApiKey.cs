﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public class WLApiKey
    {
        public long Id { get; set; }

        public string Key { get; set; }
                
        public DateTime? ValidFrom { get; set; }
                
        public DateTime? ValidTo { get; set; }

        //public ICollection<Permission> Permissions { get; set; }

        public WhiteLabel Wl { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Storage.Models
{
    public enum DepartmentStatus : int
    {
        Active = 1,
        Deleted = 2        
    }

    public class Department
    {
        public long Id { get; set; }

        public string Name { get; set; }
        public Department Parent { get; set; }
        public int Status { get; set; }

        public IEnumerable<Manager> Managers { get; set; }

        public IEnumerable<string> Permissions { get; set; }
        public IEnumerable<string> Tags { get; set; }
    }
}

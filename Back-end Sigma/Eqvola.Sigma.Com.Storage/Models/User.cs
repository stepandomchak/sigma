﻿namespace Eqvola.Sigma.Com.Storage.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string TimeZone { get; set; }
        public bool IsTwoFactorEnabled { get; set; }
        public string SecretKey { get; set; }
        public string AvatarUrl { get; set; }
        public bool IsVerified { get; set; }
    }
}

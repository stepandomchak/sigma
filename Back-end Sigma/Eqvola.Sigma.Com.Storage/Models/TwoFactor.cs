﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Storage.Models
{

    public enum TwoFactorStatus
    {
        Enabled = 1,
        Disabled = 2
    }

    public class TwoFactor
    {
        public string Type { get; set; }

        public int Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Eqvola.Sigma.Com.Storage
{
    public class GetWLApiKeyRequest : Request
    {
        public override string QueueName => "storage";
        public override string RequestName => "wlkey-get";

        public long userId { get; set; }
        public long wlId { get; set; }
    }
}
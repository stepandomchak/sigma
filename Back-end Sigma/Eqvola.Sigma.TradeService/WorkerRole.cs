using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.TradeService;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace Eqvola.Sigma.TradeService
{
    public class WorkerRole : ServiceBusRole<Service.TradeService.Service>
    {
        private const string _tradeServiceQueueName = "trade-service";
        private const string _tradeHandlerName = "trade";

        protected override IEnumerable<string> queues => new string[] { _tradeServiceQueueName };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case _tradeServiceQueueName:
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        { _tradeHandlerName, Trade }
                    };
                default:
                    throw new ArgumentException(nameof(queueName));
            }
        }

        private async Task<Response> Trade(BrokeredMessage message)
        {
            Trace.Write("Trade");

            TradeRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TradeRequest>(json);
            }
            await service.Trade(request);
            return null;
        }
    }
}

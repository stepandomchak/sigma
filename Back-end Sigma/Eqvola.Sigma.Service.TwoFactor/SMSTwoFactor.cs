﻿using Eqvola.Sigma.Com.Twilio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.TwoFactor
{
    public class SMSTwoFactor : BaseTwoFactor
    {

        public override string Type => "Sms";

        public async override Task<string> SendCodeToUser(string number, string action, string secret)
        {
            
            var request = new SendMessageRequest()
            {
                PhoneNumber = number,
                Message = $"Code: " + secret
            };

            var response = await request.GetResponse<SendMessageResponse>();

            return secret;
        }

        public override bool VerifyCode(string secret, string code)
        {

            //////////   Clear Secret Code

            if (secret.Equals(code))
            {
                return true;
            }
            return false;
        }

    }
}

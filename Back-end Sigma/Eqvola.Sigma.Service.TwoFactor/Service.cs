﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.TwoFactor
{

    public class Service
    {
        private BaseTwoFactor GetWorker(string twoFactorType)
        {
            switch (twoFactorType)
            {
                case "Google2FA":
                    {
                        return new GoogleTwoFactor();
                    }
                case "Sms":
                    {
                        return new SMSTwoFactor();
                    }
                default:
                    {
                        throw new NotImplementedException("Two Factor type is not implemented.");
                    }
            }
            return null;
        }


        public async Task<TwoFactorResponse> EnableTwoFactor(string email, string action, string type, Dictionary<string, double> options)
        {
            var service = new Storage.Service();

            var user = await service.GetUserByEmail(email);
            if (user == null)
                return new TwoFactorResponse() { result = TwoFactorResult.WrongEmail };

            var twoFactor = await service.GetTwoFactorByType(type);
            if (twoFactor == null)
                return new TwoFactorResponse() { result = TwoFactorResult.WrongTwoFactor };

            var twoFactorAction = await service.GetActionByName(action);
            if (twoFactorAction == null)
                return new TwoFactorResponse() { result = TwoFactorResult.WrongTwoFactor };

            if (twoFactorAction.Status != 1)
                return new TwoFactorResponse() { result = TwoFactorResult.ActionIsNotEnabled };

            var userSetting = await service.GetSettingsByUserAction(user.Id, twoFactorAction.Id);

            if (userSetting != null && userSetting.State == 1)
            {
                return new TwoFactorResponse() { result = TwoFactorResult.AlredyEnabled };
            }
            if (userSetting != null && userSetting.State == 0)
            {
                foreach (var op in userSetting.UserTwoFactorOptions.ToArray())
                {
                    var delOption = await service.DeleteOptions(op);

                    if (delOption == null)
                        return new TwoFactorResponse() { result = TwoFactorResult.Exception };
                }

                var deleteResponse = await service.DeleteSetting(userSetting);
                if(deleteResponse == null)
                    return new TwoFactorResponse() { result = TwoFactorResult.Exception };
            }

            var twoFactorOptions = await service.GetActionOptions(twoFactorAction.Name);

            foreach (var option in twoFactorOptions)
            {
                if (!options.ContainsKey(option.Key))
                {
                    return new TwoFactorResponse() { result = TwoFactorResult.WrongOptions };
                }
            }
            
            var userSettings = new UserTwoFactor()
            {
                User = user,
                TwoFactor = twoFactor,
                TwoFactorAction = twoFactorAction,
                State = 0,
                Secret = GetWorker(type).GenerateSecret(),
            };
            var response = await service.CreateSetting(userSettings);

            foreach (var t in options)
            {
                var option = await service.GetActionKeyOption(action, t.Key);
                if (option == null)
                    continue;
                UserTwoFactorOption twoFactorOption = new UserTwoFactorOption()
                {
                    Key = option.Key,
                    Value = t.Value,
                    UserTwoFactor = response.entity
                };

                var addOption = await service.CreateUserTwoFactorOption(twoFactorOption);
                if (addOption.entity == null)
                    return new TwoFactorResponse()
                    {
                        result = TwoFactorResult.Exception
                    };
            }

            if (type.Equals("Google2FA"))
            {
                var url = await GetWorker(type).SendCodeToUser(user.Email, twoFactorAction.Name, userSettings.Secret);
                return new TwoFactorResponse()
                {
                    entity = response.entity,
                    url = url,
                    result = TwoFactorResult.Ok
                };

            }
            else if (type.Equals("Sms"))
            {
                var code = GetWorker(type).SendCodeToUser(user.Phone_code + user.Phone_number, twoFactorAction.Name, userSettings.Secret);
                return new TwoFactorResponse()
                {
                    entity = response.entity,
                    result = TwoFactorResult.Ok
                };
            }

            return new TwoFactorResponse() { result = TwoFactorResult.Exception };
        }

        public async Task<TwoFactorResponse> DisableTwoFactor(long id)
        {

            var service = new Storage.Service();
            var userTwoFactor = await service.GetUserTwoFactorById(id);

            if (userTwoFactor == null)
            {
                return new TwoFactorResponse() { result = TwoFactorResult.ActionIsNotEnabled };
            }

            if(userTwoFactor.State == 2)
            {
                if (userTwoFactor.TwoFactor.Type.Equals("Sms"))
                {
                    userTwoFactor.Secret = GetWorker(userTwoFactor.TwoFactor.Type).GenerateSecret();
                    await service.UpdateSetting(userTwoFactor);
                    string send = await GetWorker(userTwoFactor.TwoFactor.Type).SendCodeToUser(userTwoFactor.User.Phone_code + userTwoFactor.User.Phone_number, userTwoFactor.TwoFactorAction.Name, userTwoFactor.Secret);
                }
               return new TwoFactorResponse() { result = TwoFactorResult.SendCode };
            }
            else if(userTwoFactor.State == 1)
            {
                userTwoFactor.State = 2;
                var res = await service.UpdateSetting(userTwoFactor);

                if (res.entity != null)
                {
                    if (userTwoFactor.TwoFactor.Type.Equals("Sms"))
                    {
                        userTwoFactor.Secret = GetWorker(userTwoFactor.TwoFactor.Type).GenerateSecret();
                        await service.UpdateSetting(userTwoFactor);
                        string send = await GetWorker(userTwoFactor.TwoFactor.Type).SendCodeToUser(userTwoFactor.User.Phone_code + userTwoFactor.User.Phone_number, userTwoFactor.TwoFactorAction.Name, userTwoFactor.Secret);
                    }
                    return new TwoFactorResponse() { result = TwoFactorResult.SendCode };
                }
                return new TwoFactorResponse() { result = TwoFactorResult.WaitForAccept };
            }
            else
            {
                return new TwoFactorResponse() { result = TwoFactorResult.WaitForAccept };
            }
        }

        public async Task<TwoFactorResponse> AcceptTwoFactorAction(long id, string code)
        {
            var service = new Storage.Service();
            var userTwoFactor = await service.GetUserTwoFactorById(id);

            if(userTwoFactor == null)
            {
                return new TwoFactorResponse() { result = TwoFactorResult.ActionIsNotEnabled };
            }

            switch (userTwoFactor.State)
            {
                case 0:
                    {
                        if (userTwoFactor.TwoFactor.Type.Equals("Google2FA"))
                        {
                            if (GetWorker(userTwoFactor.TwoFactor.Type).VerifyCode(userTwoFactor.Secret, code))
                            {
                                userTwoFactor.State = 1;
                                await service.UpdateSetting(userTwoFactor);
                                return new TwoFactorResponse() { result = TwoFactorResult.Ok };
                            }
                        }
                        else if (userTwoFactor.TwoFactor.Type.Equals("Sms"))
                        {
                            if (string.IsNullOrEmpty(code))
                            {
                                userTwoFactor.Secret = GetWorker(userTwoFactor.TwoFactor.Type).GenerateSecret();
                                await service.UpdateSetting(userTwoFactor);
                                string send = await GetWorker(userTwoFactor.TwoFactor.Type).SendCodeToUser(userTwoFactor.User.Phone_code + userTwoFactor.User.Phone_number, userTwoFactor.TwoFactorAction.Name, userTwoFactor.Secret);
                                return new TwoFactorResponse() { result = TwoFactorResult.SendCode };
                            }
                            else
                            {
                                if (GetWorker(userTwoFactor.TwoFactor.Type).VerifyCode(userTwoFactor.Secret, code))
                                {
                                    userTwoFactor.State = 1;
                                    await service.UpdateSetting(userTwoFactor);
                                    return new TwoFactorResponse() { result = TwoFactorResult.Ok };
                                }
                            }

                        }
                        return new TwoFactorResponse() { result = TwoFactorResult.WrongCode };
                    }
                case 1:
                    {
                        if (userTwoFactor.TwoFactor.Type.Equals("Google2FA"))
                        {
                            if (GetWorker(userTwoFactor.TwoFactor.Type).VerifyCode(userTwoFactor.Secret, code))
                            {
                                return new TwoFactorResponse() { result = TwoFactorResult.Ok };
                            }
                            return new TwoFactorResponse() { result = TwoFactorResult.WrongCode };                            
                        }
                        else if(userTwoFactor.TwoFactor.Type.Equals("Sms"))
                        {
                            if (string.IsNullOrEmpty(code))
                            {
                                userTwoFactor.Secret = GetWorker(userTwoFactor.TwoFactor.Type).GenerateSecret();
                                await service.UpdateSetting(userTwoFactor);
                                string send = await GetWorker(userTwoFactor.TwoFactor.Type).SendCodeToUser(userTwoFactor.User.Phone_code + userTwoFactor.User.Phone_number, userTwoFactor.TwoFactorAction.Name, userTwoFactor.Secret);
                                return new TwoFactorResponse() { result = TwoFactorResult.SendCode };
                            }
                            else
                            {
                                if (GetWorker(userTwoFactor.TwoFactor.Type).VerifyCode(userTwoFactor.Secret, code))
                                {
                                    return new TwoFactorResponse() { result = TwoFactorResult.Ok };
                                }                                
                            }

                        }
                        return new TwoFactorResponse() { result = TwoFactorResult.WrongCode };
                    }
                case 2:
                    {
                        if (userTwoFactor.TwoFactor.Type.Equals("Google2FA"))
                        {
                            if (GetWorker(userTwoFactor.TwoFactor.Type).VerifyCode(userTwoFactor.Secret, code))
                            {
                                foreach (var op in userTwoFactor.UserTwoFactorOptions.ToArray())
                                {
                                    var delOption = await service.DeleteOptions(op);

                                    if (delOption == null)
                                        return new TwoFactorResponse() { result = TwoFactorResult.Exception };
                                }

                                var response = await service.DeleteSetting(userTwoFactor);
                                if (response == null)
                                {
                                    return new TwoFactorResponse() { result = TwoFactorResult.Exception };
                                }
                                return new TwoFactorResponse() { result = TwoFactorResult.Ok };
                            }
                                                   
                        }
                        else if (userTwoFactor.TwoFactor.Type.Equals("Sms"))
                        {
                            if (string.IsNullOrEmpty(code))
                            {
                                userTwoFactor.Secret = GetWorker(userTwoFactor.TwoFactor.Type).GenerateSecret();
                                await service.UpdateSetting(userTwoFactor);
                                string send = await GetWorker(userTwoFactor.TwoFactor.Type).SendCodeToUser(userTwoFactor.User.Phone_code + userTwoFactor.User.Phone_number, userTwoFactor.TwoFactorAction.Name, userTwoFactor.Secret);
                                return new TwoFactorResponse() { result = TwoFactorResult.SendCode };
                            }
                            else
                            {
                                if (GetWorker(userTwoFactor.TwoFactor.Type).VerifyCode(userTwoFactor.Secret, code))
                                {
                                    foreach (var op in userTwoFactor.UserTwoFactorOptions.ToArray())
                                    {
                                        var delOption = await service.DeleteOptions(op);

                                        if (delOption == null)
                                            return new TwoFactorResponse() { result = TwoFactorResult.Exception };
                                    }

                                    var response = await service.DeleteSetting(userTwoFactor);
                                    if (response == null)
                                    {
                                        return new TwoFactorResponse() { result = TwoFactorResult.Exception };
                                    }
                                    return new TwoFactorResponse() { result = TwoFactorResult.Ok };
                                }                            
                            }
                        }
                        return new TwoFactorResponse() { result = TwoFactorResult.WrongCode };
                    }
            }
            return new TwoFactorResponse() { result = TwoFactorResult.Ok };
        }

    }
}

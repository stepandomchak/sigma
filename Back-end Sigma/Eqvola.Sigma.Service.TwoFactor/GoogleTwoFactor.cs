﻿using Eqvola.Sigma.Com.Email;
using Google.Authenticator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.TwoFactor
{
    public class GoogleTwoFactor : BaseTwoFactor
    {
        public override string Type => "Google2FA";

        public async override Task<string> SendCodeToUser(string uid, string action, string secret)
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();

            var setupInfo = tfa.GenerateSetupCode("Sigma " + action, uid, secret, 300, 300);

            string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
            string manualEntrySetupCode = setupInfo.ManualEntryKey;

            return qrCodeImageUrl;
        }

        public override bool VerifyCode(string secret, string code)
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if (tfa.ValidateTwoFactorPIN(secret, code))
            {
                return true;
            }
            return false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.TwoFactor
{
    public abstract class BaseTwoFactor
    {

        public abstract string Type { get; }


        public virtual string GenerateSecret()
    {
            if (Type == "Google2FA")
                return RandomString();
            else if (Type == "Sms")
                return RandomPin();
            else
                return "";
        }

        protected string RandomString(int count = 10)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, count)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        protected string RandomPin(int count = 6)
        {
            Random random = new Random();
            return (random.Next(100000, 1000000)).ToString();
        }

        public abstract bool VerifyCode(string secret, string code);

        public abstract Task<string> SendCodeToUser(string uid, string action, string secret);
    }
}

﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.TwoFactor
{
    public enum TwoFactorResult
    {
        Ok,
        WaitForAccept,
        WrongEmail,
        WrongTwoFactor,
        WrongAction,
        ActionIsNotEnabled,
        AlredyEnabled,
        WrongOptions,
        Exception,
        WrongCode,
        SendCode
    }

    public class TwoFactorResponse
    {
        public string url { get; set; }
        public TwoFactorResult result { get; set; }
        public UserTwoFactor entity { get; set; }
    }
}

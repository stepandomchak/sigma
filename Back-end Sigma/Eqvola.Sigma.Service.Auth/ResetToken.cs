﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Auth
{
    public class ResetToken
    {
        public string email { get; set; }
        public DateTime createDate { get; set; }
        public DateTime expiresDate { get; set; }

        public ResetToken(string email)
        {
            this.email = email;
            this.expiresDate = DateTime.UtcNow.AddMinutes(30);
            this.createDate = DateTime.UtcNow;
        }
    }
}

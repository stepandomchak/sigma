﻿using Eqvola.Sigma.Com.Auth;
using Eqvola.Sigma.Com.Auth.TwoFactor;
using Eqvola.Sigma.Com.Email;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.Com.Twilio;
using Google.Authenticator;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Auth
{
    public class Service
    {
        public async Task<LoginResponse> Login(string uid, string password, string code, string headers, string key)
        {
            var request = new GetManagerRequest()
            {
                uid = uid,
                password = password
            };

            var response = await request.GetResponse<ManagerResponse>();
            if (response.result == Result.Ok)
            {
                return await CheckStatus(response.entity, code, headers, key);
            }
            return new LoginResponse() { token = "", message = "No Such Manager", status = 0};
        }

        private async Task<LoginResponse> CheckStatus(Manager manager, string code, string headers, string key)
        {
            switch (manager.Status)
            {
                case (int)ManagerStatus.Invited:
                    {
                        if (manager.Department.Status == (int)DepartmentStatus.Deleted)
                            return new LoginResponse() { token = "", message = "Department Was Deleted", status = 3 };
                        else
                            return new LoginResponse() { token = await CreateToken(headers, manager, key), message = "", status = 1 };
                    }
                case (int)ManagerStatus.Active:
                    {
                        if (manager.Department.Status == (int)DepartmentStatus.Deleted)
                            return new LoginResponse() { token = "", message = "Department Was Deleted", status = 3 };

                        if (manager.User.IsTwoFactorEnabled)
                        {
                            if (string.IsNullOrEmpty(code))
                            {
                                return new LoginResponse() { token = "", message = "Code not found", status = 4 };
                            }
                            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                            if (!tfa.ValidateTwoFactorPIN(manager.User.SecretKey, code))
                            {
                                return new LoginResponse() { token = "", message = "Wrong code", status = 5 };
                            }
                        }
                        var token = await CreateToken(headers, manager, key);
                        return new LoginResponse() { token = token, message = "", status = 1 };
                    }
                case (int)ManagerStatus.Deleted:
                    {
                        return new LoginResponse() { token = "", message = manager.FiringReason, status = 2 };
                    }
                default:
                    return new LoginResponse() { token = "", message = "Undefind status", status = 7 };
            }

        }

        private async Task<string> CreateToken(string headers, object payload, string key)
        {
            return await Task.Run(() =>
            {
                return TokenManager.Encode<object>(headers, payload, Encoding.UTF8.GetBytes(key));
            });
        }

        public async Task<StorageResponse<string>> ResetPassword(string email, string phone_number)
        {
            Random generator = new Random();
            string code = generator.Next(100000, 1000000).ToString();

            var saveCodeRequest = new ResetPasswordRequest() { email = email, phone_number = phone_number, code = code };
            var saveCodeResponse = await saveCodeRequest.GetResponse<ResetPasswordResponse>();

            if(saveCodeResponse != null)
            {
                if (saveCodeResponse.result == Result.Ok)
                {
                    var request = new SendMessageRequest()
                    {
                        PhoneNumber = phone_number,
                        Message = $"Code: {code}"
                    };
                    await request.Send();
                }

                return new StorageResponse<string>() { result = saveCodeResponse.result, message = saveCodeResponse.message };
            }
           
            return new StorageResponse<string>() { result = Result.Exception};
        }

        public async Task<StorageResponse<string>> AcceptResetPassword(string email, string phone_number, string code, string newPassword, bool isHash = false)
        {
            var requestReset = new SetPasswordRequest()
            {
                email = email,
                phone_number = phone_number,
                code = code,
                newPassword = newPassword,
                isHash = isHash
            };
            var responseReset = await requestReset.GetResponse<SetPasswordResponse>();
            
            return new StorageResponse<string>() { result = responseReset.result, message = responseReset.message };
        }

        public async Task<StorageResponse<string>> ChangePassword(string email, string password)
        {
            var requestManager = new GetManagerRequest()
            {
                uid = email,
                password = password
            };
            var responseManager = await requestManager.GetResponse<ManagerResponse>();

            if(responseManager.result == Result.Ok)
            {
                return await ResetPassword(responseManager.entity.User.Email, responseManager.entity.User.PhoneCode + responseManager.entity.User.PhoneNumber);
            }

            return new StorageResponse<string>() { result = responseManager.result, message = responseManager.message };
        }

        public async Task<StorageResponse<string>> AcceptChangePassword(string email, string newPassword, string code, bool isHash)
        {
            var requestManager = new GetManagerRequest()
            {
                uid = email
            };
            var responseManager = await requestManager.GetResponse<ManagerResponse>();

            if (responseManager.result == Result.Ok)
            {
                return await AcceptResetPassword(responseManager.entity.User.Email, responseManager.entity.User.PhoneCode + responseManager.entity.User.PhoneNumber, code, newPassword, isHash);
            }

            return new StorageResponse<string>() { result = responseManager.result, message = responseManager.message };
        }

        public async Task<TwoFactorEnableResponse> TwoFactorEnable(string uid)
        {
            var userRequest = new GetUserRequest()
            {
                uid = uid
            };

            var userResponse = await userRequest.GetResponse<UserResponse>();

            if(userResponse == null || userResponse.entity == null)
            {
                return new TwoFactorEnableResponse() { status = false, message = "User not found", manualCode = "", url = "" };
            }
            if (userResponse.entity.IsTwoFactorEnabled)
            {
                return new TwoFactorEnableResponse() { status = false, message = "Two factor already enabled", manualCode = "", url = "" };
            }

            string secret = RandomString();

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            var setupInfo = tfa.GenerateSetupCode("Sigma", uid, secret, 300, 300);

            string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
            Trace.WriteLine("2FA ulr: " + qrCodeImageUrl);

            string manualEntrySetupCode = setupInfo.ManualEntryKey;
            Trace.WriteLine("2FA manual: " + manualEntrySetupCode);

            var request = new UpdateTwoFactorStateRequest()
            {
                email = uid,
                //isEnabled = Com.Storage.TwoFactorActions.Enable,
                secretkey = secret
            };
            var response = await request.GetResponse<UpdateTwoFactorStateResponse>();

            Trace.WriteLine("2FA after storage update: ");

            if (response.result == Result.Ok)
                return new TwoFactorEnableResponse() { status = true, url = qrCodeImageUrl, manualCode = manualEntrySetupCode };
            else
                return new TwoFactorEnableResponse() { status = false, message = "Exception" };
        }

        public async Task<TwoFactorEnableResponse> TwoFactorAcceptEnable(string uid, string code)
        {
            var userRequest = new GetUserRequest()
            {
                uid = uid
            };

            var userResponse = await userRequest.GetResponse<UserResponse>();

            if (userResponse == null || userResponse.entity == null)
            {
                return new TwoFactorEnableResponse() { status = false, message = "User not found", manualCode = "", url = "" };
            }

            if (userResponse.entity.IsTwoFactorEnabled)
            {
                return new TwoFactorEnableResponse() { status = false, message = "Two factor already enabled", manualCode = "", url = "" };
            }

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if (!tfa.ValidateTwoFactorPIN(userResponse.entity.SecretKey, code))
            {
                return new TwoFactorEnableResponse() { status = false, message = "Wrong code" };
            }

            var request = new UpdateTwoFactorStateRequest()
            {
                email = uid,
                //isEnabled = Com.Storage.TwoFactorAction.Accept
            };
            var response = await request.GetResponse<UpdateTwoFactorStateResponse>();

            if (response.result == Result.Ok)
                return new TwoFactorEnableResponse() { status = true };
            else
                return new TwoFactorEnableResponse() { status = false, message = "Exception" };
        }
        public async Task<TwoFactorDisableResponse> TwoFactorDisable(string uid, string code)
        {
            var userRequest = new GetUserRequest()
            {
                uid = uid
            };

            var userResponse = await userRequest.GetResponse<UserResponse>();

            if (userResponse == null || userResponse.entity == null)
            {
                return new TwoFactorDisableResponse() { status = false, message = "User not found"};
            }

            if (!userResponse.entity.IsTwoFactorEnabled)
            {
                return new TwoFactorDisableResponse() { status = false, message = "Two factor already disabled" };
            }

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if(!tfa.ValidateTwoFactorPIN(userResponse.entity.SecretKey, code))
            {
                return new TwoFactorDisableResponse() { status = false, message = "Wrong code" };
            }
            
            var request = new UpdateTwoFactorStateRequest()
            {
                email = uid,
                //isEnabled = Com.Storage.TwoFactorAction.Disable,
                secretkey = ""
            };

            var response = await request.GetResponse<UpdateTwoFactorStateResponse>();

            return new TwoFactorDisableResponse() { status = true };
        }

        private string RandomString(int count = 10)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, count)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}

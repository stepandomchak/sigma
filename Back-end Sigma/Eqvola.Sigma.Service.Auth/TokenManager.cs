﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;
using System;
using System.Text;
using Microsoft.Azure;
using System.Linq;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.ComponentModel;

namespace Eqvola.Sigma.Service.Auth
{
    public static class TokenManager
    {
        #region Private Fields
        private static byte[] _secretKey;
        private static IJwtEncoder _encoder;
        private static IJwtDecoder _decoder;
        #endregion

        #region Private Properties
        private static IJwtEncoder Encoder
        {
            get
            {
                if (_encoder == null)
                    _encoder = CreateEncoder();
                return _encoder;
            }
        }

        private static IJwtDecoder Decoder
        {
            get
            {
                if (_decoder == null)
                    _decoder = CreateDecoder();
                return _decoder;
            }
        }

        private static byte[] SecretKey
        {
            get
            {
                if (_secretKey == null)
                    _secretKey = Encoding.UTF8.GetBytes(CloudConfigurationManager.GetSetting("JWT.Secret"));
                return _secretKey;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Encode entity to JWT
        /// </summary>
        /// <param name="entity">object to be encoded</param>
        /// <returns>JWT string</returns>
        public static string Encode<T>(string extraHeaders, T payload, byte[] key) where T : class
        {
            var eHeaders = KeepExtraHeaders(extraHeaders);
            return Encoder.Encode(eHeaders, payload, key);
            
        }

        /// <summary>
        /// Decode JWT string to Model
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="token">JWT string</param>
        /// <returns>Object with Type<typeparamref name="T"/></returns>
        public static T Decode<T>(string token) where T : class
        {
            try
            {
                var payload = Decoder.Decode(token, SecretKey, true);
                return JsonConvert.DeserializeObject<T>(payload);
            }
            catch(Exception e)
            {
                return null;
            }
        }
        #endregion

        #region Private Methods

        #region Helper JWT methods
        private static IDictionary<string, object> KeepExtraHeaders(string extraHeaders)
        {
            IDictionary<string, object> headers = new Dictionary<string, object>();
            if (extraHeaders != null)
            {
                var objHeaders = JsonConvert.DeserializeObject<Dictionary<string, object>>(extraHeaders);
                if (objHeaders.Keys.Count > 0)
                {
                    foreach (var header in objHeaders)
                    {
                        headers.Add(header.Key, header.Value);
                    }
                }
            }

            return headers;
        }
        #endregion
        #region Init Methods
        private static IJwtEncoder CreateEncoder()
        {
            return new JwtEncoder(
                new HMACSHA256Algorithm(),
                new JsonNetSerializer(),
                new JwtBase64UrlEncoder());
        }

        private static IJwtDecoder CreateDecoder()
        {
            var serializer = new JsonNetSerializer();
            var provider = new UtcDateTimeProvider();
            var validator = new JwtValidator(serializer, provider);

            var urlEncoder = new JwtBase64UrlEncoder();

            return new JwtDecoder(serializer, validator, urlEncoder);
        }
        #endregion

        #endregion
    }
}

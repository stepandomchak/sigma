﻿using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Eqvola.Sigma.Service.Twilio
{
    public class Service
    {
        public async Task<string> SendMessage(string phoneNumber, string messageBody)
        {
            string accountSid = CloudConfigurationManager.GetSetting("TwilioAccoundSid");
            string authToken = CloudConfigurationManager.GetSetting("TwilioAuthToken");

            try
            {
                TwilioClient.Init(accountSid, authToken);

                var to = new PhoneNumber(phoneNumber);
                var from = new PhoneNumber(CloudConfigurationManager.GetSetting("TwilioFromNumber"));

                var message = await MessageResource.CreateAsync(
                    body: messageBody,
                    from: from,
                    to: to
                );

                Trace.WriteLine($"Twilio: Message Sended, sid: { message.Sid}");
                return $"Message Sended, sid: {message.Sid}";
            } catch(Exception ex)
            {
                Trace.WriteLine("Twilio: exception " + ex.Message);
                return $"Error {ex.Message}";
            }

        }
    }
}

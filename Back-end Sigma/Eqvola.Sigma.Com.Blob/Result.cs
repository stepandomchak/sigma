﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Blob
{
    public enum Result
    {
        Ok,
        BlobReferenceIsEmpty,
        BlobContainerIsEmpty,
        FileIsEmpty,
        NotFound,
        Exception,
    }
}

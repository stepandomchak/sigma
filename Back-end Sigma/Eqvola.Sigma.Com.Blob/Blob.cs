﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Blob
{
    public class Blob
    {
        public string BlobReference { get; set; }
        public string MimeType { get; set; }
        public byte[] File { get; set; }
        public string BlobContainer { get; set; }
    }
}

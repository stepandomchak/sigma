﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Blob
{
    public class BlobResponse : Response
    {
        public Result result { get; set; }
    }

    public class BlobResponse<T> : BlobResponse
    {
        public T entity { get; set; }
        public string message { get; set; } = "";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Blob
{
    public class UploadFileRequest : Request
    {
        public override string QueueName => "blob";
        public override string RequestName => "upload-file";

        public string BlobReference { get; set; }
        public string MimeType { get; set; }
        public byte[] File { get; set; }
        public string BlobContainer { get; set; }
    }
}

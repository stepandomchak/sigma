﻿using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StandAlone.Common
{
    public abstract class WorkRoleWrapper<T> where T : RoleEntryPoint, new()
    {
        protected static void Run()
        {
            Trace.Listeners.Add(new Eqvola.Sigma.StandAlone.Common.ConsoleTraceListener());

            T role = new T();
            Task task = Task.Run(() =>
            {
                role.Run();
            });

            Console.ReadLine();
            role.OnStop();
            Console.ReadLine();
        }
    }
}

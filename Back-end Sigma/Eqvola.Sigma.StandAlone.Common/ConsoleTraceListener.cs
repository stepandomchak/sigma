﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Eqvola.Sigma.StandAlone.Common
{
    public class ConsoleTraceListener : TraceListener
    {
        public ConsoleTraceListener()
        {

        }

        public override void Write(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                Console.Write(message);
            else
                ParseMessage(message, (m) =>
                {
                    Console.Write(m);
                });
        }

        public override void WriteLine(string message)
        {
            if (Trace.IndentLevel > 0) Console.ForegroundColor = ConsoleColor.DarkGray;

            WriteIndent();
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private void ParseMessage(string message, Action<string> action)
        {
            try
            {
                DateTime t = DateTime.UtcNow;

                string[] parts = message.Split(' ');
                string[] sparts = parts[1].Split(':');
                switch(sparts[0])
                {
                    case "Information":
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        action($"[INF] {t.ToString("HH:mm:ss")}.{t.Millisecond.ToString("000")} ");
                        break;
                    case "Warning":
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        action($"[WRN] {t.ToString("HH:mm:ss")}.{t.Millisecond.ToString("000")} ");
                        break;
                    case "Error":
                        Console.ForegroundColor = ConsoleColor.Red;
                        action($"[ERR] {t.ToString("HH:mm:ss")}.{t.Millisecond.ToString("000")} ");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        action($"{message}");
                        break;
                }
            }
            catch(Exception)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                action(message);
            }
        }
    }
}

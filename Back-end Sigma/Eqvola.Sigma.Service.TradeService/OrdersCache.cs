﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;

namespace Eqvola.Sigma.Service.TradeService
{
    internal sealed class OrdersCache
    {
        internal class Entry
        {
            private readonly object _rootSync = new object();
            private int _referenceCount = 0;
            private Order _order = null;
            private long _orderId;

            internal Order Order
            {
                get
                {
                    lock (_rootSync)
                    {
                        if (_order == null)
                        {
                            FetchOrder().Wait();
                        }
                        return _order;
                    }
                }
            }

            public Entry(long orderId) => _orderId = orderId;

            internal void IncrementReferenceCount()
            {
                lock (_rootSync)
                {
                    _referenceCount++;
                }
            }

            internal bool DecrementReferenceCount()
            {
                lock (_rootSync)
                {
                    _referenceCount--;
                    return _referenceCount < 1;
                }
            }

            private async Task FetchOrder()
            {
                GetOrderRequest getOrderRequest = new GetOrderRequest
                {
                    id = _orderId
                };
                OrderResponse orderResponse = await getOrderRequest
                    .GetResponse<OrderResponse>()
                    .ConfigureAwait(false);
                if (orderResponse.result == Result.Ok)
                {
                    _order = orderResponse.entity;
                }
                else
                {
                    throw new ArgumentException($"Order with ID {_orderId} not found.");
                }
            }
        }

        private ConcurrentDictionary<long, Entry> _orderEntries = new ConcurrentDictionary<long, Entry>();

        public static OrdersCache Instance { get; } = new OrdersCache();

        private OrdersCache() { }

        internal void LockOrder(Entry orderEntry) => orderEntry.IncrementReferenceCount();

        internal void ReleaseOrder(Entry orderEntry)
        {
            if (orderEntry.DecrementReferenceCount())
            {
                _orderEntries.TryRemove(orderEntry.Order.Id, out _);
            }
        }

        public OrderIterator GetTaker(long orderId)
        {
            if (!_orderEntries.ContainsKey(orderId))
            {
                CacheOrders(new long[] { orderId });
            }

            IEnumerable<Entry> orderEntries = new Entry[] { _orderEntries[orderId] };
            return new OrderIterator(orderEntries);
        }

        public async Task<OrderIterator> GetMakers(Order taker)
        {
            IEnumerable<Entry> orderEntries = null;
            IEnumerable<long> makerIds = await FetchMakers(taker.Id);
            if (makerIds != null)
            {
                orderEntries = _orderEntries
                    .Where(item => makerIds.Contains(item.Key))
                    .Select(item => item.Value);

                Func<Entry, double> orderKeySelector = item => item.Order.Price;
                if (taker.OrderType == 1)    // Buy.
                {
                    orderEntries = orderEntries.OrderBy(orderKeySelector);
                }
                else if (taker.OrderType == 2)    // Sell.
                {
                    orderEntries = orderEntries.OrderByDescending(orderKeySelector);
                }

                orderEntries = (orderEntries as IOrderedEnumerable<Entry>).ThenBy(item => item.Order.CreationTime);
            }

            return new OrderIterator(orderEntries);
        }

        private async Task<IEnumerable<long>> FetchMakers(long takerId)
        {
            GetSuitOrderRequest getSuitOrderRequest = new GetSuitOrderRequest
            {
                orderId = takerId
            };
            SuitOrdersResponse suitOrdersResponse = await getSuitOrderRequest.GetResponse<SuitOrdersResponse>();
            if (suitOrdersResponse.result != Result.Ok)
            {
                return null;
            }

            List<long> orderIds = suitOrdersResponse.ordersId.ToList();
            if (orderIds.Count == 0)
            {
                return null;
            }

            List<long> newOrderIds = orderIds
                .Except(_orderEntries.Select(item => item.Key))
                .ToList();
            if (newOrderIds.Count > 0)
            {
                CacheOrders(newOrderIds);
            }

            return orderIds;
        }

        private void CacheOrders(IEnumerable<long> orderIds)
        {
            Dictionary<long, Entry> newOrderEntries = orderIds
                .Except(_orderEntries.Keys)
                .ToDictionary(key => key, element => new Entry(element));
            foreach (var item in newOrderEntries)
            {
                _orderEntries.TryAdd(item.Key, item.Value);
            }
        }
    }
}

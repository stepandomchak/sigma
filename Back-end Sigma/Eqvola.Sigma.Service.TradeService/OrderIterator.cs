﻿using System.Collections;
using System.Collections.Generic;
using Eqvola.Sigma.Com.OrderStorage.Models;

namespace Eqvola.Sigma.Service.TradeService
{
    internal sealed class OrderIterator : IEnumerator<Order>
    {
        private IEnumerable<OrdersCache.Entry> _orderEntries;
        private IEnumerator<OrdersCache.Entry> _enumerator;

        public OrderIterator(IEnumerable<OrdersCache.Entry> orderEntries = null)
        {
            if (orderEntries != null)
            {
                _orderEntries = orderEntries;

                foreach (OrdersCache.Entry orderEntry in _orderEntries)
                {
                    OrdersCache.Instance.LockOrder(orderEntry);
                }
            }
            else
            {
                _orderEntries = new OrdersCache.Entry[0];
            }

            _enumerator = _orderEntries.GetEnumerator();
        }

        public Order Current => _enumerator.Current.Order;

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            foreach (OrdersCache.Entry orderEntry in _orderEntries)
            {
                OrdersCache.Instance.ReleaseOrder(orderEntry);
            }
        }

        public bool MoveNext() => _enumerator.MoveNext();

        public void Reset() => _enumerator.Reset();

        public IEnumerator<OrdersCache.Entry> GetEnumerator() => _enumerator;
    }
}

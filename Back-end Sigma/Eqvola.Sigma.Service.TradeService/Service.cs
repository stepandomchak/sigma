﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.OrderStorage.Transaction;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.TradeService;
using Eqvola.Sigma.Service.Storage.Models;

namespace Eqvola.Sigma.Service.TradeService
{
    public class Service
    {
        public async Task Trade(TradeRequest tradeRequest)
        {
            var messages = new List<string>();
            long takerId = tradeRequest.OrderId;

            using (OrderIterator takerIterator = OrdersCache.Instance.GetTaker(takerId))
            {
                takerIterator.MoveNext();
                Order order = takerIterator.Current;
                var reversPrice = 1 / order.Price;
                using (OrderIterator makerIterator = await OrdersCache.Instance.GetMakers(order))
                {
                    foreach (OrdersCache.Entry makerEntry in makerIterator)
                    {
                        if(makerEntry.Order.Price > reversPrice) break;
                        
                        Order maker = makerEntry.Order;
                        if (maker.Rest > 0)
                        {
                            double transactionAmount = Math.Min(order.Rest, maker.Rest);
                            Transaction transaction = await CreateTransaction(order, maker, transactionAmount, messages);
                            if (transaction != null)
                            {
                                order.Rest -= transaction.Ammount;
                                maker.Rest -= transaction.Ammount;

                                if (maker.Rest.EqualsWithInaccuracy(0D))
                                {
                                    maker.Rest = 0D;
                                }
                                if (order.Rest.EqualsWithInaccuracy(0D))
                                {
                                    order.Rest = 0D;
                                    break;
                                }
                            }
                        }
                        
                    }
                }
                
                if (!order.Rest.EqualsWithInaccuracy(0D))
                {
                    await NotifyOnOrderUpdate(order);
                    //await UpdateOrder(order.Id);
                }
            }
        }

        private async Task<Transaction> CreateTransaction(Order taker, Order maker, double amount, List<string> messages)
        {
            CreateTransactionRequest createTransactionRequest = new CreateTransactionRequest
            {
                BuyOrderId = taker.OrderType == 1 ? taker.Id : maker.Id,    // Buy.
                SellOrderId = taker.OrderType == 2 ? taker.Id : maker.Id,    // Sell.
                Amount = amount,
                Taker = taker.Id
            };
            TransactionResponse transactionResponse = await createTransactionRequest.GetResponse<TransactionResponse>();
            if (transactionResponse.result == Com.OrderStorage.Result.Ok)
            {
                return transactionResponse.entity;
            }
            else
            {
                messages.Add(transactionResponse.message);
                return null;
            }
        }

        private async Task UpdateOrder(long orderId)
        {
            try
            {
                var service = new Storage.Service();
                var responseSettings = await service.GetSettings(new List<string>() { "MakerCommission" });

                var commission = responseSettings.FirstOrDefault();

                var request = new UpdateOrderCommissionRequest() { orderId = orderId, commission = commission == null ? 0.2 : commission.Value };
                await request.Send();
            } catch(Exception ex)
            {
                throw ex;
            }
        }

        private Task NotifyOnOrderUpdate(Order order)
        {
            UpdatePriceRequest updatePriceRequest = new UpdatePriceRequest
            {
                updatedOrder = order
            };
            return updatePriceRequest.Send();
        }
    }
}

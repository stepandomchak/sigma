using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;

namespace Eqvola.Sigma.TwoFactorService
{
    public class WorkerRole : ServiceBusRole<Service.TwoFactor.Service>
    {
        protected override IEnumerable<string> queues => new string[] { "twofactor" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "twofactor":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}

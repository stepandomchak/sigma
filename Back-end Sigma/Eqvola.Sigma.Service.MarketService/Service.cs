﻿using Microsoft.Azure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;
using Eqvola.Sigma.Service.MarketService.Repositories;
using Eqvola.Sigma.Service.MarketService.Models;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderService;
using Result = Eqvola.Sigma.Com.OrderStorage.Result;
using Eqvola.Sigma.Com;

namespace Eqvola.Sigma.Service.MarketService
{
    public class Service : IDisposable
    {
        private MarketContext db;
        private static List<BaseUpdater> UpdateListeners = new List<BaseUpdater>();

        public static void Init()
        {
            var task = InitListeners();
            task.Wait();
        }

        public Service()
        {
            db = new MarketContext(CloudConfigurationManager.GetSetting("SigmaMarketDbContext"));
        }

        private static async Task InitListeners()
        {
            try
            {
                var request = new GetFilteredOrdersRequest() { status = 3 };
                var orders = await request.GetResponse<OrdersResponse>();

                var pairs = await getPairs();

                QuotationsUpdater quotetionsUpdater = new QuotationsUpdater(orders.orders.ToList(), pairs);
                UpdateListeners.Add(quotetionsUpdater);

                GroupedOrdersUpdater groupedUpdater = new GroupedOrdersUpdater(orders.orders.ToList(), pairs);
                UpdateListeners.Add(groupedUpdater);

                OrdersUpdater ordersUpdater = new OrdersUpdater();
                UpdateListeners.Add(ordersUpdater);
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        void AddListener(BaseUpdater updater)
        {
            UpdateListeners.Add(updater);
        }

        public async static Task<List<Pair>> getPairs(bool isNeedRevers = true)
        {
            List<Pair> pairs = new List<Pair>();
            var markets = await GetAllMarketsStatic();
            if(markets == null)
            {
                return null;
            }

            foreach(var market in markets)
            {
                foreach (var targetCurrency in market.TargetCurrencies)
                {
                    var pair = new Pair()
                    {
                        CurrencyPair = market.MasterCurrencyCode + "_" + targetCurrency.Code,
                        MarketCurrency = market.MasterCurrencyCode
                    };
                    pairs.Add(pair);
                    if (isNeedRevers)
                    {

                        var reversPair = new Pair()
                        {
                            CurrencyPair = targetCurrency.Code + "_" + market.MasterCurrencyCode,
                            MarketCurrency = market.MasterCurrencyCode,
                            IsReverse = true
                        };
                        pairs.Add(reversPair);
                    }
                }
            }
            return pairs;
        }

        public void OrderUpdated(Order order, Order oldStateOfOrder)
        {
            order.Price = Math.Round(order.Price, 15);
            order.Rest = Math.Round(order.Rest, 15);
            if (oldStateOfOrder != null)
            {
                oldStateOfOrder.Price = Math.Round(oldStateOfOrder.Price, 15);
                oldStateOfOrder.Rest = Math.Round(oldStateOfOrder.Rest, 15);
            }
            foreach (var updater in UpdateListeners)
            {
                updater.UpdateData(order, oldStateOfOrder);
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public Quotations GetQuotations()
        {
            return QuotationsUpdater.GetPairPrices();
        }
        public async Task<ICollection<Currency>> GetAllCurrencies()
        {
            var tmp = (await (new Repository<Currency, long>(db).All()));
            return tmp;
        }

        public async Task<Currency> GetCurrencyByCode(string code)
        {
            return (await (new Repository<Currency, long>(db)).Where(m => m.Code == code)).FirstOrDefault();
        }

        public async Task<RepositoryResponse<Currency>> UpdateCurrency(Currency currency)
        {
            return (await (new Repository<Currency, long>(db)).Update(currency));
        }

        #region Market
        public static async Task<ICollection<Market>> GetAllMarketsStatic()
        {
            List<Market> markets = null;
            try
            {
                markets = (await (new Repository<Market, long>(new Service().db)).All()).ToList();
            }
            catch (Exception ex)
            {
                string stre = ex.Message;
            }

            return markets;
        }

        public async Task<ICollection<Market>> GetAllMarkets()
        {
            List<Market> markets = null;
            try
            {
                markets = (await (new Repository<Market, long>(db)).All()).ToList();
            }
            catch (Exception ex)
            {
                string stre = ex.Message;
            }

            return markets;
        }

        public async Task<Market> GetMarketByMasterCurrency(string currency)
        {
            return (await (new Repository<Market, long>(db)).Where(m => m.MasterCurrencyCode == currency)).FirstOrDefault();
        }
        #endregion

        #region Pair
        public async Task<bool> CheckPair(string firstCurrencyCode, string secondCurrencyCode)
        {
            var pairs = await getPairs();
            if (pairs == null) return false;
            var check = pairs.Where(w => w.CurrencyPair == (firstCurrencyCode + "_" + secondCurrencyCode)).FirstOrDefault(); ;
            return check == null ? false : true;
        }
        #endregion

    }
}

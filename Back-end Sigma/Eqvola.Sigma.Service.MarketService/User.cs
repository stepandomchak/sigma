﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.Service.MarketService
{
    public class User
    {
        public string email { get; set; }
        
        public string firstName { get; set; }

        public string lastName { get; set; }
        
        public string phoneCode { get; set; }

        public string phoneNumber { get; set; }
        
        public string country { get; set; }
        
        public string city { get; set; }

        public string address { get; set; }

        public string avatarUrl { get; set; }
        
        public bool isVerified { get; set; }

        public IDictionary<string, string> messangers { get; set; }

        public User()
        {
            messangers = new Dictionary<string, string>();
        }

    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public abstract class BaseEntity
    {
    }

    public abstract class BaseEntity<TKey> : BaseEntity
    {
        [Key]
        public TKey Id { get; set; }
    }
}

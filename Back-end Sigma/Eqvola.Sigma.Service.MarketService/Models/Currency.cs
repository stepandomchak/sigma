﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.Market.Models;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class Currency: BaseEntity<long>
    {
        [Required]
        [Index(IsUnique = true)]
        [MinLength(2)]
        [MaxLength(512)]
        public string Name { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [MinLength(2)]
        [MaxLength(64)]
        public string Code { get; set; }

        public int Status { get; set; }

        public bool IsFixedFee { get; set; }

        public double FixedFee { get; set; }

        public double MinFee { get; set; }

        public double MaxFee { get; set; }

        public virtual ICollection<Market> Markets { get; set; }
    }
}

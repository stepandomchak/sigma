﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class Market : BaseEntity<long>
    {
        [Required]
        [Index(IsUnique = true)]
        public string MasterCurrencyCode { get; set; }

        public virtual ICollection<Currency> TargetCurrencies { get; set; }

        public int Status { get; set; }
    }
}
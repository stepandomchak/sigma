﻿using Eqvola.Sigma.Service.MarketService.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService
{
    public interface IQuotationClient
    {
        void Init(Dictionary<string, double> PairPrices);
        void Update(Dictionary<string, double> UpdatedPairPrices);
    }

    public class QuotationHub : Hub<IQuotationClient>
    {
        public void Start()
        {
            var tmp = QuotationsUpdater.GetPairPrices().GetPairPricesWithoutRevers();
            Clients.Caller.Init(tmp);
        }
    }
}

﻿using Eqvola.Sigma.Service.MarketService.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService
{

    public interface IGroupedOrderClient
    {
        void Init(Dictionary<string, Dictionary<double, GroupedOrder>> pairPrices);
        void Update(Dictionary<string, Dictionary<double, GroupedOrder>> pairPrices);
    }

    public class GroupedOrdersHub : Hub<IGroupedOrderClient>
    {
        public async Task Start(string currency, string targetCurrency)
        {
            string pair = currency + "_" + targetCurrency;
            string revPair = targetCurrency + "_" + currency;

            if(GroupedOrdersUpdater.GroupedOrders.ContainsKey(pair) || GroupedOrdersUpdater.GroupedOrders.ContainsKey(revPair))
            {
                await Groups.Add(Context.ConnectionId, pair);
                await Groups.Add(Context.ConnectionId, revPair);
                var result = new Dictionary<string, Dictionary<double, GroupedOrder>>();
                if(GroupedOrdersUpdater.GroupedOrders.ContainsKey(pair))
                    result.Add(pair, GroupedOrdersUpdater.GroupedOrders[pair]);
                if (GroupedOrdersUpdater.GroupedOrders.ContainsKey(revPair))
                    result.Add(revPair, GroupedOrdersUpdater.GroupedOrders[revPair]);

                Clients.Caller.Init(result);
            }
            else
            {
                if((await Service.getPairs()).Where(p => p.CurrencyPair == pair).FirstOrDefault() != null)
                {
                    await Groups.Add(Context.ConnectionId, pair);
                    await Groups.Add(Context.ConnectionId, revPair);
                }
            }
        }

        public void Stop(string currency, string targetCurrency)
        {
            string pair = currency + "_" + targetCurrency;
            string revPair = targetCurrency + "_" + currency;
            Groups.Remove(Context.ConnectionId, pair);
            Groups.Remove(Context.ConnectionId, revPair);
        }
    }
}

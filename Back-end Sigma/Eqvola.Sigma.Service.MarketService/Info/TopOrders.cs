﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class TopOrders
    {
        private const int ORDERS_LIST_DEPTH = 10;

        public Dictionary<string, LinkedList<Order>> PairOrders { get; set; }
        
        public TopOrders(List<Pair> pairs)
        {
            PairOrders = new Dictionary<string, LinkedList<Order>>();
            foreach(var pair in pairs)
            {
                if (!PairOrders.ContainsKey(pair.CurrencyPair))
                    PairOrders.Add(pair.CurrencyPair, new LinkedList<Order>());
            }
        }
        
        public void AddOrder(string pair, Order order)
        {
            if (order.Price == 0)
                return;
            if (PairOrders.ContainsKey(pair))
            {
                var list = PairOrders[pair];

                int count = 0;
                var current = list.First;

                while (current != null && current.Value != null && order.Price > current.Value.Price && count++ < ORDERS_LIST_DEPTH)
                {
                    current = current.Next;
                }

                if (current != null)
                    list.AddBefore(current, order);
                else if (count < ORDERS_LIST_DEPTH)
                    list.AddLast(order);

                while (list.Count > ORDERS_LIST_DEPTH)
                    list.RemoveLast();
            }
        }

        public bool RemoveOrder(string pair, long id)
        {
            if (!PairOrders.ContainsKey(pair))
                return false;
            var removedOrder = PairOrders[pair].Where(o => o.Id == id).FirstOrDefault();
            if(removedOrder != null)
            {
                PairOrders[pair].Remove(removedOrder);
                return true;
            }
            return false;
        }

        public Order GetOrderById(string pair, long id)
        {
            if (!PairOrders.ContainsKey(pair))
                return null;
            return PairOrders[pair].Where(o => o.Id == id).FirstOrDefault();
        }

        public Order GetFirstOrder(string pair)
        {
            if (!PairOrders.ContainsKey(pair) || PairOrders[pair].Count == 0)
                return null;
            return PairOrders[pair].First();
        }

        public Order GetLastOrder(string pair)
        {
            if (!PairOrders.ContainsKey(pair))
                return null;
            return PairOrders[pair].Last();
        }

        public int GetOrdersCount(string pair)
        {
            if (!PairOrders.ContainsKey(pair))
                return 0;
            return PairOrders[pair].Count;
        }
    }
}

﻿using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class GroupedOrder
    {
        public double Price { get; private set; }
        public string Pair { get; private set; }
        public double Amount { get; private set; }
        public double Sum { get; private set; }
        public double TotalAmount { get; private set; }
        public double TotalSum { get; private set; }

        [JsonIgnore]
        public Order FirstOrder { get; set; }
        

        public GroupedOrder(string pair, Order order)
        {
            this.Price = Math.Round(order.Price, 15);
            this.Pair = pair;
            this.TotalAmount = Math.Round(order.Rest, 15);
            FirstOrder = order;
            UpdateFirstOrderInfo();
        }

        public void Subtract(Order order)
        {
            TotalAmount = Math.Round(TotalAmount - order.Rest, 15);
        }

        public void Add(Order order)
        {
            TotalAmount = Math.Round(TotalAmount + order.Rest, 15);
        }

        public async Task UpdateFirstOrder(Order order)
        {
            if(FirstOrder.Id != order.Id)
            {
                if(FirstOrder.CreationTime < order.CreationTime)
                {
                    return;
                }
                else
                {
                    if(order.Status != 1 || order.Status != 2)
                    {
                        return;
                    }
                    else
                    {
                        FirstOrder = order;
                        UpdateFirstOrderInfo();
                    }
                }
            }
            else
            {
                if (order.Status != 1 || order.Status != 2)
                {
                    // to do get first order from order storage
                    var request = new GetFirstOrderRequest()
                    {
                        currency = order.Currency,
                        targetCurrency = order.TargetCurrency
                    };
                    var response = await request.GetResponse<OrderResponse>();
                    if(response != null && response.entity != null)
                    {
                        FirstOrder = response.entity;
                    }
                }
                else
                {
                    FirstOrder = order;
                }
                UpdateFirstOrderInfo();
            }
        }

        private void UpdateFirstOrderInfo()
        {
            Amount = Math.Round(FirstOrder.Rest, 15);
            Sum = Math.Round(Amount * Price, 15);
        }

    }
}

﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class Quotations
    {
        public Dictionary<string, double> PairPrices { get; set; }
        private List<Pair> Pairs { get; set; }

        public Quotations(List<Pair> pairs)
        {
            Pairs = pairs;
            PairPrices = new Dictionary<string, double>();
            foreach (var pair in pairs)
            {
                if(!PairPrices.ContainsKey(pair.CurrencyPair))
                    PairPrices.Add(pair.CurrencyPair, 0);
            }
        }

        public Dictionary<string, double> GetPairPricesWithoutRevers()
        {
            var prices = PairPrices;
            Pairs.Where(p => p.IsReverse != true).Select(p => prices.Remove(p.CurrencyPair));
            return prices;
        }
    }
}

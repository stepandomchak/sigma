﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public abstract class BaseUpdater
    {
        public abstract void UpdateData(Order order, Order oldStateOfOrder = null);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Microsoft.AspNet.SignalR;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class QuotationsUpdater : BaseUpdater
    {
        private readonly static Lazy<IHubContext> _context = new Lazy<IHubContext>(
            () => GlobalHost.ConnectionManager.GetHubContext<QuotationHub>());

        private static Quotations Quotations;
        private static TopOrders TopOrders { get; set; }

        public static Quotations GetPairPrices()
        {
            if (TopOrders == null) return null;

            foreach(var pair in TopOrders.PairOrders)
            {
                if(pair.Value.First != null)
                    Quotations.PairPrices[pair.Key] = Math.Round(pair.Value.First.Value.Price, 15);
            }
            return Quotations;
        }

        public QuotationsUpdater(List<Order> orders, List<Pair> pairs)
        {
            Quotations = new Quotations(pairs);
            TopOrders = new TopOrders(pairs);

            InitData(orders);
        }

        public Quotations GetData()
        {
            return Quotations;
        }

        private void InitData(List<Order> orders)
        {
            try
            {
                foreach (var order in orders)
                {
                    string orderPair = order.Currency + "_" + order.TargetCurrency;
                    TopOrders.AddOrder(orderPair, order);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public override async void UpdateData(Order order, Order oldStateOfOrder = null)
        {
            bool isPairPriceChanged = false;
            string orderPair = order.Currency + "_" + order.TargetCurrency;
            switch (order.Status)
            {
                case 1:
                    {
                        var firstOrder = TopOrders.GetFirstOrder(orderPair);
                        if (TopOrders.GetOrdersCount(orderPair) >= 10) // check if new order price bigger than our top 10 prices 
                        {
                            if (TopOrders.GetLastOrder(orderPair).Price < order.Price) // check if new order price bigger than our top 10 prices 
                                return;
                        }
                        
                        TopOrders.AddOrder(orderPair, order);
                        /// 
                        if (firstOrder != null && firstOrder.Id != TopOrders.GetFirstOrder(orderPair).Id)
                        {
                            isPairPriceChanged = true;
                            var request = new CheckLimitsRequest() { currency = order.Currency, targetCurrency = order.TargetCurrency, price = Math.Round(order.Price, 15) };
                            await request.Send();
                        }
                        break;
                    }
                case 2:
                    {
                        var firstOrder = TopOrders.GetFirstOrder(orderPair);
                        
                        if(TopOrders.RemoveOrder(orderPair, order.Id))
                        {
                            TopOrders.AddOrder(orderPair, order);
                        }

                        if (firstOrder != null && firstOrder.Id != TopOrders.GetFirstOrder(orderPair).Id)
                        {
                            isPairPriceChanged = true;
                            var request = new CheckLimitsRequest() { currency = order.Currency, targetCurrency = order.TargetCurrency, price = Math.Round(order.Price, 15) };
                            await request.Send();
                        }
                        break;
                    }
                default:
                    {
                        var firstOrder = TopOrders.GetFirstOrder(orderPair);

                        if (TopOrders.RemoveOrder(orderPair, order.Id))
                        {
                            var request = new GetFilteredOrdersRequest() { currency = order.Currency, targetCurrency = order.TargetCurrency, status = 3, ordersCount = 10};
                            var response = await request.GetResponse<OrdersResponse>();
                            foreach(var filteredOrder in response.orders)
                            {
                                if(TopOrders.GetOrderById(orderPair, filteredOrder.Id) == null)
                                {
                                    TopOrders.AddOrder(orderPair, filteredOrder);
                                    if (TopOrders.GetOrdersCount(orderPair) >= 10)
                                    {
                                        break;
                                    }
                                }
                            }
                        }

                        if (firstOrder != null && firstOrder.Id != TopOrders.GetFirstOrder(orderPair).Id)
                        {
                            isPairPriceChanged = true;
                            var request = new CheckLimitsRequest() { currency = order.Currency, targetCurrency = order.TargetCurrency, price = Math.Round(order.Price, 15) };
                            await request.Send();
                        }
                        break;
                    }
            }
            if(isPairPriceChanged)
            {
                var updated = new Dictionary<string, double>();
                Quotations.PairPrices[orderPair] = Math.Round(TopOrders.GetFirstOrder(orderPair).Price, 15);

                updated.Add(orderPair, Quotations.PairPrices[orderPair]);
                _context.Value.Clients.All.Update(updated);
            }
        }
    }
}

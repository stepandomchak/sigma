﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Microsoft.AspNet.SignalR;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class OrdersUpdater : BaseUpdater
    {
        private readonly static Lazy<IHubContext> _context = new Lazy<IHubContext>(
            () => GlobalHost.ConnectionManager.GetHubContext<OrdersHub>());


        public static async Task<IEnumerable<Order>> GetUserOrders(long userId, string market)
        {
            var request = new GetOrderRequest()
            {
                userId = userId,
                status = 67,
                market = market
            };
            var response = await request.GetResponse<OrdersResponse>();
            return response.orders;
        }

        public override async void UpdateData(Order order, Order oldStateOfOrder = null)
        {
            Info.Order orderInfo = new Info.Order(order);

            var orders = new Dictionary<long, Info.Order>();
            orders.Add(orderInfo.Id, orderInfo);

            Service marketService = new Service();
            if ((await marketService.GetMarketByMasterCurrency(order.Currency) != null))
            {
                _context.Value.Clients.Group(order.UserId + "_" + order.Currency).Update(orders);
            }
            else
            {
                _context.Value.Clients.Group(order.UserId + "_" + order.TargetCurrency).Update(orders);
            }
        }
    }
}

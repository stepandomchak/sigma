﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.OrderStorage.Transaction;
using Microsoft.AspNet.SignalR;

namespace Eqvola.Sigma.Service.MarketService.Models
{
    public class GroupedOrdersUpdater : BaseUpdater
    {
        private readonly static Lazy<IHubContext> _context = new Lazy<IHubContext>(
            () => GlobalHost.ConnectionManager.GetHubContext<GroupedOrdersHub>());

        public static Dictionary<string, Dictionary<double, GroupedOrder>> GroupedOrders;

        public GroupedOrdersUpdater(List<Order> orders, List<Pair> pairs)
        {
            GroupedOrders = new Dictionary<string, Dictionary<double, GroupedOrder>>();

            InitData(orders);
        }

        public Dictionary<string, Dictionary<double, GroupedOrder>> GetData()
        {
            return GroupedOrders;
        }

        private void InitData(List<Order> orders)
        {
            foreach (var order in orders)
            {
                order.Price = Math.Round(order.Price, 15);
                string orderPair = order.Currency + "_" + order.TargetCurrency;
                if (!GroupedOrders.ContainsKey(orderPair))
                {
                    var priceList = new Dictionary<double, GroupedOrder>();
                    priceList.Add(order.Price, new GroupedOrder(orderPair, order));
                    GroupedOrders.Add(orderPair, priceList);
                }
                else
                {
                    if (!GroupedOrders[orderPair].ContainsKey(order.Price))
                    {
                        GroupedOrders[orderPair].Add(order.Price, new GroupedOrder(orderPair, order));
                    }
                    else
                    {
                        GroupedOrders[orderPair][order.Price].Add(order);
                    }
                }
            }
        }

        public override async void UpdateData(Order order, Order oldStateOfOrder = null)
        {

            var updatedGroupedOrders = new Dictionary<double, GroupedOrder>();
            order.Price = Math.Round(order.Price, 15);
            string orderPair = order.Currency + "_" + order.TargetCurrency;
            
            if (!GroupedOrders.ContainsKey(orderPair)) // Если у нас нет ордеров по валютной паре: создаем нужно коллекцию -- добавляем ордер по данной цене и все.
            {
                var priceList = new Dictionary<double, GroupedOrder>();
                priceList.Add(order.Price, new GroupedOrder(orderPair, order));
                GroupedOrders.Add(orderPair, priceList);
                updatedGroupedOrders.Add(order.Price, GroupedOrders[orderPair][order.Price]);
            }
            else
            {

                if (!GroupedOrders[orderPair].ContainsKey(order.Price)) // если у нас нет ордеров по заданной цене: в коллекции по данной валютной паре добавляем запись с данной ценой
                {
                    if (oldStateOfOrder != null)
                    {
                        GroupedOrders[orderPair][oldStateOfOrder.Price].Subtract(oldStateOfOrder);
                        await GroupedOrders[orderPair][order.Price].UpdateFirstOrder(order);
                    }
                    var newGrouped = new GroupedOrder(orderPair, order);
                    GroupedOrders[orderPair].Add(order.Price, newGrouped);
                    updatedGroupedOrders.Add(order.Price, newGrouped);
                }
                else
                {
                    if(oldStateOfOrder == null)
                    {
                        GroupedOrders[orderPair][order.Price].Add(order);
                        await GroupedOrders[orderPair][order.Price].UpdateFirstOrder(order);

                        updatedGroupedOrders.Add(order.Price, GroupedOrders[orderPair][order.Price]);
                    }
                    else if (oldStateOfOrder.Price == order.Price)
                    {
                        GroupedOrders[orderPair][oldStateOfOrder.Price].Subtract(oldStateOfOrder);
                        GroupedOrders[orderPair][order.Price].Add(order);
                        await GroupedOrders[orderPair][oldStateOfOrder.Price].UpdateFirstOrder(order);

                        updatedGroupedOrders.Add(order.Price, GroupedOrders[orderPair][order.Price]);
                    }
                    else
                    {
                        GroupedOrders[orderPair][oldStateOfOrder.Price].Subtract(oldStateOfOrder);
                        GroupedOrders[orderPair][order.Price].Add(order);
                        await GroupedOrders[orderPair][oldStateOfOrder.Price].UpdateFirstOrder(order);
                        await GroupedOrders[orderPair][order.Price].UpdateFirstOrder(order);

                        updatedGroupedOrders.Add(order.Price, GroupedOrders[orderPair][order.Price]);
                        updatedGroupedOrders.Add(oldStateOfOrder.Price, GroupedOrders[orderPair][oldStateOfOrder.Price]);
                    }
                     // обновление информации по всем ордерам в данной цене
                }
            }

            /*foreach(var updatedOrder in updatedGroupedOrders)
            {
                if(updatedOrder.Value.TotalAmount == 0)
                {
                    GroupedOrders[orderPair].Remove(updatedOrder.Key);
                }
            }*/
            //todo send grouped to socket
            var types = new Dictionary<string, Dictionary<double, GroupedOrder>>();
            types.Add(orderPair, updatedGroupedOrders);

            _context.Value.Clients.Group(orderPair).Update(types);
            
        }
        
    }
}

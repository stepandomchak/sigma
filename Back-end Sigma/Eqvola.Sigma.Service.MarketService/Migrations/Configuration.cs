namespace Eqvola.Sigma.Service.MarketService.Migrations
{
    using Eqvola.Sigma.Service.MarketService.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Eqvola.Sigma.Service.MarketService.MarketContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Eqvola.Sigma.Service.MarketService.MarketContext context)
        {
            Currency currencyBtc = new Currency() { Name = "Bitcoin", Code = "BTC", Status = 1, IsFixedFee = false, MinFee = 0.1, MaxFee = 0.5 };
            Currency currencyEth = new Currency() { Name = "Ethereum", Code = "ETH", Status = 1, IsFixedFee = false, MinFee = 0.1, MaxFee = 0.3 };
            Currency currencyLtc = new Currency() { Name = "Litecoin", Code = "LTC", Status = 1, IsFixedFee = true, FixedFee = 0.4 };
            Currency currencyXrp = new Currency() { Name = "Ripple", Code = "XRP", Status = 1, IsFixedFee = true, FixedFee = 0.25};
            Currency currencyUsdt = new Currency() { Name = "Tether", Code = "USDT", Status = 1, IsFixedFee = false, MinFee = 0.05, MaxFee = 0.4 };
            Currency currencyBch = new Currency() { Name = "BitcoinCash", Code = "BCH", Status = 1, IsFixedFee = false, MinFee = 0.1, MaxFee = 0.5 };
            Currency currencyRwb = new Currency() { Name = "RockWorldBi", Code = "RWB", Status = 1, IsFixedFee = true, FixedFee = 0.6};
            Currency currencyMdp = new Currency() { Name = "MBAexDigitalPass", Code = "MDP", Status = 1, IsFixedFee = false, MinFee = 0.01, MaxFee = 0.7 };
            Currency currencyWcg = new Currency() { Name = "WorldCryptoGold", Code = "WCG", Status = 1, IsFixedFee = true, FixedFee = 1};
            Currency currencyMtr = new Currency() { Name = "MasterTraderCoin", Code = "MTR", Status = 1, IsFixedFee = true, FixedFee = 0.45 };
            Currency currencyMat = new Currency() { Name = "ManetCoin", Code = "MAT", Status = 1, IsFixedFee = false, MinFee = 0.23, MaxFee = 0.56 };
            Currency currencyDrt = new Currency() { Name = "DomRaider", Code = "DRT", Status = 1, IsFixedFee = false, MinFee = 0.15, MaxFee = 0.38 };
            Currency currencyEos = new Currency() { Name = "Eos", Code = "EOS", Status = 1, IsFixedFee = true, FixedFee = 0.85 };
            Currency currencyZec = new Currency() { Name = "ZCash", Code = "ZEC", Status = 1, IsFixedFee = true, FixedFee = 1 };
            Currency currencyXmr = new Currency() { Name = "Monero", Code = "XMR", Status = 1, IsFixedFee = false, MinFee = 0.04, MaxFee = 0.2 };
            Currency currencyXtz = new Currency() { Name = "Tezos", Code = "XTZ", Status = 1, IsFixedFee = false, MinFee = 0.4, MaxFee = 0.9 };
            Currency currencyBtg = new Currency() { Name = "BitcoinGold", Code = "BTG", Status = 1, IsFixedFee = false, MinFee = 0.1, MaxFee = 0.4 };


            context.Set<Currency>().AddOrUpdate(currencyBtc);
            context.Set<Currency>().AddOrUpdate(currencyEth);
            context.Set<Currency>().AddOrUpdate(currencyLtc);
            context.Set<Currency>().AddOrUpdate(currencyXrp);
            context.Set<Currency>().AddOrUpdate(currencyUsdt);
            context.Set<Currency>().AddOrUpdate(currencyBch);
            context.Set<Currency>().AddOrUpdate(currencyRwb);
            context.Set<Currency>().AddOrUpdate(currencyMdp);
            context.Set<Currency>().AddOrUpdate(currencyWcg);
            context.Set<Currency>().AddOrUpdate(currencyMtr);
            context.Set<Currency>().AddOrUpdate(currencyMat);
            context.Set<Currency>().AddOrUpdate(currencyDrt);
            context.Set<Currency>().AddOrUpdate(currencyEos);
            context.Set<Currency>().AddOrUpdate(currencyZec);
            context.Set<Currency>().AddOrUpdate(currencyXmr);
            context.Set<Currency>().AddOrUpdate(currencyXtz);
            context.Set<Currency>().AddOrUpdate(currencyBtg);

            context.SaveChanges();

            Market marketBtc = new Market() { MasterCurrencyCode = currencyBtc.Code, Status = 1};
            marketBtc.TargetCurrencies = new List<Currency>();
            marketBtc.TargetCurrencies.Add(currencyEth);
            marketBtc.TargetCurrencies.Add(currencyLtc);
            marketBtc.TargetCurrencies.Add(currencyXrp);
            marketBtc.TargetCurrencies.Add(currencyUsdt);
            marketBtc.TargetCurrencies.Add(currencyBch);
            marketBtc.TargetCurrencies.Add(currencyRwb);
            marketBtc.TargetCurrencies.Add(currencyMdp);
            marketBtc.TargetCurrencies.Add(currencyWcg);
            marketBtc.TargetCurrencies.Add(currencyMtr);
            marketBtc.TargetCurrencies.Add(currencyMat);
            marketBtc.TargetCurrencies.Add(currencyDrt);
            marketBtc.TargetCurrencies.Add(currencyEos);
            marketBtc.TargetCurrencies.Add(currencyZec);
            marketBtc.TargetCurrencies.Add(currencyXmr);
            marketBtc.TargetCurrencies.Add(currencyXtz);
            marketBtc.TargetCurrencies.Add(currencyBtg);

            Market marketEth = new Market() { MasterCurrencyCode = currencyEth.Code, Status = 1};
            marketEth.TargetCurrencies = new List<Currency>();
            marketEth.TargetCurrencies.Add(currencyBtc);
            marketEth.TargetCurrencies.Add(currencyLtc);
            marketEth.TargetCurrencies.Add(currencyXrp);
            marketEth.TargetCurrencies.Add(currencyUsdt);
            marketEth.TargetCurrencies.Add(currencyBch);
            marketEth.TargetCurrencies.Add(currencyRwb);
            marketEth.TargetCurrencies.Add(currencyMdp);
            marketEth.TargetCurrencies.Add(currencyWcg);
            marketEth.TargetCurrencies.Add(currencyMtr);
            marketEth.TargetCurrencies.Add(currencyMat);
            marketEth.TargetCurrencies.Add(currencyDrt);
            marketEth.TargetCurrencies.Add(currencyEos);
            marketEth.TargetCurrencies.Add(currencyZec);
            marketEth.TargetCurrencies.Add(currencyXmr);
            marketEth.TargetCurrencies.Add(currencyXtz);
            marketEth.TargetCurrencies.Add(currencyBtg);

            Market marketUsdt = new Market() { MasterCurrencyCode = currencyUsdt.Code, Status = 1 };
            marketUsdt.TargetCurrencies = new List<Currency>();
            marketUsdt.TargetCurrencies.Add(currencyBtc);
            marketUsdt.TargetCurrencies.Add(currencyEth);
            marketUsdt.TargetCurrencies.Add(currencyLtc);
            marketUsdt.TargetCurrencies.Add(currencyXrp);
            marketUsdt.TargetCurrencies.Add(currencyBch);
            marketUsdt.TargetCurrencies.Add(currencyRwb);
            marketUsdt.TargetCurrencies.Add(currencyMdp);
            marketUsdt.TargetCurrencies.Add(currencyWcg);
            marketUsdt.TargetCurrencies.Add(currencyMtr);
            marketUsdt.TargetCurrencies.Add(currencyMat);
            marketUsdt.TargetCurrencies.Add(currencyDrt);
            marketUsdt.TargetCurrencies.Add(currencyEos);
            marketUsdt.TargetCurrencies.Add(currencyZec);
            marketUsdt.TargetCurrencies.Add(currencyXmr);
            marketUsdt.TargetCurrencies.Add(currencyXtz);
            marketUsdt.TargetCurrencies.Add(currencyBtg);

            Market marketMdp = new Market() { MasterCurrencyCode = currencyMdp.Code, Status = 1 };
            marketMdp.TargetCurrencies = new List<Currency>();
            marketMdp.TargetCurrencies.Add(currencyBtc);
            marketMdp.TargetCurrencies.Add(currencyEth);
            marketMdp.TargetCurrencies.Add(currencyLtc);
            marketMdp.TargetCurrencies.Add(currencyXrp);
            marketMdp.TargetCurrencies.Add(currencyUsdt);
            marketMdp.TargetCurrencies.Add(currencyBch);
            marketMdp.TargetCurrencies.Add(currencyRwb);
            marketMdp.TargetCurrencies.Add(currencyWcg);
            marketMdp.TargetCurrencies.Add(currencyMtr);
            marketMdp.TargetCurrencies.Add(currencyMat);
            marketMdp.TargetCurrencies.Add(currencyDrt);
            marketMdp.TargetCurrencies.Add(currencyEos);
            marketMdp.TargetCurrencies.Add(currencyZec);
            marketMdp.TargetCurrencies.Add(currencyXmr);
            marketMdp.TargetCurrencies.Add(currencyXtz);
            marketMdp.TargetCurrencies.Add(currencyBtg);

            context.Set<Market>().AddOrUpdate(marketBtc);
            context.Set<Market>().AddOrUpdate(marketEth);
            context.Set<Market>().AddOrUpdate(marketUsdt);
            context.Set<Market>().AddOrUpdate(marketMdp);

            context.SaveChanges();
        }
    }
}

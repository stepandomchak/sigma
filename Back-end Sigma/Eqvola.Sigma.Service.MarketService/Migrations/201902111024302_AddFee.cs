namespace Eqvola.Sigma.Service.MarketService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Currencies", "IsFixedFee", c => c.Boolean(nullable: false));
            AddColumn("dbo.Currencies", "FixedFee", c => c.Double(nullable: false));
            AddColumn("dbo.Currencies", "MinFee", c => c.Double(nullable: false));
            AddColumn("dbo.Currencies", "MaxFee", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Currencies", "MaxFee");
            DropColumn("dbo.Currencies", "MinFee");
            DropColumn("dbo.Currencies", "FixedFee");
            DropColumn("dbo.Currencies", "IsFixedFee");
        }
    }
}

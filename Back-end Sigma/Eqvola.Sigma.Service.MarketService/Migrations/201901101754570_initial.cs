namespace Eqvola.Sigma.Service.MarketService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Markets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MasterCurrencyCode = c.String(nullable: false, maxLength: 64),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.MasterCurrencyCode, unique: true);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 512),
                        Code = c.String(nullable: false, maxLength: 64),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true)
                .Index(t => t.Code, unique: true);
            
            CreateTable(
                "dbo.CurrencyMarkets",
                c => new
                    {
                        Currency_Id = c.Long(nullable: false),
                        Market_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Currency_Id, t.Market_Id })
                .ForeignKey("dbo.Currencies", t => t.Currency_Id, cascadeDelete: true)
                .ForeignKey("dbo.Markets", t => t.Market_Id, cascadeDelete: true)
                .Index(t => t.Currency_Id)
                .Index(t => t.Market_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CurrencyMarkets", "Market_Id", "dbo.Markets");
            DropForeignKey("dbo.CurrencyMarkets", "Currency_Id", "dbo.Currencies");
            DropIndex("dbo.CurrencyMarkets", new[] { "Market_Id" });
            DropIndex("dbo.CurrencyMarkets", new[] { "Currency_Id" });
            DropIndex("dbo.Currencies", new[] { "Code" });
            DropIndex("dbo.Currencies", new[] { "Name" });
            DropIndex("dbo.Markets", new[] { "MasterCurrencyCode" });
            DropTable("dbo.CurrencyMarkets");
            DropTable("dbo.Currencies");
            DropTable("dbo.Markets");
        }
    }
}

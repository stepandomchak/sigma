﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.Service.MarketService.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.MarketService
{
    public interface IOrdersClient
    {
        void Init(Dictionary<long, Info.Order> orders);
        void Update(Dictionary<long, Info.Order> orders);
    }

    public class OrdersHub : Hub<IOrdersClient>
    {
        public async Task Start(string market, string token)
        {
            Storage.Service service = null;
            try
            {
                service = new Storage.Service();
            } catch(Exception ex)
            {
                string str = ex.Message;
            }
            User tokenUser = null;
            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;

            try
            {
                tokenUser = TokenValidator.Decode<User>(token);
            }
            catch
            {
                return;
            }

            var user = await service.GetUserByEmail(tokenUser.email);

            if (user == null)
                return;

            Service marketService = new Service();
            if ((await marketService.GetMarketByMasterCurrency(market) == null))
                return;

            var orders = await OrdersUpdater.GetUserOrders(user.Id, market);
            var initOrders = new Dictionary<long, Info.Order>();
            foreach(var o in orders)
            {
                initOrders.Add(o.Id, new Info.Order(o));
            }

            Clients.Caller.Init(initOrders);
            await Groups.Add(Context.ConnectionId, user.Id + "_" + market);
            
        }

        public async void Stop(string market, string token)
        {

            User tokenUser = null;
            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;

            try
            {
                tokenUser = TokenValidator.Decode<User>(token);
            }
            catch
            {
                return;
            }

            Storage.Service service = new Storage.Service();
            var user = await service.GetUserByEmail(tokenUser.email);

            if (user == null)
                return;
            await Groups.Remove(Context.ConnectionId, user.Id + "_" + market);
        }
    }
}

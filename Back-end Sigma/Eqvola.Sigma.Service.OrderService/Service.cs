﻿using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Wallet;

namespace Eqvola.Sigma.Service.OrderService
{
    public class Service
    {
        public async Task<ValidateOrderResponse<Order>> OrderValidation(ValidateOrderRequest request)
        {
            try
            {
                ValidateOrderResponse<Order> resp = null;

                // Validate currency pair with market service
                if (string.IsNullOrEmpty(request.order.Currency) || string.IsNullOrEmpty(request.order.TargetCurrency))
                {
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.UnexpectedCurrency };
                }
                var checkPair = new CheckPairRequest() { fisrtCurrencyCode = request.order.Currency, secondCurrencyCode = request.order.TargetCurrency };
                var checkPairResponse = await checkPair.GetResponse<CheckPairResponse>();
                if (checkPairResponse == null)
                {
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.Exception };
                }
                if (!checkPairResponse.pairStatus)
                {
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.NonExistenPair };
                }

                // Validate Order price and amount
                if (request.order.Amount <= 0)
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.UnexpectedAmount };
                if (request.order.Price <= 0)
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.UnexpectedPrice };

                var walletRequest = new GetBalanceRequest() { userId = request.userId, currency = request.order.Currency };
                var walletResponse = await walletRequest.GetResponse<WalletResponse>();

                if (walletResponse == null)
                {
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.Exception};
                }
                if (walletResponse.result == Com.Wallet.Result.NoSuchWallet || walletResponse.entity.Balance - request.order.Amount < 0)
                {
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.NotEnoughtBalance };
                }

                // The order can contain only one of the two fields (Stop/Limit)
                if ((request.order.Stop > 0 && request.order.Limit > 0) || request.order.Stop < 0 || request.order.Limit < 0)
                    return new ValidateOrderResponse<Order>() { result = Com.OrderService.Result.InvalidRequest };

                var createRequest = new CreateOrderRequest()
                {
                    currency = request.order.Currency,
                    targetCurrency = request.order.TargetCurrency,
                    expiresTime = request.order.ExpiresTime,
                    price = request.order.Price,
                    amount = request.order.Amount,
                    stopLoss = request.order.StopLoss,
                    takeProfit = request.order.TakeProfit,
                    stop = request.order.Stop,
                    limit = request.order.Limit,
                    commission = request.order.Commission,
                    userId = request.userId,
                    orderType = request.order.OrderType
                };
                var response = await createRequest.GetResponse<OrderResponse>();

                if (response.result == Com.OrderStorage.Result.Ok)
                {
                    if (response.entity.Status != (int)StatusOrder.Canceled && response.entity.Status != (int)StatusOrder.Сlosed)
                    {
                        /*var updatePrice = new UpdatePriceRequest()
                        {
                            oldOrder = null,
                            updatedOrder = response.entity
                        };
                        await updatePrice.Send();*/
                    }

                    resp = new ValidateOrderResponse<Order>()
                    {
                        entity = response.entity,
                        result = Com.OrderService.Result.Ok
                    };
                }
                

                return resp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<OrderResponse> CheckLimits(CheckLimitsRequest request)
        {
            try
            {
                var getRequest = new GetFilteredOrdersRequest()
                {
                    currency = request.currency,
                    targetCurrency = request.targetCurrency
                };

                IEnumerable<Order> orders = (await getRequest.GetResponse<OrdersResponse>()).orders;

                foreach (Order order in orders)
                {
                    var oldOrder = order;
                    if (order.Status == (int)StatusOrder.Сlosed)
                    {
                        if (order.TakeProfit != 0 && request.price >= order.TakeProfit)
                            await CreateOrderByLimit(order, request.price);
                        else if (order.StopLoss != 0 && request.price <= order.StopLoss)
                            await CreateOrderByLimit(order, request.price);
                    }
                    else if(order.Status == (int)StatusOrder.PostponedByLimit)
                    {
                        if ((order.Stop != 0 && request.price >= order.Stop) || (order.Limit != 0 && request.price <= order.Limit))
                        {
                            order.Status = (int)StatusOrder.NewOrder;
                            await UpdateOrder(order);

                            var updatePrice = new UpdatePriceRequest()
                            {
                                oldOrder = oldOrder,
                                updatedOrder = order
                            };
                            await updatePrice.Send();
                        }
                    }
                    else
                        continue;
                }
                return new OrderResponse()
                {
                    result = Com.OrderStorage.Result.Ok
                };
            }catch(Exception ex) { throw ex; }
        }

        protected async Task UpdateOrder(Order order)
        {
            var updateRequest = new UpdateOrderRequest()
            {
                orderId = order.Id,
                currency = order.Currency,
                targetCurrency = order.TargetCurrency,
                expiresTime = order.ExpiresTime,
                price = order.Price,
                amount = order.Amount,
                stopLoss = order.StopLoss,
                takeProfit = order.TakeProfit,
                stop = order.Stop,
                limit = order.Limit,

                orderType = order.OrderType,
                status = order.Status
            };
            await updateRequest.GetResponse<OrderResponse>();
        }

        protected async Task CreateOrderByLimit(Order order, double price)
        {
            try
            {
                var createRequest = new CreateOrderRequest()
                {
                    currency = order.TargetCurrency,
                    targetCurrency = order.Currency,
                    expiresTime = null,
                    price = price,
                    amount = order.Amount,
                    stopLoss = 0,
                    takeProfit = 0,
                    stop = 0,
                    limit = 0,
                    userId = order.UserId,
                    orderType = order.OrderType == 1 ? 2 : 1
                };

                await createRequest.GetResponse<OrderResponse>();
            }
            catch (Exception ex) { throw ex; }
        }

    }
}

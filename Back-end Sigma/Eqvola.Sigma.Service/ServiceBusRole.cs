﻿using Eqvola.Sigma.Com;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service
{
    public abstract class ServiceBusRole : ServiceRole
    {
        protected abstract IEnumerable<string> queues { get; }

        private List<QueueListener> listeners = new List<QueueListener>();

        public override bool OnStart()
        {
            ServicePointManager.DefaultConnectionLimit = 12;

            return base.OnStart();
        }

        public override void Run()
        {
            foreach (string queue in queues)
                listeners.Add(ListenQueue(queue));

            CompletedEvent.WaitOne();

            foreach (var listener in listeners)
                listener.Stop().Wait();

            CompletedEvent.Set();
        }

        private QueueListener ListenQueue(string queue)
        {
            var listener = new QueueListener(queue);
            listener.StartListen(CreateHandlers(queue));

            return listener;
        }

        protected abstract IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName);
    }

    public abstract class ServiceBusRole<TService> : ServiceBusRole where TService : new()
    {
        protected TService service { get; private set; }

        public override bool OnStart()
        {
            service = new TService();

            return base.OnStart();
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service
{
    internal class QueueListener
    {
        private QueueClient receiver;

        private ManualResetEvent complateEvent = new ManualResetEvent(false);
        private Task listenTask = null;

        public QueueListener(string queueName)
        {
            receiver = ServiceBusFactory.GetQueueClient(queueName);
        }

        public void StartListen(IDictionary<string, Func<BrokeredMessage, Task<Response>>> handlers)
        {
            listenTask = Task.Run(async () =>
            {
                Task complateTask = complateEvent.ToTask();
                do
                {
                    var task = receiver.ReceiveAsync();
                    if (Task.WaitAny(
                        task,
                        complateTask
                    ) == 0)
                    {
                        try
                        {
                            if (task.Result != null)
                            {
                                var t = Task.Run(async () => {
                                    var message = task.Result;
                                    var response = await handlers[message.Label](message);
                                    receiver.Complete(message.LockToken);
                                    response?.Send(message.SessionId);
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            Trace.WriteLine(e.Message);
                        }
                    }
                } while (!complateEvent.WaitOne(0));
            });
        }

        public async Task Stop()
        {
            if (listenTask != null)
            {
                complateEvent.Set();
                await listenTask;
            }
        }
    }
}

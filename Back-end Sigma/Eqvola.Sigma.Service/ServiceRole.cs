﻿using Eqvola.Sigma.Com;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service
{
    public abstract class ServiceRole : RoleEntryPoint
    {
        protected AutoResetEvent CompletedEvent = new AutoResetEvent(false);

        public override void OnStop()
        {
            CompletedEvent.Set();
            CompletedEvent.WaitOne();
            base.OnStop();
        }
    }
}

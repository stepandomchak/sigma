﻿using Eqvola.Sigma.Com;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service
{
    public abstract class BaseHandler : IDisposable
    {
        public abstract void Dispose();
        protected abstract Task<Response> ProccessAsync();
    }

    public abstract class BaseHandler<TRequest> : BaseHandler
    {
        protected TRequest request { get; set; }

        public virtual Task<Response> ProccessMessage(BrokeredMessage message)
        {
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TRequest>(json);
            }
            return ProccessAsync();
        }
    }

    public abstract class BaseHandler<TRequest, TService> : BaseHandler<TRequest> where TRequest : Request where TService : class, new()
    {
        protected TService service { get; set; }

        public BaseHandler()
        {
            InitService();
        }

        public virtual void InitService()
        {
            service = new TService();
        }
    }

    public static class HandlerFactory<TRequest, THandler> where THandler : BaseHandler<TRequest>, new()
    {
        public static Task<Response> ProccessMessage(BrokeredMessage message)
        {
            return Task<Response>.Run(() =>
            {
                using (var handler = new THandler())
                {
                    return handler.ProccessMessage(message);
                }
            });
        }
    }
}

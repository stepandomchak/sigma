﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Com.Wallet.Models;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class CreateWithdrawal : WalletHandler<CreateWithdrawalRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new WithdrawalResponse()
            {
                result = Com.Wallet.Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.amount <= 0)
                return new WithdrawalResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.UnexpectedAmount
                };

            if (!string.IsNullOrEmpty(request.email))
                return await SetWithdrawal();

            return new WithdrawalResponse()
            {
                entity = null,
                result = Com.Wallet.Result.InvalidRequest
            };

        }

        protected async Task<Response> SetWithdrawal()
        {
            var storageService = new Service.Storage.Service();
            var user = await storageService.GetUserByEmail(request.email);

            var currencyRequest = new GetCurrencyRequest() { currencyCode = request.currencyCode };
            var currencyResponse = await currencyRequest.GetResponse<CurrencyResponse>();

            if(currencyResponse.entity == null)
                return new WithdrawalResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.UnexpectedCurrency
                };

            if(!currencyResponse.entity.IsFixedFee)
            {
                if(request.fee > currencyResponse.entity.MaxFee || request.fee < currencyResponse.entity.MinFee)
                    return new WithdrawalResponse()
                    {
                        entity = null,
                        result = Com.Wallet.Result.UnexpectedFee
                    };
            }

            var wallet = await service.GetWalletByCurrency(user.Id, request.currencyCode);

            if (wallet == null)
            {
                return new WithdrawalResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.NoSuchWallet
                };
            }
            else if(wallet.Balance < request.amount)
                return new WithdrawalResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.UnexpectedAmount
                };

            var withdrawal = new Service.WalletService.Models.Withdrawal()
            {
                Amount = request.amount,
                Wallet = wallet,
                CreationTime = DateTime.UtcNow,
                ConfirmationTime = null,
                To = request.to,
                Comment = request.comment,
                TxId = request.txId,
                Status = (int)OperationStatus.InProcessing,
                TransactionFee = currencyResponse.entity.IsFixedFee ? currencyResponse.entity.FixedFee : request.fee
            };

            var result = await service.CreateWithdrawal(withdrawal);

            if (result.entity != null)
            {
                wallet.Balance -= request.amount + request.fee;
                await service.UpdateWallet(wallet);

                return new WithdrawalResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Com.Wallet.Result.Ok
                };
            }
            return new WithdrawalResponse()
            {
                entity = null,
                result = Com.Wallet.Result.InvalidRequest
            };
        }

    }
}

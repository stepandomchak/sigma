﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Service.Storage.Models;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class GetWithdrawal : WalletHandler<GetWithdrawalRequest>
    {
        protected override Response CreateFailResponse()
        {
            throw new NotImplementedException();
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (!string.IsNullOrEmpty(request.txId))
                return await GetWithdrawalByTxId();

            return await GetWithdrawals();
        }

        protected async Task<Response> GetWithdrawalByTxId()
        {
            var response = new WithdrawalResponse();

            response.entity = (await service.GetWithdrawal(request.txId))?.ToComObject();
            
            if (response.entity != null)
            {
                response.result = Com.Wallet.Result.Ok;
                return response;
            }
            response.result = Com.Wallet.Result.InvalidRequest;
            return response;
        }

        protected async Task<Response> GetWithdrawals()
        {
            var response = new WithdrawalsResponse();

            long userId = 0;

            User user = null;
            if(!string.IsNullOrEmpty(request.email))
            {
                var storageService = new Service.Storage.Service();
                user = await storageService.GetUserByEmail(request.email);
                if(user != null)
                    userId = user.Id;
            }

            var resp = await service.GetWithdrawals(status: request.status, userId: userId, currencyCode: request.currencyCode, 
                count: request.count, margin: request.margin, dateFrom: request.dateFrom, dateTo: request.dateTo, orderBy: request.orderBy, isAscending: request.isAscending, search: request.search);

            response.withdrawals = resp?.Select(m => m.ToComObject()).ToList();
            response.totalCount = await service.GetWithdrawalsCount(status: request.status, userId: userId, currencyCode: request.currencyCode, 
                dateFrom: request.dateFrom, dateTo: request.dateTo, search: request.search);

            if (response.withdrawals != null)
            {
                response.result = Com.Wallet.Result.Ok;
                return response;
            }
            response.result = Com.Wallet.Result.InvalidRequest;
            return response;
        }
    }
}

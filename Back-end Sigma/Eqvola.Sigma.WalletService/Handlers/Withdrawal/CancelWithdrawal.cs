﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Com.Wallet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class CancelWithdrawal : WalletHandler<CancelWithdrawalRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new WithdrawalResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if(!string.IsNullOrEmpty(request.txId))
            {
                var withdrawal = await service.GetWithdrawal(request.txId);

                if(withdrawal == null)
                    return new WithdrawalResponse()
                    {
                        result = Result.InvalidRequest
                    };

                withdrawal.Status = (int)OperationStatus.Canceled;
                var res = await service.UpdateWithdrawal(withdrawal);

                if(res.entity == null)
                    return new WithdrawalResponse()
                    {
                        result = Result.InvalidRequest
                    };

                return new WithdrawalResponse()
                {
                    result = Result.Ok
                };
            }
            return new WithdrawalResponse()
            {
                result = Result.InvalidRequest
            };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Com.Wallet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class ProcessTransaction : WalletHandler<ProcessTransactionRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new ProcessTransactionResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if(request.transaction != null)
            {
                var firstWallet = await service.GetWalletByCurrency(request.transaction.SellOrder.UserId, request.transaction.SellOrder.Currency);

                var firstWalletTarget = await service.GetWalletByCurrency(request.transaction.SellOrder.UserId, request.transaction.SellOrder.TargetCurrency);
                if (firstWalletTarget == null)
                {
                    await service.CreateWallet(new Service.WalletService.Models.Wallet()
                    {
                        CurrencyCode = request.transaction.SellOrder.TargetCurrency,
                        UserId = request.transaction.SellOrder.UserId,
                        Address = "Address",
                        Balance = 0,
                        CurrencyName = request.transaction.SellOrder.TargetCurrency
                    });
                }

                var secondWallet = await service.GetWalletByCurrency(request.transaction.BuyOrder.UserId, request.transaction.BuyOrder.Currency);

                var secondWalletTarget = await service.GetWalletByCurrency(request.transaction.BuyOrder.UserId, request.transaction.BuyOrder.TargetCurrency);
                if (firstWalletTarget == null)
                {
                    await service.CreateWallet(new Service.WalletService.Models.Wallet()
                    {
                        CurrencyCode = request.transaction.BuyOrder.TargetCurrency,
                        UserId = request.transaction.BuyOrder.UserId,
                        Address = "Address",
                        Balance = 0,
                        CurrencyName = request.transaction.BuyOrder.TargetCurrency
                    });
                }

                var firstBlocked = firstWallet.BlockedFunds.Where(s => s.OrderId == request.transaction.SellOrder.Id).FirstOrDefault();
                var secondBlocked = secondWallet.BlockedFunds.Where(s => s.OrderId == request.transaction.BuyOrder.Id).FirstOrDefault();

                if(firstBlocked == null || secondBlocked == null)
                {
                    return new ProcessTransactionResponse()
                    {
                        result = Result.InvalidRequest
                    };
                }

                await service.DeleteFunds(firstBlocked);
                await service.DeleteFunds(secondBlocked);

                double buyCommission = request.transaction.BuyOrder.Amount / 100 * request.transaction.BuyOrder.Commission;
                double sellCommission = request.transaction.SellOrder.Amount / 100 * request.transaction.SellOrder.Commission;

                //Добавляем средства на счет
                firstWalletTarget.Balance += request.transaction.BuyOrder.Amount - buyCommission;
                secondWalletTarget.Balance += request.transaction.SellOrder.Amount - sellCommission;

                await service.UpdateWallet(firstWalletTarget);
                await service.UpdateWallet(secondWalletTarget);
                
            }
            return new ProcessTransactionResponse()
            {
                result = Result.InvalidRequest
            };
        }
    }
}

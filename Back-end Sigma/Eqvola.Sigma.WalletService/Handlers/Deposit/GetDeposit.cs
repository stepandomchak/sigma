﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Service.Storage.Models;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class GetDeposit : WalletHandler<GetDepositRequest>
    {
        protected override Response CreateFailResponse()
        {
            throw new NotImplementedException();
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (!string.IsNullOrEmpty(request.txId))
                return await GetDepositByTxId();
            return await GetDeposits();
        }

        protected async Task<Response> GetDepositByTxId()
        {
            var response = new DepositResponse();

            response.entity = (await service.GetDeposit(request.txId))?.ToComObject();

            if (response.entity != null)
            {
                response.result = Com.Wallet.Result.Ok;
                return response;
            }
            response.result = Com.Wallet.Result.InvalidRequest;
            return response;
        }

        protected async Task<Response> GetDeposits()
        {
            var response = new DepositsResponse();

            long userId = 0;

            User user = null;
            if (!string.IsNullOrEmpty(request.email))
            {
                var storageService = new Service.Storage.Service();
                user = await storageService.GetUserByEmail(request.email);
                if (user != null)
                    userId = user.Id;
            }

            var resp = await service.GetDeposits(status: request.status, userId: userId, currencyCode: request.currencyCode, 
                count: request.count, margin: request.margin, dateFrom: request.dateFrom, dateTo: request.dateTo, orderBy: request.orderBy, isAscending: request.isAscending, search: request.search);

            response.deposits = resp?.Select(m => m.ToComObject()).ToList();
            response.totalCount = await service.GetDepositsCount(status: request.status, userId: userId, currencyCode: request.currencyCode, 
                dateFrom: request.dateFrom, dateTo: request.dateTo, search: request.search);
            if (response.deposits != null)
            {
                response.result = Com.Wallet.Result.Ok;
                return response;
            }
            response.result = Com.Wallet.Result.InvalidRequest;
            return response;
        }
    }
}

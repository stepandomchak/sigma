﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Com.Wallet.Models;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class CreateDeposit : WalletHandler<CreateDepositRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new DepositResponse()
            {                
                result = Com.Wallet.Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.amount <= 0)
                return new DepositResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.UnexpectedAmount
                };

            if (request.walletId <= 0)
                return new DepositResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.NoSuchWallet
                };

            if (request.walletId > 0)
                return await SetDeposit();
        
            return  new DepositResponse()
            {
                entity = null,
                result = Com.Wallet.Result.InvalidRequest
            }; 

        }

        protected async Task<Response> SetDeposit()
        {
            var storageService = new Service.Storage.Service();
            var wallet = await service.GetWalletById(request.walletId);

            if (wallet == null)
                return new DepositResponse()
                {
                    entity = null,
                    result = Com.Wallet.Result.NoSuchWallet
                };

            var deposit = new Service.WalletService.Models.Deposit()
            {
                Amount = request.amount,
                Wallet = wallet,
                CreationTime = request.Created,
                From = request.from,                
                TxId = request.txId,
                Status = (int)OperationStatus.InProcessing
            };

            var result = await service.CreateDeposit(deposit);

            if(result.entity != null)
                return new DepositResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Com.Wallet.Result.Ok
                };
            return new DepositResponse()
            {
                entity = null,
                result = Com.Wallet.Result.InvalidRequest
            };
        }

    }
}

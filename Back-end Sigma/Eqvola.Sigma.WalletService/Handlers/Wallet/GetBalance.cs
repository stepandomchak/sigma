﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Com.Wallet.Models;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class GetBalance : WalletHandler<GetBalanceRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new WalletResponse() { result = Result.InvalidRequest };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.userId > 0 && !string.IsNullOrEmpty(request.currency))
            {
                var wallet = (await service.GetWalletByCurrency(request.userId, request.currency))?.ToComObject();
                if (wallet != null)
                {
                    return new WalletResponse()
                    {
                        result = Result.Ok,
                        entity = wallet
                    };
                }
            }
            return new WalletResponse()
            {
                result = Result.NoSuchWallet
            };
        }
        
    }
}

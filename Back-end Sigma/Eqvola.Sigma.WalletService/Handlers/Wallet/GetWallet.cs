﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class GetWallet : WalletHandler<GetUserWalletRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new GetWalletsResponse();
        }

        protected async override Task<Response> ProccessAsync()
        {
            var storageService = new Service.Storage.Service();

            if (request.userId > 0)
                return await GetUserWallet(request.userId);

            if (string.IsNullOrEmpty(request.email))
                return await GetAllWallets();

            var user = await storageService.GetUserByEmail(request.email);

            if (user == null)
            {
                return new GetWalletsResponse();
            }

            if (!string.IsNullOrEmpty(request.email) && !string.IsNullOrEmpty(request.currency))
                return await GetUserWallet(user.Id);

            if (!string.IsNullOrEmpty(request.email))
                return await GetUserWallets(user.Id);
        
            return new WalletResponse()
            {
                result = Com.Wallet.Result.InvalidRequest
            };
        }

        protected async Task<Response> GetUserWallets(long userId)
        {
            var response = new GetWalletsResponse();
            response.wallets = (await service.GetUserWallets(userId)).Select(m => m.ToComObject()).ToList();
            
            return response;
        }

        protected async Task<Response> GetUserWallet(long userId)
        {
            var response = new GetWalletsResponse();
            response.wallets = (await service.GetUserWallets(userId))?.Select(m => m.ToComObject()).ToList();

            if (response.entity != null)
                response.result = Com.Wallet.Result.Ok;

            return response;
        }

        protected async Task<Response> GetUserWalletByCurrency(long userId, string currency)
        {
            var response = new WalletResponse();
            response.entity = (await service.GetWalletByCurrency(userId, currency))?.ToComObject();

            if (response.entity != null)
                response.result = Com.Wallet.Result.Ok;

            return response;
        }

        protected async Task<Response> GetAllWallets()
        {
            var response = new GetWalletsResponse();
            response.wallets = (await service.GetAllWallets()).Select(m => m.ToComObject()).ToList();

            if (response.entity != null)
                response.result = Com.Wallet.Result.Ok;

            return response;
        }

    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class CreateWallet : WalletHandler<CreateWalletRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new WalletResponse() { result = Com.Wallet.Result.InvalidRequest };
        }

        protected async override Task<Response> ProccessAsync()
        {

            /*
             GET WALLET ADDRESS FROM NODE
             */

            var storageService = new Service.Storage.Service();
            var user = await storageService.GetUserByEmail(request.email);

            if (user == null)
            {
                return new WalletResponse() { result = Com.Wallet.Result.InvalidRequest };
            }


            var getCurrencyRequest = new GetCurrencyRequest() { currencyCode = request.currencyCode };
            var getCurrencyResponse = await getCurrencyRequest.GetResponse<CurrencyResponse>();

            if (getCurrencyResponse.entity == null)
            {
                return new WalletResponse() { result = Com.Wallet.Result.InvalidRequest };
            }

            var walletCheck = await service.GetWalletByCurrency(user.Id, request.currencyCode);
            if(walletCheck != null)
            {
                return new WalletResponse() { result = Com.Wallet.Result.WalletAlreadyExists };
            }

            var wallet = new Service.WalletService.Models.Wallet()
            {
                UserId = user.Id,
                CurrencyName = getCurrencyResponse.entity.Name,
                CurrencyCode = request.currencyCode,
                Balance = 0,
                Address = "Address"
            };

            var result = await service.CreateWallet(wallet);
            if (result.entity != null)
                return new WalletResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Com.Wallet.Result.Ok
                };
            return new WalletResponse()
            {
                entity = null,
                result = Com.Wallet.Result.InvalidRequest
            };
        }

    }
}

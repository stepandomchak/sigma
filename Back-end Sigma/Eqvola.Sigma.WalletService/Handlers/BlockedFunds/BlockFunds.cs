﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class BlockFunds : WalletHandler<BlockFundsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new WalletResponse() { result = Result.InvalidRequest };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var wallet = (await service.GetWalletByCurrency(request.userId, request.currency));

            if (wallet == null)
                return new WalletResponse() { result = Result.NoSuchWallet };
            if (wallet.Balance - request.amount < 0)
                return new WalletResponse() { result = Result.InsufficientFunds };
                        
            Service.WalletService.Models.BlockedFunds funds = new Service.WalletService.Models.BlockedFunds()
            {
                Wallet = wallet,
                OrderId = request.orderId,
                Amount = request.amount
            };

            var res = (await service.SetBlockFunds(funds));

            if (res.entity != null)
            {
                wallet.Balance -= request.amount;

                await service.UpdateWallet(wallet);

                return new WalletResponse() { result = Result.Ok };
            }

            return new WalletResponse() { result = Result.InvalidRequest }; 
        }
    }
}

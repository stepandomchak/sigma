﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.WalletService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class GetBlockedFunds : WalletHandler<WalletBlockedFundsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new BlockedFundsResponse() { result = Result.InvalidRequest };
        }

        protected async override Task<Response> ProccessAsync()
        {
            if (request.userId <= 0 || string.IsNullOrEmpty(request.currencyCode))
                return new BlockedFundsResponse() { result = Result.InvalidRequest };

            var wallet = await service.GetWalletByCurrency(request.userId, request.currencyCode);

            if (wallet == null)
                return new BlockedFundsResponse() { result = Result.NoSuchWallet };


            var response = new BlockedFundsResponse();
            response.funds = (await service.GetFundsByWallet(wallet.Id)).Select(m => m.ToComObject()).ToList();

            if (response.funds != null)
            {
                return new BlockedFundsResponse()
                {
                    funds = response.funds,
                    result = Result.Ok
                };
            }
            return new BlockedFundsResponse()
            {
                result = Result.InvalidRequest
            };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Handlers
{
    public class UpdateFunds : WalletHandler<UpdateFundsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new BlockedFundsResponse() { result = Result.InvalidRequest };
        }

        protected async override Task<Response> ProccessAsync()
        {

            if (request.orderId != 0 && request.amount == 0)
            {
                return await UnblockFunds();
            }
            else
                return await DecreaseFunds();

            return new BlockedFundsResponse() { result = Result.InvalidRequest };
        }

        protected async Task<Response> UnblockFunds()
        {
            WalletResponse resp = null;

            var funds = await service.GetFundsByOrder(request.orderId);

            if (funds == null)
                return null;
            
            funds.Wallet.Balance += funds.Amount;
            funds.Amount = 0;
            await service.UpdateFunds(funds);

            resp = new WalletResponse() { result = Result.Ok };
            return resp;
        }

        protected async Task<Response> DecreaseFunds()
        {
            WalletResponse resp = null;
            var funds = await service.GetFundsByOrder(request.orderId);

            if (funds == null)
                return null;
            funds.Amount -= request.amount;

            await service.UpdateFunds(funds);

            resp = new WalletResponse() { result = Result.Ok };
            return resp;
        }
    
    }
}

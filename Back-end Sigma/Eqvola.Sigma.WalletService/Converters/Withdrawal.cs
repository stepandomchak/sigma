﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Converters
{
    public static class Withdrawal
    {
        public static Com.Wallet.Models.Withdrawal ToComObject(this Service.WalletService.Models.Withdrawal src)
        {
            return new Com.Wallet.Models.Withdrawal()
            {
                Id = src.Id,
                Amount = src.Amount,
                Wallet = src.Wallet.ToComObject(),
                Status = src.Status,
                CreationTime = src.CreationTime,
                ConfirmationTime = src.ConfirmationTime,
                TxId = src.TxId,
                TransactionFee = src.TransactionFee,
                Comment = src.Comment,
                To = src.To
            };
        }
    }
}

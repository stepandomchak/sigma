﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Converters
{
    public static class Wallet
    {
        public static Com.Wallet.Models.Wallet ToComObject(this Service.WalletService.Models.Wallet src)
        {
            return new Com.Wallet.Models.Wallet()
            {
                Id = src.Id,
                UserId = src.UserId,
                CurrencyName = src.CurrencyName,
                CurrencyCode = src.CurrencyCode,
                Balance = src.Balance,
                Address = src.Address,
                BlockedFunds = src.BlockedFunds?.Select(m => m.ToShortComObject()).ToList()
            };
        }
    }
}

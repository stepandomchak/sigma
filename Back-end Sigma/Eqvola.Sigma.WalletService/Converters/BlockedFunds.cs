﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Converters
{
    public static class BlockedFunds
    {
        public static Com.Wallet.Models.BlockedFunds ToComObject(this Service.WalletService.Models.BlockedFunds src)
        {
            return new Com.Wallet.Models.BlockedFunds()
            {
                Id = src.Id,
                Amount = src.Amount,
                Wallet = src.Wallet.ToComObject(),
                OrderId = src.OrderId
            };
        }
        public static Com.Wallet.Models.BlockedFunds ToShortComObject(this Service.WalletService.Models.BlockedFunds src)
        {
            return new Com.Wallet.Models.BlockedFunds()
            {
                Id = src.Id,
                Amount = src.Amount,
                OrderId = src.OrderId
            };
        }
    }
}

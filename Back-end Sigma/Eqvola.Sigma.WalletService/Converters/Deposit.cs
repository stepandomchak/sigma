﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.WalletService.Converters
{
    public static class Deposit
    {
        public static Com.Wallet.Models.Deposit ToComObject(this Service.WalletService.Models.Deposit src)
        {
            return new Com.Wallet.Models.Deposit()
            {
                Id = src.Id,
                Amount = src.Amount,
                Wallet = src.Wallet.ToComObject(),
                Status = src.Status,
                CreationTime = src.CreationTime,
                ConfirmationTime = src.ConfirmationTime,
                TxId = src.TxId,
                From = src.From
            };
        }
    }
}

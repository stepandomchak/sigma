using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.Service;
using Eqvola.Sigma.WalletService.Handlers;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;

namespace Eqvola.Sigma.WalletService
{
    public partial class WorkerRole : ServiceBusRole
    {
        protected override IEnumerable<string> queues => new string[] { "wallet" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Sigma.Com.Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "wallet":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Sigma.Com.Response>>>
                    {
                        { "get-balance", (message) => { return HandlerFactory<GetBalanceRequest, GetBalance>.ProccessMessage(message); } },
                        { "get-wallet", (message) => { return HandlerFactory<GetUserWalletRequest, GetWallet>.ProccessMessage(message); } },
                        { "create-wallet", (message) => { return HandlerFactory<CreateWalletRequest, CreateWallet>.ProccessMessage(message); } },

                        { "block-funds", (message) => { return HandlerFactory<BlockFundsRequest, BlockFunds>.ProccessMessage(message); } },
                        { "update-funds",  (message) => { return HandlerFactory<UpdateFundsRequest, UpdateFunds>.ProccessMessage(message); } },
                        { "get-blocked",  (message) => { return HandlerFactory<WalletBlockedFundsRequest, GetBlockedFunds>.ProccessMessage(message); } },

                        { "get-deposit", (message) => { return HandlerFactory<GetDepositRequest, GetDeposit>.ProccessMessage(message); } },
                        { "create-deposit", (message) => { return HandlerFactory<CreateDepositRequest, CreateDeposit>.ProccessMessage(message); } },

                        { "get-withdrawal", (message) => { return HandlerFactory<GetWithdrawalRequest, GetWithdrawal>.ProccessMessage(message); } },
                        { "create-withdrawal",  (message) => { return HandlerFactory<CreateWithdrawalRequest, CreateWithdrawal>.ProccessMessage(message); } },

                        { "process-transaction",  (message) => { return HandlerFactory<ProcessTransactionRequest, ProcessTransaction>.ProccessMessage(message); } }
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}

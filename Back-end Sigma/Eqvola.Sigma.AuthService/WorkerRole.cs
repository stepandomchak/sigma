﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Auth;
using Eqvola.Sigma.Com.Auth.TwoFactor;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.ServiceRuntime;
using Newtonsoft.Json;

namespace Eqvola.Sigma.AuthService
{
    public class WorkerRole : ServiceBusRole<Service.Auth.Service>
    {
        protected override IEnumerable<string> queues => new string[] { "auth" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch(queueName)
            {
                case "auth":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        {
                            "login",
                            Login
                        },
                        {
                            "reset-password",
                            ResetPassword
                        },
                        {
                            "accept-reset-password",
                            AcceptResetPassword
                        },
                        {
                            "change-password",
                            ChangePassword
                        },
                        {
                            "accept-change-password",
                            AcceptChangePassword
                        },
                        {
                            "two-factor-enable",
                            TwoFactorEnable
                        },
                        {
                            "two-factor-accept-enable",
                            TwoFactorAcceptEnable
                        },
                        {
                            "two-factor-disable",
                            TwoFactorDisable
                        }
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }

        private async Task<Response> Login(BrokeredMessage message)
        {
            Trace.WriteLine($"Login request received (SID: {message.SessionId})");
            LoginRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<LoginRequest>(json);
            }
            var response = await service.Login(request.uid, request.password, request.code, request.headers, request.key);

            return new LoginResponse() { token = response.token, status = response.status, message = response.message };
        }

        private async Task<Response> ResetPassword(BrokeredMessage message)
        {
            Trace.WriteLine($"ResetPassword request received (SID: {message.SessionId})");
            ResetRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<ResetRequest>(json);
            }

            var response = await service.ResetPassword(request.email, request.phone_number);

            Trace.WriteLine("ResetPassword " + response.ToString());
            
            return new ResetResponse() { result = response.result, message = response.message};
        }

        private async Task<Response> AcceptResetPassword(BrokeredMessage message)
        {

            Trace.WriteLine($"AcceptResetPassword request received (SID: {message.SessionId})");
            AcceptResetRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<AcceptResetRequest>(json);
            }

            var response = await service.AcceptResetPassword(request.email, request.phone_number, request.code, request.newPassword);

            return new AcceptResetResponse() { result = response.result, message = response.message };
        }

        private async Task<Response> ChangePassword(BrokeredMessage message)
        {

            Trace.WriteLine($"ChangePassword request received (SID: {message.SessionId})");
            ChangePasswordRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<ChangePasswordRequest>(json);
            }

            var response = await service.ChangePassword(request.email, request.password);

            return new ChangePasswordResponse() { result = response.result, message = response.message };
        }

        private async Task<Response> AcceptChangePassword(BrokeredMessage message)
        {

            Trace.WriteLine($"AcceptChangePassword request received (SID: {message.SessionId})");
            AcceptChangePasswordRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<AcceptChangePasswordRequest>(json);
            }

            var response = await service.AcceptChangePassword(request.email, request.newPassword, request.code, request.isHash);

            return new AcceptChangePasswordResponse() { result = response.result, message = response.message };
        }

        private async Task<Response> TwoFactorEnable(BrokeredMessage message)
        {
            Trace.WriteLine($"Twofactorstatechange request received (SID: {message.SessionId})");
            TwoFactorEnableRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TwoFactorEnableRequest>(json);
            }
            var response = await service.TwoFactorEnable(request.uid);
            Trace.WriteLine("2FA response" + response.message);
            return response;
        }

        private async Task<Response> TwoFactorAcceptEnable(BrokeredMessage message)
        {
            TwoFactorAcceptEnableRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TwoFactorAcceptEnableRequest>(json);
            }
            var response = await service.TwoFactorAcceptEnable(request.uid, request.code);
            return response;
        }

        private async Task<Response> TwoFactorDisable(BrokeredMessage message)
        {
            Trace.WriteLine($"Twofactorstatechange request received (SID: {message.SessionId})");
            TwoFactorDisableRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TwoFactorDisableRequest>(json);
            }

            return await service.TwoFactorDisable(request.uid, request.code);
        }
    }
}

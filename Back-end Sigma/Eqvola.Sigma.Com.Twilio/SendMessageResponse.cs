﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Twilio
{
    public class SendMessageResponse : Response
    {
        public string status { get; set; }
    }
}

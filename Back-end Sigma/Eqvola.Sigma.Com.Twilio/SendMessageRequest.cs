﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Twilio
{
    public class SendMessageRequest : Request
    {
        public override string QueueName => "twilio";
        public override string RequestName => "send-message";
        
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }
}

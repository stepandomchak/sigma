﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support
{
    public enum Result
    {
        Ok,
        NotFound,
        InvalidRequest,
        Exception,
        ExistenCategory,
        TopicIsEmpty,
        CategoryNotFound,
        ExistenTopic,
        TopicNotFound,
        TopicStatusIsEmptyOrNotCorrect,
        QuestionIsEmpty,
        ProblemNotFound,
        CategoryIsNotTopic,
        MessageIsEmpty,
        FileNameIsEmpty,
        AttachmentNotFound,
        FileIsNotUpload,
        FileIsNotAllowed
    }
}

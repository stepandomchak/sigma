﻿namespace Eqvola.Sigma.Com.Support.Attachment
{
    public class UploadAttachmentRequest: Request
    {
        public override string QueueName => "support";
        public override string RequestName => "attachment-upload";

        public long id { get; set; }
        public byte[] base64 { get; set; }
        public string url { get; set; }
    }
}

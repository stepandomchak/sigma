﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Category
{
    public class GetCategoryRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "category-get";

        public string name { get; set; }

        public bool IsEnable { get; set; }
    }
}

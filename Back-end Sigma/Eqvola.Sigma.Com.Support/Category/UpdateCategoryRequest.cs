﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Category
{
    public class UpdateCategoryRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "category-update";

        public long id { get; set; }
        public bool IsEnable { get; set; }

        public string newName { get; set; }
    }
}

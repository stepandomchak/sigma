﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Category
{
    public class CategoriesResponse : SupportResponse
    {
        public List<Models.Category> categories { get; set; }

        public CategoriesResponse()
        {
            categories = new List<Models.Category>();
        }
    }
}

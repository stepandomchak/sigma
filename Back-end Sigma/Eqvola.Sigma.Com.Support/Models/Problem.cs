﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public class Problem
    {
        public long Id { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public Topic Topic { get; set; }
    }
}

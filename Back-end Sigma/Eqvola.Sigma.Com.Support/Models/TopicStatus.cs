﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public enum TopicStatus
    {
        Active = 1,
        Disabled = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public class Attachment
    {
        public long Id { get; set; }

        public string Url { get; set; }

        public string FileName { get; set; }

    }
}

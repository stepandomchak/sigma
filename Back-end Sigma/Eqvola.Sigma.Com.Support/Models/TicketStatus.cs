﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public enum TicketStatus
    {
        Open = 1,
        Closed = 2,
        Processing = 4
    }
}

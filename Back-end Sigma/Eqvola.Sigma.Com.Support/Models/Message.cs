﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public class Message
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public DateTime Created { get; set; }
        
        public string Text { get; set; }

        public int Status { get; set; }
        
        public Ticket Ticket { get; set; }

        public IEnumerable<Attachment> Attachments { get; set; }
    }
}

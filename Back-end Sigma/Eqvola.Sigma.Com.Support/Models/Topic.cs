﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public class Topic
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public Category Category { get; set; }
        public TopicStatus TopicStatus { get; set; }
    }
}

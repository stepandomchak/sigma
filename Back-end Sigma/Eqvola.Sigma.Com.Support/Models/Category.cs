﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public class Category
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsEnable { get; set; }

        public ICollection<Topic> Topics { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Models
{
    public class Ticket
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public DateTime Created { get; set; }

        public Category Category { get; set; }

        public int TicketStatus { get; set; }

        public IEnumerable<Message> Messages { get; set; }

    }
}

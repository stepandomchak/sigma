﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Message
{
    public class GetMessageRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "message-get";

        public long ticketId { get; set; }

        public int count { get; set; }
        public int margin { get; set; }
        public string takerEmail { get; set; }
    }
}

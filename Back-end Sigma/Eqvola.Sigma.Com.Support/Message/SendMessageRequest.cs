﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Message
{
    public class SendMessageRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "message-send";

        public string email { get; set; }        
        public string text { get; set; }
        public long ticketId { get; set; }
        public IEnumerable<string> listAttachments { get; set; }
    }
       
}

﻿using Eqvola.Sigma.Com.Support.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Message
{
    public class MessagesResponse : SupportResponse<Models.Message>
    {
        public IEnumerable<Models.Message> messages { get; set; }

        public MessagesResponse()
        {
            messages = new List<Models.Message>();
        }
    }
}

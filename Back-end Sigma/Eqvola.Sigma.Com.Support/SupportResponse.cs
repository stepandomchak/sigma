﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support
{
    public class SupportResponse : Response
    {
        public Result result { get; set; }
    }

    public class SupportResponse<T> : SupportResponse
    {
        public T entity { get; set; }
        public string message { get; set; } = "";            
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Ticket
{
    public class UpdateTicketRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "ticket-update";
        
        public long id { get; set; }

        public int status { get; set; }
    }
}

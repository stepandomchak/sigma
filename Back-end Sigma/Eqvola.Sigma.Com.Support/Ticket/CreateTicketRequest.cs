﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Ticket
{
    public class CreateTicketRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "ticket-create";
        
        public string email { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public List<string> attachments { get; set; }
        public long categoryId { get; set; }

    }
}

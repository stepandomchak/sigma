﻿using Eqvola.Sigma.Com.Support.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Ticket
{
    public class TicketsResponse : SupportResponse
    {
        public IEnumerable<Models.Ticket> tickets { get; set; }
        public int totalCount { get; set; }

        public TicketsResponse()
        {
            tickets = new List<Models.Ticket>();
        }

    }
}

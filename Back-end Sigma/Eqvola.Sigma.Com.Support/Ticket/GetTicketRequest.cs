﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Ticket
{
    public class GetTicketRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "ticket-get";

        public long id { get; set; }

        public string email { get; set; }

        public int count { get; set; }
        public int margin { get; set; }
        public int status { get; set; }
        public string categoryName { get; set; }
        public string search { get; set; }
        public string orderBy { get; set; }
        public bool isAscending { get; set; }

        public string takerEmail { get; set; }
    }
}

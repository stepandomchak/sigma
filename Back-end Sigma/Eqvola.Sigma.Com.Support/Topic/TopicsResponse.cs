﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Topic
{
    public class TopicsResponse : SupportResponse
    {
        public List<Models.Topic> topics { get; set; }

        public TopicsResponse()
        {
            topics = new List<Models.Topic>();
        }
    }


}

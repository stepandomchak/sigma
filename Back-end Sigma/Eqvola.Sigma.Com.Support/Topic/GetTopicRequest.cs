﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Topic
{
    public class GetTopicRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "topic-get";

        public long categoryId { get; set; }
    }
}

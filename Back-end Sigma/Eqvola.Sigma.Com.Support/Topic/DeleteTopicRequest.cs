﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Topic
{
    public class DeleteTopicRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "topic-delete";

        public long id { get; set; }
    }
}

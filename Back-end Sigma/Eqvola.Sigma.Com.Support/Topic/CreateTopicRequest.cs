﻿using Eqvola.Sigma.Com.Support.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Topic
{
    public class CreateTopicRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "topic-create";

        public string Name { get; set; }
        public string Text { get; set; }
        public long CategoryId { get; set; }   
    }
}

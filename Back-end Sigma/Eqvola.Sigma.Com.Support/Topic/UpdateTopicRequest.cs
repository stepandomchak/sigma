﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Support.Topic
{
    public class UpdateTopicRequest : Request
    {
        public override string QueueName => "support";
        public override string RequestName => "topic-update";

        public long Id { get; set; }
        public string NewName { get; set; }
        public string Text { get; set; }
        public long NewCategoryId { get; set; }
        public int TopicStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Converters
{
    public static class Currency
    {
        public static Com.Market.Models.Currency ToComObject(this Service.MarketService.Models.Currency src)
        {
            return new Com.Market.Models.Currency()
            {
                Code = src.Code,
                Name = src.Name,
                Status = src.Status,
                IsFixedFee = src.IsFixedFee,
                FixedFee = src.FixedFee,
                MinFee = src.MinFee,
                MaxFee = src.MaxFee
            };
        }
    }
}

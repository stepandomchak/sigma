﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Converters
{
    public static class Market
    {
        public static Com.Market.Models.Market ToComObject(this Service.MarketService.Models.Market src)
        {
            return new Com.Market.Models.Market()
            {
                MasterCurrecyCode = src.MasterCurrencyCode,
                TargetCurrencies = src.TargetCurrencies?.Select(m => m.ToComObject()).ToList(),
                Status = src.Status
            };
        }
    }
}

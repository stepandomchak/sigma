﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Converters
{
    public static class Pair
    {
        public static Com.Market.Models.Pair ToComObject(this Service.MarketService.Models.Pair src)
        {
            return new Com.Market.Models.Pair()
            {
                CurrencyPair = src.CurrencyPair,
                MarketCurrency = src.MarketCurrency
                //Status =src.Status
            };
        }
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.MarketService.Handlers;
using Eqvola.Sigma.Service;
using Microsoft.Owin.Hosting;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure;

namespace Eqvola.Sigma.MarketService
{
    public partial class WorkerRole : ServiceBusRole
    {
        protected override IEnumerable<string> queues => new string[] { "market" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Sigma.Com.Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "market":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        { "price-update", (message) => { return HandlerFactory<UpdatePriceRequest, UpdatePrice>.ProccessMessage(message); } },
                        { "pair-check", (message) => { return HandlerFactory<CheckPairRequest, CheckPair>.ProccessMessage(message); } },
                        { "get-all", (message) => { return HandlerFactory<GetMarketsRequest, GetMarkets>.ProccessMessage(message); } },
                        { "get-currency", (message) => { return HandlerFactory<GetCurrencyRequest, GetCurrency>.ProccessMessage(message); } },
                        { "update-currency", (message) => { return HandlerFactory<UpdateCurrencyRequest, UpdateCurrency>.ProccessMessage(message); } },
                        { "get-all-currencies", (message) => { return HandlerFactory<GetAllCurrenciesRequest, GetAllCurrencies>.ProccessMessage(message); } },
                        { "get-quotations", (message) => { return HandlerFactory<GetQuotationsRequest, GetQuotations>.ProccessMessage(message); } }
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }

        public override void Run()
        {
            var endpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["wsMarket"];
            string baseUri = String.Format("{0}://{1}", endpoint.Protocol, endpoint.IPEndpoint);
            WebApp.Start<Startup>(baseUri);
            
            try
            {
                Service.MarketService.Service.Init();
            } catch(Exception ex)
            {
                string str = ex.Message;
            }

            base.Run();
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.MarketService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class GetMarkets : MarketHandler<GetMarketsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new MarketsResponse();
        }

        protected override async Task<Response> ProccessAsync()
        {
            var response = new MarketsResponse();
            response.markets = (await service.GetAllMarkets()).Select(x => x.ToComObject()).ToList();
            return response;
        }
    }
}

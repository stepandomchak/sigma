﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.MarketService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class UpdateCurrency : MarketHandler<UpdateCurrencyRequest>
    {
        protected override Response CreateFailResponse()
        {
            return null;
        }

        protected override async Task<Response> ProccessAsync()
        {
            Service.MarketService.Models.Currency currency = await service.GetCurrencyByCode(request.CurrencyCode);

            if (currency == null)
                return null;

            currency.IsFixedFee = request.IsFixedFee;
            currency.FixedFee = request.FixedFee;
            currency.MinFee = request.MinFee;
            currency.MaxFee = request.MaxFee;

            var res = await service.UpdateCurrency(currency);

            if (res.entity != null)
                return new CurrencyResponse()
                {
                    entity = res.entity.ToComObject()
                };

            return null;
        }
    }
}

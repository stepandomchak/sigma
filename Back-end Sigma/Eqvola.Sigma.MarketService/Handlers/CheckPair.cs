﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class CheckPair : MarketHandler<CheckPairRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CheckPairResponse() { pairStatus = false };
        }

        protected override async Task<Response> ProccessAsync()
        {
            var pair = await service.CheckPair(request.fisrtCurrencyCode, request.secondCurrencyCode);
            return new CheckPairResponse() { pairStatus = pair };
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Eqvola.Sigma.MarketService.Converters;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class GetQuotations : MarketHandler<GetQuotationsRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new GetQuotationsResponse();
        }

        protected async override Task<Response> ProccessAsync()
        {
            return await Task.Run(() =>
            {
                var quotations = service.GetQuotations();

                if (quotations == null || quotations.PairPrices == null)
                {
                    return new GetQuotationsResponse();
                }
                return new GetQuotationsResponse() { PairPrices = quotations.PairPrices };
            });            
        }
    }
}
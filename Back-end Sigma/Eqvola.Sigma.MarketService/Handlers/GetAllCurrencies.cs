﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Eqvola.Sigma.MarketService.Converters;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class GetAllCurrencies : MarketHandler<GetAllCurrenciesRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CurrenciesResponse();
        }

        protected async override Task<Response> ProccessAsync()
        {
            var currencies = (await service.GetAllCurrencies());

            if (currencies != null)
                return new CurrenciesResponse()
                {
                    currencies = currencies.Select(c => c.ToComObject()).ToList()
                };
            else
                return new CurrenciesResponse();            
        }
    }
}
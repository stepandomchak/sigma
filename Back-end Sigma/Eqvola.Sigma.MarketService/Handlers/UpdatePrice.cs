﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class UpdatePrice : MarketHandler<UpdatePriceRequest>
    {
        protected override Response CreateFailResponse()
        {
            return null;
        }

        protected override async Task<Response> ProccessAsync()
        {
            service.OrderUpdated(request.updatedOrder, request.oldOrder);
            return null;
        }
    }
}

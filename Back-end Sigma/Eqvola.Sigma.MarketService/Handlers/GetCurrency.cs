﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.MarketService.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.MarketService.Handlers
{
    public class GetCurrency : MarketHandler<GetCurrencyRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new CurrencyResponse()
            {
                entity = null
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var currency = (await service.GetCurrencyByCode(request.currencyCode));

            if (currency != null)
                return new CurrencyResponse()
                {
                    entity = currency.ToComObject()
                };
            else
                return new CurrencyResponse()
                {
                    entity = null
                };            
        }
    }
}
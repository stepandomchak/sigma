﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;

namespace Eqvola.Sigma.Service.Storage
{
    public class Context : DbContext
    {
        private bool isAdminModel;

        static Context()
        {
            Database.SetInitializer<Context>(null);
        }

        public Context()
            : base("Name=SigmaDbContext")
        {
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public Context(string connectionString, bool isAdminModel = false)
            : base(connectionString)
        {
            this.isAdminModel = isAdminModel;
        }

        public DbModelBuilder GetAdminModelBuilder(DbModelBuilder modelBuilder)
        {
             var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
          .Where(type => !string.IsNullOrEmpty(type.Namespace))
          .Where(type => type.BaseType != null && type.BaseType.IsGenericType
               && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }

            modelBuilder.Entity<WLUser>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<User>().Ignore(i => i.IsVerified);
            modelBuilder.Entity<User>().Ignore(i => i.UserDocuments);
            modelBuilder.Entity<User>().Ignore(i => i.RegistrationDate);

            modelBuilder.Entity<User>()
                .HasMany<ImContact>(g => g.ImContacts).WithOptional(o => o.user);

            modelBuilder.Entity<Manager>()
                .HasMany<ImContact>(g => g.ImContacts).WithOptional(o => o.manager);

            modelBuilder.Entity<TwoFactor>().ToTable("TwoFactors");
            modelBuilder.Entity<UserTwoFactor>().ToTable("UserTwoFactors");
            modelBuilder.Entity<TwoFactorAction>().ToTable("TwoFactorActions");
            return modelBuilder;
        }

        public DbModelBuilder GetWLModelBuilder(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
          .Where(type => !string.IsNullOrEmpty(type.Namespace))
          .Where(type => type.BaseType != null && type.BaseType.IsGenericType
               && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }

            modelBuilder.Ignore<Department>();
            modelBuilder.Entity<Manager>().Ignore(i => i.Department);
            modelBuilder.Entity<Manager>().Ignore(i => i.Rank);

            modelBuilder.Entity<WLUser>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<User>()
                .HasMany<ImContact>(g => g.ImContacts).WithOptional(o => o.user);

            modelBuilder.Entity<Manager>()
                .HasMany<ImContact>(g => g.ImContacts).WithOptional(o => o.manager);

            modelBuilder.Entity<TwoFactor>().ToTable("TwoFactors");
            modelBuilder.Entity<UserTwoFactor>().ToTable("UserTwoFactors");
            modelBuilder.Entity<TwoFactorAction>().ToTable("TwoFactorActions");
            return modelBuilder;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if(isAdminModel)
            {
                modelBuilder = GetAdminModelBuilder(modelBuilder);
            }
            else
            {
                modelBuilder = GetWLModelBuilder(modelBuilder);
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}

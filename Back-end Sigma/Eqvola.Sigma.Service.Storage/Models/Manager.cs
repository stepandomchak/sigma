﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class ManagerTag : Tag<Manager> { }
    
    public class Manager : BaseEntity<long>
    {
        public virtual Department Department { get; set; }
        [Required]
        public virtual User User { get; set; }
        [Required]
        public int Rank { get; set; }
        [Required]
        [DefaultValue(true)]
        public int Status { get; set; }

        public string FiringReason { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }
        public virtual ICollection<ManagerTag> Tags { get; set; }
        public virtual ICollection<ImContact> ImContacts { get; set; }

        public Manager()
        {
            Permissions = new List<Permission>();
            Tags = new List<ManagerTag>();
            ImContacts = new List<ImContact>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public enum TrashState
    {
        Active,
        Deleted,
        Restored
    }

    public interface ITrashDelete
    {
        TrashState TarshState { get; set; }
        DateTime Created { get; set; }
        DateTime LastDeleted { get; set; }
        DateTime LastRestored { get; set; }
    }
}

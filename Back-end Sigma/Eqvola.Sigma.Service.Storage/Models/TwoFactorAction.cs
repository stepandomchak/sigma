﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class TwoFactorAction : BaseEntity<long>
    {
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string Name { get; set; }

        public int Status { get; set; }

        public virtual ICollection<TwoFactorActionOption> TwoFactorActionOptions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class ResetPassword : BaseEntity
    {

        
        [Key]
        [ForeignKey("User")]
        [Index(IsUnique = true)]
        public long UserId { get; set; }

        public virtual User User { get; set; }
        
        [MaxLength(128)]
        public string Code { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime Created { get; set; }

        public int Attempts { get; set; }

        private ResetPassword() { }

        public ResetPassword(string code)
        {
            Created = DateTime.UtcNow;
            Attempts = 1;
            Code = code;
        }

        public string UpdateCode(string code)
        {
            
            if(Created.DayOfYear == DateTime.UtcNow.DayOfYear && Created.Year == DateTime.UtcNow.Year)
            {
                if(Attempts >= 10)
                {
                    return "Limit of attempts to change password per day exceeded";
                }
                if(Created.AddMinutes(1) > DateTime.UtcNow)
                {
                    return "Reset code cannot be updated";
                }
                Attempts++;
            }
            else
            {
                Attempts = 1;
            }
            Created = DateTime.UtcNow;
            this.Code = code;
            return null;
        }

        public bool CheckCode(string code)
        {
            return this.Code.Equals(code);
        }


    }
}

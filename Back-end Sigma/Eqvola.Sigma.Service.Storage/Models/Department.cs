﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class DepartmentTag : Tag<Department> { }

    public class Department : BaseEntity<long>
    {
        public Department()
        {
            Permissions = new List<Permission>();
            Tags = new List<DepartmentTag>();
            Managers = new List<Manager>();
        }

        [Required]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [DefaultValue(true)]
        public int Status { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }
        public virtual ICollection<DepartmentTag> Tags { get; set; }

        public virtual ICollection<Manager> Managers { get; set; }
    }
}

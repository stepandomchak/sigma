﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public enum DocTypeStatus
    {
        Active = 1,
        Disable = 2
    }
    public class DocumentType : BaseEntity<long>
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public int Status { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public virtual IEnumerable<UserDocument> UserDocuments { get; set; }        

    }
}

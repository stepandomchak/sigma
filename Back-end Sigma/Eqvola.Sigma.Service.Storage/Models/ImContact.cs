﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class ImContact : BaseEntity<long>
    {
        public string messanger { get; set; }
        public string contact { get; set; }
        public virtual Manager manager { get; set; }
        public virtual User user { get; set; }
    }
}

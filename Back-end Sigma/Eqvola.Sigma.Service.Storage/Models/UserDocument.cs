﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public enum DocumentStatus
    {
        InProcessing = 1,
        Accepted = 2,
        Rejected = 4
    }

    public class UserDocument : BaseEntity<long>
    {
        [Required]
        public virtual DocumentType Type { get; set; }

        [Required]
        public virtual User User { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
                
        [Column(TypeName = "DateTime")]
        public DateTime UploadDate { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string Guid { get; set; }

        [MaxLength(128)]
        public string Comment { get; set; }

        public int Status { get; set; }
               
    }
}

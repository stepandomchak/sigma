﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class TwoFactorActionOption : BaseEntity<long>
    {
        [Required]
        public virtual TwoFactorAction Action { get; set; }

        [Required]
        [MaxLength(128)]
        public string Key { get; set; }
    }
}

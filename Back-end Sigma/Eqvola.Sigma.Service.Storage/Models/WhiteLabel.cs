﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class WhiteLabel: BaseEntity<long>
    {
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string name { get; set; }


        public virtual ICollection<WLUser> Users { get; set; }
        public virtual ICollection<WlApiKey> ApiKeys { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class User : BaseEntity<long>
    {
        [Required]
        [MinLength(6)]
        [MaxLength(128)]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [MaxLength(64)]
        public string FirstName { get; set; }
        [MaxLength(64)]
        public string LastName { get; set; }

        
        [MaxLength(16)]
        public string Phone_code { get; set; }
        [MaxLength(32)]
        public string Phone_number { get; set; }

        [MaxLength(128)]
        public string Country { get; set; }

        [MaxLength(128)]
        public string City { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        [MaxLength(64)]
        public string TimeZone { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime RegistrationDate { get; set; }

        [MaxLength(512)]
        public string AvatarUrl { get; set; }

        public bool IsTwoFactorEnabled { get; set; }

        public string TwoFactorSecretKey { get; set; }

        public virtual ResetPassword ResetPassword { get; set; }
        
        public bool IsVerified { get; set; }
        
        public virtual ICollection<ImContact> ImContacts { get; set; }

        public virtual ICollection<UserDocument> UserDocuments { get; set; }

        public User()
        {
            ImContacts = new List<ImContact>();
            UserDocuments = new List<UserDocument>();
            RegistrationDate = DateTime.UtcNow;
        }
    }
}

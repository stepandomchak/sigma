﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class Permission : BaseEntity<long>
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public virtual ICollection<Manager> Managers { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
    }
}

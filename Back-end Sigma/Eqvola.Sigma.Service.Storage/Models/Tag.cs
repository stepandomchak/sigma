﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public interface ITagable<T>
    {
        ICollection<ITag<T>> Tags { get; set; }
    }

    public interface ITag
    {
        long Id { get; set; }
        string Name { get; set; }
    }

    public interface ITag<T> : ITag
    {
        ICollection<T> Entities { get; set; }
    }

    public abstract class Tag : BaseEntity<long>, ITag
    {
        public string Name { get; set; }
    }

    public class Tag<T> : Tag, ITag<T>
    {
        public ICollection<T> Entities { get; set; }
    }
}

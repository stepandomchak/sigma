﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class UserTwoFactorOption: BaseEntity<long>
    {
        public virtual UserTwoFactor UserTwoFactor { get; set; }

        public string Key { get; set; }

        public double Value { get; set; }
    }
}

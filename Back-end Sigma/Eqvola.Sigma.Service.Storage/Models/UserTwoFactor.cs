﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class UserTwoFactor : BaseEntity<long>
    {
        [Index("IX_UserTwoFactor", 1, IsUnique = true)]
        public virtual User User { get; set; }

        public virtual TwoFactor TwoFactor { get; set; }

        [Index("IX_UserTwoFactor", 2, IsUnique = true)]
        public virtual TwoFactorAction TwoFactorAction { get; set; }

        public int State { get; set; }

        public string Secret { get; set; }
        
        public virtual ICollection<UserTwoFactorOption> UserTwoFactorOptions { get; set; }

        public UserTwoFactor()
        {
            UserTwoFactorOptions = new List<UserTwoFactorOption>();
        }

    }
}

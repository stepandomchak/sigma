﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class WlApiKey : BaseEntity<long>
    {
        [Required]
        [MaxLength(128)]
        public string Key { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime? ValidFrom { get; set; }
        
        [Column(TypeName = "DateTime")]
        public DateTime? ValidTo { get; set; }
        
        //public virtual ICollection<Permission> Permissions { get; set; }

        public WhiteLabel Wl { get; set; }
    }

}

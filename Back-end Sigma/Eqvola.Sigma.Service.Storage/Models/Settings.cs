﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Models
{
    public class Settings : BaseEntity<long>
    {
        [Required]
        [MaxLength(128)]
        public string Key { get; set; }

        [Required]
        public double Value { get; set; }
    }
}

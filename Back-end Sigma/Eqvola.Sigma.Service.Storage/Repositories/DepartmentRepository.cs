﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Repositories
{
    public class DepartmentRepository : Repository<Department, long>
    {
        public DepartmentRepository(Context context) : base(context) { }

        public async Task<Department> GetByName(string name)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(d => d.Name == name).FirstOrDefault();
            });
        }
    }
}

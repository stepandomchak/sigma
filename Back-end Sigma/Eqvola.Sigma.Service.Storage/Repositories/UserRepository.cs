﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Repositories
{
    public class UserRepository : Repository<User, long>
    {
        public UserRepository(Context context) : base(context) { }

        public async Task<User> GetByEmail(string email)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Email == email).FirstOrDefault();
            });
        }

        public async Task<int> GetCount()
        {
            return await Task.Run(() =>
            {
                return Entities.Count();
            });
        }

        public async Task<User> GetByEmailAndPassword(string email, string password)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Email == email && m.Password == password).FirstOrDefault();
            });
        }

        public async Task<List<User>> GetUsersByPagination(int count, int margin)
        {
            return await Task.Run(() =>
            {
                return Entities.OrderBy(s => s.Id).Skip(margin).Take(count).ToList();
            });
        }

        public async Task<List<User>> GetRangeByEmails(List<string> emails)
        {
            if (emails == null || emails.Count == 0)
                return await Task.Run(() =>
                {
                    return new List<User>();
                });

            return await Task.Run(() =>
            {
                return Entities.Where(m => emails.Contains(m.Email)).ToList();
            });
        }

        public async Task<List<User>> GetRangeByIds(List<long> ids)
        {
            if (ids == null || ids.Count == 0)
                return await Task.Run(() =>
                {
                    return new List<User>();
                });

            return await Task.Run(() =>
            {
                return Entities.Where(m => ids.Contains(m.Id)).ToList();
            });
        }
    }
}

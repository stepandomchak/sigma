﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Repositories
{
    public class TagRepository<T> : Repository<T, long> where T : BaseEntity<long>, ITag
    {
        public TagRepository(Context context) : base(context) { }

        public async Task<T> GetByName(string name)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(e => e.Name == name).FirstOrDefault();
            });
        }

        public async Task<ICollection<T>> GetAll(string name)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(e => e.Name == name).ToList();
            });
        }
    }
}

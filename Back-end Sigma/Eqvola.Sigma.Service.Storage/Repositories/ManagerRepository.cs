﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Repositories
{
    public class ManagerRepository : Repository<Manager, long>
    {
        public ManagerRepository(Context context) : base(context) { }

        public async Task<Manager> GetByUid(string uid)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.User.Email == uid).FirstOrDefault();
            });
        }
    }
}

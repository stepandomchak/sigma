﻿using Eqvola.Sigma.Service.Storage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.Storage.Repositories
{
    public class DocumentRepository : Repository<UserDocument, long>
    {
        public DocumentRepository(Context context) : base(context) { }


        public async Task<List<UserDocument>> GetDocsByPagination(int status, int count, int margin)
        {
            return await Task.Run(() =>
            {
                return Entities.OrderBy(s => s.Id).Skip(margin).Take(count).Where(d => (d.Status & status) > 0).ToList();
            });
        }

        public async Task<int> GetUserDocsCount()
        {
            return await Task.Run(() =>
            {
                return Entities.Count();
            });
        }

    }
}

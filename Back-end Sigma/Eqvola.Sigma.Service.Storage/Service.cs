﻿using Microsoft.Azure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eqvola.Sigma.Service.Storage.Repositories;
using Eqvola.Sigma.Service.Storage.Models;
using System.Diagnostics;
using System;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;

namespace Eqvola.Sigma.Service.Storage
{
    public class Service : IDisposable
    {
        public Context db;

        public Service()
        {
            db = new Context(CloudConfigurationManager.GetSetting("SigmaDbContext"));
        }

        public Service(bool isAdmin)
        {
            db = new Context(CloudConfigurationManager.GetSetting("SigmaDbContext"), isAdmin);

        }

        public void Dispose()
        {
            db.Dispose();
        }


        #region User
        public async Task<Manager> GetManagerByUidAndPassword(string uid, string password)
        {
            try
            {
                var res = (await (new Repository<Manager, long>(db)).Where(m => m.User.Email == uid
                                                            && m.User.Password == password)).FirstOrDefault();
                if (res == null)
                    return null;
                if (res.User != null)
                {
                    if (res.User.Password.GetHashCode() ==
                        password.GetHashCode())
                        return res;
                }
                return null;
            }
            catch (Exception ex) { return null; }
        }

        public async Task<User> GetUserByEmailAndPassword(string email, string password)
        {
            return await (new UserRepository(db)).GetByEmailAndPassword(email, password);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await (new UserRepository(db)).GetByEmail(email);
        }

        public async Task<IEnumerable<User>> GetAllUsers(int count, int margin)
        {
            return (await (new UserRepository(db)).GetUsersByPagination(count, margin));
        }

        public async Task<int> GetUsersCount()
        {
            return (await (new UserRepository(db)).GetCount());
        }

        public async Task<List<User>> GetRangeUsersByEmails(List<string> users)
        {
            return await (new UserRepository(db)).GetRangeByEmails(users);
        }

        public async Task<List<User>> GetRangeUsersByIds(List<long> users)
        {
            return await (new UserRepository(db)).GetRangeByIds(users);
        }

        public async Task<RepositoryResponse<User>> AddUser(User user)
        {
            return await (new UserRepository(db)).Add(user);
        }

        public async Task<WhiteLabel> GetWhiteLabel(int id)
        {
            return (await (new Repository<WhiteLabel>(db)).Where(m => m.Id == id)).FirstOrDefault();
        }
        public async Task<WhiteLabel> GetWhiteLabelByName(string name)
        {
            return (await (new Repository<WhiteLabel>(db)).Where(m => m.name == name)).FirstOrDefault();
        }
        public async Task<WhiteLabel> GetWhiteLabelByUser(long userId)
        {
            long wlId = (await (new Repository<WLUser>(db)).Where(m => m.UserId == userId)).FirstOrDefault().Id;
            return (await (new Repository<WhiteLabel>(db)).Where(m => m.Id == wlId)).FirstOrDefault();
        }
        public async Task<WlApiKey> GetWlApiKey(long wlId)
        {
            try
            {
                WlApiKey wlApiKey = (await (new Repository<WlApiKey, long>(db)).Where(m => m.Wl.Id == wlId)).FirstOrDefault();
                return wlApiKey;
            }
            catch (Exception ex) { return null; }
        }

        public async Task<WLUser> AddWlUser(WLUser user)
        {
            return await (new Repository<WLUser>(db)).Add(user);
        }
        public async Task<WLUser> GetWlUserByUser(long userId)
        {
            var res = (await (new Repository<WLUser>(db)).Where(m => m.UserId == userId)).FirstOrDefault();

            return res;
        }

        public async Task<RepositoryResponse<User>> UpdateUser(User user)
        {
            return await (new UserRepository(db)).Update(user);
        }

        public async Task<RepositoryResponse<User>> UpdateUserPassword(string email, string phone_number, string code, string password, bool isHash = false)
        {
            UserRepository userRepository = new UserRepository(db);
            User user = await (userRepository.GetByEmail(email));

            string phone = user.Phone_code + user.Phone_number;
            if (!phone.Equals(phone_number))
                return new RepositoryResponse<User>()
                {
                    entity = null,
                    message = "Phone numbers is not equal"
                };
            if (!isHash)
            {
                if (user.ResetPassword == null)
                    return new RepositoryResponse<User>()
                    {
                        entity = null,
                        message = "Reset password is empty"
                    };

                if (!user.ResetPassword.CheckCode(code))
                    return new RepositoryResponse<User>()
                    {
                        entity = null,
                        message = "Failed to change password"
                    };
                user.Password = password;

                var codeRepository = new Repository<ResetPassword>(db);
                var resetCodeModel = (await codeRepository.Where(q => q.UserId == user.Id)).FirstOrDefault();
                if (resetCodeModel != null)
                {
                    await codeRepository.Delete(resetCodeModel);
                }
            }
            else
            {
                if (!(user.Password == EncodingPassword(code)))
                    return new RepositoryResponse<User>()
                    {
                        entity = null,
                        message = "Failed to change password"
                    };
                user.Password = password;
                ManagerRepository managerRepository = new ManagerRepository(db);
                var manager = await managerRepository.GetByUid(user.Email);
                if (manager.Status == 0)
                {
                    manager.Status = 1;
                    await managerRepository.Update(manager);
                }

            }
            return await userRepository.Update(user);

        }

        private string EncodingPassword(string password)
        {
            string salt = CloudConfigurationManager.GetSetting("MD5.Salt");
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] bytes = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(salt + password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }

        public async Task<string> UpdateUserResetCode(string code, User user)
        {
            var repository = new Repository<ResetPassword>(db);

            var resetCodeModel = (await repository.Where(q => q.UserId == user.Id)).FirstOrDefault();
            string response = null;
            if (resetCodeModel == null)
            {
                resetCodeModel = new ResetPassword(code);
                resetCodeModel.UserId = user.Id;
                await repository.Add(resetCodeModel);
            }
            else
            {
                response = resetCodeModel.UpdateCode(code);
                if (response == null)
                {
                    await repository.Update(resetCodeModel);
                }
            }
            return response;
        }

        public async Task<string> UpdateRegistrationCode(string code, string number)
        {
            var repository = new Repository<Registration>(db);

            var registrationModel = (await repository.Where(q => q.phoneNumber == number)).FirstOrDefault();
            string response = null;
            if (registrationModel == null)
            {
                registrationModel = new Registration(code);
                registrationModel.phoneNumber = number;
                await repository.Add(registrationModel);
            }
            else
            {
                response = registrationModel.UpdateCode(code);
                if (response == null)
                {
                    await repository.Update(registrationModel);
                }
            }
            return response;
        }

        public async Task<bool> CheckUserRegistrationCode(string code, string phone)
        {
            var registration = (await (new Repository<Registration>(db)).Where(x => x.phoneNumber == phone)).FirstOrDefault();
            if (registration == null)
            {
                return false;
            }
            return registration.CheckCode(code);
        }

        public async Task<bool> CheckNumber(string phone_code, string phone_number)
        {
            var user = (await (new Repository<User>(db)).Where(u => u.Phone_code == phone_code &&
                                                            u.Phone_number == phone_number)).FirstOrDefault();
            return user == null;
        }

        #endregion

        #region Manager
        public async Task<Manager> GetManagerById(int id)
        {
            return (await (new Repository<Manager, long>(db)).Where(m => m.Id == id)).FirstOrDefault();
        }

        public async Task<Manager> GetManagerByUId(string uid)
        {
            return await new ManagerRepository(db).GetByUid(uid);
        }

        public async Task<ICollection<Manager>> GetAllManagers(int status)
        {
            if (status == 0)
                return (await (new ManagerRepository(db)).Where(m => m.Status == 0)).ToList();

            return (await (new ManagerRepository(db)).Where(m => (m.Status & status) > 0)).ToList();
        }

        public async Task<RepositoryResponse<Manager>> CreateManager(Manager manager)
        {
            return await new ManagerRepository(db).Add(manager);
        }

        public async Task<RepositoryResponse<Manager>> UpdateManager(Manager manager)
        {
            return await new ManagerRepository(db).Update(manager);
        }
        #endregion

        #region Department
        public async Task<Department> GetDepartmentById(long id)
        {
            return (await (new DepartmentRepository(db)).GetById(id));
        }

        public async Task<Department> GetDepartmentByName(string name)
        {
            var tmp = (await (new DepartmentRepository(db)).Where(m => m.Name == name)).FirstOrDefault();
            return tmp;
        }

        public async Task<ICollection<Department>> GetAllDepartments(int status)
        {
            return (await (new DepartmentRepository(db)).Where(m => (m.Status & status) > 0)).ToList();
        }

        public async Task<RepositoryResponse<Department>> CreateDepartment(Department department)
        {
            return await (new DepartmentRepository(db)).Add(department);
        }

        public async Task<RepositoryResponse<Department>> UpdateDepartment(Department department)
        {
            return await (new DepartmentRepository(db)).Update(department);
        }

        #endregion

        #region Permissions
        public async Task<ICollection<Permission>> MapPermissions(IEnumerable<string> permissions)
        {
            return (await (new Repository<Permission, long>(db)).Where(p => permissions.Contains(p.Name))).ToList();
        }
        #endregion

        #region Tags
        public async Task<bool> TagDepartment(string departmentName, string tagName)
        {
            var departments = new DepartmentRepository(db);
            var dep = await departments.GetByName(departmentName);
            if (dep != null && !dep.Tags.Any(t => t.Name == tagName))
            {
                var tags = new TagRepository<DepartmentTag>(db);
                var tag = await tags.GetByName(tagName);
                if (tag == null)
                {
                    tag = new DepartmentTag
                    {
                        Name = tagName
                    };
                    await tags.Add(tag);
                }
                dep.Tags.Add(tag);
                await db.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<ICollection<DepartmentTag>> GetAllDepartmentTags()
        {
            IDbSet<DepartmentTag> entitiesDepartment = db.Set<DepartmentTag>();
            return await Task.Run(() =>
            {
                return entitiesDepartment.Select(s => s).OrderByDescending(o => o.Entities.Count).ToList(); ;
            });
        }

        public async Task<ICollection<ManagerTag>> GetAllManagerTags()
        {
            IDbSet<ManagerTag> entitiesDepartment = db.Set<ManagerTag>();
            return await Task.Run(() =>
            {
                return entitiesDepartment.Select(s => s).OrderByDescending(o => o.Entities.Count).ToList(); ;
            });
        }

        public async Task<bool> DeleteDepartmentTag(string tagName)
        {
            Trace.WriteLine("Delete Department Tag request received");
            var repository = new TagRepository<DepartmentTag>(db);
            DepartmentTag tag = await repository.GetByName(tagName);
            if (tag == null)
                return false;

            Trace.WriteLine($"Delete {tag.Id} {tag.Name}");
            tag = await repository.Delete(tag);


            return true;
        }

        public async Task<bool> DeleteManagerTag(string tagName)
        {
            var repository = new TagRepository<ManagerTag>(db);
            ManagerTag tag = await repository.GetByName(tagName);
            if (tag == null)
                return false;
            await repository.Delete(tag);
            return true;
        }

        public async Task<bool> UntagDepartment(string departmentName, string tagName)
        {
            var dep = await (new DepartmentRepository(db).GetByName(departmentName));
            if (dep != null && dep.Tags.Any(t => t.Name == tagName))
            {
                var tag = dep.Tags.FirstOrDefault(t => t.Name == tagName);
                if (tag != null)
                {
                    dep.Tags.Remove(tag);
                    await db.SaveChangesAsync();

                    return true;
                }
            }
            return false;
        }

        public async Task<bool> TagManager(string uid, string tagName)
        {
            var managers = new ManagerRepository(db);
            var manager = await managers.GetByUid(uid);
            if (manager != null && !manager.Tags.Any(t => t.Name == tagName))
            {
                var tags = new TagRepository<ManagerTag>(db);
                var tag = await tags.GetByName(tagName);
                if (tag == null)
                {
                    tag = new ManagerTag
                    {
                        Name = tagName
                    };
                    await tags.Add(tag);
                }
                manager.Tags.Add(tag);
                await db.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> UntagManager(string uid, string tagName)
        {
            var manager = await (new ManagerRepository(db).GetByUid(uid));
            if (manager != null)
            {
                var tag = manager.Tags.FirstOrDefault(t => t.Name == tagName);
                if (tag != null)
                {
                    manager.Tags.Remove(tag);
                    await db.SaveChangesAsync();

                    return true;
                }
            }
            return false;
        }

        #endregion

        #region TwoFactor
        public async Task<RepositoryResponse<TwoFactor>> CreateTwoFactor(TwoFactor twoFactor)
        {
            return await (new Repository<TwoFactor, long>(db)).Add(twoFactor);
        }
        public async Task<RepositoryResponse<TwoFactor>> UpdateTwoFactor(TwoFactor twoFactor)
        {
            return await (new Repository<TwoFactor, long>(db)).Update(twoFactor);
        }

        public async Task<TwoFactor> GetTwoFactorByType(string type)
        {
            return (await (new Repository<TwoFactor, long>(db)).Where(s => s.Type == type)).FirstOrDefault();
        }
        public async Task<IEnumerable<TwoFactor>> GetAllTwoFactors()
        {
            return (await (new Repository<TwoFactor, long>(db)).All()).ToList();
        }

        public async Task<RepositoryResponse<TwoFactorAction>> UpdateAction(TwoFactorAction twoFactor)
        {
            return await (new Repository<TwoFactorAction, long>(db)).Update(twoFactor);
        }
        public async Task<TwoFactorAction> GetActionByName(string name)
        {
            return (await (new Repository<TwoFactorAction, long>(db)).Where(s => s.Name == name)).FirstOrDefault();
        }
        public async Task<IEnumerable<TwoFactorAction>> GetAllActions()
        {
            return (await (new Repository<TwoFactorAction, long>(db)).All()).ToList();
        }

        public async Task<UserTwoFactor> GetUserTwoFactorById(long id)
        {
            return await (new Repository<UserTwoFactor, long>(db)).GetById(id);
        }
        public async Task<UserTwoFactor> GetSettingsByUserAction(long userId, long actionId)
        {
            return (await (new Repository<UserTwoFactor, long>(db)).Where(s => s.User.Id == userId && s.TwoFactorAction.Id == actionId)).FirstOrDefault();
        }
        public async Task<IEnumerable<UserTwoFactor>> GetSettingsByUser(long userId)
        {
            return (await (new Repository<UserTwoFactor, long>(db)).Where(s => s.User.Id == userId)).ToList();
        }
        public async Task<RepositoryResponse<UserTwoFactor>> CreateSetting(UserTwoFactor userTwoFactor)
        {
            return await (new Repository<UserTwoFactor, long>(db)).Add(userTwoFactor);
        }
        public async Task<RepositoryResponse<UserTwoFactor>> UpdateSetting(UserTwoFactor userTwoFactor)
        {
            try
            {
                var res = await (new Repository<UserTwoFactor, long>(db)).Update(userTwoFactor);
                return res;
            }
            catch (Exception ex) { throw ex; }
        }
        public async Task<UserTwoFactor> DeleteSetting(UserTwoFactor userTwoFactor)
        {
            return await (new Repository<UserTwoFactor, long>(db)).Delete(userTwoFactor);
        }

        public async Task<IEnumerable<TwoFactorActionOption>> GetActionOptions(string action)
        {
            return (await (new Repository<TwoFactorActionOption, long>(db)).Where(s => s.Action.Name == action)).ToList();
        }
        public async Task<TwoFactorActionOption> GetActionKeyOption(string action, string key)
        {
            return (await (new Repository<TwoFactorActionOption, long>(db)).Where(s => s.Action.Name == action && s.Key == key)).FirstOrDefault();
        }


        public async Task<UserTwoFactorOption> GetUserTwoFactorOption(long userSettingId, string key)
        {
            return (await (new Repository<UserTwoFactorOption, long>(db)).Where(s => s.UserTwoFactor.Id == userSettingId && s.Key == key)).FirstOrDefault();
        }
        public async Task<RepositoryResponse<UserTwoFactorOption>> CreateUserTwoFactorOption(UserTwoFactorOption userTwoFactorOption)
        {
            return await (new Repository<UserTwoFactorOption, long>(db)).Add(userTwoFactorOption);
        }
        public async Task<RepositoryResponse<UserTwoFactorOption>> UpdateUserTwoFactorOption(UserTwoFactorOption userTwoFactorOption)
        {
            return await (new Repository<UserTwoFactorOption, long>(db)).Update(userTwoFactorOption);
        }
        public async Task<UserTwoFactorOption> DeleteOptions(UserTwoFactorOption twoFactorActionOption)
        {
            return await (new Repository<UserTwoFactorOption, long>(db)).Delete(twoFactorActionOption);
        }
        #endregion

        #region Documents
        public async Task<RepositoryResponse<DocumentType>> CreateDocumentType(DocumentType documentType)
        {
            return await (new Repository<DocumentType, long>(db)).Add(documentType);
        }
        public async Task<RepositoryResponse<DocumentType>> UpdateDocumentType(DocumentType documentType)
        {
            return await (new Repository<DocumentType, long>(db)).Update(documentType);
        }
        public async Task<IEnumerable<DocumentType>> GetDocumentTypes()
        {
            return (await (new Repository<DocumentType, long>(db)).All()).ToList();
        }
        public async Task<DocumentType> GetDocumentType(string name)
        {
            return (await (new Repository<DocumentType, long>(db)).Where(s => s.Name == name)).FirstOrDefault();
        }

        public async Task<UserDocument> CreateUserDocument(UserDocument userDocument)
        {
            return (await (new Repository<UserDocument, long>(db)).Add(userDocument)).entity;
        }
        public async Task<UserDocument> UpdateUserDocument(UserDocument userDocument)
        {
            return (await (new Repository<UserDocument, long>(db)).Update(userDocument)).entity;
        }
        public async Task<UserDocument> DeleteUserDoc(UserDocument doc)
        {
            var res = (await (new Repository<UserDocument, long>(db)).Delete(doc));
            return res;
        }
        public async Task<IEnumerable<UserDocument>> GetUserDocuments(long userId)
        {
            return (await (new Repository<UserDocument, long>(db)).Where(s => s.User.Id == userId)).ToList();
        }

        public async Task<IEnumerable<UserDocument>> GetAllDocumentsByStatus(int status, int count, int margin)
        {
            return (await (new DocumentRepository(db).GetDocsByPagination(status, count, margin)));
        }
        public async Task<int> GetUserDocsCount()
        {
            return (await (new DocumentRepository(db)).GetUserDocsCount());
        }
        public async Task<IEnumerable<UserDocument>> GetUserDocumentsByStatus(long userId, DocumentStatus status)
        {
            return (await (new Repository<UserDocument, long>(db)).Where(s => s.User.Id == userId && s.Status == (int)status)).ToList();
        }
        public async Task<UserDocument> GetUserDocumentByType(long userId, long typeId)
        {
            return (await (new Repository<UserDocument, long>(db)).Where(s => s.User.Id == userId && s.Type.Id == typeId)).FirstOrDefault();
        }
        public async Task<UserDocument> GetUserDocument(string guid)
        {
            return (await (new Repository<UserDocument, long>(db)).Where(s => s.Guid == guid)).FirstOrDefault();
        }
        #endregion

        #region Settings
        public async Task<IEnumerable<Settings>> GetSettings(List<string> keys)
        {
            return (await (new Repository<Settings, long>(db)).Where(s => keys.Contains(s.Key))).ToList();
        }

        public async Task<IEnumerable<Settings>> UpdateSettings(Dictionary<string, double> newSettings)
        {
            var repository = new Repository<Settings, long>(db);
            var settings = await repository.All();
            foreach(var setting in settings)
            {
                if(newSettings.ContainsKey(setting.Key))
                {
                    setting.Value = newSettings[setting.Key];
                    await repository.Update(setting);
                }
            }

            return settings;
        }
        #endregion
    }

}
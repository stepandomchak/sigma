﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    public class ChangePasswordRequest : Request
    {
        public override string QueueName => "auth";
        public override string RequestName => "change-password";

        public string email { get; set; }
        public string password { get; set; }
    }
}

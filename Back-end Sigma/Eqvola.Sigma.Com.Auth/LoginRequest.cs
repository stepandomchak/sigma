﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    public class LoginRequest : Request
    {
        public override string QueueName => "auth";
        public override string RequestName => "login";

        public string uid { get; set; }
        public string password { get; set; }
        public string code { get; set; }

        public string headers { get; set; }
        public string key { get; set; }
    }

}

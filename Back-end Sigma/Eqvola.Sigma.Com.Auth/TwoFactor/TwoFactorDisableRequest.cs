﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth.TwoFactor
{
    public class TwoFactorDisableRequest : Request
    {
        public override string QueueName => "auth";
        public override string RequestName => "two-factor-disable";

        public string uid { get; set; }
        public string code { get; set; }
    }
}

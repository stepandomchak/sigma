﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth.TwoFactor
{
    public class TwoFactorDisableResponse : Response
    {
        public bool status { get; set; }
        public string message { get; set; }
        
    }
}

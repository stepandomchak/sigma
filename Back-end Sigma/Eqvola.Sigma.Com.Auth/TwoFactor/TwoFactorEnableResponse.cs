﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth.TwoFactor
{
    public class TwoFactorEnableResponse : Response
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string url { get; set; }
        public string manualCode { get; set; }
        
    }
}

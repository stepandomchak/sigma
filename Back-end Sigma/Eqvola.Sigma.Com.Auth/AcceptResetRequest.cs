﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    public class AcceptResetRequest : Request
    {
        public override string QueueName => "auth";
        public override string RequestName => "accept-reset-password";


        public string email { get; set; }
        public string phone_number { get; set; }
        public string code { get; set; }
        public string newPassword { get; set; }
    }
}

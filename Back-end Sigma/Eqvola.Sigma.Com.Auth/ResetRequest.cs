﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    public class ResetRequest : Request
    {
        public override string QueueName => "auth";
        public override string RequestName => "reset-password";

        public string phone_number { get; set; }
        public string email { get; set; }
    }
}

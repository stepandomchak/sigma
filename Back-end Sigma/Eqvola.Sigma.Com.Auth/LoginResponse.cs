﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    /// <summary>
    /// User authentication response
    /// </summary>
    public class LoginResponse : Response
    {
        /// <summary>
        /// Token for access to other methods
        /// 
        /// </summary>
        public string token { get; set; }
        //

        /// <summary>
        /// Status of request:
        /// 
        /// </summary>
        public int status { get; set; }
        /*  <para>0 - wrong data</para>
        /// <para>1 - success</para>
        /// <para>2 - manager fired</para>
        /// <para>3 - department deleted </para>
        /// <para>4 - code not found </para>
        /// <para>5 - wrong code </para>
        /// <para>6 - manager invited </para>
        /// <para>7 - status undefind </para>*/

        /// <summary>
        /// Description of failed attempt
        /// </summary>
        public string message { get; set; }
    }
}

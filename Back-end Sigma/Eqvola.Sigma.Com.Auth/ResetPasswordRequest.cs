﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    public class ResetPasswordRequest : Request
    {
        public override string QueueName => "auth";
        public override string RequestName => "reset-password";

        public string uid { get; set; }
        public string password { get; set; }

        public string headers { get; set; }
        public string key { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Auth
{
    public class ResetPasswordResponse : Response
    {
        public string token { get; set; }
    }
}

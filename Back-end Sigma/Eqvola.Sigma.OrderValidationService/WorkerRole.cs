using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderService
{
    public class WorkerRole : ServiceBusRole<Service.OrderService.Service>
    {
        protected override IEnumerable<string> queues => new string[] { "order-service" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "order-service":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        {
                            "check-order", CheckOrder
                        },
                        {
                            "check-limits", CheckLimits
                        },
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }
        private async Task<Response> CheckOrder(BrokeredMessage message)
        {
            var request = message.GetBody<ValidateOrderRequest>();

            var response = await service.OrderValidation(request);

            return response;
        }

        private async Task<Response> CheckLimits(BrokeredMessage message)
        {
            var request = message.GetBody<CheckLimitsRequest>();  
            
            await service.CheckLimits(request);

            return null;
        }
    }
}

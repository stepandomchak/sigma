﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Market
{
    public class GetMarketsRequest : Request
    {
        public override string QueueName => "market";
        public override string RequestName => "get-all";
        
    }
}

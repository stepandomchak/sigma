﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Market
{
    public class CheckPairRequest : Request
    {
        public override string QueueName => "market";
        public override string RequestName => "pair-check";

        public string fisrtCurrencyCode;
        public string secondCurrencyCode;
    }
}

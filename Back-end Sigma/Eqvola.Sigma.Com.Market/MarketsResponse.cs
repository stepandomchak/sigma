﻿
using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Market
{
    public class MarketsResponse : Response
    {
        public List<Market.Models.Market> markets { get; set; }

        public MarketsResponse()
        {
            markets = new List<Market.Models.Market>();
        }
    }
}

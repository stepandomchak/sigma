﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Market
{
    public class UpdateCurrencyRequest : Request
    {
        public override string QueueName => "market";

        public override string RequestName => "update-currency";


        public string CurrencyCode { get; set; }

        public bool IsFixedFee { get; set; }

        public double FixedFee { get; set; }

        public double MinFee { get; set; }

        public double MaxFee { get; set; }
    }
}

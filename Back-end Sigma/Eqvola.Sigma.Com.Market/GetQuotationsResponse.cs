﻿
using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Market
{
    public class GetQuotationsResponse : Response
    {
        public Dictionary<string, double> PairPrices { get; set; }
    }
}

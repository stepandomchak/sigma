﻿using Eqvola.Sigma.Com.Market.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Market
{
    public class CurrencyResponse : Response
    {
        public string currencyName { get; set; }

        public Currency entity { get; set; }
    }
}

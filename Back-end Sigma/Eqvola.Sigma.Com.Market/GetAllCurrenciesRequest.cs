﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Market
{
    public class GetAllCurrenciesRequest : Request
    {
        public override string QueueName => "market";

        public override string RequestName => "get-all-currencies";

    }
}

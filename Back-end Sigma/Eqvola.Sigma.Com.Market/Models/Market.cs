﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Eqvola.Sigma.Com.Market.Models
{
    public class Market
    {
        [Required]
        [MinLength(2)]
        [MaxLength(64)]
        public string MasterCurrecyCode { get; set; }
        public IEnumerable<Currency> TargetCurrencies { get; set; }
        public int Status { get; set; }
    }
}
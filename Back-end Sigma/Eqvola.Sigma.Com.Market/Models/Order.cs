﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.Market.Models
{
    public class Order
    {
        public long Id { get; set; }
        public string Pair { get; set; }
        public int Type { get; set; }
        public DateTime? CreationTime { get; set; }
        public DateTime? CloseTime { get; set; }
        public double Price { get; set; }
        public double Volume { get; set; }
        public double Sum { get; set; }
        public double StopLoss { get; set; }
        public double TakeProfit { get; set; }
        public double Commision { get; set; }
        public double ClearSum { get; set; }
        public DateTime? GoodTillDate { get; set; }
        public int Status { get; set; }
        public double AvarageCost { get; set; }

        public Order(Com.OrderStorage.Models.Order order)
        {
            this.Id = order.Id;
            this.Pair = order.Currency + "_" + order.TargetCurrency;
            this.Type = order.OrderType;
            this.CreationTime = order.CreationTime;
            this.Price = Math.Round(order.Price, 15);
            this.Volume = Math.Round(order.Amount, 15);
            this.Sum = Math.Round(order.Amount * this.Price, 15);
            this.StopLoss = Math.Round(order.StopLoss, 15);
            this.TakeProfit = Math.Round(order.TakeProfit, 15);
            if (order.OrderType == 1)
                this.Commision = Math.Round((this.Sum / 100) * 0.15, 15);
            else
                this.Commision = Math.Round((this.Sum / 100) * 0.25, 15);
            this.ClearSum = Math.Round(this.Sum - this.Commision, 15);
            this.GoodTillDate = order.ExpiresTime;
            this.Status = order.Status;
            this.CloseTime = order.CloseTime;
            this.AvarageCost = order.AverageCost;
        }
    }
}

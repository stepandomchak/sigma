﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Eqvola.Sigma.Com.Market.Models
{
    public class Pair
    {
        public string CurrencyPair { get; set; }
        public string MarketCurrency { get; set; }
        public int Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Eqvola.Sigma.Com.Market.Models
{
    public class Currency
    {
        public string Name { get; set; }
        
        public string Code { get; set; }

        public int Status { get; set; }

        public bool IsFixedFee { get; set; }

        public double FixedFee { get; set; }

        public double MinFee { get; set; }

        public double MaxFee { get; set; }
    }
}

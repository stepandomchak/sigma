﻿
using System.Collections.Generic;

namespace Eqvola.Sigma.Com.Market
{
    public class CurrenciesResponse : Response
    {
        public List<Market.Models.Currency> currencies { get; set; }

        public CurrenciesResponse()
        {
            currencies = new List<Market.Models.Currency>();
        }
    }
}

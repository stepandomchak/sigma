﻿using Eqvola.Sigma.Service.SupportService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Repository
{
    public class MessageRepository : Repository<Message, long>
    {
        public MessageRepository(Context context) : base(context) { }

        public async Task<List<Message>> GetMessagesByPagination(int count, int margin)
        {
            return await Task.Run(() =>
            {
                return Entities.OrderByDescending(s => s.Id).Skip(margin).Take(count).ToList();
            });
        }
    }
}

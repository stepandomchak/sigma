﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Repository
{
    public class TopicRepository : Repository<Models.Topic, long>
    {
        public TopicRepository(Context context) : base(context) { }
    }
}

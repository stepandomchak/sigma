﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Repository
{
    public class AttachmentRepository : Repository<Models.Attachment, long>
    {
        public AttachmentRepository(Context context) : base(context) { }
    }
}

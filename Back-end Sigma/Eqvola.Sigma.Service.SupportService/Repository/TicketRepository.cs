﻿using Eqvola.Sigma.Service.SupportService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Repository
{
    public class TicketRepository : Repository<Ticket, long>
    {
        public TicketRepository(Context context) : base(context) { }

        public async Task<List<Ticket>> GetTicketsByPagination(int count, int margin)
        {
            return await Task.Run(() =>
            {
                return Entities.OrderByDescending(s => s.Id).Skip(margin).Take(count).ToList();
            });
        }

        public async Task<List<Ticket>> GetTickets(string email, int status, string category, string search, int count, int margin, string orderBy = null, bool isAscending = true)
        {
            return await Task.Run(() =>
            {
                var request = BuildRequest(email, status, search, category);
                if (margin < 0)
                    margin = 0;
                if (count <= 0)
                    count = 20;

                if (!string.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.ToLower())
                    {
                        case "category":
                            {
                                request = isAscending ? request.OrderBy(o => o.Category.Name) : request.OrderByDescending(o => o.Category.Name);
                                break;
                            }
                        case "dateCreation":
                            {
                                request = isAscending ? request.OrderBy(o => o.Created) : request.OrderByDescending(o => o.Created);
                                break;
                            }
                        case "dateUpdate":
                            {
                                request = isAscending ? request.OrderBy(o => o.Messages.OrderByDescending(m => m.Created).FirstOrDefault()) : request.OrderByDescending(o => o.Messages.OrderByDescending(m => m.Created).FirstOrDefault());
                                break;
                            }
                        case "status":
                            {
                                request = isAscending ? request.OrderBy(o => o.TicketStatus) : request.OrderByDescending(o => o.TicketStatus);
                                break;
                            }
                        default:
                            {
                                request = isAscending ? request.OrderBy(o => o.Id) : request.OrderByDescending(o => o.Id);
                                break;
                            }
                    }
                }
                else
                {
                    request = isAscending ? request.OrderBy(o => o.Id) : request.OrderByDescending(o => o.Id);
                }
                request = request.Skip(margin).Take(count);
                return request.ToList();
            });
        }

        private IQueryable<Ticket> BuildRequest(string email, int status, string search, string category)
        {
            IQueryable<Ticket> request = Entities.AsQueryable();
            if (!string.IsNullOrEmpty(email))
            {
                request = Entities.Where(o => o.Email == email);
            }
            if (status > 0)
            {
                request = request.Where(o => (o.TicketStatus & status) > 0);
            }
            if (!string.IsNullOrEmpty(category))
            {
                request = request.Where(o => (o.Category.Name == category));
            }
            if (!string.IsNullOrEmpty(search))
            {
                request = request.Where(o => (o.Email.Contains(search) || o.Category.Name.Contains(search) ||
                    o.Title.Contains(search) || o.Messages.Where(m => m.Text.Contains(search)).Count() > 0));
            }

            return request;
        }

        public async Task<int> GetTicketsCount(string email, int status, string category, string search = null)
        {
            return await Task.Run(() =>
            {
                return BuildRequest(email, status, search, category).Count();
            });
        }

    }
}

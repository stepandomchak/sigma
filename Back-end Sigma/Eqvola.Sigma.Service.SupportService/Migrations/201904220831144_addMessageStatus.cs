namespace Eqvola.Sigma.Service.SupportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMessageStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "Status");
        }
    }
}

namespace Eqvola.Sigma.Service.SupportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        IsTopic = c.Boolean(nullable: false),
                        IsTicket = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Created = c.DateTime(nullable: false),
                        TicketStatus = c.Int(nullable: false),
                        Category_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Text = c.String(nullable: false, maxLength: 1024),
                        Ticket_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tickets", t => t.Ticket_Id)
                .Index(t => t.Ticket_Id);
            
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Url = c.String(),
                        FileName = c.String(nullable: false),
                        Message_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Messages", t => t.Message_Id, cascadeDelete: true)
                .Index(t => t.Message_Id);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 128),
                        TopicStatus = c.Int(nullable: false),
                        Category_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.Problems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Question = c.String(nullable: false, maxLength: 128),
                        Answer = c.String(nullable: false, maxLength: 2048),
                        Topic_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Topics", t => t.Topic_Id)
                .Index(t => t.Topic_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Problems", "Topic_Id", "dbo.Topics");
            DropForeignKey("dbo.Topics", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Messages", "Ticket_Id", "dbo.Tickets");
            DropForeignKey("dbo.Attachments", "Message_Id", "dbo.Messages");
            DropForeignKey("dbo.Tickets", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Problems", new[] { "Topic_Id" });
            DropIndex("dbo.Topics", new[] { "Category_Id" });
            DropIndex("dbo.Attachments", new[] { "Message_Id" });
            DropIndex("dbo.Messages", new[] { "Ticket_Id" });
            DropIndex("dbo.Tickets", new[] { "Category_Id" });
            DropIndex("dbo.Categories", new[] { "Name" });
            DropTable("dbo.Problems");
            DropTable("dbo.Topics");
            DropTable("dbo.Attachments");
            DropTable("dbo.Messages");
            DropTable("dbo.Tickets");
            DropTable("dbo.Categories");
        }
    }
}

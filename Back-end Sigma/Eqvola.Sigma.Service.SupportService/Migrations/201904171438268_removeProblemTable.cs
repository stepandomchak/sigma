namespace Eqvola.Sigma.Service.SupportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeProblemTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Problems", "Topic_Id", "dbo.Topics");
            DropIndex("dbo.Problems", new[] { "Topic_Id" });
            AddColumn("dbo.Categories", "IsEnable", c => c.Boolean(nullable: false));
            AddColumn("dbo.Topics", "Text", c => c.String(nullable: false, maxLength: 2048));
            AlterColumn("dbo.Messages", "Text", c => c.String(maxLength: 1024));
            DropColumn("dbo.Categories", "IsTopic");
            DropColumn("dbo.Categories", "IsTicket");
            DropTable("dbo.Problems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Problems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Question = c.String(nullable: false, maxLength: 128),
                        Answer = c.String(nullable: false, maxLength: 128),
                        Topic_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Categories", "IsTicket", c => c.Boolean(nullable: false));
            AddColumn("dbo.Categories", "IsTopic", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Messages", "Text", c => c.String(nullable: false, maxLength: 1024));
            DropColumn("dbo.Topics", "Text");
            DropColumn("dbo.Categories", "IsEnable");
            CreateIndex("dbo.Problems", "Topic_Id");
            AddForeignKey("dbo.Problems", "Topic_Id", "dbo.Topics", "Id");
        }
    }
}

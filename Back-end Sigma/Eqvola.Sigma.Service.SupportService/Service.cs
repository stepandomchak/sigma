﻿using Eqvola.Sigma.Service.SupportService.Models;
using Eqvola.Sigma.Service.SupportService.Repository;
using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService
{
    public class Service : IDisposable
    {
        private Context db;

        public Service()
        {
            db = new Context(CloudConfigurationManager.GetSetting("SupportDbContext"));
        }

        public void Dispose()
        {
            db.Dispose();
        }

        #region Topic

        public async Task<Topic> GetTopicById(long id)
        {
            return (await new Repository<Topic, long>(db).Where(s => s.Id == id)).FirstOrDefault();
        }
        public async Task<RepositoryResponse<Topic>> CreateTopic(Topic topic)
        {
            return (await new TopicRepository(db).Add(topic));
        }

        public async Task<ICollection<Topic>> GetAllTopics()
        {
            return (await new TopicRepository(db).All());           
        }

        public async Task<IEnumerable<Topic>> GetAllTopics(string category)
        {
            return (await new TopicRepository(db).Where(m => m.Category.Name == category));
        }

        public async Task<RepositoryResponse<Topic>> UpdateTopic(Topic topic)
        {
            return (await new TopicRepository(db).Update(topic));
        }

        public async Task<Topic> DeleteTopic(Topic topic)
        {
            return (await new Repository<Topic, long>(db).Delete(topic));
        }

        #endregion

        #region Category
        public async Task<Category> GetCategoryByName(string name)
        {
            return (await new Repository<Category, long>(db).Where(s => s.Name == name)).FirstOrDefault();
        }
        public async Task<Category> GetCategoryById(long id)
        {
            return (await new Repository<Category, long>(db).Where(s => s.Id == id)).FirstOrDefault();
        }
        public async Task<Category> DeleteCategory(Category category)
        {
            return (await new Repository<Category, long>(db).Delete(category));
        }
        public async Task<IEnumerable<Category>> GetCategories()
        {
            return (await new Repository<Category, long>(db).All()).ToList();
        }
        public async Task<IEnumerable<Category>> GetCategoryByStatus(bool IsEnable)
        {
            return (await new Repository<Category, long>(db).Where(s => s.IsEnable == IsEnable)).ToList();
        }
        public async Task<RepositoryResponse<Category>> CreateCategory(Category category)
        {
            return (await new Repository<Category, long>(db).Add(category));
        }
        public async Task<RepositoryResponse<Category>> UpdateCategory(Category category)
        {
            return (await new Repository<Category, long>(db).Update(category));
        }
        #endregion

        #region Ticket
        public async Task<Ticket> GetTicketById(long id, string takerEmail = null)
        {
            var repository = new Repository<Ticket, long>(db);
            var messageRepository = new Repository<Message, long>(db);
            var ticket = (await repository.Where(s => s.Id == id)).FirstOrDefault();

            if (takerEmail != null)
            {
                foreach (var message in ticket.Messages)
                {
                    if (message.Email != takerEmail && message.Status == 0)
                    {
                        message.Status = 1;
                        await messageRepository.Update(message);
                    }
                }
            }
            return ticket;
        }
        public async Task<IEnumerable<Ticket>> GetUsersTickets(string email, int count, int margin, int status)
        {
            if(status == 0)
                return (await new TicketRepository(db).GetTicketsByPagination(count, margin)).Where(s => s.Email == email).ToList();
            else
                return (await new TicketRepository(db).GetTicketsByPagination(count, margin)).Where(s => s.Email == email && s.TicketStatus == status).ToList();
        }

        public async Task<IEnumerable<Ticket>> GetTickets(string email, string categoryName, int status, string search, int count, int margin, string orderBy, bool isAscending)
        {
            return (await new TicketRepository(db).GetTickets(email, status, categoryName, search, count, margin, orderBy, isAscending));
        }
        public async Task<int> GetTicketsCount(string email, string categoryName, int status, string search)
        {
            return (await new TicketRepository(db).GetTicketsCount(email, status, categoryName, search));
        }
        
        public async Task<RepositoryResponse<Ticket>> CreateTicket(Ticket ticket)
        {
            return (await new Repository<Ticket, long>(db).Add(ticket));
        }
        public async Task<RepositoryResponse<Ticket>> UpdateTicket(Ticket ticket)
        {
            return (await new Repository<Ticket, long>(db).Update(ticket));
        }
        #endregion

        #region Message

        public async Task<Message> GetMessagesById(long id)
        {
            return (await new Repository<Message, long>(db).GetById(id));
        }

        public async Task<IEnumerable<Message>> GetMessagesByTicket(long ticketId, int count, int margin)
        {
            return (await new MessageRepository(db).GetMessagesByPagination(count, margin)).Where(s => s.Ticket.Id == ticketId && s.Id >= margin).ToList();
        }
        public async Task<RepositoryResponse<Message>> CreateMessage(Message message)
        {
            return (await new Repository<Message, long>(db).Add(message));
        }
        #endregion

        #region Attachment
        public async Task<RepositoryResponse<Attachment>> CreateAttachment(Attachment attachment)
        {
             return (await new AttachmentRepository(db).Add(attachment));
        }

        public async Task<RepositoryResponse<Attachment>> UploadAttachment(Attachment attachment)
        {
            return (await new AttachmentRepository(db).Update(attachment));
        }

        public async Task<Attachment> GetAttachmenttById(long id)
        {
            return (await new Repository<Attachment, long>(db).Where(s => s.Id == id)).FirstOrDefault();
        }

        public async Task<ICollection<Attachment>> GetAttachmentsByMessageId(long messagesId)
        {
            return (await new Repository<Attachment, long>(db).Where(x => x.Message.Id == messagesId)).ToList();
        }

        #endregion

    }
}

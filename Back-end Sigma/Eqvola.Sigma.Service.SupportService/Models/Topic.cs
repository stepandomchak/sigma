﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Models
{
    public class Topic : BaseEntity<long>
    {
        [Required]
        [MaxLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength(2048)]
        public string Text { get; set; }

        public virtual Category Category { get; set; }

        public int TopicStatus { get; set; }
    }
}

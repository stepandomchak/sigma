﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Models
{
    public class Category : BaseEntity<long>
    {
        [Required]
        [MaxLength(128)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public bool IsEnable { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }

        public Category()
        {
            Topics = new List<Topic>();
            Tickets = new List<Ticket>();
        }

    }
}

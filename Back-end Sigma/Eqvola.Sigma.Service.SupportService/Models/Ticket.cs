﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Models
{
    public class Ticket : BaseEntity<long>
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Title { get; set; }

        public DateTime Created { get; set; }

        public virtual Category Category { get; set; }

        public int TicketStatus { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public Ticket ()
        {
            Messages = new List<Message>();
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Models
{
    public class Attachment : BaseEntity<long>
    {
        public string Url { get; set; }

        [Required]
        public virtual Message Message { get; set; }

        [Required]
        public string FileName { get; set; }

        public Attachment()
        {
            /// Generatio random unique URL
        }
    }
}

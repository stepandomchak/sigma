﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Service.SupportService.Models
{
    public class Message : BaseEntity<long>
    {
        [Required]
        public string Email { get; set; }

        public DateTime Created { get; set; }

        [MaxLength(1024)]
        public string Text { get; set; }

        public int Status { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }

        public virtual Ticket Ticket { get; set; }

        public Message()
        {
            Attachments = new List<Attachment>();
            Text = "";
        }
    }
}

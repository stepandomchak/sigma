@echo off
for /f "tokens=2" %%I in ('git.exe branch 2^> NUL ^| findstr /b "* "') do set BRANCH=%%I

if not "%BRANCH%" == "master" (
	git status --porcelain | FIND /v /c "" > %TMP%/git-status
	set /P CHANGES = < %TMP%/git-status
	if !CHANGES! GTR 0 git stash	

	git checkout master
)

git pull

devenv Sigma.sln /Clean
devenv Sigma.sln /Build


if not "%BRANCH%" == "master" (
	git checkout %BRANCH%
	if !CHANGES! GTR 0 git stash pop
)

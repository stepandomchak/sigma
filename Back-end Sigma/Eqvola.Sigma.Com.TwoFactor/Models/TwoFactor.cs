﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.TwoFactor.Models
{
    public class TwoFactor
    {
        public int Type { get; set; }
        public int Status { get; set; }
    }
}

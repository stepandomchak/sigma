using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Transaction;
using Eqvola.Sigma.OrderStorageService.Handlers;
using Eqvola.Sigma.OrderStorageService.Handlers.Transaction;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;

namespace Eqvola.Sigma.OrderStorageService
{
    public partial class WorkerRole : ServiceBusRole
    {
        protected override IEnumerable<string> queues => new string[] { "order-storage" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Sigma.Com.Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "order-storage":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        { "order-create", (message) => { return HandlerFactory<CreateOrderRequest, CreateOrder>.ProccessMessage(message); } },
                        { "order-update", (message) => { return HandlerFactory<UpdateOrderRequest, UpdateOrder>.ProccessMessage(message); } },
                        { "order-updateCommission", (message) => { return HandlerFactory<UpdateOrderCommissionRequest, UpdateOrderCommission>.ProccessMessage(message); } },
                        { "order-get", (message) => { return HandlerFactory<GetOrderRequest, GetOrder>.ProccessMessage(message); } },
                        { "order-filtered", (message) => { return HandlerFactory<GetFilteredOrdersRequest, GetFilteredOrders>.ProccessMessage(message); } },
                        { "order-suit", (message) => { return HandlerFactory<GetSuitOrderRequest, GetSuitOrders>.ProccessMessage(message); } },
                        { "order-cancel", (message) => { return HandlerFactory<CancelOrderRequest, CancelOrder>.ProccessMessage(message); } },
                        { "order-first", (message) => { return HandlerFactory<GetFirstOrderRequest, FirstOrder>.ProccessMessage(message); } },
                        { "order-user", (message) => { return HandlerFactory<GetUserOrdersRequest, UserOrders>.ProccessMessage(message); } },

                        { "transaction-create", (message) => { return HandlerFactory<CreateTransactionRequest, CreateTransaction>.ProccessMessage(message); } },
                        { "transaction-get", (message) => { return HandlerFactory<GetTransactionRequest, GetTransaction>.ProccessMessage(message); } }
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }

    }

}

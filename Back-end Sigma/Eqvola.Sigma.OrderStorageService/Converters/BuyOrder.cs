﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Converters
{
    public static class BuyOrder
    {
        public static Com.OrderStorage.Models.BuyOrder ToComObject(this Service.OrderStorage.Models.BuyOrder src)
        {
            return new Com.OrderStorage.Models.BuyOrder()
            {
                Id = src.Id,
                Currency = src.Currency,
                TargetCurrency = src.TargetCurrency,
                CreationTime = src.CreationTime,
                ExpiresTime = src.ExpiresTime,
                Price = src.Price,
                Amount = src.Amount,
                UserId = src.UserId,
                Rest = src.Rest,
                AverageCost = src.AverageCost,
                Status = src.Status,
                StopLoss = src.StopLoss,
                TakeProfit = src.TakeProfit,
                WLId = src.WLId
            };
        }
    }
}

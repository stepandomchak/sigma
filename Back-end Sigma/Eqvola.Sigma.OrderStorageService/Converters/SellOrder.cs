﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Converters
{
    public static class SellOrder
    {
        public static Com.OrderStorage.Models.SellOrder ToComObject(this Service.OrderStorage.Models.SellOrder src)
        {
            return new Com.OrderStorage.Models.SellOrder()
            {
                Id = src.Id,
                Currency = src.Currency,
                TargetCurrency = src.TargetCurrency,
                CreationTime = src.CreationTime,
                ExpiresTime = src.ExpiresTime,
                Price = src.Price,
                Amount = src.Amount,
                UserId = src.UserId,
                Rest = src.Rest,
                AverageCost = src.AverageCost,
                Status = src.Status,
                StopLoss = src.StopLoss,
                TakeProfit = src.TakeProfit,
                WLId = src.WLId
            };
        }
    }
}

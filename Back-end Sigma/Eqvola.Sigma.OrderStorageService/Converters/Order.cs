﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Converters
{
    public static class Order
    {
        public static Com.OrderStorage.Models.Order ToComObject(this Service.OrderStorage.Models.Order src)
        {
            return new Com.OrderStorage.Models.Order()
            {
                Id = src.Id,
                Currency = src.Currency,
                TargetCurrency = src.TargetCurrency,
                CreationTime = src.CreationTime,
                ExpiresTime = src.ExpiresTime,
                CloseTime = src.CloseTime,
                Price = src.Price,
                Amount = src.Amount,
                UserId = src.UserId,
                Rest = src.Rest,
                AverageCost = src.AverageCost,
                Status = src.Status,
                StopLoss = src.StopLoss,
                TakeProfit = src.TakeProfit,
                Stop = src.Stop,
                Limit = src.Limit,
                WLId = src.WLId,
                Comment = src.Comment,
                Commission = src.Commission,
                OrderType = src.Type.Equals(Service.OrderStorage.Models.OrderType.Buy.ToString())?
                            (int)Service.OrderStorage.Models.OrderType.Buy:(int)Service.OrderStorage.Models.OrderType.Sell
            };
        }
    }
}

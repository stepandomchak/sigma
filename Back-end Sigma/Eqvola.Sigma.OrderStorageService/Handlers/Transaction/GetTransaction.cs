﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Transaction;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers.Transaction
{
    public class GetTransaction : StorageHandler<GetTransactionRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            Response resp = null;
            if(request.orderId != 0)
            {
                resp = await GetTransactionByOrderId(request.orderId);
            }
            else if (request.id != 0)
                resp = await GetTransactionById(request.id);
            else
                resp = await GetAllTransactions();
            
            return resp;
        }

        protected override Response CreateFailResponse()
        {
            return new TransactionResponse()
            {
                result = Result.InvalidRequest
            };
        }
        protected async Task<TransactionsResponse> GetAllTransactions()
        {
            var response = new TransactionsResponse();
            response.transactions = (await service.GetTransactions()).Select(m => m.ToComObject()).ToList();
            return response;
        }
        protected async Task<TransactionResponse> GetTransactionById(long id)
        {
            var response = new TransactionResponse();
            response.entity = (await service.GetTransactionById(id)).ToComObject();
            return response;
        }

        protected async Task<TransactionsResponse> GetTransactionByOrderId(long orderId)
        {
            var response = new TransactionsResponse();
            response.transactions = (await service.GetTransactionByOrderId(orderId)).Select(t => t.ToComObject()).ToList();
            return response;
        }
    }
}

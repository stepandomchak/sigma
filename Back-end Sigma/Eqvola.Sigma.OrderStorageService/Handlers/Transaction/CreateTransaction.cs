﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.OrderStorage.Transaction;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers.Transaction
{
    public class CreateTransaction : StorageHandler<CreateTransactionRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new TransactionResponse()
            {
                result = Com.OrderStorage.Result.InvalidRequest
            };
        }

        protected override async Task<Response> ProccessAsync()
        {              
            var sellOrder = await service.GetOrderById(request.SellOrderId);
            var oldSellOrder = sellOrder;
            var buyOrder = await service.GetOrderById(request.BuyOrderId);
            var oldBuyOrder = buyOrder;


            var valResponse = await ValidateTransaction(buyOrder, sellOrder);

            if (valResponse != null)
            {
                return valResponse;
            }

            var transaction = new Service.OrderStorage.Models.Transaction()
            {
                SellOrder = sellOrder,
                BuyOrder = buyOrder,
                Date_Time = DateTime.UtcNow,
                Taker = request.Taker,
                Amount = request.Amount
            };

            transaction.Price = (transaction.getMaker()).Price;

            var result = await service.CreateTransaction(transaction);

            if (result != null)
            {
                double soldBuy = buyOrder.Amount - buyOrder.Rest + transaction.Amount;

                double transctionPriceBuy = 0;
                if (transaction.getMaker().Id == buyOrder.Id)
                {
                    transctionPriceBuy = transaction.Price;
                }
                else {
                    transctionPriceBuy = 1 / transaction.Price;
                }

                buyOrder.AverageCost = (buyOrder.Amount - buyOrder.Rest) / soldBuy * buyOrder.AverageCost + transaction.Amount / soldBuy * transctionPriceBuy;
                buyOrder.Rest -= transaction.Amount;

                if (buyOrder.Rest.EqualsWithInaccuracy(0D))
                {
                    buyOrder.Rest = 0D;
                    buyOrder.Status = (int)StatusOrder.Сlosed;
                    buyOrder.CloseTime = DateTime.UtcNow;
                }
                else
                {
                    buyOrder.Status = (int)StatusOrder.PartiallyСlosed;
                }

                double transctionPriceSell = 0;
                if (transaction.getMaker().Id == sellOrder.Id)
                {
                    transctionPriceSell = transaction.Price;
                }
                else
                {
                    transctionPriceSell = 1 / transaction.Price;
                }

                double soldSell = sellOrder.Amount - sellOrder.Rest + transaction.Amount;
                sellOrder.AverageCost = (sellOrder.Amount - sellOrder.Rest) / soldSell * sellOrder.AverageCost + transaction.Amount / soldSell * transctionPriceSell;
                sellOrder.Rest -= transaction.Amount;
                
                if (sellOrder.Rest.EqualsWithInaccuracy(0D))
                {
                    sellOrder.Rest = 0D;
                    sellOrder.Status = (int)StatusOrder.Сlosed;
                    sellOrder.CloseTime = DateTime.UtcNow;
                }
                else
                {
                    sellOrder.Status = (int)StatusOrder.PartiallyСlosed;
                }

                await service.UpdateOrder(buyOrder);
                await service.UpdateOrder(sellOrder);

                // Change balances in users wallets
                var processTransaction = new ProcessTransactionRequest() { transaction = result.entity.ToComObject() };
                var processTransactionResponse = await processTransaction.GetResponse<Response>();


                // NotificationsToMarketService
                var updatePrice = new UpdatePriceRequest()
                {
                    oldOrder = oldBuyOrder.ToComObject(),
                    updatedOrder = buyOrder.ToComObject()
                };
                await updatePrice.Send();

                var orderChange = new UpdatePriceRequest()
                {
                    oldOrder = oldSellOrder.ToComObject(),
                    updatedOrder = sellOrder.ToComObject()
                };
                await updatePrice.Send();
                

                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.Ok,
                    entity = result.entity.ToComObject()
                };
            }
            return new TransactionResponse()
            {
                result = Com.OrderStorage.Result.Exception,
                entity = null
            };

        }
        protected async Task<Response> ValidateTransaction(Service.OrderStorage.Models.Order buyOrder, Service.OrderStorage.Models.Order sellOrder)
        {

            if (sellOrder == null || buyOrder == null)
            {
                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.NonExistenOrder,
                    entity = null
                };
            }
            if (sellOrder.Rest <= 0 || buyOrder.Rest <= 0)
            {
                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.UnexpectedRest,
                    entity = null
                };
            }
            if (request.Amount < 0)
            {
                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.UnexpectedAmount,
                    entity = null
                };
            }

            if(request.Amount > buyOrder.Rest || request.Amount > sellOrder.Rest)
            {
                request.Amount = Math.Min(buyOrder.Rest, sellOrder.Rest);
            }

            if (sellOrder.Price <= 0 || buyOrder.Price <= 0)
            {
                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.UnexpectedPrice,
                    entity = null
                };
            }
            if (sellOrder.Currency != buyOrder.TargetCurrency || buyOrder.Currency != sellOrder.TargetCurrency)
            {
                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.UnexpectedCurrency,
                    entity = null
                };
            }

            if (sellOrder.ExpiresTime.HasValue && sellOrder.ExpiresTime < DateTime.UtcNow)
            {
                if (sellOrder.Status != (int)StatusOrder.Сlosed || sellOrder.Status != (int)StatusOrder.Canceled)
                    sellOrder.Status = (int)StatusOrder.Сlosed;

                await service.UpdateOrder(sellOrder);

                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.ExpiredOrder,
                    entity = null
                };
            }
            if (buyOrder.ExpiresTime.HasValue && buyOrder.ExpiresTime < DateTime.UtcNow)
            {
                if (buyOrder.Status != (int)StatusOrder.Сlosed || buyOrder.Status != (int)StatusOrder.Canceled)
                    buyOrder.Status = (int)StatusOrder.Сlosed;

                await service.UpdateOrder(buyOrder);

                return new TransactionResponse()
                {
                    result = Com.OrderStorage.Result.ExpiredOrder,
                    entity = null
                };
            }
            return null;
        }

    }
}

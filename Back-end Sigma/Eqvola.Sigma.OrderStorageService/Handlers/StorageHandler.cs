﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Service;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public abstract class StorageHandler<TRequest> : BaseHandler<TRequest, Service.OrderStorage.Service> where TRequest : Request
    {
        public override void Dispose()
        {
            //service.Dispose();
        }

        public override async Task<Response> ProccessMessage(BrokeredMessage message)
        {
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<TRequest>(json);
            }
            try
            {
                return await ProccessAsync();
            }
            catch (DataException ex)
            {
                return CreateFailResponse();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected abstract Response CreateFailResponse();
    }
}

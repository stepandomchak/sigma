﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class UserOrders : StorageHandler<GetUserOrdersRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new UserOrdersResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            var response = new UserOrdersResponse();
            response.orders = (await service.GetUserOrders(request.userId, request.ordersCount, request.margin, request.status, request.market)).Select(m => m.ToComObject()).ToList();

            if (response.orders != null)
            {
                response.result = Result.Ok;
                response.totalCount = await service.GetUserOrdersCount(request.userId, request.status, request.market);
            }
            else
                response.result = Result.NotFound;

            return response;
        }
    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.OrderStorageService.Converters;
using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class UpdateOrderCommission : StorageHandler<UpdateOrderCommissionRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            var order = await service.GetOrderById(request.orderId);
            

            if (order == null)
            {
                return new OrderResponse()
                {
                    result = Com.OrderStorage.Result.NotFound
                };
            }
            order.Commission = request.commission;

            var res = await service.UpdateOrder(order);

            if (res.entity != null)
            {
                return new OrderResponse()
                {
                    entity = res.entity.ToComObject(),
                    result = Com.OrderStorage.Result.Ok
                };
            }
            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.Exception
            };
        }
        

        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.InvalidRequest
            };
        }
    }
}

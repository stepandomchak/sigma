﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.OrderStorageService.Converters;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class GetOrder : StorageHandler<GetOrderRequest>
    {

        protected async override Task<Response> ProccessAsync()
        {
            Response resp = null;
            if (request.id != 0)
            {
                resp = await GetOrderById(request.id);
            }
            else if (request.wlId != 0)
            {
                resp = await GetOrdersByWl(request.wlId);
            }
            else if (request.ids != null)
            {
                resp = await GetOrdersByIds(request.ids);
            }
            else if(request.userId != 0)
            {
                resp = await GetUserOrdersByStatus(request.userId, request.status, request.market);
            }
            else
            {
                resp = await GetAllOrders();
            }

            return resp;
        }

        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async Task<OrdersResponse> GetAllOrders()
        {
            var response = new OrdersResponse();
            response.orders = (await service.GetOrders()).Select(m => m.ToComObject()).ToList();
            if (response.orders != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

        protected async Task<OrdersResponse> GetUserOrdersByStatus(long userId, int status, string market)
        {
            var response = new OrdersResponse();
            response.orders = (await service.GetOrdersByUserStatusAndMarket(userId, status, market)).Select(m => m.ToComObject()).ToList();
            if (response.orders != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

        protected async Task<Response> GetOrderById(long id)
        {
            var response = new OrderResponse();
            response.entity = (await service.GetOrderById(id))?.ToComObject();
            if (response.entity != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

        protected async Task<OrdersResponse> GetOrdersByWl(long wlId)
        {
            var response = new OrdersResponse();
            response.orders = (await service.GetOrdersByWl(wlId)).Select(m => m.ToComObject()).ToList();
            if (response.orders != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

        protected async Task<OrdersResponse> GetOrdersByIds(IEnumerable<long> ids)
        {
            var response = new OrdersResponse();
            response.orders = (await service.GetOrdersByIds(ids)).Select(m => m.ToComObject()).ToList();
            if (response.orders != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

    }
}

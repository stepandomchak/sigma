﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class GetSuitOrders : StorageHandler<GetSuitOrderRequest>
    {

        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Result.InvalidRequest
            };
        }

        protected async override Task<Response> ProccessAsync()
        {
            Response resp = null;

            if (request.orderId != 0)
            {
                resp = await GetSuit(request.orderId);
            }
            else
                resp = new OrderResponse()
                {
                    result = Result.InvalidRequest
                };
            return resp;
        }

        protected async Task<SuitOrdersResponse> GetSuit(long id)
        {
            var response = new SuitOrdersResponse();

            var order = await service.GetOrderById(id);
            response.ordersId = (await service.GetSuitOrders(order)).OrderBy(m => m.Price).Select(m => m.Id).ToList();

            if (response.ordersId != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

    }
}

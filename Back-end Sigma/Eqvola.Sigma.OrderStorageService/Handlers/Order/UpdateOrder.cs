﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.OrderStorageService.Converters;
using Eqvola.Sigma.Service.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class UpdateOrder : StorageHandler<UpdateOrderRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            
            if(request.status != 0)
            {
                return await UpdateOrderStatus();
            }
            else
            {
                return await UpdateOrderLimits();
            }
            
        }

        public async Task<Response> UpdateOrderStatus()
        {
            var order = await service.GetOrderById(request.orderId);
            var oldOrder = order;

            if (order == null)
            {
                return new OrderResponse()
                {
                    result = Com.OrderStorage.Result.NotFound
                };
            }
            order.Status = request.status;

            var res = await service.UpdateOrder(order);

            if (res.entity != null)
            {
                var updatePrice = new UpdatePriceRequest()
                {
                    oldOrder = oldOrder.ToComObject(),
                    updatedOrder = order.ToComObject()
                };
                await updatePrice.Send();

                return new OrderResponse()
                {
                    entity = res.entity.ToComObject(),
                    result = Com.OrderStorage.Result.Ok
                };
            }

            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.Exception
            };
        }

        public async Task<Response> UpdateOrderLimits()
        {
            try
            {
                var order = await service.GetOrderById(request.orderId);
                var oldOrder = order;

                order.Price = request.price;
                order.Amount = request.amount;
                order.Comment = request.comment;
                order.ExpiresTime = request.expiresTime;
                order.Rest = request.amount;

                if (order.Type == OrderType.Buy.ToString())
                {
                    if((request.stopLoss > order.Price && request.stopLoss != 0) || (request.takeProfit < order.Price && request.takeProfit != 0))
                        return new OrderResponse()
                        {
                            result = Com.OrderStorage.Result.Exception
                        };
                }
                else
                {
                    if((request.stopLoss < order.Price && request.stopLoss != 0) || (request.takeProfit > order.Price && request.takeProfit != 0))
                        return new OrderResponse()
                        {
                            result = Com.OrderStorage.Result.Exception
                        };
                }

                order.StopLoss = request.stopLoss;
                order.TakeProfit = request.takeProfit;
                order.Stop = request.stop;
                order.Limit = request.limit;

                var res = await service.UpdateOrder(order);

                if (res.entity != null)
                {
                    var updatePrice = new UpdatePriceRequest()
                    {
                        oldOrder = oldOrder.ToComObject(),
                        updatedOrder = order.ToComObject()
                    };
                    await updatePrice.Send();

                    return new OrderResponse()
                    {
                        entity = res.entity.ToComObject(),
                        result = Com.OrderStorage.Result.Ok
                    };
                }

                return new OrderResponse()
                {
                    result = Com.OrderStorage.Result.Exception
                };
            }catch(Exception ex)
            {
                return new OrderResponse()
                {
                    result = Com.OrderStorage.Result.Exception,
                    message = ex.Message
                };
            }
        }

        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.InvalidRequest
            };
        }
    }
}

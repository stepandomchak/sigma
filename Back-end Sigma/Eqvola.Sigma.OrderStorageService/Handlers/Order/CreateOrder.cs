﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Storage.Models;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class CreateOrder : StorageHandler<CreateOrderRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {
            Service.OrderStorage.Models.Order order = null;

            if (request.orderType == 1)
                order = new Service.OrderStorage.Models.BuyOrder();          
            else if (request.orderType == 2)
                order = new Service.OrderStorage.Models.SellOrder();

            if (order != null)
            {
                var wlId = await GetWLId(request.userId);

                order.Currency = request.currency;
                order.TargetCurrency = request.targetCurrency;
                order.CreationTime = DateTime.UtcNow;
                order.Rest = request.amount;
                order.ExpiresTime = request.expiresTime;
                order.Price = request.price;
                order.Amount = request.amount;
                order.UserId = request.userId;
                order.StopLoss = request.stopLoss;
                order.TakeProfit = request.takeProfit;
                order.Stop = request.stop;
                order.Limit = request.limit;
                order.WLId = wlId;
                order.Commission = request.commission;

                if (request.stop != 0 || request.limit != 0)
                    order.Status = (int)StatusOrder.PostponedByLimit;
                else
                    order.Status = (int)StatusOrder.NewOrder;


                var result = await service.CreateOrder(order);
                if (result == null || result.entity == null)
                {
                    return new OrderResponse()
                    {
                        result = Com.OrderStorage.Result.Exception
                    };
                }

                var blockFundsRequest = new BlockFundsRequest()
                {
                    userId = result.entity.UserId,
                    amount = result.entity.Amount,
                    currency = result.entity.Currency,
                    orderId = result.entity.Id
                };
                var blockFundsResponse = await blockFundsRequest.GetResponse<WalletResponse>();

                if(blockFundsResponse == null || blockFundsResponse.result != Com.Wallet.Result.Ok)
                {
                    await service.DeleteOrder(result.entity);
                    return new OrderResponse()
                    {
                        result = Com.OrderStorage.Result.InvalidRequest
                    };
                }
                
                return new OrderResponse()
                {
                    entity = result.entity.ToComObject(),
                    result = Com.OrderStorage.Result.Ok
                };
            }

            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.Exception
            };
        }

        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.InvalidRequest
            };
        }

        private async Task<long> GetWLId (long userId)
        {
            var getWLUserRequest = new GetWLUserRequest()
            {
                userId = userId
            };
            
            var getWLUserResponse = await getWLUserRequest.GetResponse<StorageResponse<WLUser>>();

            if (getWLUserResponse.entity == null)
                return 0;

            BitArray wlIdBitArray = new BitArray(BitConverter.GetBytes(getWLUserResponse.entity.Id));
            BitArray result = new BitArray(64);

            for (int i = 0, wl = 0, uid = 0; i < 64; i++)
            {
                if (i > 21 && i <= 42)
                {
                    result[wl] = wlIdBitArray[i];
                    wl++;
                }
            }

            Byte[] array = new Byte[8];
            result.CopyTo(array, 0);
            long wlId = BitConverter.ToInt64(array, 0);

            return wlId;
        }

    }
}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class FirstOrder : StorageHandler<GetFirstOrderRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Result.InvalidRequest
            };
        }
        protected async override Task<Response> ProccessAsync()
        {
            if (!string.IsNullOrEmpty(request.currency) && !string.IsNullOrEmpty(request.targetCurrency))
            {
                return CreateFailResponse();
            }
            var response = new OrderResponse();
            response.entity = (await service.GetFirstOrder(request.currency, request.targetCurrency)).ToComObject();

            if (response.entity != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

    }

}

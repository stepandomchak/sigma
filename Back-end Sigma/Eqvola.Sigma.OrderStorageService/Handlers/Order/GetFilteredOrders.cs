﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class GetFilteredOrders : StorageHandler<GetFilteredOrdersRequest>
    {
        protected override Response CreateFailResponse()
        {
            return new OrdersResponse()
            {
                result = Result.InvalidRequest
            };
        }
        protected async override Task<Response> ProccessAsync()
        {
            if(request.userId > 0)
            {
                var response = new OrdersResponse();

                response.orders = (await service.GetOrdersByUser(request.userId)).Select(m => m.ToComObject()).ToList();

                if (response.orders != null)
                    response.result = Result.Ok;
                else
                    response.result = Result.NotFound;

                return response;
            }
            if (!string.IsNullOrEmpty(request.currency) && !string.IsNullOrEmpty(request.targetCurrency))
            {
                return await GetOrdersByCurrency(request.status, request.currency, request.targetCurrency);
            }
            else
            {
                return await GetOrdersByStatus(request.status);
            }
        }

        protected async Task<OrdersResponse> GetOrdersByCurrency(int status, string currency, string targetCurrency)
        {
            var response = new OrdersResponse();
            response.orders = (await service.GetOrdersByCurrencyPair(status, currency, targetCurrency, request.ordersCount)).Select(m => m.ToComObject()).ToList();

            if (response.orders != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

        protected async Task<OrdersResponse> GetOrdersByStatus(int status)
        {
            var response = new OrdersResponse();
            response.orders = (await service.GetOrdersByStatus(status)).Select(m => m.ToComObject()).ToList();
            if (response.orders != null)
                response.result = Result.Ok;
            else
                response.result = Result.NotFound;

            return response;
        }

    }

}

﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderStorage.Models;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.OrderStorageService.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderStorageService.Handlers
{
    public class CancelOrder : StorageHandler<CancelOrderRequest>
    {
        protected override async Task<Response> ProccessAsync()
        {

            if (request.orderId != 0)
            {
                var order = (await service.GetOrderById(request.orderId));
                var oldOrder = order;
                if(order == null)
                    return new OrderResponse()
                    {
                        result = Com.OrderStorage.Result.Exception
                    };
                order.Status = (int)StatusOrder.Canceled;
                await service.UpdateOrder(order);

                var updatePrice = new UpdatePriceRequest()
                {
                    oldOrder = oldOrder.ToComObject(),
                    updatedOrder = order.ToComObject()
                };
                await updatePrice.Send();

                /////////////   Check how it works if order closed partially
                var unblockRequest = new UpdateFundsRequest()
                {
                    orderId = order.Id
                };
                var response = await unblockRequest.GetResponse<WalletResponse>();

                if (response.result == Com.Wallet.Result.Ok)
                    return new OrderResponse()
                    {
                        result = Com.OrderStorage.Result.Ok
                    };
            }

            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.Exception
            };
        }

        protected override Response CreateFailResponse()
        {
            return new OrderResponse()
            {
                result = Com.OrderStorage.Result.InvalidRequest
            };
        }
    }
}

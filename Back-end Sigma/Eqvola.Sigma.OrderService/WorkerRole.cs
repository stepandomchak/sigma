using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.Service;
using Eqvola.Sigma.Service.OrderStorage;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Eqvola.Sigma.OrderService
{
    public class WorkerRole : ServiceBusRole<Service.OrderService.Service>
    {
        protected override IEnumerable<string> queues => new string[] { "order-service" };

        protected override IDictionary<string, Func<BrokeredMessage, Task<Response>>> CreateHandlers(string queueName)
        {
            switch (queueName)
            {
                case "order-service":
                    return new Dictionary<string, Func<BrokeredMessage, Task<Response>>>
                    {
                        {
                            "check-order", CheckOrder
                        },
                        {
                            "check-limits", CheckLimits
                        },
                    };
                default:
                    throw new IndexOutOfRangeException();
            }
        }
        private async Task<Response> CheckOrder(BrokeredMessage message)
        {
            ValidateOrderRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<ValidateOrderRequest>(json);
            }
            var response = await service.OrderValidation(request);

            return response;
        }

        private async Task<Response> CheckLimits(BrokeredMessage message)
        {
            CheckLimitsRequest request = null;
            using (Stream stream = message.GetBody<Stream>())
            {
                StreamReader reader = new StreamReader(stream);
                string json = reader.ReadToEnd();

                request = JsonConvert.DeserializeObject<CheckLimitsRequest>(json);
            }
            await service.CheckLimits(request);

            return null;
        }
    }
}

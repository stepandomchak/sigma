﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    /// <summary>
    /// User data to confirm resetting user password
    /// </summary>
    public class AcceptResetPassword : Password
    {
        /// <summary>
        /// User email address
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        [Required]
        public string phoneNumber { get; set; }

        /// <summary>
        /// Code from SMS
        /// </summary>
        [Required]
        public string code { get; set; }
    }
}
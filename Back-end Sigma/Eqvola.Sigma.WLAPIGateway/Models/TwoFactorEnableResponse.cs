﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class TwoFactorEnableResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string url { get; set; }
        public string manualCode { get; set; }
    }
}
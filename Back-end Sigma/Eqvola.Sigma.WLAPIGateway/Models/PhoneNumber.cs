﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class PhoneNumber
    {

        [Required]
        [MaxLength(64)]
        public string phoneCode { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string phoneNumber { get; set; }
    }
}
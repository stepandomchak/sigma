﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class CreateWithdrawal : Operation
    {
        public string to { get; set; }

        public string comment { get; set; }

        public double fee { get; set; }

        public string twoFactorCode { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class CreateDeposit
    {
        public double amount { get; set; }
        public string txId { get; set; }
        public string from { get; set; }
        public long walletId { get; set; }
    }
}
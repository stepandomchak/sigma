﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Operation
    {
        public double amount { get; set; }
        
        public string email { get; set; }

        public string currencyCode { get; set; }                    
                
    }
}
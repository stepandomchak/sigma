﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class OrderPagination
    {
        public int count { get; set; }
        public int margin { get; set; }
        public int status { get; set; }

        public OrderPagination()
        {
            count = 20;
            margin = 0;
        }

    }
}
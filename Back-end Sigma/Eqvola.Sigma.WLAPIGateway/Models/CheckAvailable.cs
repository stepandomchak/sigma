﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class CheckAvailable
    {
        public bool isExist { get; set; }
        public string message { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models.Document
{
    public class UploadUserDoc
    {
        public string type { get; set; }

        public string email { get; set; }
               
        public string name { get; set; }       

        public byte[] data { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    /// <summary>
    /// Data for adding messangers
    /// </summary>
    public class MessangerEntry
    {
        /// <summary>
        /// Name of manager's messanger
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Contact of manager's messanger
        /// </summary>
        public string value { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models.Support
{
    public class Attachment
    {
        public byte[] base64 { get; set; }     
    }
}
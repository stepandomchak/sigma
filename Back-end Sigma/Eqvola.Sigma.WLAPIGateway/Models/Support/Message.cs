﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Message
    {

        [Required]
        public string text { get; set; }


        [Required]
        public long ticketId { get; set; }

        public List<string> attachments { get; set; }
    }
}
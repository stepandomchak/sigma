﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Category
    {
        public string name { get; set; }
        [Required]
        public bool isTicket { get; set; }
        [Required]
        public bool isTopic { get; set; }
    }
}
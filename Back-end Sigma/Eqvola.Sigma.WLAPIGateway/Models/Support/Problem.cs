﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Problem
    {
        [Required]
        public string question { get; set; }
        [Required]
        public string answer { get; set; }        
        [Required]
        public long topicId { get; set; }
    }
}
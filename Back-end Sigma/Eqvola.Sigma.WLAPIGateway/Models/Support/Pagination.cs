﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Pagination
    {
        public string email { get; set; }

        public int status { get; set; }

        public string categoryName { get; set; }

        public int count { get; set; }

        public int margin { get; set; }

        public Pagination()
        {
            count = 20;
            margin = 0;
        }

    }
}
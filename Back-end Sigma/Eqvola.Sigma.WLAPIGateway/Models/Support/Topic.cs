﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class TopicCreate
    {
        [Required]
        public string name { get; set; }
        [Required]
        public long categoryId { get; set; }        
    }

    public class TopicUpdate : TopicCreate
    {
        [Required]
        public int topicStatus { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class UserUpdate
    {
        /// <summary>
        /// Country in which user live
        /// </summary>
        public string country { get; set; }

        /// <summary>
        /// City in which manager live
        /// </summary>
        public string city { get; set; }

        public string address { get; set; }


        /// <summary>
        /// Messangers of manager
        /// </summary>
        public IEnumerable<MessangerEntry> messangers { get; set; }

    }
}
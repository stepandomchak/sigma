﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models.TwoFactor
{
    public class TwoFactorSettings
    {
        public string email { get; set; }

        public string twoFactorType { get; set; }

        public string twoFactorAction { get; set; }

        public List<TwoFactorOption> options { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models.TwoFactor
{
    public class EnableTwoFactor
    {
        public string email { get; set; }
        public string action { get; set; }
        public string type { get; set; }
        public Dictionary<string, double> options { get; set; }

        public EnableTwoFactor()
        {
            options = new Dictionary<string, double>();
        }

    }
}
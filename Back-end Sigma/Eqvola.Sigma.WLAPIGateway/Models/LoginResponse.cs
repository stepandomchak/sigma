﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class LoginResponse
    {
        /// <summary>
        /// Token for access to other methods
        /// 
        /// </summary>
        public string token { get; set; }
        //

        /// <summary>
        /// Status of request:
        /// 
        /// </summary>
        public int status { get; set; }
        /*<para>0 - wrong data</para>
        /// <para>1 - success</para>
        /// <para>2 - manager fired</para>
        /// <para>3 - department deleted</para>*/

        /// <summary>
        /// Description of failed attempt
        /// </summary>
        public string message { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class OldTwoFactor
    {
        /// <summary>
        /// Code from two-factor application
        /// </summary>
        public string code { get; set; }
    }
}
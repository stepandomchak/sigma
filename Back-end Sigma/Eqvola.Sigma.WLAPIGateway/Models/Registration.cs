﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Registration: Password
    {
        [Required]
        [EmailAddress]
        public string email { get; set; }


        [Required]
        [MaxLength(64)]
        public string firstName { get; set; }

        [Required]
        [MaxLength(64)]
        public string lastName { get; set; }


        [Required]
        [MaxLength(16)]
        public string phoneCode { get; set; }

        [Required]
        [MaxLength(32)]
        public string phoneNumber { get; set; }

        [MaxLength(128)]
        public string country { get; set; }

        [MaxLength(128)]
        public string city { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string code { get; set; }
    }
}
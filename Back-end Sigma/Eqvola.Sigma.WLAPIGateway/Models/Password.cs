﻿using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Password
    {
        private string _password;

        /// <summary>
        /// User password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string password
        {
            get { return _password; }
            set { _password = EncodingPassword(value); }
        }


        private string EncodingPassword(string password)
        {
            string salt = CloudConfigurationManager.GetSetting("MD5.Salt");
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] bytes = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(salt + password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }
    }
}
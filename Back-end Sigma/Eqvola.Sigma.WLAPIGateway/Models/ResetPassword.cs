﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    
    /// <summary>
    /// User data for reset password
    /// </summary>
    public class ResetPassword
    {
        /// <summary>
        /// User email address
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        [Required]
        public string phoneNumber { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class OrderUpdate
    {

        public DateTime? expiresTime { get; set; }

        public double price { get; set; }

        public double amount { get; set; }

        public double stopLoss { get; set; }

        public double takeProfit { get; set; }

        public double stop { get; set; }

        public double limit { get; set; }

        public string comment { get; set; }
    }
}
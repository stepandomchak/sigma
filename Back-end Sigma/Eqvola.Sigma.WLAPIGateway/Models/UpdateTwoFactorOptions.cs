﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class UpdateTwoFactorOptions
    {

        public long settingId { get; set; }
        public Dictionary<string, long> options { get; set; }
        public string code { get; set; }

    }
}
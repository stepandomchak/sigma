﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    /// <summary>
    /// User data to confirm resetting user password
    /// </summary>
    public class AcceptChangePassword : Password
    {
        /// <summary>
        /// Code from SMS
        /// </summary>
        [Required]
        public string code { get; set; }
    }
}
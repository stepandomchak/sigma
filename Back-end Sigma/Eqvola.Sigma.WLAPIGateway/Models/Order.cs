﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Models
{
    public class Order
    {
        [Required]
        public string currency { get; set; }
        [Required]
        public string targetCurrency { get; set; }
        public DateTime? expiresTime { get; set; }
        [Required]
        public double price { get; set; }
        [Required]
        public double amount { get; set; }
        public double stopLoss { get; set; }
        public double takeProfit { get; set; }
        public double stop { get; set; }
        public double limit { get; set; }
        public string comment { get; set; }

        public string twoFactorCode { get; set; }
    }
}
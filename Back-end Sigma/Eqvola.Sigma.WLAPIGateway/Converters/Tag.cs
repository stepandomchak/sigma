﻿namespace Eqvola.Sigma.StorageService.Converters
{
    public static class Tag
    {
        public static string ToComObject(this Service.Storage.Models.ManagerTag src)
        {
            return src.Name;
        }
    }
}

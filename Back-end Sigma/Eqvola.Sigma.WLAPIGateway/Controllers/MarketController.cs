﻿using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.WLAPIGateway.Models;
using Eqvola.Sigma.WLAPIGateway.Providers;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Eqvola.Sigma.Com.Market;
using Eqvola.Sigma.Com.Storage;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class MarketController : ApiController
    {
        /// <summary>
        /// Get list of markets
        /// </summary>
        /// <returns> List of markets </returns>
        [HttpGet]
        [Route("api/market/get")]
        [SwaggerOperation("GetAll")]
        public async Task<IHttpActionResult> Get()
        {
            var request = new GetMarketsRequest();
            var response = await request.GetResponse<MarketsResponse>();

            return Ok(response.markets.Select(m =>
            new {
                masterCurrencyCode = m.MasterCurrecyCode,
                status = m.Status,
                targetCurrencies = m.TargetCurrencies.Select(c =>
                new {
                    name = c.Name,
                    code = c.Code,
                    status = c.Status,
                    isFixedFee = c.IsFixedFee,
                    fixedFee = c.FixedFee,
                    minFee = c.MinFee,
                    maxFee = c.MaxFee
                }).ToList()
            }).ToList());
        }

        /// <summary>
        /// Get list of currencies
        /// </summary>
        /// <returns> List of markets </returns>
        [HttpGet]
        [Route("api/market/currency/get")]
        [SwaggerOperation("GetAll")]
        public async Task<IHttpActionResult> GetAllCurrencies()
        {
            var request = new GetAllCurrenciesRequest();
            var response = await request.GetResponse<CurrenciesResponse>();

            return Ok(response.currencies.Select(c => 
            new {
                name = c.Name,
                code = c.Code,
                status = c.Status,
                isFixedFee = c.IsFixedFee,
                fixedFee = c.FixedFee,
                minFee = c.MinFee,
                maxFee = c.MaxFee
            }).ToList());

        }

    }
}

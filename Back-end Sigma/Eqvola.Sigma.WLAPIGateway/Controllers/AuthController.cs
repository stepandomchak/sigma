﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Service.Storage.Models;
using Eqvola.Sigma.WLAPIGateway.Converters;
using Eqvola.Sigma.WLAPIGateway.Models;
using Eqvola.Sigma.WLAPIGateway.Providers;
using Google.Authenticator;
using Swashbuckle.Swagger.Annotations;
using User = Eqvola.Sigma.Service.Storage.Models.User;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class AuthController : ApiController
    {
        /// <summary>
        /// Method for authentication.
        /// </summary>
        /// <param name="model">User data</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("api/auth/Login")]
        [SwaggerOperation("Login")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(LoginResponse), Description = "token - Token for access to other methods\n\r" +
            "Status values:\n\r\t" +
            "0 - wrong data\n\t" +
            "1 - success\n\t" +
            "2 - manager fired\n\t" +
            "3 - department deleted\n\t" +
            "4 - 2FA code not found\n\t" +
            "5 - Wrong 2FAcode\n\t" +
            "6 - Send code\n\t" +
            "7 - status undefind\n\r" +
            "message - Description of failed attempt")]
        public async Task<IHttpActionResult> Login(Login model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var service = new Service.Storage.Service();
            
            var user = await service.GetUserByEmailAndPassword(model.email, model.password);

            if(user == null)
            {
                return Ok(new LoginResponse() { token = "", message = "No Such User", status = 0 });
            }

            var twoFactors = await service.GetSettingsByUser(user.Id);
            if (twoFactors == null)
                return BadRequest();

            var twoFactor = await service.GetSettingsByUserAction(user.Id, 1);

            if (twoFactor != null && twoFactor.State == 1)
            {

                if (string.IsNullOrEmpty(model.code) && twoFactor.TwoFactor.Type.Equals("Google2FA"))                
                    return Ok(new LoginResponse() { token = "", message = "Сode not found", status = 4 });
                

                Service.TwoFactor.Service twoFactorService = new Service.TwoFactor.Service();
                var res = await twoFactorService.AcceptTwoFactorAction(twoFactor.Id, model.code);
                
                if (res.result == Service.TwoFactor.TwoFactorResult.SendCode)
                    return Ok(new LoginResponse() { token = "", message = "Send code", status = 6 });

                if (res.result == Service.TwoFactor.TwoFactorResult.WrongCode)
                    return Ok(new LoginResponse() { token = "", message = "Wrong code", status = 5 });
            }

            UserToken userToken = new UserToken()
            {
                email = user.Email,
                firstName = user.FirstName,
                lastName = user.LastName,
                phoneCode = user.Phone_code,
                phoneNumber = user.Phone_number,
                country = user.Country,
                address = user.Address,
                city = user.City,
                avatarUrl = user.AvatarUrl,
                isVerified = user.IsVerified,
                messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            };
            userToken.twoFactorSettings = new List<Models.UserTwoFactor>();
            foreach(var t in twoFactors)
            {
                Models.UserTwoFactor setting = new Models.UserTwoFactor()
                {
                    settingId = t.Id,
                    type = t.TwoFactor.Type,
                    action = t.TwoFactorAction.Name,
                    state = t.State
                };
                userToken.twoFactorSettings.Add(setting);
            }

            var token = await CreateToken(TokenValidator.JWTExtraHeaders, userToken, TokenValidator.JwtSecretKey);
                
            return Ok(new LoginResponse() { token = token, message = "", status = 1 });
        }

        
        private async Task<string> CreateToken(string headers, object payload, string key)
        {
            return await Task.Run(() =>
            {
                return TokenValidator.Encode<object>(headers, payload, Encoding.UTF8.GetBytes(key));
            });
        }

        /// <summary>
        /// Reset user password
        /// </summary>
        /// <param name="model">User data for reset password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("ResetPassword")]
        [Route("api/auth/ResetPassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> ResetPassword(Models.ResetPassword model)
        {
            if (string.IsNullOrEmpty(model.email) || string.IsNullOrEmpty(model.phoneNumber))
            {
                return BadRequest("Parametrs is not valid ");
            }

            var service = new Service.Storage.Service();

            Random generator = new Random();
            string code = generator.Next(100000, 1000000).ToString();

            if ((!string.IsNullOrEmpty(code)) && (!string.IsNullOrEmpty(model.email)) && (!string.IsNullOrEmpty(model.phoneNumber)))
            {
                var user = await service.GetUserByEmail(model.email);

                if (user != null)
                {

                    string phone = user.Phone_code + user.Phone_number;

                    if (!phone.Equals(model.phoneNumber))
                    {
                        return BadRequest("Wrong email or phone number");
                    }

                    var saveCodeResponse = await service.UpdateUserResetCode(code, user);

                    if (saveCodeResponse == "Reset code cannot be updated")
                        return BadRequest(saveCodeResponse);

                    if(saveCodeResponse == null)
                    {
                        Service.Twilio.Service twilio = new Service.Twilio.Service();
                        await twilio.SendMessage(phone, $"Code: {code}");
                    }

                    return Ok(saveCodeResponse);
                }
                return BadRequest("Wrong email or phone number");
            }
            return BadRequest("Wrong params");
        }

        /// <summary>
        /// Confirm resetting user password
        /// </summary>
        /// <param name="model">User data to confirm resetting user password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("AcceptResetPassword")]
        [Route("api/auth/AcceptResetPassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptResetPassword(AcceptResetPassword model)
        {
            if (string.IsNullOrEmpty(model.password) || string.IsNullOrEmpty(model.code))
            {
                return BadRequest("Parametrs is not valid");
            }

            var service = new Service.Storage.Service();
            
            var response = await service.UpdateUserPassword(model.email, model.phoneNumber, model.code, model.password);

            if (response.entity != null)
            {
                return Ok();
            }

            return BadRequest(response.message ?? "Failed to change password");
        }
        
        /// <summary>
        /// Registration user
        /// </summary>
        /// <param name="model">User data</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/Registration")]
        [SwaggerOperation("Registration")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> Registration(Models.Registration model)
        {
            var service = new Service.Storage.Service();

            var checkCode = await service.CheckUserRegistrationCode(model.code, model.phoneCode + model.phoneNumber);

            if(!checkCode)
            {
                return BadRequest("Wrong code");
            }

            User user = new User()
            {
                Email = model.email, 
                FirstName = model.firstName,
                LastName = model.lastName,
                Password = model.password,
                Phone_code = model.phoneCode,
                Phone_number = model.phoneNumber,
                Country = model.country,
                City = model.city
            };

            var userResponse = await service.AddUser(user);
            
            if (userResponse != null)
            {
                if (userResponse.message.Contains("unique"))
                {
                    if (userResponse.message.Contains("Email"))
                        return BadRequest("ExistenEmail");
                    else if (userResponse.message.Contains("Phone"))
                        return BadRequest("ExistenPhone");
                }
                else if (userResponse.entity != null)
                {
                    AddWLUserRequest request = new AddWLUserRequest() { wlName = "EqvolaTest", userId = userResponse.entity.Id};
                    await request.Send();

                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            
            return BadRequest();
        }

        /// <summary>
        /// Check user phone number
        /// </summary>
        /// <param name="model">Phone number</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/CheckPhone")]
        [SwaggerOperation("Check phone number")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success", Type = typeof(CheckAvailable))]
        public async Task<IHttpActionResult> CheckPhone(PhoneNumber model)
        {
            var service = new Service.Storage.Service();
            var isEmpty = await service.CheckNumber(model.phoneCode, model.phoneNumber);
            if(isEmpty)
            {
                return Ok(new CheckAvailable()
                {
                    isExist = false,
                    message = "Phone not found"
                });
            }
            return Ok(new CheckAvailable()
            {
                isExist = true,
                message = "Phone already used"
            });
        }

        /// <summary>
        /// Check the availability of email
        /// </summary>
        /// <param name="model">Email address</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/CheckEmail")]
        [SwaggerOperation("Check email address")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success", Type = typeof(CheckAvailable))]
        public async Task<IHttpActionResult> CheckEmail([FromBody]Email model)
        {
            var service = new Service.Storage.Service();
            var user = await service.GetUserByEmail(model.email);
            if (user == null)
            {
                return Ok(new CheckAvailable()
                {
                    isExist = false,
                    message = "Email not found"
                });
            }
            return Ok(new CheckAvailable()
            {
                isExist = true,
                message = "Email already used"
            });
        }

        /// <summary>
        /// Accept user phone number
        /// </summary>
        /// <param name="model">Phone number</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/AcceptPhone")]
        [SwaggerOperation("Accept phone number")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptPhone(PhoneNumber model)
        {
            if (!IsPhoneNumber(model.phoneCode + model.phoneNumber))
                return BadRequest("Phone is not valid");
            var service = new Service.Storage.Service();
            var isEmpty = await service.CheckNumber(model.phoneCode, model.phoneNumber);
            if (isEmpty)
            {
                Random generator = new Random();
                string code = generator.Next(100000, 1000000).ToString();

                var response = await service.UpdateRegistrationCode(code, model.phoneCode + model.phoneNumber);
                
                if(response == null)
                {
                    Service.Twilio.Service twilio = new Service.Twilio.Service();
                    await twilio.SendMessage(model.phoneCode + model.phoneNumber, $"Code: {code}");

                    return Ok();
                }
                return BadRequest(response);
            }
            return BadRequest("Phone number already exist");
        }


        private bool IsPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{12})$").Success;
        }

    }
}

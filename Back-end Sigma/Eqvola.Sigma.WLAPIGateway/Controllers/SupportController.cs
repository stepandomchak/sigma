﻿using System;
using Eqvola.Sigma.Com.Storage;
using Eqvola.Sigma.Com.Support.Attachment;
using Eqvola.Sigma.Com.Support.Category;
using Eqvola.Sigma.Com.Support.Message;
using Eqvola.Sigma.Com.Support.Ticket;
using Eqvola.Sigma.Com.Support.Topic;
using Eqvola.Sigma.WLAPIGateway.Models;
using Eqvola.Sigma.WLAPIGateway.Providers;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Result = Eqvola.Sigma.Com.Support.Result;
using System.Web;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class SupportController : ApiController
    {
        #region Category
        [HttpGet]
        [Route("api/support/getCategories")]
        [SwaggerOperation("GetCategories")]
        public async Task<IHttpActionResult> GetCategories()
        {
            var request = new GetCategoriesRequest();

            var response = await request.GetResponse<CategoriesResponse>();

            return Ok(response.categories.Select(c => new
            {
                id = c.Id,
                name = c.Name,
                IsEnable = c.IsEnable
            }));
        }
        #endregion

        #region Ticket

        [HttpPost]
        [Route("api/support/getTickets/")]
        [SwaggerOperation("GetTickets")]
        public async Task<IHttpActionResult> GetTickets([FromBody]Pagination model)
        {
            var request = new GetTicketRequest()
            {
                email = model.email,
                count = model.count,
                margin = model.margin,
                status = model.status
            };

            var response = await request.GetResponse<TicketsResponse>();

            if (response.result == Result.Ok)
            {
                var service = new Service.Storage.Service();

                List<string> users = response.tickets.GroupBy(t => t.Email).Select(s => s.Key).ToList();
                var getUsers = await service.GetRangeUsersByEmails(users); 

                return Ok(new
                {
                    tickets = response.tickets.Select(m => new
                    {
                        id = m.Id,
                        title = m.Title,
                        ticketStatus = m.TicketStatus,
                        created = m.Created,
                        categoryName = m.Category.Name,
                        lastMessage = m.Messages.Select(s => new
                        {
                            email = s.Email,
                            text = s.Text,
                            date = s.Created,
                            fullName = "",
                            attachments = s.Attachments?.Select(a =>
                            new
                            {
                                id = a.Id,
                                url = a.Url,
                                fileName = a.FileName
                            })
                        }).LastOrDefault()
                    }),
                    users = getUsers?.Select(u => new { email = u.Email, avatarUrl = u.AvatarUrl, fullName = u.FirstName + " " + u.LastName })
                });
            }
            return BadRequest(response.result.ToString());
        }

        [HttpGet]
        [Route("api/support/getTicketById/{id}/")]
        [SwaggerOperation("GetTicketById")]
        public async Task<IHttpActionResult> GetTicketById(long id)
        {
            var request = new GetTicketRequest()
            {
                id = id
            };

            var response = await request.GetResponse<TicketResponse>();

            if (response.result == Result.Ok)
            {
                List<string> users = response.entity?.Messages?.GroupBy(t => t?.Email).Select(s => s?.Key).ToList();
                var getUsersRequest = new GetRangeUsersRequest() { emailUsers = users };
                var usersResponse = await getUsersRequest.GetResponse<UsersResponse>();

                return Ok(new
                {
                    ticket = (response.entity == null) ?
                        null :
                        new
                        {
                            id = response.entity.Id,
                            title = response.entity.Title,
                            email = response.entity.Email,
                            created = response.entity.Created,
                            catetoryName = response.entity.Category.Name,
                            ticketStatus = response.entity.TicketStatus,
                            messages = response.entity.Messages.Select(m => new
                            {
                                email = response.entity.Email,
                                text = m.Text,
                                date = m.Created,
                                attachments = m.Attachments.Select(a =>
                                new
                                {
                                    id = a.Id,
                                    url = a.Url,
                                    fileName = a.FileName
                                })
                            })
                        },
                    users = usersResponse?.users?.Select(u => new { email = u?.Email, avatarUrl = u?.AvatarUrl, fullName = u?.FirstName + " " + u?.LastName })
                });
            }
            return BadRequest(response.result.ToString());
        }

        [HttpPost]
        [Route("api/support/createTicket/")]
        [SwaggerOperation("CreateTicket")]
        public async Task<IHttpActionResult> CreateTicket([FromBody] Ticket ticket)
        {
            if (ticket.attachments.Count > 10)
            {
                ticket.attachments = ticket.attachments.GetRange(0, 10);
            }
            var request = new CreateTicketRequest()
            {
                title = ticket.title,
                email = ticket.email,
                categoryId = ticket.categoryId,
                message = ticket.message,
                attachments = ticket.attachments
            };
            var response = await request.GetResponse<TicketResponse>();

            if (response.result == Result.Ok)
            {

                var service = new Service.Storage.Service();
                
                List<string> users = response.entity.Messages.GroupBy(t => t.Email).Select(s => s.Key).ToList();

                var getUsers = await service.GetRangeUsersByEmails(users);

                return Ok(new
                {
                    ticket = new
                    {
                        id = response.entity.Id,
                        title = response.entity.Title,
                        ticketStatus = response.entity.TicketStatus,
                        created = response.entity.Created,
                        categoryName = response.entity.Category.Name,
                        lastMessage = response.entity.Messages.Select(s => new
                        {
                            email = s.Email,
                            text = s.Text,
                            date = s.Created,
                            fullName = "",
                            attachments = s.Attachments.Select(a =>
                            new
                            {
                                id = a.Id,
                                url = a.Url,
                                fileName = a.FileName
                            })
                        }).LastOrDefault()
                    },
                    users = getUsers?.Select(u => new { email = u.Email, avatarUrl = u.AvatarUrl, fullName = u.FirstName + " " + u.LastName })
                });
            }

            return BadRequest(response.result.ToString());
        }

        [HttpPut]
        [Route("api/support/updateTicket/{ticketId}")]
        [SwaggerOperation("UpdateTicket")]
        public async Task<IHttpActionResult> UpdateTicket(long ticketId, int status)
        {
            var request = new UpdateTicketRequest()
            {
                id = ticketId,
                status = status
            };
            var response = await request.GetResponse<TicketResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    id = response.entity.Id,
                    title = response.entity.Title,
                    email = response.entity.Email,
                    created = response.entity.Created,
                    catetoryId = response.entity.Category?.Id,
                    ticketStatus = response.entity.TicketStatus
                });

            return BadRequest(response.result.ToString());
        }

        #endregion

        #region Message

        [HttpPost]
        [Route("api/support/sendMessage/")]
        [SwaggerOperation("SendMessage")]
        public async Task<IHttpActionResult> SendMessage([FromBody]Message message)
        {
            if (Request?.Headers?.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();

            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;
            UserToken tokenUser;

            try
            {
                tokenUser = TokenValidator.Decode<UserToken>(token);
            }
            catch
            {
                return BadRequest("Wrong token");
            }

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(tokenUser.email);

            if (user == null)
                return BadRequest("Wrong authorization");

            if (string.IsNullOrEmpty(message.text) && (message.attachments == null ||  message.attachments.Count == 0))
                return BadRequest("Message is empty");

            var request = new SendMessageRequest()
            {
                email = user.Email,
                text = message.text ?? string.Empty,
                ticketId = message.ticketId,
                listAttachments = message.attachments,
                Created = DateTime.UtcNow
            };
            var response = await request.GetResponse<MessageResponse>();

            if (response.result == Result.Ok)
                return Ok(new
                {
                    email = response.entity.Email,
                    text = response.entity.Text,
                    date = response.entity.Created,
                    attachments = response.entity.Attachments.Select(a =>
                    new
                    {
                        id = a.Id,
                        url = a.Url,
                        fileName = a.FileName
                    })
                });

            return BadRequest(response.result.ToString());
        }

        #endregion

        #region Topic

        [HttpGet]
        [Route("api/support/getTopicsByCategory/{categoryId}/")]
        [SwaggerOperation("GetTopicsByCategory")]
        public async Task<IHttpActionResult> GetTopicsByCategory(long categoryId)
        {
            var request = new GetTopicRequest
            {
                categoryId = categoryId
            };
            var response = await request.GetResponse<TopicsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.topics.Select(item => new
                {
                    id = item.Id,
                    name = item.Name,
                    topicStatus = item.TopicStatus,
                    categoryId = item.Category?.Id
                }));

            return BadRequest(response.result.ToString());
        }

        [HttpGet]
        [Route("api/support/getAllTopics/")]
        [SwaggerOperation("GetAllTopics")]
        public async Task<IHttpActionResult> GetAllTopics()
        {
            var request = new GetTopicRequest();
            var response = await request.GetResponse<TopicsResponse>();
            if (response.result == Result.Ok)
                return Ok(response.topics.Select(item => new
                {
                    id = item.Id,
                    name = item.Name,
                    topicStatus = item.TopicStatus,
                    categoryId = item.Category?.Id
                }));
            return BadRequest(response.result.ToString());
        }

        #endregion

       

        #region Attachment
        [HttpPut]
        [Route("api/support/uploadAttachment/{id}/")]
        [SwaggerOperation("UploadAttachmentById")]
        public async Task<IHttpActionResult> UploadAttachment(long id, [FromBody] Models.Support.Attachment model)
        {
            var request = new UploadAttachmentRequest()
            {
                id = id,
            };

            var response = await request.GetResponse<AttachmentResponse>();

            if (response.result != Result.Ok)
                return BadRequest(response.result.ToString());

            var mimeType = MimeMapping.GetMimeMapping(response.entity.FileName);
            var url = await new Service.Blob.Service().UploadFile(response.entity.Id.ToString(), mimeType,
                model.base64, "attachments");
            if (string.IsNullOrEmpty(url))
                return BadRequest("File is not upload");

            request.url = url;
            response = await request.GetResponse<AttachmentResponse>();

            if (response.result == Result.Ok)

                return Ok(
                    new
                    {
                        id = response.entity.Id,
                        url = response.entity.Url,
                        fileName = response.entity.FileName
                    });

            return BadRequest(response.result.ToString());
        }
        #endregion
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Eqvola.Sigma.WLAPIGateway.Converters;
using Eqvola.Sigma.WLAPIGateway.Models;
using Eqvola.Sigma.WLAPIGateway.Providers;
using Google.Authenticator;
using Swashbuckle.Swagger.Annotations;
using User = Eqvola.Sigma.Service.Storage.Models.User;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class UserController : ApiController
    {
        private async Task<string> CreateToken(string headers, object payload, string key)
        {
            return await Task.Run(() =>
            {
                return TokenValidator.Encode<object>(headers, payload, Encoding.UTF8.GetBytes(key));
            });
        }
        /// <summary>
        /// Get user information
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns> List of managers </returns>
        [HttpGet]
        [SwaggerOperation("Get")]
        [Route("api/user/{email}/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Models.User>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> GetUserByEmail(string email)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if(user == null)
            {
                return NotFound();
            }

            return Ok(UserConverter.ToComObject(user));
        }

        /// <summary>
        /// Update user information
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="model">User data for updating</param>
        /// <returns> Updated user </returns>
        [HttpPut]
        [SwaggerOperation("Update")]
        [Route("api/user/{email}/")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(IEnumerable<Models.User>), Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> UpdateUser(string email, [FromBody]UserUpdate model)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }
            else
            {
                user.Address = model.address;
                user.City = model.city;
                user.Country = model.country;

                var messangers = new Dictionary<string, string>();
                foreach (var entry in model.messangers)
                    messangers.Add(entry.name, entry.value);

                var contactsToRemove = user.ImContacts.Where(p => !messangers.Keys.Contains(p.messanger)).ToList();

                // Remove unnecessary contacts
                foreach (var p in contactsToRemove)
                {
                    user.ImContacts.Remove(p);
                }

                // Update corrent contacts
                foreach (var c in user.ImContacts)
                {
                    if (messangers.ContainsKey(c.messanger))
                        c.contact = messangers[c.messanger];
                }

                // Add new contacts
                var exists = user.ImContacts.Select(p => p.messanger);
                var add = messangers.Where(e => !exists.Contains(e.Key));

                foreach (var p in add)
                    user.ImContacts.Add(new Service.Storage.Models.ImContact()
                    {
                        messanger = p.Key,
                        contact = p.Value
                    });

                var response = await service.UpdateUser(user);
                if (response.entity == null) 
                    return BadRequest(response.message);

            }            

            var twoFactors = await service.GetSettingsByUser(user.Id);
            if (twoFactors == null)
                return BadRequest();

            UserToken userToken = new UserToken()
            {
                email = user.Email,
                firstName = user.FirstName,
                lastName = user.LastName,
                phoneCode = user.Phone_code,
                phoneNumber = user.Phone_number,
                country = user.Country,
                address = user.Address,
                city = user.City,
                avatarUrl = user.AvatarUrl,
                isVerified = user.IsVerified,
                messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            };

            userToken.twoFactorSettings = new List<Models.UserTwoFactor>();
            foreach (var t in twoFactors)
            {
                Models.UserTwoFactor setting = new Models.UserTwoFactor()
                {
                    settingId = t.Id,
                    type = t.TwoFactor.Type,
                    action = t.TwoFactorAction.Name,
                    state = t.State
                };
                userToken.twoFactorSettings.Add(setting);
            }

            var token = await CreateToken(TokenValidator.JWTExtraHeaders, userToken, TokenValidator.JwtSecretKey);
            
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            context.Response.Headers.Add("X-Authorization", token);

            return Ok(userToken);
        }
        
        /// <summary>
        /// Uploading avatar for user
        /// </summary>
        /// <param name="email"> Managers email </param>
        /// <param name="model"> Data about avatar</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/user/avatar/{email}")]
        [SwaggerOperation("UploadAvatar")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound, Description = "Error description")]
        public async Task<IHttpActionResult> UploadAvatar(string email, [FromBody]UploadAvatar model)
        {
            if (model == null)
                return BadRequest();

            ValidateStatus validateResponse = model.ValidateAvatar();

            if (!validateResponse.status)
            {
                return BadRequest(validateResponse.description);
            }

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if (user == null)
            {
                return BadRequest("User not found");
            }

            var url =  await new Service.Blob.Service().UploadFile(email, model.mimeType, validateResponse.bytes, "user-avatars");

            user.AvatarUrl = url;
            await service.UpdateUser(user);

            var twoFactors = await service.GetSettingsByUser(user.Id);
            if (twoFactors == null)
                return BadRequest();

            UserToken userToken = new UserToken()
            {
                email = user.Email,
                firstName = user.FirstName,
                lastName = user.LastName,
                phoneCode = user.Phone_code,
                phoneNumber = user.Phone_number,
                country = user.Country,
                address = user.Address,
                city = user.City,
                avatarUrl = user.AvatarUrl,
                isVerified = user.IsVerified,
                messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            };

            userToken.twoFactorSettings = new List<Models.UserTwoFactor>();
            foreach (var t in twoFactors)
            {
                Models.UserTwoFactor setting = new Models.UserTwoFactor()
                {
                    settingId = t.Id,
                    type = t.TwoFactor.Type,
                    action = t.TwoFactorAction.Name,
                    state = t.State
                };
                userToken.twoFactorSettings.Add(setting);
            }

            var token = await CreateToken(TokenValidator.JWTExtraHeaders, userToken, TokenValidator.JwtSecretKey);

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            context.Response.Headers.Add("X-Authorization", token);

            return Ok(url);
        }

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="model">User data to change user password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("ChangePassword")]
        [Route("api/user/ChangePassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> ChangePassword(Password model)
        {

            if (string.IsNullOrEmpty(model.password))
            {
                return BadRequest("Parametrs is not valid ");
            }

            if (Request == null || Request.Headers == null || Request.Headers.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();
            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;

            UserToken user = null;

            try
            {
                user = TokenValidator.Decode<UserToken>(token);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            var service = new Service.Storage.Service();

            var responseUser = await service.GetUserByEmail(user.email);

            if (responseUser == null)
            {
                return BadRequest("Wrong token");
            }

            return await ResetPassword(new Models.ResetPassword() { email = responseUser.Email, phoneNumber = responseUser.Phone_code + responseUser.Phone_number });

        }

        private Task<IHttpActionResult> ResetPassword(Models.ResetPassword resetPassword)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Confirm changing user password
        /// </summary>
        /// <param name="model">User data to confirm changing user password</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("AcceptChangePassword")]
        [Route("api/user/AcceptChangePassword")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptChangePassword(AcceptChangePassword model)
        {

            if (string.IsNullOrEmpty(model.password))
            {
                return BadRequest("Parametrs is not valid ");
            }

            if (Request == null || Request.Headers == null || Request.Headers.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();
            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;

            UserToken userToken = null;

            try
            {
                userToken = TokenValidator.Decode<UserToken>(token);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            bool isHash = model.code.Length == 32 ? true : false;

            var service = new Service.Storage.Service();

            var responseUser = await service.GetUserByEmail(userToken.email);

            if (responseUser == null)
            {
                return BadRequest("Wrong token");
            }

            var response = await service.UpdateUserPassword(responseUser.Email, responseUser.Phone_code + responseUser.Phone_number, model.code, model.password);

            if (response != null)                           
                return Ok();
            

            return BadRequest();
        }

        /// <summary>
        /// Generate two-factor secret
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/user/EnableTwoFactor/{email}")]
        [SwaggerOperation("EnableTwoFactor")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Type = typeof(TwoFactorEnableResponse), Description = "Status values:\n\r\t0 - wrong data\n\t1 - success\n\r" +
            "message - Description of failed attempt\n\r" +
            "url - link to QR code\n\r" +
            "manualCode - code for manually adding two-factor")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> EnableTwoFactor(string email)
        {
            var service = new Service.Storage.Service();

            var userResponse = await service.GetUserByEmail(email);

            if (userResponse == null)
            {
                return BadRequest();
            }

            if (userResponse.IsTwoFactorEnabled)
            {
                return BadRequest("Two factor already enabled");
            }

            string secret = RandomString();

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            var setupInfo = tfa.GenerateSetupCode("Sigma", email, secret, 300, 300);

            string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;

            string manualEntrySetupCode = setupInfo.ManualEntryKey;


            userResponse.IsTwoFactorEnabled = false;
            userResponse.TwoFactorSecretKey = secret;
            await service.UpdateUser(userResponse);
                      
            return Ok(new TwoFactorEnableResponse() { status = true, url = qrCodeImageUrl, manualCode = manualEntrySetupCode });
        }
        
        private string RandomString(int count = 10)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, count)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Enabling two-factor
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="model">Data to confirm setting two-factor</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/user/AcceptEnableTwoFactor/{email}")]
        [SwaggerOperation("AcceptEnableTwoFactor")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> AcceptEnableTwoFactor(string email, OldTwoFactor model)
        {

            var service = new Service.Storage.Service();

            var userResponse = await service.GetUserByEmail(email);

            if (userResponse == null)
            {
                return BadRequest();
            }

            if (userResponse.IsTwoFactorEnabled)
            {
                return BadRequest("Two factor already enabled");
            }

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if (!tfa.ValidateTwoFactorPIN(userResponse.TwoFactorSecretKey, model.code))
            {
                return BadRequest("Wrong code");
            }


            userResponse.IsTwoFactorEnabled = true;
            await service.UpdateUser(userResponse);

            return Ok();

        }

        /// <summary>
        /// Disabling two-factor
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="model">Data to confirm setting two-factor</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/user/DisableTwoFactor/{email}")]
        [SwaggerOperation("DisableTwoFactor")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, Description = "Success")]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, Type = typeof(string), Description = "Error description")]
        public async Task<IHttpActionResult> DisableTwoFactor(string email, OldTwoFactor model)
        {
            var service = new Service.Storage.Service();

            var userResponse = await service.GetUserByEmail(email);

            if (userResponse == null)
            {
                return BadRequest();
            }

            if (userResponse.IsTwoFactorEnabled)
            {
                return BadRequest("Two factor already enabled");
            }

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if (!tfa.ValidateTwoFactorPIN(userResponse.TwoFactorSecretKey, model.code))
            {
                return BadRequest("Wrong code");
            }

            userResponse.IsTwoFactorEnabled = false;
            userResponse.TwoFactorSecretKey = "";
            await service.UpdateUser(userResponse);

            return Ok();
        }


    }
}

﻿using Eqvola.Sigma.Com.Storage.TwoFactor;
using Eqvola.Sigma.Com.Twilio;
using Eqvola.Sigma.WLAPIGateway.Models;
using Eqvola.Sigma.WLAPIGateway.Models.TwoFactor;
using Eqvola.Sigma.WLAPIGateway.Providers;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class TwoFactorController : ApiController
    {
        [HttpGet]
        [SwaggerOperation("GetAllTwoFactor")]
        [Route("api/twofactor/getAllTwoFactor/")]
        public async Task<IHttpActionResult> GetAllTwoFactor()
        {
            var service = new Service.Storage.Service();

            var response = await service.GetAllTwoFactors();

            if (response != null)
                return Ok(response.Select(item => new
                {
                    type = item.Type,
                    status = item.Status
                }));

            return BadRequest();
        }

        [HttpPut]
        [SwaggerOperation("UpdateTwoFactor")]
        [Route("api/twofactor/updateTwoFactor")]
        public async Task<IHttpActionResult> UpdateTwoFactor([FromBody]Models.TwoFactor.TwoFactor twoFactor)
        {

            var service = new Service.Storage.Service();

            var getResponse = await service.GetTwoFactorByType(twoFactor.type);

            if (getResponse == null)
                return BadRequest();

            if (twoFactor.status != 0)
                getResponse.Status = twoFactor.status;

            var response = await service.UpdateTwoFactor(getResponse);

            if (response.entity != null)
            {
                return Ok(new
                {
                    type = response.entity.Type,
                    status = response.entity.Status
                });
            }

            return BadRequest();
        }



        [HttpGet]
        [SwaggerOperation("GetAllActions")]
        [Route("api/twofactor/getAllActions/")]
        public async Task<IHttpActionResult> GetAllActions()
        {
            var service = new Service.Storage.Service();

            var response = await service.GetAllActions();

            if (response != null)
                return Ok(response.Select(item => new
                {
                    name = item.Name,
                    status = item.Status,
                    options = item.TwoFactorActionOptions.Select(o => new { key = o.Key })
                }));

            return BadRequest();
        }

        [HttpPut]
        [SwaggerOperation("UpdateAction")]
        [Route("api/twofactor/updateAction")]
        public async Task<IHttpActionResult> UpdateAction([FromBody]Models.TwoFactor.TwoFactor twoFactor)
        {

            var service = new Service.Storage.Service();

            var getResponse = await service.GetActionByName(twoFactor.type);

            if (getResponse == null)
                return BadRequest();

            if (twoFactor.status != 0)
                getResponse.Status = twoFactor.status;

            var response = await service.UpdateAction(getResponse);

            if (response.entity != null)
            {
                return Ok(new
                {
                    name = response.entity.Name,
                    status = response.entity.Status
                });
            }

            return BadRequest();
        }


        [HttpGet]
        [SwaggerOperation("GetUserSettings")]
        [Route("api/twofactor/getUserSettings/{email}")]
        public async Task<IHttpActionResult> GetUserSettings(string email) //todo add user twofactor options in response
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);
            if (user == null)
                return BadRequest();

            var settings = await service.GetSettingsByUser(user.Id);

            if (settings == null)
                BadRequest();

            return Ok(settings.Select(item => new
            {
                id = item.Id,
                type = item.TwoFactor.Type,
                action = item.TwoFactorAction.Name,
                state = item.State,
                secret = item.Secret,
                options = item.UserTwoFactorOptions.Select(o => new
                {
                    key = o.Key,
                    value = o.Value
                })
            }));
        }
    
        [HttpPut]
        [SwaggerOperation("UpdateSettings")]
        [Route("api/twofactor/updateSettings/id")]
        public async Task<IHttpActionResult> UpdateSettings([FromBody]UpdateTwoFactorOptions model)//todo add action options
        {

            var service = new Service.Storage.Service();

            // todo rewrite this method (user can change only options)

            var userTwoFactor = await service.GetUserTwoFactorById(model.settingId);

            if(userTwoFactor == null)
                return BadRequest("No Such TwoFactor");

            Service.TwoFactor.Service twoFactorService = new Service.TwoFactor.Service();
            var res = await twoFactorService.AcceptTwoFactorAction(userTwoFactor.Id, model.code);

            if (res.result == Service.TwoFactor.TwoFactorResult.WrongCode)
                return Ok(new { message = "Wrong code" });

            foreach (var s in model.options)
            {
                var actionOption = await service.GetActionKeyOption(userTwoFactor.TwoFactorAction.Name, s.Key);
                if (actionOption != null)
                {
                    var option = await service.GetUserTwoFactorOption(userTwoFactor.Id, s.Key);
                    if (option == null)
                    {
                        Service.Storage.Models.UserTwoFactorOption twoFactorOption = new Service.Storage.Models.UserTwoFactorOption()
                        {
                            Key = s.Key,
                            Value = s.Value,
                            UserTwoFactor = userTwoFactor
                        };
                        await service.CreateUserTwoFactorOption(twoFactorOption);
                    }
                    else
                    {
                        option.Value = s.Value;
                        await service.UpdateUserTwoFactorOption(option);
                    }
                }
            }
            
            return Ok();
        }

        [HttpDelete]
        [SwaggerOperation("RemoveSettings")]
        [Route("api/twofactor/removeSettings/{id}")]
        public async Task<IHttpActionResult> RemoveSetting(long id)
        {            
            var twoFactorService = new Service.TwoFactor.Service();
            var res = await twoFactorService.DisableTwoFactor(id);

            if (res.result == Service.TwoFactor.TwoFactorResult.SendCode)
                return Ok(res.result.ToString());

            var service = new Service.Storage.Service();

            var twoFactor = await service.GetUserTwoFactorById(id);
            var user = await service.GetUserByEmail(twoFactor.User.Email);

            UserToken userToken = new UserToken()
            {
                email = user.Email,
                firstName = user.FirstName,
                lastName = user.LastName,
                phoneCode = user.Phone_code,
                phoneNumber = user.Phone_number,
                country = user.Country,
                address = user.Address,
                city = user.City,
                avatarUrl = user.AvatarUrl,
                isVerified = user.IsVerified,
                messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
            };

            userToken.twoFactorSettings = new List<UserTwoFactor>();
            var twoFactors = await service.GetSettingsByUser(user.Id);
            if (twoFactors == null)
                return BadRequest();

            foreach (var t in twoFactors)
            {
                UserTwoFactor setting = new UserTwoFactor()
                {
                    settingId = t.Id,
                    type = t.TwoFactor.Type,
                    action = t.TwoFactorAction.Name,
                    state = t.State
                };
                userToken.twoFactorSettings.Add(setting);
            }

            var token = await CreateToken(TokenValidator.JWTExtraHeaders, userToken, TokenValidator.JwtSecretKey);

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            context.Response.Headers.Add("X-Authorization", token);
            
            if(res.result == Service.TwoFactor.TwoFactorResult.Ok)
                return Ok();

            return BadRequest();
                        
        }

        [HttpPost]
        [SwaggerOperation("AcceptAction")]
        [Route("api/twofactor/acceptAction/{id}/{code}")]
        public async Task<IHttpActionResult> AcceptAction(long id, string code)
        {
            Service.TwoFactor.Service twoFactorService = new Service.TwoFactor.Service();

            var service = new Service.Storage.Service();
            var twoFactor = await service.GetUserTwoFactorById(id);

            var user = await service.GetUserByEmail(twoFactor.User.Email);
            var res = await twoFactorService.AcceptTwoFactorAction(id, code);
            // todo: 

            if (res.result == Service.TwoFactor.TwoFactorResult.Ok)
            {

                var twoFactors = await service.GetSettingsByUser(user.Id);
                if (twoFactors == null)
                    return BadRequest();

                UserToken userToken = new UserToken()
                {
                    email = user.Email,
                    firstName = user.FirstName,
                    lastName = user.LastName,
                    phoneCode = user.Phone_code,
                    phoneNumber = user.Phone_number,
                    country = user.Country,
                    address = user.Address,
                    city = user.City,
                    avatarUrl = user.AvatarUrl,
                    isVerified = user.IsVerified,
                    messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
                };

                userToken.twoFactorSettings = new List<UserTwoFactor>();
                foreach (var t in twoFactors)
                {
                    UserTwoFactor setting = new UserTwoFactor()
                    {
                        settingId = t.Id,
                        type = t.TwoFactor.Type,
                        action = t.TwoFactorAction.Name,
                        state = t.State
                    };
                    userToken.twoFactorSettings.Add(setting);
                }

                var token = await CreateToken(TokenValidator.JWTExtraHeaders, userToken, TokenValidator.JwtSecretKey);

                System.Web.HttpContext context = System.Web.HttpContext.Current;
                context.Response.Headers.Add("X-Authorization", token);

                return Ok();
            }

            return BadRequest();
        }


        [HttpPost]
        [SwaggerOperation("EnableAction")]
        [Route("api/twofactor/enableAction/")]
        public async Task<IHttpActionResult> EnableAction([FromBody] EnableTwoFactor model)
        {

            Service.Storage.Service service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(model.email);
            if (user == null)
                return BadRequest();

            Service.TwoFactor.Service twoFactorService = new Service.TwoFactor.Service();
            var res = await twoFactorService.EnableTwoFactor(model.email, model.action, model.type, model.options);
            // todo: 

            var twoFactors = await service.GetSettingsByUser(user.Id);
            if (twoFactors == null)
                return BadRequest();

            if (res.result == Service.TwoFactor.TwoFactorResult.Ok)
            {
                UserToken userToken = new UserToken()
                {
                    email = user.Email,
                    firstName = user.FirstName,
                    lastName = user.LastName,
                    phoneCode = user.Phone_code,
                    phoneNumber = user.Phone_number,
                    country = user.Country,
                    address = user.Address,
                    city = user.City,
                    avatarUrl = user.AvatarUrl,
                    isVerified = user.IsVerified,
                    messangers = user.ImContacts.ToDictionary(m => m.messanger, c => c.contact)
                };

                userToken.twoFactorSettings = new List<UserTwoFactor>();
                foreach (var t in twoFactors)
                {
                    UserTwoFactor setting = new UserTwoFactor()
                    {
                        settingId = t.Id,
                        type = t.TwoFactor.Type,
                        action = t.TwoFactorAction.Name,
                        state = t.State
                    };
                    userToken.twoFactorSettings.Add(setting);
                }

                var token = await CreateToken(TokenValidator.JWTExtraHeaders, userToken, TokenValidator.JwtSecretKey);

                System.Web.HttpContext context = System.Web.HttpContext.Current;
                context.Response.Headers.Add("X-Authorization", token);

                return Ok(new
                {
                    id = res.entity.Id,
                    type = res.entity.TwoFactor.Type,
                    action = res.entity.TwoFactorAction.Name,
                    secret = res.entity.Secret,
                    url = res.url,
                    options = (res.entity.UserTwoFactorOptions?.Select(s => new
                    {
                        key = s.Key,
                        value = s.Value
                    }))
                });
            }

            return BadRequest();
        }

        private async Task<string> CreateToken(string headers, object payload, string key)
        {
            return await Task.Run(() =>
            {
                return TokenValidator.Encode<object>(headers, payload, Encoding.UTF8.GetBytes(key));
            });
        }
    }
}

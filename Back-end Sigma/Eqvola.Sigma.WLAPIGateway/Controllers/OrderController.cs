﻿using Eqvola.Sigma.Com.OrderStorage;
using Eqvola.Sigma.Com.OrderService;
using Eqvola.Sigma.WLAPIGateway.Models;
using Eqvola.Sigma.WLAPIGateway.Providers;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Eqvola.Sigma.Com.TradeService;
using Eqvola.Sigma.Com.OrderStorage.Transaction;
using Eqvola.Sigma.Com.Market;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class OrderController : ApiController
    {
        /// <summary>
        /// Get list of orders
        /// </summary>
        /// <returns> List of orders </returns>
        [HttpGet]
        [Route("api/order/get")]
        [SwaggerOperation("GetAll")]
        public async Task<IHttpActionResult> Get()
        {
            var request = new GetOrderRequest();
            var response = await request.GetResponse<OrdersResponse>();
            

            if (response.result == Com.OrderStorage.Result.Ok)
                return Ok(response.orders.Select(item => new
                {
                    orderId = item.Id,
                    currency = item.Currency,
                    targetCurrency = item.TargetCurrency,
                    expiresTime = item.ExpiresTime,
                    creationTime = item.CreationTime,
                    price = item.Price,
                    amount = item.Amount,
                    takeProfit = item.TakeProfit,
                    stopLoss = item.StopLoss,
                    stop = item.Stop,
                    limit = item.Limit,
                    orderType = item.OrderType,
                    comment = item.Comment
                }));
                
            return BadRequest();
        }

        [HttpPost]
        [Route("api/order/getOrderByUser")]
        [SwaggerOperation("GetOrderByUser")]
        public async Task<IHttpActionResult> GetOrderByUser([FromBody]GetUserByOrders model)
        {
            if (Request == null || Request.Headers == null || Request.Headers.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();

            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;
            UserToken tokenUser = null;

            try
            {
                tokenUser = TokenValidator.Decode<UserToken>(token);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(tokenUser.email);
            
            var request = new GetUserOrdersRequest()
            {
                ordersCount = model.count,
                margin = model.margin,
                status = model.status,
                userId = user.Id,
                market = model.market
            };
            var response = await request.GetResponse<UserOrdersResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
                return Ok(new
                {
                    totalCount = response.totalCount,
                    orders = response.orders.Select(item => new Com.Market.Models.Order(item)).ToList()
                });

            return BadRequest();
        }


        /// <summary>
        /// Get order details
        /// </summary>
        /// <param name="id"> Order id</param>
        /// <returns> Data about order </returns>
        [HttpGet]
        [Route("api/order/getOrder/{id}")]
        [SwaggerOperation("GetOrder")]
        public async Task<IHttpActionResult> Get(long id)
        {
            var request = new GetOrderRequest()
            {
                id = id
            };

            var response = await request.GetResponse<OrderResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)            
                return Ok(new
                {
                    orderId = response.entity.Id,
                    currency = response.entity.Currency,
                    targetCurrency = response.entity.TargetCurrency,
                    expiresTime = response.entity.ExpiresTime,
                    creationTime = response.entity.CreationTime,
                    price = response.entity.Price,
                    amount = response.entity.Amount,
                    takeProfit = response.entity.TakeProfit,
                    stopLoss = response.entity.StopLoss,
                    stop = response.entity.Stop,
                    limit = response.entity.Limit,
                    orderType = response.entity.OrderType,
                    comment = response.entity.Comment
                });            
            return BadRequest();
        }

        /// <summary>
        /// Get details about several orders
        /// </summary>
        /// <param name="ids"> Array of order id ( [1, 2, 3] )</param>
        /// <returns> List of data about orders </returns>
        [HttpPost]
        [Route("api/order/getByIds")]
        [SwaggerOperation("GetByIds")]
        public async Task<IHttpActionResult> Post([FromBody]IEnumerable<long> ids)
        {
            var request = new GetOrderRequest()
            {
                ids = ids
            };

            var response = await request.GetResponse<OrdersResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
            {
                return Ok(response.orders.Select(item => new
                {
                    orderId = item.Id,
                    currency = item.Currency,
                    targetCurrency = item.TargetCurrency,
                    expiresTime = item.ExpiresTime,
                    creationTime = item.CreationTime,
                    price = item.Price,
                    amount = item.Amount,
                    takeProfit = item.TakeProfit,
                    stopLoss = item.StopLoss,
                    stop = item.Stop,
                    limit = item.Limit,
                    orderType = item.OrderType,
                    comment = item.Comment
                }));
            }

            return BadRequest();
        }

        /// <summary>
        /// Get pair order
        /// </summary>
        /// <param name="id"> Id of order which needs a pair order </param>
        /// <returns> List of suitable orders id </returns>
        [HttpGet]
        [Route("api/order/getSuitOrder/{id}")]
        [SwaggerOperation("GetSuitOrder")]
        public async Task<IHttpActionResult> GetSuit(long id)
        {
            var request = new GetSuitOrderRequest()
            {
                orderId = id
            };

            var response = await request.GetResponse<SuitOrdersResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
            {
                return Ok(response.ordersId);
            }

            return BadRequest();
        }

        /// <summary>
        /// Get orders of certain WL by ApiKey
        /// </summary>
        /// <param name="wlApiKey"> ApiKey of Wl </param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/order/getByWl/{wlApiKey}")]
        [SwaggerOperation("GetByWl")]
        public async Task<IHttpActionResult> GetByWl(string wlApiKey)
        {
            //some changes
            /*
            var request = new GetOrderRequest()
            {
                wlId = wlId
            };

            var response = await request.GetResponse<OrdersResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
            {
                return Ok(response);
            }*/

            return BadRequest();
        }

        /// <summary>
        /// Create New Order
        /// </summary>
        /// <param name="orderType"> 1-Buy Order; 2-Sell Order </param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/order/{orderType}")]
        [SwaggerOperation("Create")]
        public async Task<IHttpActionResult> Post(int orderType, [FromBody] Order model)
        {
            if (Request == null || Request.Headers == null || Request.Headers.Authorization == null)
            {
                return BadRequest("Wrong authorization");
            }

            string token = Request.Headers.Authorization.ToString();

            token = token.StartsWith("Bearer ") ? token.Substring(7) : token;
            UserToken tokenUser = null;

            try
            {
                tokenUser = TokenValidator.Decode<UserToken>(token);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            double amountInBtc = 0;
            bool isNeed2FA = false;
            //TODO
            if (model.currency == "BTC")
                amountInBtc = model.amount;
            else
            {

                var quotationsRequest = new GetQuotationsRequest();
                var quotationsResponse = await quotationsRequest.GetResponse<GetQuotationsResponse>();
                foreach (var item in quotationsResponse.PairPrices)
                {
                    //todo
                    var pairs = item.Key.Split(new char[] { '_' });
                    if (item.Value == 0 || pairs == null || pairs.Count() < 2) continue;
                    if (pairs[0] == model.currency && pairs[1] == model.targetCurrency)
                    {
                        amountInBtc = model.amount / item.Value;
                    }
                }
                if (amountInBtc == 0) isNeed2FA = true;
            }

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(tokenUser.email);

            var twoFactor = await service.GetSettingsByUserAction(user.Id, 4);

            if (twoFactor != null)
            {
                if (twoFactor.UserTwoFactorOptions != null)
                {
                    foreach (var option in twoFactor.UserTwoFactorOptions)
                    {
                        if (option.Key.Equals("from") && option.Value <= amountInBtc || isNeed2FA)
                        {
                            Service.TwoFactor.Service twoFactorService = new Service.TwoFactor.Service();
                            var res = await twoFactorService.AcceptTwoFactorAction(twoFactor.Id, model.twoFactorCode);

                            if (res.result == Service.TwoFactor.TwoFactorResult.SendCode)
                                return Ok(res.result.ToString());

                            if (res.result == Service.TwoFactor.TwoFactorResult.WrongCode)
                                return BadRequest("Wrong Code");
                        }
                    }
                }
            }

            if(model.expiresTime.HasValue)
            {
                model.expiresTime = model.expiresTime.Value.ToUniversalTime();
            }
            
            var responseSettings = await service.GetSettings(new List<string>() { "MakerCommission", "TakerCommission" });

            var commission = responseSettings.Where(s => s.Key == "TakerCommission").FirstOrDefault();
            

            var request = new ValidateOrderRequest()
            {
                userId = user.Id,
                order = new Com.OrderStorage.Models.Order()
                {
                    Currency = model.currency,
                    TargetCurrency = model.targetCurrency,
                    ExpiresTime = model.expiresTime,
                    Price = model.price,
                    Amount = model.amount,
                    StopLoss = model.stopLoss,
                    TakeProfit = model.takeProfit,
                    Stop = model.stop,
                    Limit = model.limit,
                    OrderType = orderType,
                    Comment = model.comment,
                    Commission = commission == null? 0.1: commission.Value
                }
            };

            var response = await request.GetResponse<ValidateOrderResponse<Com.OrderStorage.Models.Order>>();

            if (response.result == Com.OrderService.Result.Ok)
            {
                await new TradeRequest() { OrderId = response.entity.Id }.Send();

                return Ok(new
                {
                    orderId = response.entity.Id,
                    currency = response.entity.Currency,
                    targetCurrency = response.entity.TargetCurrency,
                    expiresTime = response.entity.ExpiresTime,
                    creationTime = response.entity.CreationTime,
                    price = response.entity.Price,
                    amount = response.entity.Amount,
                    takeProfit = response.entity.TakeProfit,
                    stopLoss = response.entity.StopLoss,
                    stop = response.entity.Stop,
                    limit = response.entity.Limit,
                    orderType = response.entity.OrderType,
                    comment = response.entity.Comment
                });
            }
            return BadRequest(response.result.ToString());

        }

        /// <summary>
        /// Update order
        /// </summary>
        /// <param name="orderId">Id of updatable order </param>
        /// <param name="order"> New data of order</param>
        /// <returns> Data about updatable manager or reason why it's not updated </returns>
        [HttpPut]
        [Route("api/order/{orderId}")]
        [SwaggerOperation("Update")]
        public async Task<IHttpActionResult> Put(long orderId, [FromBody] OrderUpdate order)
        {
            if (order.expiresTime.HasValue)
            {
                order.expiresTime = order.expiresTime.Value.ToUniversalTime();
            }

            var request = new UpdateOrderRequest()
            {
                orderId = orderId,
              
                stopLoss = order.stopLoss,
                takeProfit = order.takeProfit,
                stop = order.stop,
                limit = order.limit,
                expiresTime = order.expiresTime,
                comment = order.comment,
                amount = order.amount,
                price = order.price
            };
            var response = await request.GetResponse<OrderResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
            {
                return Ok(new
                {
                    stopLoss = response.entity.StopLoss,
                    takeProfit = response.entity.TakeProfit,
                    stop = response.entity.Stop,
                    limit = response.entity.Limit,
                    expiresTime = response.entity.ExpiresTime,
                    creationTime = response.entity.CreationTime,
                    comment = response.entity.Comment,
                    amount = response.entity.Amount,
                    price = response.entity.Price
                });
            }

            return BadRequest(response.result.ToString());
        }        

        /// <summary>
        /// Create New Order
        /// </summary>
        /// <param name="orderType"> 1-Buy Order; 2-Sell Order </param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/order/CancelOrder/{orderId}")]
        [SwaggerOperation("Cancel")]
        public async Task<IHttpActionResult> CancelOrder(long orderId)
        {
            var request = new CancelOrderRequest()
            {
                orderId = orderId
            };

            var response = await request.GetResponse<OrderResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
            {
                return Ok();
            }            

            return BadRequest();
        }

        /// <summary>
        /// Get transactions by orderId
        /// </summary>
        /// <param name="orderId"> Id of order</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/order/getTransactionByOrder/{orderId}")]
        [SwaggerOperation("GetTransactionByOrder")]
        public async Task<IHttpActionResult> GetTransactionByOrder(long orderId)
        {
            
            var request = new GetTransactionRequest()
            {
                orderId = orderId
            };

            var response = await request.GetResponse<TransactionsResponse>();

            if (response.result == Com.OrderStorage.Result.Ok)
            {
                return Ok(new
                {
                    orders = response.transactions.Select(item => new {
                        id = item.Id,
                        dateTime = item.Date_Time,
                        amount = Math.Round(item.Ammount, 15),
                        price = Math.Round(item.Price, 15),
                        sum = Math.Round(item.Price * item.Ammount, 15),
                        commission = Math.Round((item.Price * item.Ammount / 100) * 0.15, 15),
                        clearSum = Math.Round((item.Price * item.Ammount) - ((item.Price * item.Ammount / 100)) * 0.15, 15),
                        takerId = item.TakerId
                    }).ToList()
                });
            }

            return BadRequest();
        }


        [HttpPost]
        [Route("api/order/createTransaction")]
        [SwaggerOperation("CreateTransaction")]
        public async Task<IHttpActionResult> CreateTransaction()
        {
            var request = new CreateTransactionRequest()
            {
                Amount = 1,
                BuyOrderId = 10043,
                SellOrderId = 10044,
                Taker = 1
            };
            var response = await request.GetResponse<TransactionResponse>();

            if(response.result == Com.OrderStorage.Result.Ok)
                return Ok(response.entity);

            return BadRequest();
        }


    }
}

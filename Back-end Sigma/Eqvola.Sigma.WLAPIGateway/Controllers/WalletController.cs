﻿using Eqvola.Sigma.Com;
using Eqvola.Sigma.Com.Wallet;
using Eqvola.Sigma.WLAPIGateway.Models;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class WalletController : ApiController
    {
        #region Wallet
        /// <summary>
        /// Get wallet by currency 
        /// </summary>
        /// <returns> Users wallet by currency </returns>
        [HttpGet]
        [Route("api/wallet/get/{email}/{currencyCode}")]
        [SwaggerOperation("GetWalletByCurrency")]
        public async Task<IHttpActionResult> Get(string email, string currencyCode)
        {
            var request = new GetUserWalletRequest()
            {
                email = email,
                currency = currencyCode
            };
            var response = await request.GetResponse<WalletResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.GetFullWallet());
            return BadRequest(response.result.ToString());
        }

        /// <summary>
        /// Get list of user's wallets
        /// </summary>
        /// <returns> List of wallets </returns>
        [HttpGet]
        [Route("api/wallet/get/{email}")]
        [SwaggerOperation("GetUserWallets")]
        public async Task<IHttpActionResult> GetWallets(string email)
        {
            var request = new GetUserWalletRequest()
            {
                email = email
            };
            var response = await request.GetResponse<GetWalletsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.wallets.Select(m => m.GetFullWallet()));

            return BadRequest();
        }

        /// <summary>
        /// Create wallet
        /// </summary>
        /// <returns> Created wallet </returns>
        [HttpPost]
        [Route("api/wallet/createWallet")]
        [SwaggerOperation("CreateWallet")]
        public async Task<IHttpActionResult> CreateWallet([FromBody]CreateWallet model)
        {
            var request = new CreateWalletRequest()
            {
                email = model.email,
                currencyCode = model.currencyCode
            };
            var response = await request.GetResponse<WalletResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.GetShotWallet());

            return BadRequest();
        }
        #endregion

        #region Deposit

        /*/// <summary>
        /// Get deposit by txId
        /// </summary>
        /// <returns> Deposit </returns>
        [HttpGet]
        [Route("api/wallet/getDeposit/{txId}")]
        [SwaggerOperation("GetDeposit")]
        public async Task<IHttpActionResult> GetDeposit(string txId)
        {
            var request = new GetDepositRequest()
            {
                txId = txId
            };
            var response = await request.GetResponse<DepositResponse>();

            if (response.result == Result.Ok)
                return Ok(response);

            return BadRequest();
        }*/

        /// <summary>
        /// Get user deposits
        /// </summary>
        /// <returns> Deposit </returns>
        [HttpGet]
        [Route("api/wallet/getUserDeposits/{email}")]
        [SwaggerOperation("GetUserDeposits")]
        public async Task<IHttpActionResult> GetUserDeposits(string email)
        {
            var request = new GetDepositRequest()
            {
                email = email
            };
            var response = await request.GetResponse<DepositsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.deposits.Select(x => x.GetDeposit()));
               
            return BadRequest();
        }

        /// <summary>
        /// Get user withdrawal
        /// </summary>
        /// <returns> Withdrawal </returns>
        [HttpGet]
        [Route("api/wallet/getDepositsByCurrency/{email}/{currencyCode}")]
        [SwaggerOperation("GetDepositsByCurrency")]
        public async Task<IHttpActionResult> GetDepositsByCurrency(string email, string currencyCode)
        {
            var request = new GetDepositRequest()
            {
                email = email,
                currencyCode = currencyCode
            };
            var response = await request.GetResponse<DepositsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.deposits.Select(x=>x.GetDeposit()));

            return BadRequest();
        }

        //// <summary>
        // Create deposit
        // </summary>
        // <returns> Created deposit</returns>
        [HttpPost]
        [Route("api/wallet/createDeposit")]
        [SwaggerOperation("CreateDeposit")]
        public async Task<IHttpActionResult> CreateDeposit([FromBody]CreateDeposit model)
        {
            var request = new CreateDepositRequest()
            {                
                walletId = model.walletId,                           
                amount = model.amount,
                from = model.from,
                txId = model.txId,  
            };
            var response = await request.GetResponse<DepositResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.GetDeposit());

            return BadRequest(response.result.ToString());
        }
        #endregion

        #region Withdrawal
        /// <summary>
        /// Get withdrawal by txId
        /// </summary>
        /// <returns> Withdrawal </returns>
        [HttpGet]
        [Route("api/wallet/getWithdrawal/{txId}")]
        [SwaggerOperation("GetWithdrawal")]
        public async Task<IHttpActionResult> GetWithdrawal(string txId)
        {
            var request = new GetWithdrawalRequest()
            {
                txId = txId
            };
            var response = await request.GetResponse<WithdrawalResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.GetWithdrawal());
                
            return BadRequest();
        }

        /// <summary>
        /// Get user withdrawal
        /// </summary>
        /// <returns> Withdrawal </returns>
        [HttpGet]
        [Route("api/wallet/getUserWithdrawals/{email}")]
        [SwaggerOperation("GetUserWithdrawals")]
        public async Task<IHttpActionResult> GetUserWithdrawals(string email)
        {
            var request = new GetWithdrawalRequest()
            {
                email = email
            };
            var response = await request.GetResponse<WithdrawalsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.withdrawals.Select(m => m.GetWithdrawal()));

            return BadRequest();
        }

        /// <summary>
        /// Get user withdrawal
        /// </summary>
        /// <returns> Withdrawal </returns>
        [HttpGet]
        [Route("api/wallet/getWithdrawalsByCurrency/{email}/{currencyCode}")]
        [SwaggerOperation("GetWithdrawalsByCurrency")]
        public async Task<IHttpActionResult> GetWithdrawalsByCurrency(string email, string currencyCode)
        {
            var request = new GetWithdrawalRequest()
            {
                email = email,
                currencyCode = currencyCode
            };
            var response = await request.GetResponse<WithdrawalsResponse>();

            if (response.result == Result.Ok)
                return Ok(response.withdrawals.Select(m => m.GetWithdrawal()));

            return BadRequest();
        }

        /// <summary>
        /// Create withdrawal
        /// </summary>
        /// <returns> Created withdrawal </returns>
        [HttpPost]
        [Route("api/wallet/createWithdrawal")]
        [SwaggerOperation("CreateWithdrawal")]
        public async Task<IHttpActionResult> CreateWithdrawal([FromBody]CreateWithdrawal model)
        {

            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(model.email);
            var twoFactor = await service.GetSettingsByUserAction(user.Id, 2);

            if (twoFactor != null)
            {
                if (twoFactor.UserTwoFactorOptions != null)
                {
                    foreach (var option in twoFactor.UserTwoFactorOptions)
                    {
                        if(option.Key.Equals("from") && (Int32)option.Value <= model.amount)
                        {
                            Service.TwoFactor.Service twoFactorService = new Service.TwoFactor.Service();
                            var res = await twoFactorService.AcceptTwoFactorAction(twoFactor.Id, model.twoFactorCode);

                            if (res.result == Service.TwoFactor.TwoFactorResult.SendCode)
                                return Ok(res.result.ToString());

                            if (res.result == Service.TwoFactor.TwoFactorResult.WrongCode)
                                return BadRequest("Wrong Code");                            
                        }
                    }
                }
            }
            var request = new CreateWithdrawalRequest()
            {
                email = model.email,
                currencyCode = model.currencyCode,
                amount = model.amount,
                to = model.to,
                fee = model.fee,
                comment = model.comment
            };
            var response = await request.GetResponse<WithdrawalResponse>();

            if (response.result == Result.Ok)
                return Ok(response.entity.Wallet.GetFullWallet());

            return BadRequest(response.result.ToString());

        }

        /* /// <summary>
         /// Cancel withdrawal
         /// </summary>
         /// <returns> Cancel withdrawal </returns>
         [HttpGet]
         [Route("api/wallet/cancelWithdrawal/{txId}")]
         [SwaggerOperation("CancelWithdrawal")]
         public async Task<IHttpActionResult> CancelWithdrawal(string txId)
         {
             var request = new CancelWithdrawalRequest()
             {
                 txId = txId
             };
             var response = await request.GetResponse<WithdrawalResponse>();

             if (response.result == Result.Ok)
                 return Ok();

             return BadRequest();
         }*/
        #endregion

    }
}

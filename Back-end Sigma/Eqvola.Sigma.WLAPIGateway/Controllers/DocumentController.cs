﻿using Eqvola.Sigma.WLAPIGateway.Models.Document;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Eqvola.Sigma.WLAPIGateway.Controllers
{
    public class DocumentController : ApiController
    {
        /// <summary>
        /// Get document types 
        /// </summary>
        /// <returns> List of document types </returns>
        [HttpGet]
        [Route("api/document/get/")]
        [SwaggerOperation("GetDocTypes")]
        public async Task<IHttpActionResult> GetDocTypes()
        {
            var service = new Service.Storage.Service();

            var response = await service.GetDocumentTypes();

            if (response != null)
                return Ok(response.Select(item => new
                {
                    name = item.Name,
                    description = item.Description,
                    status = item.Status
                }).ToList());

            return BadRequest();
        }


        [HttpGet]
        [Route("api/document/get/{email}")]
        [SwaggerOperation("GetUserDocs")]
        public async Task<IHttpActionResult> GetUserDocs(string email)
        {
            var service = new Service.Storage.Service();

            var user = await service.GetUserByEmail(email);

            if(user == null)
                return BadRequest("No Such User!");

            var response = await service.GetUserDocuments(user.Id);

            if (response != null)
                return Ok(response.Select(item => new
                {
                    name = item.Name,
                    type = item.Type.Name,
                    guid = item.Guid,
                    url = item.Url,
                    status = item.Status,
                    comment = item.Comment,
                    uploadDate = item.UploadDate
                }).ToList());

            return BadRequest();
        }
                      
        [HttpPost]
        [SwaggerOperation("UploadUserDoc")]
        [Route("api/document/uploadUserDoc/")]
        public async Task<IHttpActionResult> UploadUserDoc([FromBody]UploadUserDoc model)
        {
            var service = new Service.Storage.Service();
            Service.Storage.Models.UserDocument response = null;

            var user = await service.GetUserByEmail(model.email);

            if (user == null)
            {
                return BadRequest("No Such User!");
            }

            var type = await service.GetDocumentType(model.type);
            if (type == null)
            {
                return BadRequest("No Such Type!");
            }
            if(type.Status != (int)Service.Storage.Models.DocTypeStatus.Active)
            {
                return BadRequest("Document type is already disabled");
            }
            

            var blob = new Service.Blob.Service();
            var mimeType = MimeMapping.GetMimeMapping(model.name);
            
            var userDoc = await service.GetUserDocumentByType(user.Id, type.Id);

            if (userDoc != null && userDoc.Status == (int)Service.Storage.Models.DocumentStatus.Accepted)
            {
                return BadRequest("Document is already accepted");
            }
            else if (userDoc != null)
            {
                await blob.UploadFile(userDoc.Guid, mimeType, model.data, "documents");

                userDoc.UploadDate = DateTime.UtcNow;
                userDoc.Name = model.name;
                userDoc.Status = (int)Service.Storage.Models.DocumentStatus.InProcessing;

                response = await service.UpdateUserDocument(userDoc);
            }
            else
            {
                string guid = Guid.NewGuid().ToString();
                var uploadUrl = await blob.UploadFile(guid, mimeType, model.data, "documents");

                Service.Storage.Models.UserDocument doc = new Service.Storage.Models.UserDocument()
                {
                    Name = model.name,
                    User = user,
                    Type = type,
                    Status = (int)Service.Storage.Models.DocumentStatus.InProcessing,
                    Guid = guid,
                    UploadDate = DateTime.UtcNow,
                    Url = uploadUrl
                };

                response = await service.CreateUserDocument(doc);
            }
            if (response != null)
                return Ok(new
                {
                    name = response.Name,
                    status = response.Status,
                    email = response.User.Email,
                    type = response.Type.Name,
                    guid = response.Guid,
                    url = response.Url,
                    uploadDate = response.UploadDate
                });
            return BadRequest();

        }


    }
}

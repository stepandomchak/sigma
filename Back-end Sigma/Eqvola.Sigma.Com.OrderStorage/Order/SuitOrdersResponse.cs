﻿using System.Collections.Generic;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class SuitOrdersResponse : OrderStorageResponse
    {
        public IEnumerable<long> ordersId { get; set; }
    }
}
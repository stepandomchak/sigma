﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class UpdateOrderCommissionRequest : Request
    {
        public override string QueueName => "order-storage";

        public override string RequestName => "order-updateCommission";
        
        public long orderId { get; set; }
        public double commission { get; set; }
        
    }
}

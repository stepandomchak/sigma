﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class UserOrdersResponse : OrderStorageResponse
    {
        public List<Order> orders { get; set; }
        public List<Models.Transaction> transactions { get; set; }
        public int totalCount { get; set; }

        public UserOrdersResponse()
        {
            orders = new List<Order>();
            transactions = new List<Models.Transaction>();
        }

    }
}

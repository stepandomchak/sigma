﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class GetFirstOrderRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "order-first";
        
        public string currency { get; set; }
        public string targetCurrency { get; set; }

    }
}

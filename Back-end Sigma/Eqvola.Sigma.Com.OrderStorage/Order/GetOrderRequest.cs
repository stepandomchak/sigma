﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class GetOrderRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "order-get";

        public long id { get; set; }
        public long wlId { get; set; }
        public long userId { get; set; }
        public int status { get; set; }
        public string market { get; set; }
        

        public IEnumerable<long> ids { get; set; }
        
    }
}

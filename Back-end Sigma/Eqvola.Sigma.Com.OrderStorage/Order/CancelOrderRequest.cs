﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class CancelOrderRequest : Request
    {
        public override string QueueName => "order-storage";

        public override string RequestName => "order-cancel";

        public long orderId { get; set; }

    }
}

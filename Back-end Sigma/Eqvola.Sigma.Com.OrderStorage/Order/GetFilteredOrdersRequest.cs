﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Com.OrderStorage.Models;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class GetFilteredOrdersRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "order-filtered";
        
        public int status { get; set; }
        public string currency { get; set; }
        public string targetCurrency { get; set; }
        public int ordersCount { get; set; }
        public long userId { get; set; }

        public async override Task<T> GetResponse<T>(bool send = true)
        {
            await Send();

            var ord = new List<Order>();
            OrdersResponse resp;
            do
            {
                resp = await base.GetResponse<T>(false) as OrdersResponse;
                if(resp.orders != null)
                    ord.AddRange(resp.orders);
            } while (!resp.final);

            return new OrdersResponse()
            {
                orders = ord,
                result = Result.Ok
            } as T;
        }
    }
}

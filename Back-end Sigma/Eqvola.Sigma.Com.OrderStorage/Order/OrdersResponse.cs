﻿using Eqvola.Sigma.Com.OrderStorage.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class OrdersResponse : OrderStorageResponse
    {
        private const int MAX_ORDERS_PER_RESPONSE = 100;

        public IEnumerable<Order> orders { get; set; }
        public bool final { get; set; }

        public async override Task Send(string sessionId)
        {
            if (orders != null && orders.Count() > 0)
            {
                var iter = this.orders.GetEnumerator();
                OrdersResponse resp = new OrdersResponse()
                {
                    final = false,
                    result = this.result
                };
                var orders = new List<Order>();
                resp.orders = orders;
                int left = MAX_ORDERS_PER_RESPONSE;
                while(iter.MoveNext())
                {
                    orders.Add(iter.Current);
                    left--;
                    if (left < 1)
                    {
                        await resp.SendInternal(sessionId);
                        resp = new OrdersResponse()
                        {
                            final = false,
                            result = this.result
                        };
                        orders.Clear();
                        resp.orders = orders;
                        left = MAX_ORDERS_PER_RESPONSE;
                    }
                }
                resp.final = true;
                await resp.SendInternal(sessionId);
            }
            else
            {
                final = true;
                await SendInternal(sessionId);
            }
        }

        private async Task SendInternal(string sessionId)
        {
            await base.Send(sessionId);
        }
    }
}

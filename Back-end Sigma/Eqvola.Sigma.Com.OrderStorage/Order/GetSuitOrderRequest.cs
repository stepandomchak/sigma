﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class GetSuitOrderRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "order-suit";

        public long orderId { get; set; }
    }
}

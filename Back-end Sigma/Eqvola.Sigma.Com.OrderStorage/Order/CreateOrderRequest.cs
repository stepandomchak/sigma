﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class CreateOrderRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "order-create";

        public string currency { get; set; }
        public string targetCurrency { get; set; }   
        public DateTime? expiresTime { get; set; }
        public double price { get; set; }
        public double amount { get; set; }
        public long userId { get; set; }
        public double stopLoss { get; set; }
        public double takeProfit { get; set; }
        public double stop { get; set; }
        public double limit { get; set; }
        public string wlApiKey { get; set; }
        public double commission { get; set; }

        public int orderType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public enum Result
    {
        Ok,
        NotFound,
        InvalidRequest,
        Exception,
        NonExistenOrder,
        ExpiredOrder,
        UnexpectedCurrency,
        UnexpectedAmount,
        UnexpectedPrice,
        UnexpectedRest

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class OrderStorageResponse : Response
    {
        public Result result { get; set; }
    }

    public class OrderStorageResponse<T> : OrderStorageResponse
    {
        public T entity { get; set; }
        public string message { get; set; } = "";
    }
}

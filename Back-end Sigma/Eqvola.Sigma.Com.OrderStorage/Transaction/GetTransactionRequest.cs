﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage.Transaction
{
    public class GetTransactionRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "transaction-get";

        public long id { get; set; }
        public long orderId { get; set; }

    }
}

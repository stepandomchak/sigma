﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage.Transaction
{
    public class CreateTransactionRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "transaction-create";

        public long SellOrderId { get; set; }
        public long BuyOrderId { get; set; }
        public long Taker { get; set; }
        public double Amount { get; set; }

    }
}

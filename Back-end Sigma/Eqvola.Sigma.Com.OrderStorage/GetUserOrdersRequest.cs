﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class GetUserOrdersRequest : Request
    {
        public override string QueueName => "order-storage";
        public override string RequestName => "order-user";

        public int status { get; set; }
        public int ordersCount { get; set; }
        public int margin { get; set; }
        public long userId { get; set; }
        public string market { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eqvola.Sigma.Service.OrderStorage.Models;

namespace Eqvola.Sigma.Com.OrderStorage.Models
{
    public class Transaction
    {
        public long Id { get; set; }

        public Order SellOrder { get; set; }
        public Order BuyOrder { get; set; }
        
        public DateTime Date_Time { get; set; }
        public double Price { get; set; }
        
        public double Ammount { get; set; }

        public long TakerId { get; set; }
        public int WlId { get; set; }

    }
}

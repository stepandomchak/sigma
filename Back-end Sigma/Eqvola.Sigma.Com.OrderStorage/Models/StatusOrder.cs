﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage.Models
{
    public enum StatusOrder
    {
        NewOrder = 1,
        PartiallyСlosed = 2,
        Сlosed = 4,
        Canceled = 8,
        TakeProfit = 16,
        StopLoss = 32,
        PostponedByLimit = 64
    }
}

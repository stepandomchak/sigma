﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage.Models
{
    public class Order
    {
        public long Id { get; set; }

        public string Currency { get; set; }
        public string TargetCurrency { get; set; }

        public DateTime? CreationTime { get; set; }
        public DateTime? ExpiresTime { get; set; }
        public DateTime? CloseTime { get; set; }

        public double Price { get; set; }
        public double Amount { get; set; }

        public long UserId { get; set; }
        public double Rest { get; set; }
        public double AverageCost { get; set; }       
        public int Status { get; set; }

        public double StopLoss { get; set; }
        public double TakeProfit { get; set; }
        public double Stop { get; set; }
        public double Limit { get; set; }
        public long WLId { get; set; }

        public int OrderType { get; set; }

        public string Comment { get; set; }
        public double Commission { get; set; }
    }
}

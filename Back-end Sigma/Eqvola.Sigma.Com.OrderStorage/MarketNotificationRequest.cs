﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eqvola.Sigma.Com.OrderStorage
{
    public class MarketNotificationRequest : Request
    {
        public override string QueueName => "order-validation";

        public override string RequestName => "market-notification";


        public string currency { get; set; }    
        public string targetCurrency { get; set; }
        public string orderStatus { get; set; }

    }
}
